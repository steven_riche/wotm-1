import Immutable from 'immutable';
import types from '../actionTypes/backup';

const defaultState = new Immutable.Map();

export default function stateReducer(state = defaultState, action) {
  switch (action.type) {
    case types.WRITE_TO_BACKUP:
      return Immutable.Map({ game: action.game, world: action.world, players: action.players });
    default:
      return state;
  }
}
