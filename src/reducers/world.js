import Immutable from 'immutable';
import types from '../actionTypes/world';
import backupTypes from '../actionTypes/backup';

const defaultState = new Immutable.Map();

export default function stateReducer(state = defaultState, action) {
  switch (action.type) {
    case types.INIT_WORLD:
      return action.world;
    case types.SET_LAYOUT:
      return state.set('layout', Immutable.fromJS(action.layout));
    case types.SET_ROOM: {
      const rooms = (state.has('rooms')) ? state.get('rooms') : Immutable.List();
      const newRooms = rooms.set(action.roomIndex, Immutable.fromJS(action.room));
      return state.set('rooms', newRooms);
    }
    case backupTypes.RESTORE_FROM_BACKUP:
      return action.world;
    default:
      return state;
  }
}
