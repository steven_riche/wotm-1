import { combineReducers } from 'redux';
import game from './game';
import world from './world';
import players from './players';
import backup from './backup';

export default combineReducers({
  game,
  world,
  players,
  backup
});
