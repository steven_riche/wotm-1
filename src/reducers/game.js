import Immutable from 'immutable';
import types from '../actionTypes/game';
import backupTypes from '../actionTypes/backup';

const defaultState = new Immutable.Map();

export default function stateReducer(state = defaultState, action) {
  switch (action.type) {
    case types.ADD_TRIGGERABLE_SPACES:
      return state.set('triggerableSpaces', state.get('triggerableSpaces').merge(action.triggers));
    case types.APPEND_CURRENT_ACTION:
      return state.updateIn(['currentAction', 'data', 'moves'], (list = Immutable.List()) => list.push(Immutable.fromJS(action.newMove)));
    case types.APPEND_PROMPT:
      return state.set('prompt', state.get('prompt') + action.prompt);
    case types.CLEAR_TRIGGERABLE_SPACES:
      return state.set('triggerableSpaces', Immutable.Map());
    case types.DISCARD_CARD:
      return state.set('cardDiscardDeck', state.get('cardDiscardDeck').push(action.card));
    case types.DRAW_CARD:
      return state.set('cardDeck', state.get('cardDeck').delete(action.cardChoice));
    case types.INIT_GAME:
      return Immutable.fromJS(action.game);
    case types.LOAD_CARD_DECK:
      return state.set('cardDiscardDeck', Immutable.fromJS(action.cards));
    case types.REMOVE_TRIGGERABLE_SPACE:
      return state.deleteIn(['triggerableSpaces', action.space]);
    case types.SET_ACTIVE_PLAYER:
      return state.set('playerTurn', action.playerIndex);
    case types.SET_TURN:
      return state.withMutations(map =>
        map.set('playerTurn', action.playerTurn)
          .set('action', action.action)
          .set('totalTurn', action.totalTurn)
          .set('currentActionModifiers', Immutable.Map())
      );
    case types.SHUFFLE_CARD_DECK:
      return state.withMutations(mutState =>
        mutState.set('cardDeck', state.get('cardDeck')
          .concat(state.get('cardDiscardDeck'))
          .sortBy(() => Math.random(0, 100)))
          .set('cardDiscardDeck', Immutable.List())
      );
    case types.UPDATE_ACTION_COUNT:
      return state.set('action', action.newActionCount);
    case types.UPDATE_CAN_CONFIRM:
      return state.set('canConfirm', action.canConfirm);
    case types.UPDATE_CAN_DRAW:
      return state.set('canDrawCard', action.canDraw);
    case types.UPDATE_CURRENT_ACTION:
      return state.set('currentAction', Immutable.fromJS(action.currentAction));
    case types.UPDATE_CURRENT_ACTION_MODIFIERS:
      return state.setIn(['currentActionModifiers', action.modifierName], action.modifierValue);
    case types.UPDATE_CURRENT_ACTION_TARGET:
      return state.setIn(['currentAction', 'target'], action.target);
    case types.UPDATE_HIGHLIGHTED:
      return state.set('highlighted', Immutable.fromJS(action.highlighted));
    case types.UPDATE_PLAYER_COUNT:
      return state.set('numPlayers', action.playerCount);
    case types.UPDATE_PROMPT:
      return state.set('prompt', action.prompt);
    case types.UPDATE_SPECIAL_ACTION_NEEDED:
      return state.set('specialActionNeeded', action.specialActionNeeded);
    case backupTypes.RESTORE_FROM_BACKUP:
      return action.game;
    default:
      return state;
  }
}
