import Immutable from 'immutable';
import types from '../actionTypes/players';
import backupTypes from '../actionTypes/backup';

const defaultState = new Immutable.List();

export default function stateReducer(state = defaultState, action) {
  switch (action.type) {
    case types.ADD_PLAYER: {
      const newPlayer = Immutable.fromJS({
        role: action.playerRole,
        playerName: action.playerName,
        player: action.playerIndex,
        score: 0,
        damageCounter: 0,
        lastCaptured: null,
        cards: [],
        modifiers: {},
        pieces: []
      });
      return state.push(newPlayer);
    }
    case types.APPEND_CARD: {
      const newCards = state.getIn([action.playerIndex, 'cards']).push(action.card);
      return state.setIn([action.playerIndex, 'cards'], newCards);
    }
    case types.GET_CAPTURED: {
      const capturedPiece = state.getIn([action.playerIndex, 'pieces']).findIndex(piece => piece.get('board') === action.board && piece.get('space') === action.space);
      const newPieceList = state.getIn([action.playerIndex, 'pieces']).delete(capturedPiece).push(Immutable.Map({ board: null, space: 'captured' }));
      return state.withMutations((mutState) => {
        mutState.setIn([action.playerIndex, 'damageCounter'], action.damageCounter).setIn([action.playerIndex, 'pieces'], newPieceList);
      });
    }
    case types.MODIFY_CARD: {
      const cardIndex = state.getIn([action.playerIndex, 'cards']).findIndex(card => card.get('key') === action.cardKey);
      return state.setIn([action.playerIndex, 'cards', cardIndex, 'modifications', action.modName], action.modValue);
    }
    case types.SCORE: {
      const previouslyCaptured = state.getIn([action.playerIndex, 'lastCaptured']);
      const newScore = parseInt(state.getIn([action.playerIndex, 'score']), 10) + 1;
      return state.withMutations((mutState) => {
        if (previouslyCaptured !== null) {
          const previouslyCapturedPiece = state.getIn([previouslyCaptured, 'pieces']).findIndex(piece => piece.get('space') === 'captured');
          const updatedPieces = (previouslyCapturedPiece > -1) ? state.getIn([previouslyCaptured, 'pieces']).delete(previouslyCapturedPiece) : state.getIn([previouslyCaptured, 'pieces']);
          mutState.setIn([previouslyCaptured, 'pieces'], updatedPieces);
        }
        mutState.setIn([action.playerIndex, 'score'], newScore).setIn([action.playerIndex, 'lastCaptured'], action.lastCaptured);
      });
    }
    case types.SET_ALL_PLAYERS:
      return action.players;
    case types.SET_PLAYER:
      return state.set(action.playerIndex, action.player);
    case types.UPDATE_CARDS:
      return state.setIn([action.playerIndex, 'cards'], action.cards);
    case types.UPDATE_CARD_RECORDS: {
      const cardIndex2 = state.getIn([action.playerIndex, 'cards']).findIndex(card => card.get('key') === action.cardKey);
      return state.setIn([action.playerIndex, 'cards', cardIndex2, 'records', action.recordType], action.turnIndex);
    }
    case types.UPDATE_DAMAGE:
      return state.setIn([action.playerIndex, 'damageCounter'], action.damage);
    case types.UPDATE_MODIFIER:
      return state.setIn([action.playerIndex, 'modifiers', action.modifierName], action.modifierValue);
    case types.UPDATE_PIECES:
      return state.setIn([action.playerIndex, 'pieces'], action.pieces);
    case types.UPDATE_PIECES_GROUPS:
      return state.withMutations((mutState) => {
        action.groups.map((pieces, playerIndex) => mutState.setIn([playerIndex, 'pieces'], pieces));
      });
    case backupTypes.RESTORE_FROM_BACKUP:
      return action.players;
    default:
      return state;
  }
}
