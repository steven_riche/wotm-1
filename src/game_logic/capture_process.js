import Immutable from 'immutable';
import store from '../utils/store';
import { clearHighlightAndTriggers, whichPlayerPiece } from './base_util';
import { addTriggers, appendCurrentAction, updateCanConfirm, updateCurrentAction, updateCurrentActionTarget, updateHighlighted, updatePrompt } from '../actions/game';
import { getCaptured, updatePieces, updateScore } from '../actions/players';
import { updateBackup } from '../actions/backup';

const initCapture = () => {
  const { game, world, players } = store.getState();
  store.dispatch(updateBackup(game, world, players));
  clearHighlightAndTriggers();
  store.dispatch(updateCurrentAction({ type: 'CAPTURE', data: {}, target: null }));
  const activePlayer = players.get(game.get('playerTurn'));

  const highlightedSpaces = activePlayer.getIn(['role', 'capture', 'init'])(game, world, players);

  store.dispatch(updateHighlighted(highlightedSpaces.map(space => `${space.get('board')}_${space.get('space')}_primary`)));

  let triggers = Immutable.Map();
  highlightedSpaces.forEach((space) => {
    triggers = triggers.set(`${space.get('board')}_${space.get('space')}`, () => captureOrigin(`${space.get('board')}_${space.get('space')}`));
  });
  store.dispatch(addTriggers(triggers));
  store.dispatch(updatePrompt('Please choose an enemy piece to capture'));
};

const captureOrigin = (space) => {
  const { game, world, players } = store.getState();
  const spaceArray = space.split('_');
  const activePlayer = players.get(game.get('playerTurn'));

  const targetPlayer = whichPlayerPiece(players, spaceArray[0], spaceArray[1]);

  store.dispatch(updateCanConfirm(activePlayer.getIn(['role', 'capture', 'canCapture'])(game, players, game.get('playerTurn'), targetPlayer)));

  const highlightedSpaces = activePlayer.getIn(['role', 'capture', 'choose'])(game, world, players, spaceArray[0], spaceArray[1]);

  const newTarget = Immutable.Map({
    board: spaceArray[0],
    space: spaceArray[1],
    player: targetPlayer
  });

  Promise.resolve(store.dispatch(updateCurrentActionTarget(newTarget)))
    .then(() => {
      const allHighlightedSpaces = highlightedSpaces.map(hSpace => `${hSpace.get('board')}_${hSpace.get('space')}_primary`).push(`${space}_secondary`);
      store.dispatch(updateHighlighted(allHighlightedSpaces));

      let triggers = Immutable.Map();
      highlightedSpaces.forEach((hSpace) => {
        triggers = triggers.set(`${hSpace.get('board')}_${hSpace.get('space')}`, () => captureSelect(`${hSpace.get('board')}_${hSpace.get('space')}_${hSpace.get('player')}`, space));
      });
      store.dispatch(addTriggers(triggers));
      store.dispatch(updatePrompt('Please select all pieces needed to capture this piece, and click confirm'));
    });
};

const captureSelect = (space, origSpace) => {
  const spaceArray = space.split('_');
  const tempModifiers = moveCapturePieceToTemporary(spaceArray);

  Promise.resolve(store.dispatch(appendCurrentAction({
    to: {
      board: spaceArray[0],
      space: spaceArray[1]
    },
    player: spaceArray[2],
    modifiers: tempModifiers
  }))).then(clearHighlightAndTriggers)
    .then(() => captureOrigin(origSpace));
};

const confirmCapture = () => {
  const { game, players } = store.getState();

  const target = game.getIn(['currentAction', 'target']);
  const newDamage = players.getIn([target.get('player'), 'role', 'capture', 'newDamage'])(game, players);

  Promise.resolve(store.dispatch(updateScore(game.get('playerTurn'), target.get('player'))))
    .then(store.dispatch(getCaptured(target.get('player'), newDamage, target.get('board'), target.get('space'))));
};

const moveCapturePieceToTemporary = (spaceArray) => {
  const { players } = store.getState();
  const tempPieceIndex = players.getIn([spaceArray[2], 'pieces'])
    .findIndex(piece => piece.get('board') === spaceArray[0] && piece.get('space') === spaceArray[1]);

  const tempModifiers = players.getIn([spaceArray[2], 'pieces', tempPieceIndex, 'modifiers']);
  store.dispatch(updatePieces(spaceArray[2], players.getIn([spaceArray[2], 'pieces']).delete(tempPieceIndex)));
  return tempModifiers;
};

export { initCapture, captureOrigin, captureSelect, confirmCapture };

const captureProcess = { initCapture, confirmCapture };

export default captureProcess;
