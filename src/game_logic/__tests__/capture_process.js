import Immutable from 'immutable';
import { initCapture, captureOrigin, captureSelect, confirmCapture } from '../capture_process';
import { gameCaptureAction, sampleWorld } from './sample_inputs';

jest.unmock('../capture_process');
jest.unmock('../base_util');
const baseUtil = require('../base_util');

jest.unmock('../../actions/game');
const gameActions = require('../../actions/game');

jest.unmock('../../actions/players');
const playerActions = require('../../actions/players');

const mocks = {
  clearHighlightAndTriggers: jest.spyOn(baseUtil, 'clearHighlightAndTriggers'),
  whichPlayerPiece: jest.spyOn(baseUtil, 'whichPlayerPiece').mockImplementation(() => 1),
  updateCurrentAction: jest.spyOn(gameActions, 'updateCurrentAction'),
  updateCurrentActionTarget: jest.spyOn(gameActions, 'updateCurrentActionTarget'),
  appendCurrentAction: jest.spyOn(gameActions, 'appendCurrentAction'),
  updateHighlighted: jest.spyOn(gameActions, 'updateHighlighted'),
  updateCanConfirm: jest.spyOn(gameActions, 'updateCanConfirm'),
  updatePrompt: jest.spyOn(gameActions, 'updatePrompt'),
  addTriggers: jest.spyOn(gameActions, 'addTriggers'),
  updatePieces: jest.spyOn(playerActions, 'updatePieces'),
  updateScore: jest.spyOn(playerActions, 'updateScore'),
  getCaptured: jest.spyOn(playerActions, 'getCaptured'),
  capture_init: jest.fn().mockImplementation(() => Immutable.fromJS([{ board: 'foo', space: 'bar' }])),
  capture_canCapture: jest.fn().mockImplementation(() => true),
  capture_choose: jest.fn().mockImplementation(() => Immutable.fromJS([{ board: 'foo', space: 'a1', player: 1 }])),
  capture_newDamage: jest.fn().mockImplementation(() => 3)
};
const mockGame = Immutable.fromJS(gameCaptureAction);
const mockWorld = Immutable.fromJS(sampleWorld);
const mockPlayers = Immutable.fromJS([
  {
    role: {
      capture: {
        init: mocks.capture_init,
        canCapture: mocks.capture_canCapture,
        choose: mocks.capture_choose,
        newDamage: mocks.capture_newDamage
      }
    },
    pieces: []
  },
  {
    role: {
      capture: {
        init: mocks.capture_init,
        canCapture: mocks.capture_canCapture,
        choose: mocks.capture_choose,
        newDamage: mocks.capture_newDamage
      }
    },
    pieces: [
      { board: 'foo', space: 'a5', modifiers: [] }
    ]
  }
]);
const mockState = {
  game: mockGame,
  world: mockWorld,
  players: mockPlayers
};

jest.mock('../../utils/store', () => ({
  getState: () => mockState,
  dispatch: () => {}
}));

jest.mock('../base_util', () => ({
  clearHighlightAndTriggers: () => mocks.clearHighlightAndTriggers,
  whichPlayerPiece: () => mocks.whichPlayerPiece
}));

describe('initCapture', () => {
  beforeAll(() => initCapture());

  it('should call clearHighlightAndTriggers', () =>
    expect(mocks.clearHighlightAndTriggers).toHaveBeenCalled());

  it('should update current action', () =>
    expect(mocks.updateCurrentAction).toHaveBeenCalledWith({ type: 'CAPTURE', data: {}, target: null }));

  it('should call capture init function', () =>
    expect(mocks.capture_init).toHaveBeenCalledWith(mockGame, mockWorld, mockPlayers));

  it('should update highlighted spaces', () =>
    expect(mocks.updateHighlighted).toHaveBeenCalledWith(Immutable.List(['foo_bar_primary'])));

  it('should add triggers on highlighted spaces', () =>
    expect(mocks.addTriggers.mock.calls[0][0])
      .toEqual(Immutable.Map({ foo_bar: expect.any(Function) })));

  it('should update the prompt', () =>
    expect(mocks.updatePrompt).toHaveBeenCalled());

  afterAll(() => {
    mocks.clearHighlightAndTriggers.mockReset();
    mocks.updateCurrentAction.mockReset();
    mocks.updateHighlighted.mockReset();
    mocks.addTriggers.mockReset();
    mocks.updatePrompt.mockReset();
  });
});

describe('captureOrigin', () => {
  beforeAll(() => captureOrigin('foo_bar'));

  it('should call capture canCapture function', () =>
    expect(mocks.capture_canCapture).toHaveBeenCalledWith(mockGame, mockPlayers, 0, 1));

  it('should call updateCanConfirm', () =>
    expect(mocks.updateCanConfirm).toHaveBeenCalledWith(true));

  it('should call capture choose function', () =>
    expect(mocks.capture_choose).toHaveBeenCalledWith(mockGame, mockWorld, mockPlayers, 'foo', 'bar'));

  it('should expect whichPlayerPiece to have been called', () =>
    expect(mocks.whichPlayerPiece).toHaveBeenCalledWith(mockPlayers, 'foo', 'bar'));

  it('should call updateCurrentActionTarget', () =>
    expect(mocks.updateCurrentActionTarget).toHaveBeenCalledWith(Immutable.Map({ board: 'foo', space: 'bar', player: 1 })));

  it('should call highlightedSpaces', () =>
    expect(mocks.updateHighlighted.mock.calls[0][0]).toEqual(Immutable.List(['foo_a1_primary', 'foo_bar_secondary'])));

  it('should call addTriggers', () =>
    expect(mocks.addTriggers.mock.calls[0][0])
      .toEqual(Immutable.Map({ foo_a1: expect.any(Function) })));

  it('should update the prompt', () =>
    expect(mocks.updatePrompt).toHaveBeenCalled());

  afterAll(() => {
    mocks.updateCanConfirm.mockReset();
    mocks.whichPlayerPiece.mockReset();
    mocks.updateCurrentActionTarget.mockReset();
    mocks.updateHighlighted.mockReset();
    mocks.addTriggers.mockReset();
    mocks.updatePrompt.mockReset();
  });
});

describe('captureSelect & moveCapturePieceToTemporary', () => {
  beforeAll(() => captureSelect('foo_a1_1', 'foo_bar'));

  it('should updatePieces', () =>
    expect(mocks.updatePieces.mock.calls[0][0]).toEqual('1', Immutable.List()));

  it('should call appendCurrentAction', () =>
    expect(mocks.appendCurrentAction).toHaveBeenCalledWith({ to: { board: 'foo', space: 'a1' }, player: '1', modifiers: Immutable.List() }));

  it('should clearHighlightAndTriggers', () =>
    expect(mocks.clearHighlightAndTriggers).toHaveBeenCalled());

  afterAll(() => {
    mocks.updatePieces.mockReset();
    mocks.appendCurrentAction.mockReset();
    mocks.clearHighlightAndTriggers.mockReset();
  });
});

describe('confirmCapture', () => {
  beforeAll(() => confirmCapture());

  it('should receive new damage amount', () =>
    expect(mocks.capture_newDamage).toHaveBeenCalledWith(mockGame, mockPlayers));

  it('should update score', () =>
    expect(mocks.updateScore).toHaveBeenCalledWith(0, 1));

  it('should call getCaptured', () =>
    expect(mocks.getCaptured).toHaveBeenCalledWith(1, 3, 'foo', 'bar'));

  afterAll(() => {
    mocks.updateScore.mockReset();
    mocks.getCaptured.mockReset();
  });
});
