import Immutable from 'immutable';
import { addOrigin, confirmAdd } from '../add_process';
import { gameNoCurrentAction3, sampleWorld } from './sample_inputs';

jest.unmock('../add_process');
jest.unmock('../base_util');
const baseUtil = require('../base_util');

jest.unmock('../../actions/game');
const gameActions = require('../../actions/game');

jest.unmock('../../actions/players');
const playerActions = require('../../actions/players');

const mocks = {
  addDamage: jest.spyOn(baseUtil, 'addDamage'),
  clearHighlightAndTriggers: jest.spyOn(baseUtil, 'clearHighlightAndTriggers'),
  getCurrentInProgress: jest.spyOn(baseUtil, 'getCurrentInProgress').mockImplementation(() => Immutable.fromJS([{ board: 'foo', space: 'a1', player: 1 }, { board: 'foo', space: 'a2', player: 1 }])),
  whichPlayerPiece: jest.spyOn(baseUtil, 'whichPlayerPiece').mockImplementation(() => 1),
  updateCurrentAction: jest.spyOn(gameActions, 'updateCurrentAction'),
  updateHighlighted: jest.spyOn(gameActions, 'updateHighlighted'),
  updatePrompt: jest.spyOn(gameActions, 'updatePrompt'),
  appendCurrentAction: jest.spyOn(gameActions, 'appendCurrentAction'),
  updateCanConfirm: jest.spyOn(gameActions, 'updateCanConfirm'),
  addTriggers: jest.spyOn(gameActions, 'addTriggers'),
  updatePieces: jest.spyOn(playerActions, 'updatePieces'),
  add_init: jest.fn().mockImplementation(() => Immutable.List([{ board: 'foo', space: 'bar' }])),
  add_available: jest.fn().mockImplementation(() => Immutable.fromJS([{ board: 'foo', space: 'a1' }, { board: 'foo', space: 'a2' }])),
  add_blockedAvailable: jest.fn().mockImplementation(() => Immutable.fromJS([{ board: 'foo', space: 'a1' }, { board: 'foo', space: 'a2' }])),
  add_canAddMore: jest.fn().mockImplementation(() => false),
  add_initFirstPiece: jest.fn().mockImplementation(() => Immutable.List([{ board: 'foo', space: 'bar' }])),
  add_confirmAdd: jest.fn().mockImplementation(() => Immutable.List([{ board: 'foo', space: 'bar' }]))
};

const mockGame = Immutable.fromJS(gameNoCurrentAction3).setIn(['currentActionModifiers', 'bluePressure'], true);
const mockWorld = Immutable.fromJS(sampleWorld);
const mockPlayers = Immutable.fromJS([
  {
    role: {
      add: {
        init: mocks.add_init,
        available: mocks.add_available,
        blockedAvailable: mocks.add_blockedAvailable,
        canAddMore: mocks.add_canAddMore,
        initFirstPiece: mocks.add_initFirstPiece,
        confirmAdd: mocks.add_confirmAdd
      }
    }
  },
  {
    pieces: [
      { board: 'foo', space: 'bar', modifiers: [] }
    ]
  }
]);

const mockState = {
  game: mockGame,
  world: mockWorld,
  players: mockPlayers
};

jest.mock('../../utils/store', () => ({
  getState: () => mockState,
  dispatch: () => {}
}));

jest.mock('../base_util', () => ({
  addDamage: () => mocks.addDamage,
  clearHighlightAndTriggers: () => mocks.clearHighlightAndTriggers,
  getCurrentInProgress: () => mocks.getCurrentInProgress,
  whichPlayerPiece: () => mocks.whichPlayerPiece
}));

describe('addOrigin alternate', () => {
  beforeAll(() => addOrigin('foo_bar'));

  it('should call add blocked available function', () =>
    expect(mocks.add_blockedAvailable).toHaveBeenCalledWith(mockGame, mockWorld, mockPlayers, undefined, 'bar'));

  it('should not call add available function', () =>
    expect(mocks.add_available).not.toHaveBeenCalled());

  afterAll(() => {
    mocks.clearHighlightAndTriggers.mockReset();
    mocks.updateHighlighted.mockReset();
    mocks.addTriggers.mockReset();
    mocks.updatePrompt.mockReset();
  });
});

describe('confirmAdd alternative', () => {
  beforeAll(() => confirmAdd());

  it('should call getCurrentInProgress function', () =>
    expect(mocks.getCurrentInProgress).toHaveBeenCalledWith(mockGame));

  it('should call addDamage function', () =>
    expect(mocks.addDamage).toHaveBeenCalledWith(1, 2));

  afterAll(() => {
    mocks.updatePieces.mockReset();
  });
});
