import Immutable from 'immutable';
import { cancelAction, confirmAction, forceNextTurn, useDamageFreeAction, manageCards } from '../cancel_confirm_process';

jest.unmock('../cancel_confirm_process');
jest.unmock('../base_util');
const baseUtil = require('../base_util');

jest.unmock('../../actions/game');
const gameActions = require('../../actions/game');

jest.unmock('../../actions/players');
const playerActions = require('../../actions/players');

jest.unmock('../../actions/backup');
const backupActions = require('../../actions/backup');

jest.unmock('../move_process');
const moveProcess = require('../move_process');

const mocks = {
  clearHighlightAndTriggers: jest.spyOn(baseUtil, 'clearHighlightAndTriggers'),
  canTakeAdditionalAction: jest.spyOn(baseUtil, 'canTakeAdditionalAction').mockImplementation(() => false),
  cardsShouldHave: jest.spyOn(baseUtil, 'cardsShouldHave').mockImplementation(() => 0),
  updateCurrentAction: jest.spyOn(gameActions, 'updateCurrentAction'),
  updateCurrentActionModifier: jest.spyOn(gameActions, 'updateCurrentActionModifier'),
  finishAction: jest.spyOn(gameActions, 'finishAction'),
  updatePrompt: jest.spyOn(gameActions, 'updatePrompt'),
  appendPrompt: jest.spyOn(gameActions, 'appendPrompt'),
  updateActionCount: jest.spyOn(gameActions, 'updateActionCount'),
  updateCanConfirm: jest.spyOn(gameActions, 'updateCanConfirm'),
  forceNext: jest.spyOn(gameActions, 'forceNext'),
  shuffleCardDeck: jest.spyOn(gameActions, 'shuffleCardDeck'),
  discardCard: jest.spyOn(gameActions, 'discardCard'),
  updateCanDraw: jest.spyOn(gameActions, 'updateCanDraw'),
  updateDamage: jest.spyOn(playerActions, 'updateDamage'),
  updateCards: jest.spyOn(playerActions, 'updateCards'),
  restoreFromBackup: jest.spyOn(backupActions, 'restoreFromBackup').mockImplementation(() => 'backed up'),
  updateBackup: jest.spyOn(backupActions, 'updateBackup'),
  confirmMove: jest.spyOn(moveProcess, 'confirmMove')
};

const mockState = {
  game: Immutable.fromJS({
    playerTurn: 0,
    currentAction: { type: 'MOVE' },
    action: 5,
    cardDeck: [],
    cardDiscardDeck: []
  }),
  world: 'worldFoo',
  players: Immutable.fromJS([
    {
      playerName: 'foo',
      cards: [{}],
      damageCounter: 5,
      score: 0
    }
  ]),
  backup: 'backUpFoo'
};

jest.mock('../../utils/store', () => ({
  getState: () => mockState,
  dispatch: () => {}
}));

jest.mock('../base_util', () => ({
  clearHighlightAndTriggers: () => mocks.clearHighlightAndTriggers,
  canTakeAdditionalAction: () => mocks.canTakeAdditionalAction,
  cardsShouldHave: () => mocks.cardsShouldHave
}));

jest.mock('../move_process', () => ({
  confirmMove: () => mocks.confirmMove
}));

describe('cancelAction', () => {
  beforeAll(() => cancelAction());

  it('calls restoreFromBackup', () =>
    expect(mocks.restoreFromBackup).toHaveBeenCalledWith('backUpFoo'));
});

describe('confirmAction', () => {
  beforeAll(() => confirmAction());

  it('calls confirmMove when confirming a move action', () =>
    expect(mocks.confirmMove).toHaveBeenCalled());

  it('expect updateCurrentAction to be called with empty object', () =>
    expect(mocks.updateCurrentAction).toHaveBeenCalledWith({}));

  it('expect clearHighlightAndTriggers to be called', () =>
    expect(mocks.clearHighlightAndTriggers).toHaveBeenCalled());

  it('expect can confirm to be marked false', () =>
    expect(mocks.updateCanConfirm).toHaveBeenCalledWith(false));

  it('expect finishAction to have been called', () =>
    expect(mocks.finishAction).toHaveBeenCalled());

  it('expect updatePrompt to have been called with current players name', () =>
    expect(mocks.updatePrompt).toHaveBeenCalledWith(expect.stringContaining('foo')));

  it('expect updateBackup to have been called with current state', () =>
    expect(mocks.updateBackup)
      .toHaveBeenCalledWith(mockState.game, mockState.world, mockState.players));
});

describe('forceNextTurn', () => {
  beforeAll(() => forceNextTurn());

  it('should call forceNext', () =>
    expect(mocks.forceNext).toHaveBeenCalledWith(mockState.game));
});

describe('useDamageFreeAction', () => {
  beforeAll(() => useDamageFreeAction());

  it('should call updateCurrentActionModifier', () =>
    expect(mocks.updateCurrentActionModifier).toHaveBeenCalledWith('freeAction', true));

  it('should reset damage counter', () =>
    expect(mocks.updateDamage).toHaveBeenCalledWith(0, 0));

  it('should call appendPrompt', () =>
    expect(mocks.appendPrompt).toHaveBeenCalled());
});

describe('manageCards', () => {
  beforeAll(() => manageCards());

  it('should call shuffleCardDeck', () =>
    expect(mocks.shuffleCardDeck).toHaveBeenCalled());

  it('should call cardsShouldHave', () =>
    expect(mocks.cardsShouldHave)
      .toHaveBeenCalledWith(mockState.world, mockState.players.get(0), 2));

  it('should call discardCard', () =>
    expect(mocks.discardCard).toHaveBeenCalledWith(Immutable.Map()));

  it('should call updateCards', () =>
    expect(mocks.updateCards).toHaveBeenCalledWith(0, Immutable.List()));

  it('should call updateCanDraw', () =>
    expect(mocks.updateCanDraw).toHaveBeenCalledWith(false));

  it('should call updatePrompt', () =>
    expect(mocks.updatePrompt).toHaveBeenCalled());
});
