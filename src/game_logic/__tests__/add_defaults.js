import Immutable from 'immutable';
import addDefaults from '../add_defaults';
import {
  playersIsOccupied,
  playersIsNotOccupied,
  playersPieceLimit,
  playersPieceLimit2,
  gameNoCurrentAction,
  gameNoCurrentAction2,
  gameAddAction,
  sampleWorld,
  sampleBoard
} from './sample_inputs';

jest.unmock('../add_defaults');

describe('canAddMore', () => {
  const availableTrue = () => true;
  const availableFalse = () => false;

  it('should return false if player already has 15 pieces in play', () =>
    expect(
      addDefaults.canAddMore(3, availableTrue, gameNoCurrentAction, sampleWorld, playersPieceLimit)
    ).toBe(false));

  it('should return false if player has already reached add limit', () =>
    expect(addDefaults.canAddMore(3, availableTrue, gameAddAction, sampleWorld, playersPieceLimit))
      .toBe(false));

  it('should return false if the in play pieces and already added pieces add up to 15', () =>
    expect(addDefaults.canAddMore(5, availableTrue, gameAddAction, sampleWorld, playersPieceLimit2))
      .toBe(false));

  it('should return true if the player does not have any non-captured pieces', () =>
    expect(
      addDefaults.canAddMore(3, availableTrue, gameNoCurrentAction2, sampleWorld, playersPieceLimit)
    ).toBe(true));

  it('should return false if available function returns false', () =>
    expect(addDefaults.canAddMore(5, availableFalse, gameAddAction, sampleWorld, playersPieceLimit))
      .toBe(false));

  it('should return true if everything passes', () =>
    expect(addDefaults.canAddMore(5, availableTrue, gameAddAction, sampleWorld, playersPieceLimit))
      .toBe(true));
});

describe('init', () => {
  const result = Immutable.fromJS([
    { board: 'RhombusD', space: 'A1' },
    { board: 'PentagonC', space: 'F3' },
    { board: 'TriangleA', space: 'C3' }
  ]);

  it('should correctly return pieces that could be added onto', () =>
    expect(addDefaults.init(gameAddAction, playersIsOccupied)).toEqual(result));

  it('should return empty array if player has no available pieces', () =>
    expect(addDefaults.init(gameNoCurrentAction2, playersPieceLimit)).toEqual(Immutable.List()));
});

describe('initFirstPiece', () => {
  const result = Immutable.fromJS([
    { board: 'foo', space: 'd2' },
    { board: 'foo', space: 'd3' },
    { board: 'foo', space: 'e2' },
    { board: 'foo', space: 'e3' },
    { board: 'foo', space: 'f2' },
    { board: 'foo', space: 'c2' }
  ]);

  it('should return correct starting spaces if none are blocked', () =>
    expect(addDefaults.initFirstPiece(gameNoCurrentAction, sampleWorld, playersIsNotOccupied))
      .toEqual(result));

  it('should filter out blocked spaces', () =>
    expect(addDefaults.initFirstPiece(gameNoCurrentAction, sampleWorld, playersIsOccupied))
      .toEqual(result.delete(0)));
});

describe('available', () => {
  const result = Immutable.fromJS([
    { board: 'foo', space: 'b1' },
    { board: 'foo', space: 'b2' },
    { board: 'foo', space: 'h2' },
    { board: 'foo', space: 'h3' }
  ]);

  const result2 = Immutable.fromJS([
    { board: 'foo', space: 'b1' }
  ]);

  it('should return bordering open spaces', () =>
    expect(addDefaults.available(gameAddAction, sampleWorld, playersIsOccupied, sampleBoard, 'a2')).toEqual(result));

  it('should return bordering spaces that are not blocked', () =>
    expect(addDefaults.available(gameAddAction, sampleWorld, playersIsOccupied, sampleBoard, 'a1')).toEqual(result2));
});

describe('blockedAvailable', () => {
  const result = Immutable.fromJS([
    { board: 'foo', space: 'b1' },
    { board: 'foo', space: 'b2' },
    { board: 'foo', space: 'h2' },
    { board: 'foo', space: 'h3' }
  ]);

  const result2 = Immutable.fromJS([
    { board: 'foo', space: 'b1' },
    { board: 'foo', space: 'd2' }
  ]);

  it('should return bordering open spaces', () =>
    expect(addDefaults.blockedAvailable(gameAddAction, sampleWorld, playersIsOccupied, sampleBoard, 'a2')).toEqual(result));

  it('should return bordering spaces that are not blocked', () =>
    expect(addDefaults.blockedAvailable(gameAddAction, sampleWorld, playersIsOccupied, sampleBoard, 'a1')).toEqual(result2));
});

describe('confirmAdd', () => {
  const results = Immutable.fromJS([
    { board: 'RhombusD', space: 'A1' },
    { board: 'PentagonC', space: 'F3' },
    { board: 'TriangleA', space: 'C3' },
    { board: 'foo', space: 'A1', player: 0, modifiers: [] },
    { board: 'foo', space: 'A2', player: 0, modifiers: [] },
    { board: 'foo', space: 'A3', player: 0, modifiers: [] }
  ]);

  it('should correctly add in progress pieces with existing pieces', () =>
    expect(addDefaults.confirmAdd(gameAddAction, playersIsOccupied)).toEqual(results));
});
