import Immutable from 'immutable';
import { initMove, choosePieceToMove, addPotentialMove, confirmMove } from '../move_process';
import { gameMoveAction2, sampleWorld } from './sample_inputs';

jest.unmock('../move_process');
jest.unmock('../base_util');
const baseUtil = require('../base_util');

jest.unmock('../../actions/game');
const gameActions = require('../../actions/game');

jest.unmock('../../actions/players');
const playerActions = require('../../actions/players');

const mocks = {
  clearHighlightAndTriggers: jest.spyOn(baseUtil, 'clearHighlightAndTriggers'),
  getCurrentInProgress: jest.spyOn(baseUtil, 'getCurrentInProgress').mockImplementation(() => Immutable.fromJS([{ board: 'foo', space: 'a2', player: 0, modifiers: [] }])),
  whichPlayerPiece: jest.spyOn(baseUtil, 'whichPlayerPiece').mockImplementation(() => 1),
  updateCurrentAction: jest.spyOn(gameActions, 'updateCurrentAction'),
  appendCurrentAction: jest.spyOn(gameActions, 'appendCurrentAction'),
  updateHighlighted: jest.spyOn(gameActions, 'updateHighlighted'),
  updateCanConfirm: jest.spyOn(gameActions, 'updateCanConfirm'),
  updatePrompt: jest.spyOn(gameActions, 'updatePrompt'),
  addTriggers: jest.spyOn(gameActions, 'addTriggers'),
  updatePieces: jest.spyOn(playerActions, 'updatePieces'),
  move_init: jest.fn().mockImplementation(() => Immutable.fromJS([{ board: 'foo', space: 'bar' }])),
  move_piecesCanStillMove: jest.fn().mockImplementation(() => Immutable.fromJS([{ board: 'foo', space: 'bar', player: 0, modifiers: [] }, { board: 'foo', space: 'a1', player: 0, modifiers: [] }])),
  move_available: jest.fn().mockImplementation(() => Immutable.fromJS([{ board: 'foo', space: 'a2', player: 0, modifiers: [] }])),
  move_confirmMove: jest.fn().mockImplementation(() => 'new pieces')
};
const mockGame = Immutable.fromJS(gameMoveAction2);
const mockWorld = Immutable.fromJS(sampleWorld);
const mockPlayers = Immutable.fromJS([
  {
    role: {
      move: {
        init: mocks.move_init,
        piecesCanStillMove: mocks.move_piecesCanStillMove,
        available: mocks.move_available,
        confirmMove: mocks.move_confirmMove
      }
    },
    pieces: [
      { board: 'foo', space: 'captured', modifiers: [] },
      { board: 'foo', space: 'baz', modifiers: [] }
    ]
  },
  {
    pieces: [
      { board: 'foo', space: 'bar', modifiers: [] }
    ]
  }
]);
const mockState = {
  game: mockGame,
  world: mockWorld,
  players: mockPlayers
};

jest.mock('../../utils/store', () => ({
  getState: () => mockState,
  dispatch: () => {}
}));

jest.mock('../base_util', () => ({
  clearHighlightAndTriggers: () => mocks.clearHighlightAndTriggers,
  getCurrentInProgress: () => mocks.getCurrentInProgress,
  whichPlayerPiece: () => mocks.whichPlayerPiece
}));

describe('initMove & moveAllPiecesToTemporary', () => {
  beforeAll(() => initMove());

  it('should call clearHighlightAndTriggers', () =>
    expect(mocks.clearHighlightAndTriggers).toHaveBeenCalled());

  it('should call updateCurrentAction', () =>
    expect(mocks.updateCurrentAction).toHaveBeenCalledWith({ type: 'MOVE', data: {} }));

  it('should call move init', () =>
    expect(mocks.move_init).toHaveBeenCalledWith(mockGame, mockPlayers));

  it('should call updateHighlighted', () =>
    expect(mocks.updateHighlighted).toHaveBeenCalledWith(Immutable.List(['foo_bar_primary'])));

  it('should call addTriggers', () =>
    expect(mocks.addTriggers.mock.calls[0][0])
      .toEqual(Immutable.Map({ foo_bar: expect.any(Function) })));

  it('should update prompt', () =>
    expect(mocks.updatePrompt).toHaveBeenCalled());

  it('should call appendCurrentAction', () =>
    expect(mocks.appendCurrentAction).toHaveBeenCalledWith({ to: { board: 'foo', space: 'baz' }, player: 0, modifiers: Immutable.List() }));

  it('should call updatePieces with only captured pieces', () =>
    expect(mocks.updatePieces.mock.calls[0][0]).toEqual(0, Immutable.fromJS([{ board: 'foo', space: 'captured', modifiers: Immutable.List() }])));

  afterAll(() => {
    mocks.clearHighlightAndTriggers.mockReset();
    mocks.updateCurrentAction.mockReset();
    mocks.updateHighlighted.mockReset();
    mocks.addTriggers.mockReset();
    mocks.updatePrompt.mockReset();
    mocks.appendCurrentAction.mockReset();
    mocks.updatePieces.mockReset();
  });
});

describe('choosePieceToMove', () => {
  beforeAll(() => choosePieceToMove('foo_bar'));

  it('should call clearHighlightAndTriggers', () =>
    expect(mocks.clearHighlightAndTriggers).toHaveBeenCalled());

  it('should call init piecesCanStillMove function', () =>
    expect(mocks.move_piecesCanStillMove).toHaveBeenCalledWith(mockGame, mockPlayers));

  it('should call init available function', () =>
    expect(mocks.move_available).toHaveBeenCalledWith(mockGame, mockWorld, mockPlayers, undefined, 'bar'));

  it('should call updateHighlighted', () =>
    expect(mocks.updateHighlighted).toHaveBeenCalledWith(Immutable.List(['foo_a2_primary', 'foo_a1_secondary'])));

  it('should call addTriggers', () =>
    expect(mocks.addTriggers.mock.calls[0][0])
      .toEqual(Immutable.Map({ foo_a2: expect.any(Function) })));

  it('should updatePrompt', () =>
    expect(mocks.updatePrompt).toHaveBeenCalled());

  afterAll(() => {
    mocks.clearHighlightAndTriggers.mockReset();
    mocks.updateHighlighted.mockReset();
    mocks.addTriggers.mockReset();
    mocks.updatePrompt.mockReset();
  });
});

describe('addPotentialMove', () => {
  beforeAll(() => addPotentialMove('foo_a1', 'foo_bar'));

  it('should append action with temp enemy piece', () =>
    expect(mocks.appendCurrentAction).toHaveBeenCalledWith({ to: { board: 'foo', space: 'bar' }, player: 1, modifiers: Immutable.List() }));

  it('should call updatePieces and give an empty list for enemy', () =>
    expect(mocks.updatePieces).toHaveBeenCalledWith(1, Immutable.List()));

  it('should call appendCurrentAction', () =>
    expect(mocks.appendCurrentAction).toHaveBeenCalledWith({ from: { board: 'foo', space: 'a1' }, to: { board: 'foo', space: 'bar' }, player: 0 }));

  it('should clearHighlightAndTriggers', () =>
    expect(mocks.clearHighlightAndTriggers).toHaveBeenCalled());

  it('should call updateCanConfirm', () =>
    expect(mocks.updateCanConfirm).toHaveBeenCalledWith(true));

  afterAll(() => {
    mocks.appendCurrentAction.mockReset();
    mocks.clearHighlightAndTriggers.mockReset();
    mocks.updateCanConfirm.mockReset();
  });
});

describe('confirmMove', () => {
  beforeAll(() => confirmMove());

  it('should call confirmMove', () =>
    expect(mocks.move_confirmMove).toHaveBeenCalledWith(mockGame));

  it('should call updatePieces', () =>
    expect(mocks.updatePieces).toHaveBeenCalledWith(0, 'new pieces'));

  afterAll(() => mocks.updatePieces.mockReset());
});
