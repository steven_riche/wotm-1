import Immutable from 'immutable';

export const playersIsOccupied = Immutable.fromJS([
  {
    player: 'dfccccc4-fc47-4584-bab2-ca6217a40539',
    pieces: [
      { board: 'RhombusD', space: 'A1' },
      { board: 'PentagonC', space: 'F3' },
      { board: 'TriangleA', space: 'C3' }
    ],
    cards: []
  },
  {
    player: 'c07eb4dd-30c8-47c4-a79b-dc20f7e2c999',
    pieces: [
      { board: 'HexA', space: 'B1' },
      { board: 'foo', space: 'bar' },
      { board: 'TriangleA', space: 'D3' }
    ],
    cards: []
  },
  {
    player: '57537ae4-639f-4baa-a785-d667be32a9de',
    pieces: [
      { board: 'CobblestoneA', space: 'B2' },
      { board: 'KiteB', space: 'C4' },
      { board: 'SnowflakeB', space: 'E3' },
      { board: 'foo', space: 'd2' }
    ],
    cards: []
  }
]);

export const playersIsNotOccupied = Immutable.fromJS([
  {
    player: 'dfccccc4-fc47-4584-bab2-ca6217a40539',
    pieces: [
      { board: 'RhombusD', space: 'A1' },
      { board: 'PentagonC', space: 'bar' },
      { board: 'TriangleA', space: 'C3' }
    ],
    cards: []
  },
  {
    player: 'c07eb4dd-30c8-47c4-a79b-dc20f7e2c999',
    pieces: [
      { board: 'HexA', space: 'B1' },
      { board: 'foo', space: 'A1' },
      { board: 'TriangleA', space: 'D3' }
    ],
    cards: []
  },
  {
    player: '57537ae4-639f-4baa-a785-d667be32a9de',
    pieces: [
      { board: 'CobblestoneA', space: 'B2' },
      { board: 'KiteB', space: 'C4' },
      { board: 'SnowflakeB', space: 'E3' }
    ],
    cards: []
  }
]);

export const playersTouching = Immutable.fromJS([
  {
    player: 'dfccccc4-fc47-4584-bab2-ca6217a40539',
    pieces: [
      { board: 'foo', space: 'a1' },
      { board: 'foo', space: 'a2' }
    ],
    cards: []
  },
  {
    player: 'c07eb4dd-30c8-47c4-a79b-dc20f7e2c999',
    pieces: [
      { board: 'foo', space: 'b1' },
      { board: 'foo2', space: 'd1' },
      { board: 'foo', space: 'e3' }
    ],
    cards: []
  },
  {
    player: '57537ae4-639f-4baa-a785-d667be32a9de',
    pieces: [
      { board: 'foo', space: 'd2' },
      { board: 'foo', space: 'a3' }
    ],
    cards: []
  }
]);

export const playersPieceLimit = Immutable.fromJS([
  {
    pieces: [
      { board: 'RhombusD', space: 'A1' },
      { board: 'PentagonC', space: 'bar' },
      { board: 'TriangleA', space: 'C3' }
    ],
    cards: []
  },
  {
    pieces: [
      { board: 'HexA', space: 'B1' },
      { board: 'foo', space: 'A1' },
      { board: 'TriangleA', space: 'D3' },
      { board: 'HexA', space: 'B1' },
      { board: 'foo', space: 'A1' },
      { board: 'TriangleA', space: 'D3' },
      { board: 'HexA', space: 'B1' },
      { board: 'foo', space: 'A1' },
      { board: 'TriangleA', space: 'D3' },
      { board: 'HexA', space: 'B1' },
      { board: 'foo', space: 'A1' },
      { board: 'TriangleA', space: 'D3' },
      { board: 'HexA', space: 'B1' },
      { board: 'foo', space: 'A1' },
      { board: 'TriangleA', space: 'D3' }
    ],
    cards: []
  },
  {
    pieces: [
      { board: null, space: 'captured' },
      { board: null, space: 'captured' },
      { board: null, space: 'captured' }
    ],
    cards: []
  }
]);

export const playersPieceLimit2 = Immutable.fromJS([
  {
    pieces: [
      { board: 'RhombusD', space: 'A1' },
      { board: 'PentagonC', space: 'bar' },
      { board: 'TriangleA', space: 'C3' },
      { board: 'RhombusD', space: 'A1' },
      { board: 'PentagonC', space: 'bar' },
      { board: 'TriangleA', space: 'C3' },
      { board: 'RhombusD', space: 'A1' },
      { board: 'PentagonC', space: 'bar' },
      { board: 'TriangleA', space: 'C3' },
      { board: 'RhombusD', space: 'A1' },
      { board: 'PentagonC', space: 'bar' },
      { board: 'TriangleA', space: 'C3' }
    ],
    cards: []
  },
  {
    pieces: [
      { board: 'HexA', space: 'B1' },
      { board: 'foo', space: 'A1' },
      { board: 'TriangleA', space: 'D3' },
      { board: 'HexA', space: 'B1' },
      { board: 'foo', space: 'A1' },
      { board: 'TriangleA', space: 'D3' },
      { board: 'HexA', space: 'B1' },
      { board: 'foo', space: 'A1' },
      { board: 'TriangleA', space: 'D3' },
      { board: 'HexA', space: 'B1' },
      { board: 'foo', space: 'A1' },
      { board: 'TriangleA', space: 'D3' },
      { board: 'HexA', space: 'B1' },
      { board: 'foo', space: 'A1' },
      { board: 'TriangleA', space: 'D3' }
    ],
    cards: []
  },
  {
    pieces: [
      { board: null, space: 'captured' },
      { board: null, space: 'captured' },
      { board: null, space: 'captured' }
    ],
    cards: []
  }
]);

export const gameNoCurrentAction = Immutable.fromJS({
  currentAction: {},
  playerTurn: 1
});

export const gameNoCurrentAction2 = Immutable.fromJS({
  currentAction: {},
  playerTurn: 2
});

export const gameNoCurrentAction3 = Immutable.fromJS({
  currentAction: {},
  playerTurn: 0
});

export const gameAddAction = Immutable.fromJS({
  currentAction: {
    type: 'ADD',
    data: {
      moves: [
        {
          to: { board: 'foo', space: 'A1' },
          player: 0,
          modifiers: []
        },
        {
          to: { board: 'foo', space: 'A2' },
          player: 0,
          modifiers: []
        },
        {
          to: { board: 'foo', space: 'A3' },
          player: 0,
          modifiers: []
        }
      ]
    }
  },
  playerTurn: 0
});

export const gameMoveAction = Immutable.fromJS({
  currentAction: {
    type: 'MOVE',
    data: {
      moves: [
        {
          to: { board: 'foo', space: 'A1' },
          player: 1,
          modifiers: ['baz']
        },
        {
          from: { board: 'foo', space: 'A1' },
          to: { board: 'foo', space: 'bar' },
          player: 1
        }
      ]
    }
  }
});

export const gameMoveAction2 = Immutable.fromJS({
  currentAction: {
    type: 'MOVE',
    data: {
      moves: [
        {
          to: { board: 'foo', space: 'A1' },
          player: 1,
          modifiers: ['baz']
        },
        {
          from: { board: 'foo', space: 'A1' },
          to: { board: 'foo', space: 'bar' },
          player: 1
        },
        {
          from: { board: 'foo', space: 'bar' },
          to: { board: 'foo', space: 'A2' },
          player: 1
        }
      ]
    }
  },
  playerTurn: 0
});

export const gameCaptureAction = Immutable.fromJS({
  playerTurn: 0,
  currentAction: {
    type: 'CAPTURE',
    data: {
      moves: [
        {
          to: { board: 'foo', space: 'a1' },
          player: 0,
          modifiers: []
        },
        {
          to: { board: 'foo', space: 'a2' },
          player: 0,
          modifiers: []
        },
        {
          to: { board: 'foo', space: 'a3' },
          player: 0,
          modifiers: []
        },
        {
          to: { board: 'foo', space: 'a4' },
          player: 1,
          modifiers: []
        }
      ]
    },
    target: {
      board: 'foo',
      space: 'bar',
      player: 1
    }
  }
});

export const sampleBoard = Immutable.fromJS({
  type: {
    name: 'Triangle',
    doors: {
      n: ['a2', 'a3'],
      e: ['c3', 'f3'],
      s: ['h2', 'h3'],
      w: ['d1', 'e1']
    },
    spaces: {
      a1: {
        centerX: 37.5,
        centerY: 23.13,
        points: [{
          x: 5,
          y: 4.38
        }, {
          x: 70,
          y: 4.38
        }, {
          x: 37.5,
          y: 60.63
        }],
        bordering: ['b1', 'd2'],
        touching: ['a2', 'c1', 'd1']
      },
      a2: {
        centerX: 102.5,
        centerY: 23.13,
        points: [{
          x: 70,
          y: 4.38
        }, {
          x: 135,
          y: 4.38
        }, {
          x: 102.5,
          y: 60.63
        }],
        bordering: ['b1', 'b2'],
        touching: ['a1', 'a3', 'c1', 'c2', 'd2']
      }
    }
  },
  name: 'foo',
  meditationSpaces: ['d2', 'd3', 'e2', 'e3', 'f2', 'c2']
});

export const sampleBoard2 = Immutable.fromJS({
  type: {
    name: 'Triangle',
    doors: {
      n: ['a2', 'a3'],
      e: ['c3', 'f3'],
      s: ['h2', 'h3'],
      w: ['d1', 'e1']
    },
    spaces: {
      a1: {
        centerX: 37.5,
        centerY: 23.13,
        points: [{
          x: 5,
          y: 4.38
        }, {
          x: 70,
          y: 4.38
        }, {
          x: 37.5,
          y: 60.63
        }],
        bordering: ['b1', 'd2'],
        touching: ['a2', 'c1', 'd1']
      },
      a2: {
        centerX: 102.5,
        centerY: 23.13,
        points: [{
          x: 70,
          y: 4.38
        }, {
          x: 135,
          y: 4.38
        }, {
          x: 102.5,
          y: 60.63
        }],
        bordering: ['b1', 'b2'],
        touching: ['a1', 'a3', 'c1', 'c2', 'd2']
      }
    }
  },
  name: 'foo2',
  meditationSpaces: ['d2', 'd3', 'e2', 'e3', 'f2', 'c2']
});

export const sampleWorld = Immutable.fromJS({
  layout: [
    { x: 0, y: 0, neighbors: { n: 0, e: 1, s: 0, w: 1 } },
    { x: 300, y: 0, neighbors: { n: 1, e: 0, s: 1, w: 0 } }
  ],
  rooms: [
    {
      player: 'c07eb4dd-30c8-47c4-a79b-dc20f7e2c999',
      board: sampleBoard,
      rotation: 0
    },
    {
      player: 'dfccccc4-fc47-4584-bab2-ca6217a40539',
      board: sampleBoard2,
      rotation: 0
    }
  ]
});

export const sampleWorld2 = Immutable.fromJS({
  layout: [
    { x: 0, y: 0, neighbors: { n: 0, e: 1, s: 0, w: 1 } },
    { x: 300, y: 0, neighbors: { n: 1, e: 0, s: 1, w: 0 } }
  ],
  rooms: [
    {
      player: 'c07eb4dd-30c8-47c4-a79b-dc20f7e2c999',
      board: sampleBoard,
      rotation: 90
    },
    {
      player: 'dfccccc4-fc47-4584-bab2-ca6217a40539',
      board: sampleBoard2,
      rotation: 0
    }
  ]
});

describe('blank test', () => {
  it('test inputs', () => expect(true).toBeTruthy());
});
