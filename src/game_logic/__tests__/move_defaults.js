import Immutable from 'immutable';
import moveDefaults from '../move_defaults';

jest.unmock('../move_defaults');

jest.unmock('../base_util');
const baseUtil = require('../base_util');

const mocks = {
  getCurrentInProgress: jest.spyOn(baseUtil, 'getCurrentInProgress').mockImplementation(() => Immutable.fromJS([{ board: 'foo', space: 'a1', modifiers: [], player: 0 }])),
  getBordering: jest.spyOn(baseUtil, 'getBordering').mockImplementation(() => Immutable.fromJS([{ board: 'foo', space: 'a1' }, { board: 'foo', space: 'a2' }])),
  isBlocked: jest.spyOn(baseUtil, 'isBlocked').mockImplementation((game, players, board, space) => (players.size > 1 && space === 'a1'))
};

jest.mock('../base_util', () => ({
  getCurrentInProgress: () => mocks.getCurrentInProgress,
  getBordering: () => mocks.getBordering,
  isBlocked: () => mocks.isBlocked
}));

describe('piecesCanStillMove', () => {
  const game = Immutable.fromJS({
    playerTurn: 0,
    currentAction: {
      data: {
        moves: [{ from: {} }, { from: {} }]
      }
    }
  });
  const players = Immutable.fromJS([
    {
      pieces: [
        { board: 'foo', space: 'a2', modifiers: [] },
        { board: 'foo', space: 'captured' }
      ],
      cards: []
    }
  ]);
  const result = Immutable.fromJS([
    { board: 'foo', space: 'a2', modifiers: [] },
    { board: 'foo', space: 'a1', modifiers: [], player: 0 }
  ]);

  it('should return empty list if player has used up all moves', () =>
    expect(moveDefaults.piecesCanStillMove(2, game, players)).toEqual(Immutable.List()));

  it('should return list if player has not used up all moves', () =>
    expect(moveDefaults.piecesCanStillMove(3, game, players)).toEqual(result));
});

describe('init', () => {
  const game = Immutable.fromJS({ playerTurn: 0 });
  const players = Immutable.fromJS([
    {
      pieces: [
        { board: 'foo', space: 'bar' },
        { board: 'foo', space: 'captured' }
      ]
    }
  ]);
  const result = Immutable.fromJS([{ board: 'foo', space: 'bar' }]);

  it('should return active players pieces with captured filtered out', () =>
    expect(moveDefaults.init(game, players)).toEqual(result));
});

describe('available', () => {
  it('should correctly call getBordering and isBlocked to filter out open spaces', () =>
    expect(moveDefaults.available('foo', 'bar', Immutable.fromJS(['foo', 'bar']), 'a', 'b')).toEqual(Immutable.fromJS([{ board: 'foo', space: 'a2' }])));
});

describe('blockedAvailable', () => {
  it('should correctly call getBordering and isBlocked to filter out open spaces', () =>
    expect(moveDefaults.blockedAvailable(Immutable.fromJS({ playerTurn: 0 }), 'bar', Immutable.fromJS(['foo', 'bar']), 'a', 'b')).toEqual(Immutable.fromJS([{ board: 'foo', space: 'a1' }, { board: 'foo', space: 'a2' }])));
});

describe('confirmMove', () => {
  it('should return currentInProgress with no modifiers', () =>
    expect(moveDefaults.confirmMove(Immutable.fromJS({ playerTurn: 0 }))).toEqual(Immutable.fromJS([{ board: 'foo', space: 'a1', modifiers: [], player: 0 }])));
});
