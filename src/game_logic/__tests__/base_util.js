import Immutable from 'immutable';
import Boards from '../../game_config/boards/boards-enum';
import {
  addDamage,
  allTouchingEnemies,
  canTakeAdditionalAction,
  cardsShouldHave,
  clearHighlightAndTriggers,
  generateUUID,
  getBordering,
  getBorderingNeighbors,
  getBorderingSameRoom,
  getCurrentInProgress,
  getCurrentInProgressOriginalPosition,
  getTouching,
  isBlocked,
  isDoorSpace,
  isOccupied,
  isTouching,
  whichPlayerPiece,
  whichPlayerPieceTemp
} from '../base_util';
import {
  playersIsOccupied,
  playersIsNotOccupied,
  playersTouching,
  gameNoCurrentAction,
  gameNoCurrentAction3,
  gameAddAction,
  gameMoveAction,
  gameMoveAction2,
  sampleBoard,
  sampleBoard2,
  sampleWorld,
  sampleWorld2
} from './sample_inputs';

jest.unmock('../base_util');
jest.unmock('../../actions/game');
const gameActions = require('../../actions/game');

jest.unmock('../../actions/players');
const playerActions = require('../../actions/players');

const mockState = {
  game: gameNoCurrentAction,
  world: sampleWorld,
  players: Immutable.fromJS([
    { damageCounter: 4 }
  ])
};

jest.mock('../../utils/store', () => ({
  getState: () => mockState,
  dispatch: () => {}
}));

describe('addDamage', () => {
  const spy = jest.spyOn(playerActions, 'updateDamage');

  it('should call addDamage normally', () => {
    addDamage(0, 1);
    expect(spy).toHaveBeenCalledWith(0, 5);
  });

  it('should call addDamage correctly with maximum damage', () => {
    addDamage(0, 2);
    expect(spy).toHaveBeenCalledWith(0, 5);
  });
});

describe('allTouchingEnemies', () => {
  const results = Immutable.fromJS([
    { board: 'foo', space: 'b1', player: 1 },
    { board: 'foo', space: 'd2', player: 2 },
    { board: 'foo2', space: 'd1', player: 1 },
    { board: 'foo', space: 'a3', player: 2 }
  ]);
  Boards.foo = sampleBoard;

  it('should correctly get the list of touching enemies', () =>
    expect(allTouchingEnemies(gameNoCurrentAction3, sampleWorld2, playersTouching))
      .toEqual(results));
});

describe('canTakeAdditionalAction', () => {
  const game = Immutable.fromJS({
    playerTurn: 0
  });
  const players = Immutable.fromJS([
    {
      damageCounter: 5
    }
  ]);
  const players2 = Immutable.fromJS([
    {
      damageCounter: 1,
      cards: []
    }
  ]);

  it('should return true because player can take damage extra action', () =>
    expect(canTakeAdditionalAction(game, players)).toBe(true));

  it('should return false because player cannot take additional action', () =>
    expect(canTakeAdditionalAction(game, players2)).toBe(false));
});

describe('cardsShouldHave', () => {
  const player = Immutable.fromJS({
    pieces: [
      { board: 'foo', space: 'd2' },
      { board: 'foo', space: 'd3' },
      { board: 'foo', space: 'e2' },
      { board: 'foo2', space: 'd2' },
      { board: 'foo2', space: 'd3' }
    ]
  });

  it('should return correct number of cards at first requirement', () =>
    expect(cardsShouldHave(sampleWorld, player, 2)).toBe(2));

  it('should return correct number of cards at second requirement', () =>
    expect(cardsShouldHave(sampleWorld, player, 3)).toBe(1));
});

describe('clearHighlightAndTriggers', () => {
  const spy1 = jest.spyOn(gameActions, 'updateHighlighted');
  const spy2 = jest.spyOn(gameActions, 'clearTriggers');

  it('should call updateHighlighted with an empty array', () => {
    clearHighlightAndTriggers();
    expect(spy1).toHaveBeenCalledWith([]);
  });

  it('should call clearTriggers', () => {
    clearHighlightAndTriggers();
    expect(spy2).toHaveBeenCalled();
  });
});

describe('generateUUID', () => {
  it('should generate UUID in correct format', () =>
    expect(generateUUID()).toMatch(/[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/));

  it('should make unique UUIDs', () =>
    expect(generateUUID()).not.toEqual(generateUUID()));
});

describe('getBordering', () => {
  const result = Immutable.fromJS([{ board: 'foo', space: 'b1' }, { board: 'foo', space: 'd2' }]);

  const result2 = Immutable.fromJS([
    { board: 'foo', space: 'b1' },
    { board: 'foo', space: 'b2' },
    { board: 'foo', space: 'h2' },
    { board: 'foo', space: 'h3' }
  ]);

  it('should return the correct bordering spaces with non door spaces', () =>
    expect(getBordering(sampleWorld, sampleBoard, 'a1')).toEqual(result));

  it('should return the correct bordering spaces with door spaces', () =>
    expect(getBordering(sampleWorld, sampleBoard, 'a2')).toEqual(result2));
});

describe('getBorderingNeighbors', () => {
  const result = Immutable.fromJS([
    { board: 'foo', space: 'h2' },
    { board: 'foo', space: 'h3' }
  ]);

  const result2 = Immutable.fromJS([
    { board: 'foo2', space: 'd1' },
    { board: 'foo2', space: 'e1' }
  ]);

  it('should return empty list if space is not a door space', () =>
    expect(getBorderingNeighbors(sampleWorld, sampleBoard, 'a1')).toEqual([]));

  it('should return correct list of matching door spaces', () =>
    expect(getBorderingNeighbors(sampleWorld, sampleBoard, 'a2')).toEqual(result));

  it('should return correct list of matching door spaces with rotation', () =>
    expect(getBorderingNeighbors(sampleWorld2, sampleBoard, 'a2')).toEqual(result2));
});

describe('getBorderingSameRoom', () => {
  const result = Immutable.fromJS([{ board: 'foo', space: 'b1' }, { board: 'foo', space: 'd2' }]);

  it('should return the correct bordering spaces', () =>
    expect(getBorderingSameRoom(sampleBoard, 'a1')).toEqual(result));
});

describe('getCurrentInProgress', () => {
  const addResult = Immutable.fromJS([
    { board: 'foo', space: 'A1', player: 0, modifiers: [] },
    { board: 'foo', space: 'A2', player: 0, modifiers: [] },
    { board: 'foo', space: 'A3', player: 0, modifiers: [] }
  ]);

  const moveResult1 = Immutable.fromJS([
    { board: 'foo', space: 'bar', player: 1, modifiers: ['baz'] }
  ]);

  const moveResult2 = Immutable.fromJS([
    { board: 'foo', space: 'A2', player: 1, modifiers: ['baz'] }
  ]);

  it('should return correct pieces from an add action', () =>
    expect(getCurrentInProgress(gameAddAction)).toEqual(addResult));

  it('should return correct pieces from a move action', () =>
    expect(getCurrentInProgress(gameMoveAction)).toEqual(moveResult1));

  it('should return correct pieces from a move action with more moves', () =>
    expect(getCurrentInProgress(gameMoveAction2)).toEqual(moveResult2));
});

describe('getCurrentInProgressOriginalPosition', () => {
  const moveResult1 = Immutable.fromJS([
    { board: 'foo', space: 'A1', player: 1, modifiers: ['baz'] }
  ]);

  const moveResult2 = Immutable.fromJS([
    { board: 'foo', space: 'A1', player: 1, modifiers: ['baz'] }
  ]);

  it('should return an empty list when no current action', () =>
    expect(getCurrentInProgressOriginalPosition(gameNoCurrentAction)).toEqual(Immutable.List()));

  it('should return correct place after one move', () =>
    expect(getCurrentInProgressOriginalPosition(gameMoveAction)).toEqual(moveResult1));

  it('should return correct place after multiple moves', () =>
    expect(getCurrentInProgressOriginalPosition(gameMoveAction2)).toEqual(moveResult2));
});

describe('getTouching', () => {
  const result = Immutable.fromJS([
    { board: 'foo', space: 'a2' },
    { board: 'foo', space: 'c1' },
    { board: 'foo', space: 'd1' }
  ]);

  it('should return the correct touching spaces', () =>
    expect(getTouching(sampleBoard, 'a1')).toEqual(result));
});

describe('isBlocked', () => {
  it('should return true if the space is occupied and there is not a current action', () =>
    expect(isBlocked(gameNoCurrentAction, playersIsOccupied, sampleBoard, 'bar')).toBe(true));

  it('should return false if the space is not occupied and there is not a current action', () =>
    expect(isBlocked(gameNoCurrentAction, playersIsNotOccupied, sampleBoard, 'bar')).toBe(false));

  it('should return true if a piece has moved onto the space in current action', () =>
    expect(isBlocked(gameMoveAction, playersIsNotOccupied, sampleBoard, 'bar')).toBe(true));

  it('should return false if a piece has moved on and off the space in current action', () =>
    expect(isBlocked(gameMoveAction2, playersIsNotOccupied, sampleBoard, 'bar')).toBe(false));
});

describe('isDoorSpace', () => {
  it('should return false for not passing in a door space', () =>
    expect(isDoorSpace(sampleBoard, 'a1')).toBeFalsy());

  it('should return the correct door side if actually a door space', () =>
    expect(isDoorSpace(sampleBoard, 'h3')).toEqual('s'));
});

describe('isOccupied', () => {
  it('should return true if the space is occupied', () =>
    expect(isOccupied(playersIsOccupied, sampleBoard, 'bar')).toBe(true));

  it('should return false if the space is not occupied', () =>
    expect(isOccupied(playersIsNotOccupied, sampleBoard, 'bar')).toBe(false));
});

describe('isTouching', () => {
  it('should return false if two spaces are not touching', () =>
    expect(isTouching(sampleWorld, sampleBoard, 'a1', sampleBoard2, 'a2')).toBeFalsy());

  it('should return true if two spaces are bordering on same board', () =>
    expect(isTouching(sampleWorld, sampleBoard, 'a1', sampleBoard, 'b1')).toBeTruthy());

  it('should return true if two spaces are touching on same board', () =>
    expect(isTouching(sampleWorld, sampleBoard, 'a1', sampleBoard, 'a2')).toBeTruthy());

  it('should return true if two spaces are touching on different boards', () =>
    expect(isTouching(sampleWorld, sampleBoard, 'a2', sampleBoard, 'h2')).toBeTruthy());

  it('should return true if two spaces are touching on different boards with rotation', () =>
    expect(isTouching(sampleWorld2, sampleBoard, 'a2', sampleBoard2, 'd1')).toBeTruthy());
});

describe('whichPlayerPiece', () => {
  it('should return correct index of player who has given piece', () =>
    expect(whichPlayerPiece(playersIsOccupied, 'foo', 'bar')).toEqual(1));
});

describe('whichPlayerPieceTemp', () => {
  const game = Immutable.fromJS({
    currentAction: {
      type: 'MOVE',
      data: {
        moves: [
          {
            to: { board: 'foo', space: 'A1' },
            player: 0
          },
          {
            from: { board: 'foo', space: 'A1' },
            to: { board: 'foo', space: 'bar' },
            player: 0
          }
        ]
      }
    },
    playerTurn: 0
  });

  it('should return correct index when actual piece present', () =>
    expect(whichPlayerPieceTemp(game, playersIsOccupied, 'foo', 'bar')).toEqual(1));

  it('should return correct index when there is only temp piece', () =>
    expect(whichPlayerPieceTemp(game, playersIsNotOccupied, 'foo', 'bar')).toEqual(0));
});
