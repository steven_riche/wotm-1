import Immutable from 'immutable';
import captureDefaults from '../capture_defaults';

jest.unmock('../capture_defaults');
jest.unmock('../../game_config/constants/constants');

jest.unmock('../base_util');
const baseUtil = require('../base_util');

const mocks = {
  allTouchingEnemies: jest.spyOn(baseUtil, 'allTouchingEnemies').mockImplementation(() => Immutable.fromJS([{ board: 'foo', space: 'a1', player: 1 }, { board: 'foo', space: 'a2', player: 2 }])),
  getBordering: jest.spyOn(baseUtil, 'getBordering').mockImplementation(() => Immutable.fromJS([{ board: 'foo', space: 'a1' }, { board: 'foo', space: 'a2' }])),
  getTouching: jest.spyOn(baseUtil, 'getTouching').mockImplementation(() => Immutable.fromJS([{ board: 'foo', space: 'a3' }, { board: 'foo', space: 'a4' }])),
  whichPlayerPiece: jest.spyOn(baseUtil, 'whichPlayerPiece').mockImplementation(() => 1)
};

jest.mock('../base_util', () => ({
  allTouchingEnemies: () => mocks.allTouchingEnemies,
  getBordering: () => mocks.getBordering,
  getTouching: () => mocks.getTouching,
  whichPlayerPiece: () => mocks.whichPlayerPiece
}));

describe('init', () => {
  const game = Immutable.fromJS({ playerTurn: 0 });
  const world = 'foo';
  const players1 = Immutable.fromJS([
    { lastCaptured: 3 },
    {},
    {},
    {}
  ]);
  const players2 = Immutable.fromJS([
    { lastCaptured: 2 },
    {},
    {}
  ]);
  const players3 = Immutable.fromJS([
    { lastCaptured: 2 },
    {},
    {},
    {}
  ]);
  const result1 = Immutable.fromJS([{ board: 'foo', space: 'a1', player: 1 }, { board: 'foo', space: 'a2', player: 2 }]);
  const result2 = Immutable.fromJS([{ board: 'foo', space: 'a1', player: 1 }]);

  it('should return all touching enemies', () =>
    expect(captureDefaults.init(game, world, players1)).toEqual(result1));

  it('should still return all touching enemies despite lastCaptured because only 3 players', () =>
    expect(captureDefaults.init(game, world, players2)).toEqual(result1));

  it('should return only one enemy because of lastCaptured', () =>
    expect(captureDefaults.init(game, world, players3)).toEqual(result2));
});

describe('choose', () => {
  const game = Immutable.fromJS({
    playerTurn: 0,
    currentAction: {
      data: {
        moves: [
          { player: 0 },
          { player: 0 }
        ]
      }
    }
  });
  const players1 = Immutable.fromJS([
    { score: 0, pieces: [], cards: [] },
    { score: 0, pieces: [], cards: [] }
  ]);
  const players2 = Immutable.fromJS([
    {
      score: 1,
      pieces: [
        { board: 'foo', space: 'a1' }
      ],
      cards: []
    },
    { score: 0, pieces: [], cards: [] }
  ]);
  const players3 = Immutable.fromJS([
    {
      score: 2,
      pieces: [
        { board: 'foo', space: 'a1' }
      ],
      cards: []
    },
    {
      pieces: [
        { board: 'foo', space: 'a4' }
      ],
      cards: []
    }
  ]);

  it('should return an empty list if already have chosen enough pieces', () =>
    expect(captureDefaults.choose(game, 'world', players1, 'foo', 'bar')).toEqual(Immutable.List()));

  it('should return a list of available own pieces', () =>
    expect(captureDefaults.choose(game, 'world', players2, 'foo', 'bar')).toEqual(Immutable.fromJS([{ board: 'foo', space: 'a1', player: 0 }])));

  it('should return a list of available own and enemy pieces', () =>
    expect(captureDefaults.choose(game, 'world', players3, 'foo', 'bar')).toEqual(Immutable.fromJS([{ board: 'foo', space: 'a1', player: 0 }, { board: 'foo', space: 'a4', player: 1 }])));
});

describe('canCapture', () => {
  const players = Immutable.fromJS([{ score: 2, cards: [] }, { score: 1, cards: [] }]);
  const game1 = Immutable.fromJS({ playerTurn: 0, currentAction: {} });
  const game2 = Immutable.fromJS({
    playerTurn: 0,
    currentAction: {
      type: 'CAPTURE',
      data: {
        moves: [
          { player: 0 },
          { player: 1 }
        ]
      }
    }
  });
  const game3 = Immutable.fromJS({
    playerTurn: 0,
    currentAction: {
      type: 'CAPTURE',
      data: {
        moves: [
          { player: 0 },
          { player: 0 },
          { player: 0 }
        ]
      }
    }
  });
  const game4 = Immutable.fromJS({
    playerTurn: 0,
    currentAction: {
      type: 'CAPTURE',
      data: {
        moves: [
          { player: 0 },
          { player: 0 },
          { player: 0 },
          { player: 1 }
        ]
      }
    }
  });

  it('should return false if there is no current action', () =>
    expect(captureDefaults.canCapture(game1, players, 0, 1)).toBe(false));

  it('should return false if player does not have enough of his own pieces', () =>
    expect(captureDefaults.canCapture(game2, players, 0, 1)).toBe(false));

  it('should return false if player does not have enough enemy pieces', () =>
    expect(captureDefaults.canCapture(game3, players, 0, 1)).toBe(false));

  it('should return true if player has enough pieces', () =>
    expect(captureDefaults.canCapture(game4, players, 0, 1)).toBe(true));
});

describe('newDamage', () => {
  const game1 = Immutable.fromJS({
    playerTurn: 0,
    currentAction: {
      target: {
        player: 1
      }
    }
  });
  const players1 = Immutable.fromJS([
    {
      score: 1
    },
    {
      damageCounter: 2
    }
  ]);
  const players2 = Immutable.fromJS([
    {
      score: 2
    },
    {
      damageCounter: 2
    }
  ]);
  const players3 = Immutable.fromJS([
    {
      score: 2
    },
    {
      damageCounter: 4
    }
  ]);

  it('should correctly calculate new damage for one capture', () =>
    expect(captureDefaults.newDamage(game1, players1)).toBe(3));

  it('should correctly calculate new damage for combo capture', () =>
    expect(captureDefaults.newDamage(game1, players2)).toBe(4));

  it('should correctly calculate new damage for hitting damage limit', () =>
    expect(captureDefaults.newDamage(game1, players3)).toBe(5));
});
