import Immutable from 'immutable';
import { initAdd, addOrigin, addPiece, initAddFirstPiece, addStartingSpace, confirmAdd } from '../add_process';
import { gameNoCurrentAction3, sampleWorld } from './sample_inputs';

jest.unmock('../add_process');
jest.unmock('../base_util');
const baseUtil = require('../base_util');

jest.unmock('../../actions/game');
const gameActions = require('../../actions/game');

jest.unmock('../../actions/players');
const playerActions = require('../../actions/players');

const mocks = {
  addDamage: jest.spyOn(baseUtil, 'addDamage'),
  clearHighlightAndTriggers: jest.spyOn(baseUtil, 'clearHighlightAndTriggers'),
  getCurrentInProgress: jest.spyOn(baseUtil, 'getCurrentInProgress'),
  whichPlayerPiece: jest.spyOn(baseUtil, 'whichPlayerPiece').mockImplementation(() => 1),
  updateCurrentAction: jest.spyOn(gameActions, 'updateCurrentAction'),
  updateHighlighted: jest.spyOn(gameActions, 'updateHighlighted'),
  updatePrompt: jest.spyOn(gameActions, 'updatePrompt'),
  appendCurrentAction: jest.spyOn(gameActions, 'appendCurrentAction'),
  updateCanConfirm: jest.spyOn(gameActions, 'updateCanConfirm'),
  addTriggers: jest.spyOn(gameActions, 'addTriggers'),
  updatePieces: jest.spyOn(playerActions, 'updatePieces'),
  add_init: jest.fn().mockImplementation(() => Immutable.fromJS([{ board: 'foo', space: 'bar' }])),
  add_available: jest.fn().mockImplementation(() => Immutable.fromJS([{ board: 'foo', space: 'a1' }, { board: 'foo', space: 'a2' }])),
  add_blockedAvailable: jest.fn().mockImplementation(() => Immutable.fromJS([{ board: 'foo', space: 'a1' }, { board: 'foo', space: 'a2' }])),
  add_canAddMore: jest.fn().mockImplementation(() => false),
  add_initFirstPiece: jest.fn().mockImplementation(() => Immutable.fromJS([{ board: 'foo', space: 'bar' }])),
  add_confirmAdd: jest.fn().mockImplementation(() => Immutable.List([{ board: 'foo', space: 'bar' }]))
};
const mockGame = Immutable.fromJS(gameNoCurrentAction3);
const mockWorld = Immutable.fromJS(sampleWorld);
const mockPlayers = Immutable.fromJS([
  {
    role: {
      add: {
        init: mocks.add_init,
        available: mocks.add_available,
        blockedAvailable: mocks.add_blockedAvailable,
        canAddMore: mocks.add_canAddMore,
        initFirstPiece: mocks.add_initFirstPiece,
        confirmAdd: mocks.add_confirmAdd
      }
    }
  },
  {
    pieces: [
      { board: 'foo', space: 'bar', modifiers: [] }
    ]
  }
]);

const mockState = {
  game: mockGame,
  world: mockWorld,
  players: mockPlayers
};

jest.mock('../../utils/store', () => ({
  getState: () => mockState,
  dispatch: () => {}
}));

jest.mock('../base_util', () => ({
  addDamage: () => mocks.addDamage,
  clearHighlightAndTriggers: () => mocks.clearHighlightAndTriggers,
  getCurrentInProgress: () => mocks.getCurrentInProgress,
  whichPlayerPiece: () => mocks.whichPlayerPiece
}));

describe('initAdd', () => {
  beforeAll(() => initAdd());

  it('should clear triggers and highlights', () =>
    expect(mocks.clearHighlightAndTriggers).toHaveBeenCalled());

  it('should update current action with ADD type', () =>
    expect(mocks.updateCurrentAction).toHaveBeenCalledWith({ data: {}, type: 'ADD' }));

  it('should call add init function', () =>
    expect(mocks.add_init).toHaveBeenCalledWith(mockGame, mockPlayers));

  it('should highlight new spaces', () =>
    expect(mocks.updateHighlighted).toHaveBeenCalledWith(Immutable.List(['foo_bar_primary'])));

  it('should add triggers on highlighted spaces', () =>
    expect(mocks.addTriggers.mock.calls[0][0])
      .toEqual(Immutable.Map({ foo_bar: expect.any(Function) })));

  it('should update the prompt', () =>
    expect(mocks.updatePrompt).toHaveBeenCalled());

  afterAll(() => {
    mocks.clearHighlightAndTriggers.mockReset();
    mocks.updateCurrentAction.mockReset();
    mocks.updateHighlighted.mockReset();
    mocks.addTriggers.mockReset();
    mocks.updatePrompt.mockReset();
  });
});

describe('addOrigin', () => {
  beforeAll(() => addOrigin('foo_bar'));

  it('should clear triggers and highlights', () =>
    expect(mocks.clearHighlightAndTriggers).toHaveBeenCalled());

  it('should call add available function', () =>
    expect(mocks.add_available).toHaveBeenCalledWith(mockGame, mockWorld, mockPlayers, undefined, 'bar'));

  it('should not call add blocked available function', () =>
    expect(mocks.add_blockedAvailable).not.toHaveBeenCalled());

  it('should highlight new spaces', () =>
    expect(mocks.updateHighlighted).toHaveBeenCalledWith(Immutable.List(['foo_a1_primary', 'foo_a2_primary'])));

  it('should add triggers on highlighted spaces', () =>
    expect(mocks.addTriggers.mock.calls[0][0])
      .toEqual(Immutable.Map({ foo_a1: expect.any(Function), foo_a2: expect.any(Function) })));

  it('should update the prompt', () =>
    expect(mocks.updatePrompt).toHaveBeenCalled());

  afterAll(() => {
    mocks.clearHighlightAndTriggers.mockReset();
    mocks.updateHighlighted.mockReset();
    mocks.addTriggers.mockReset();
    mocks.updatePrompt.mockReset();
  });
});

describe('addPiece', () => {
  beforeAll(() => addPiece('foo_bar'));

  it('should append action with temp enemy piece', () =>
    expect(mocks.appendCurrentAction).toHaveBeenCalledWith({ to: { board: 'foo', space: 'bar' }, player: 1, modifiers: Immutable.List() }));

  it('should call updatePieces and give an empty list for enemy', () =>
    expect(mocks.updatePieces).toHaveBeenCalledWith(1, Immutable.List()));

  it('should append action', () =>
    expect(mocks.appendCurrentAction).toHaveBeenCalledWith({ to: { board: 'foo', space: 'bar' }, player: 0, modifiers: Immutable.List() }));

  it('should enable confirm button', () =>
    expect(mocks.updateCanConfirm).toHaveBeenCalledWith(true));

  it('should check if can add more pieces', () =>
    expect(mocks.add_canAddMore).toHaveBeenCalledWith(mockGame, mockWorld, mockPlayers));

  it('should clear triggers and highlights', () =>
    expect(mocks.clearHighlightAndTriggers).toHaveBeenCalled());

  it('should update the prompt', () =>
    expect(mocks.updatePrompt).toHaveBeenCalled());

  afterAll(() => {
    mocks.appendCurrentAction.mockReset();
    mocks.updateCanConfirm.mockReset();
    mocks.clearHighlightAndTriggers.mockReset();
    mocks.updatePrompt.mockReset();
  });
});

describe('initAddFirstPiece', () => {
  beforeAll(() => initAddFirstPiece());

  it('should clear triggers and highlights', () =>
    expect(mocks.clearHighlightAndTriggers).toHaveBeenCalled());

  it('should update current action with ADD type', () =>
    expect(mocks.updateCurrentAction).toHaveBeenCalledWith({ data: {}, type: 'ADD' }));

  it('should call add init function', () =>
    expect(mocks.add_initFirstPiece).toHaveBeenCalledWith(mockGame, mockWorld, mockPlayers));

  it('should highlight new spaces', () =>
    expect(mocks.updateHighlighted).toHaveBeenCalledWith(Immutable.List(['foo_bar_primary'])));

  it('should add triggers on highlighted spaces', () =>
    expect(mocks.addTriggers.mock.calls[0][0])
      .toEqual(Immutable.Map({ foo_bar: expect.any(Function) })));

  it('should update the prompt', () =>
    expect(mocks.updatePrompt).toHaveBeenCalled());

  afterAll(() => {
    mocks.clearHighlightAndTriggers.mockReset();
    mocks.updateCurrentAction.mockReset();
    mocks.updateHighlighted.mockReset();
    mocks.addTriggers.mockReset();
    mocks.updatePrompt.mockReset();
  });
});

describe('addStartingSpace', () => {
  beforeAll(() => addStartingSpace('foo_bar'));

  it('should clear triggers and highlights', () =>
    expect(mocks.clearHighlightAndTriggers).toHaveBeenCalled());

  it('should append action', () =>
    expect(mocks.appendCurrentAction).toHaveBeenCalledWith({ to: { board: 'foo', space: 'bar' }, player: 0, modifiers: Immutable.List() }));

  it('should enable confirm button', () =>
    expect(mocks.updateCanConfirm).toHaveBeenCalledWith(true));

  it('should update the prompt', () =>
    expect(mocks.updatePrompt).toHaveBeenCalled());

  afterAll(() => {
    mocks.clearHighlightAndTriggers.mockReset();
    mocks.appendCurrentAction.mockReset();
    mocks.updateCanConfirm.mockReset();
    mocks.updatePrompt.mockReset();
  });
});

describe('confirmAdd', () => {
  beforeAll(() => confirmAdd());

  it('should not call getCurrentInProgress', () =>
    expect(mocks.getCurrentInProgress).not.toHaveBeenCalled());

  it('should not call addDamage', () =>
    expect(mocks.addDamage).not.toHaveBeenCalled());

  it('should call confirmAdd function', () =>
    expect(mocks.add_confirmAdd).toHaveBeenCalledWith(mockGame, mockPlayers));

  it('should update pieces', () =>
    expect(mocks.updatePieces).toHaveBeenCalledWith(0, Immutable.List([{ board: 'foo', space: 'bar' }])));

  afterAll(() => {
    mocks.updatePieces.mockReset();
  });
});
