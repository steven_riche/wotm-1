import Immutable from 'immutable';
import { choosePieceToMove, confirmMove } from '../move_process';
import { gameMoveAction2, sampleWorld } from './sample_inputs';

jest.unmock('../move_process');
jest.unmock('../base_util');
const baseUtil = require('../base_util');

jest.unmock('../../actions/game');
const gameActions = require('../../actions/game');

jest.unmock('../../actions/players');
const playerActions = require('../../actions/players');

const mocks = {
  addDamage: jest.spyOn(baseUtil, 'addDamage'),
  clearHighlightAndTriggers: jest.spyOn(baseUtil, 'clearHighlightAndTriggers'),
  getCurrentInProgress: jest.spyOn(baseUtil, 'getCurrentInProgress').mockImplementation(() => Immutable.fromJS([{ board: 'foo', space: 'a2', player: 1, modifiers: [] }])),
  whichPlayerPiece: jest.spyOn(baseUtil, 'whichPlayerPiece').mockImplementation(() => 1),
  updateCurrentAction: jest.spyOn(gameActions, 'updateCurrentAction'),
  appendCurrentAction: jest.spyOn(gameActions, 'appendCurrentAction'),
  updateHighlighted: jest.spyOn(gameActions, 'updateHighlighted'),
  updateCanConfirm: jest.spyOn(gameActions, 'updateCanConfirm'),
  updatePrompt: jest.spyOn(gameActions, 'updatePrompt'),
  addTriggers: jest.spyOn(gameActions, 'addTriggers'),
  updatePieces: jest.spyOn(playerActions, 'updatePieces'),
  move_init: jest.fn().mockImplementation(() => Immutable.fromJS([{ board: 'foo', space: 'bar' }])),
  move_piecesCanStillMove: jest.fn().mockImplementation(() => Immutable.fromJS([{ board: 'foo', space: 'bar', player: 0, modifiers: [] }, { board: 'foo', space: 'a1', player: 0, modifiers: [] }])),
  move_available: jest.fn().mockImplementation(() => Immutable.fromJS([{ board: 'foo', space: 'a2', player: 0, modifiers: [] }])),
  move_blockedAvailable: jest.fn().mockImplementation(() => Immutable.fromJS([{ board: 'foo', space: 'a2', player: 0, modifiers: [] }])),
  move_confirmMove: jest.fn().mockImplementation(() => 'new pieces')
};
const mockGame = Immutable.fromJS(gameMoveAction2).setIn(['currentActionModifiers', 'bluePressure'], true);
const mockWorld = Immutable.fromJS(sampleWorld);
const mockPlayers = Immutable.fromJS([
  {
    role: {
      move: {
        init: mocks.move_init,
        piecesCanStillMove: mocks.move_piecesCanStillMove,
        available: mocks.move_available,
        blockedAvailable: mocks.move_blockedAvailable,
        confirmMove: mocks.move_confirmMove
      }
    },
    pieces: [
      { board: 'foo', space: 'captured', modifiers: [] },
      { board: 'foo', space: 'baz', modifiers: [] }
    ]
  },
  {
    pieces: [
      { board: 'foo', space: 'bar', modifiers: [] }
    ]
  }
]);
const mockState = {
  game: mockGame,
  world: mockWorld,
  players: mockPlayers
};

jest.mock('../../utils/store', () => ({
  getState: () => mockState,
  dispatch: () => {}
}));

jest.mock('../base_util', () => ({
  addDamage: () => mocks.addDamage,
  clearHighlightAndTriggers: () => mocks.clearHighlightAndTriggers,
  getCurrentInProgress: () => mocks.getCurrentInProgress,
  whichPlayerPiece: () => mocks.whichPlayerPiece
}));

describe('choosePieceToMove', () => {
  beforeAll(() => choosePieceToMove('foo_bar'));

  it('should call blocked available function', () =>
    expect(mocks.move_blockedAvailable).toHaveBeenCalledWith(mockGame, mockWorld, mockPlayers, undefined, 'bar'));

  it('should not call available function', () =>
    expect(mocks.move_available).not.toHaveBeenCalled());

  afterAll(() => {
    mocks.clearHighlightAndTriggers.mockReset();
    mocks.updateHighlighted.mockReset();
    mocks.addTriggers.mockReset();
    mocks.updatePrompt.mockReset();
  });
});

describe('confirmMove', () => {
  beforeAll(() => confirmMove());

  it('should call getCurrentInProgress', () =>
    expect(mocks.getCurrentInProgress).toHaveBeenCalledWith(mockGame));

  it('should call addDamage', () =>
    expect(mocks.addDamage).toHaveBeenCalledWith(1, 1));

  afterAll(() => mocks.updatePieces.mockReset());
});
