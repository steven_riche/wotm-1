import { A8 } from '../game_config/boards/board-layout-enum';
import Players from '../game_config/players/players-enum';

const initialGame = {
  id: 'game uuid',
  numPlayers: 8,
  playerTurn: 0,
  action: 2,
  totalTurn: 0,
  currentAction: {},
  currentActionModifiers: {},
  canConfirm: false,
  canDrawCard: false,
  specialActionNeeded: false,
  highlighted: [],
  prompt: '',
  triggerableSpaces: {},
  cardDeck: [],
  cardDiscardDeck: []
};

const initialWorld = {
  layout: A8,
  rooms: [
    {
      player: 'dfccccc4-fc47-4584-bab2-ca6217a40539',
      board: Players[0].boards[0],
      rotation: 90
    },
    {
      player: 'c07eb4dd-30c8-47c4-a79b-dc20f7e2c999',
      board: Players[1].boards[0],
      rotation: 0
    },
    {
      player: '57537ae4-639f-4baa-a785-d667be32a9de',
      board: Players[2].boards[0],
      rotation: 90
    },
    {
      player: '78a37354-63c5-4d29-b51e-ebb2a8f0c429',
      board: Players[3].boards[0],
      rotation: 270
    },
    {
      player: 'b31e535c-d372-426b-b12d-2c55fe846354',
      board: Players[4].boards[0],
      rotation: 0
    },
    {
      player: 'cc703ef3-71ec-4514-93c5-b0c280ccb401',
      board: Players[5].boards[0],
      rotation: 0
    },
    {
      player: '5f48636f-5cc5-4799-8b66-0e40ee15a407',
      board: Players[6].boards[0],
      rotation: 0
    },
    {
      player: 'cffcb782-a0d2-4d63-b9dd-307ee66db78b',
      board: Players[7].boards[0],
      rotation: 0
    }
  ]
};

const initialPlayers = [
  {
    role: Players[0],
    player: 'dfccccc4-fc47-4584-bab2-ca6217a40539',
    playerName: 'Black player',
    score: 0,
    damageCounter: 5,
    lastCaptured: null,
    cards: [],
    modifiers: {},
    pieces: [
      {
        board: 'PentagonA',
        space: 'e2',
        modifiers: ['foo']
      },
      {
        board: 'PentagonA',
        space: 'g2',
        modifiers: []
      },
      {
        board: 'PentagonA',
        space: 'f2',
        modifiers: []
      },
      {
        board: 'PentagonA',
        space: 'd2',
        modifiers: []
      }
    ]
  },
  {
    role: Players[1],
    player: '57537ae4-639f-4baa-a785-d667be32a9de',
    playerName: 'White player',
    score: 0,
    damageCounter: 2,
    lastCaptured: null,
    cards: [],
    modifiers: {},
    pieces: [
      {
        board: 'TriangleB',
        space: 'd2',
        modifiers: []
      },
      {
        board: 'TriangleB',
        space: 'b2',
        modifiers: []
      },
      {
        board: 'TriangleB',
        space: 'f1',
        modifiers: []
      }
    ]
  },
  {
    role: Players[2],
    player: '78a37354-63c5-4d29-b51e-ebb2a8f0c429',
    playerName: 'Purple player',
    score: 0,
    damageCounter: 5,
    lastCaptured: null,
    cards: [],
    modifiers: {},
    pieces: [
      {
        board: 'PentagonB',
        space: 'd3',
        modifiers: []
      },
      {
        board: 'PentagonB',
        space: 'b4',
        modifiers: []
      },
      {
        board: 'PentagonB',
        space: 'c3',
        modifiers: []
      },
      {
        board: 'PentagonB',
        space: 'c2',
        modifiers: []
      }
    ]
  },
  {
    role: Players[3],
    player: 'c07eb4dd-30c8-47c4-a79b-dc20f7e2c999',
    playerName: 'Blue player',
    score: 0,
    damageCounter: 2,
    lastCaptured: null,
    cards: [],
    modifiers: {},
    pieces: [
      {
        board: 'SnowflakeB',
        space: 'wh1',
        modifiers: []
      },
      {
        board: 'SnowflakeB',
        space: 'wh2',
        modifiers: []
      },
      {
        board: 'SnowflakeB',
        space: 'wh3',
        modifiers: []
      }
    ]
  },
  {
    role: Players[4],
    player: 'b31e535c-d372-426b-b12d-2c55fe846354',
    playerName: 'Red',
    score: 0,
    damageCounter: 0,
    lastCaptured: null,
    cards: [],
    modifiers: {},
    pieces: [
      {
        board: 'SnowflakeA',
        space: 'wh1',
        modifiers: []
      },
      {
        board: 'SnowflakeA',
        space: 'wh2',
        modifiers: []
      },
      {
        board: 'SnowflakeA',
        space: 'wh3',
        modifiers: []
      }
    ]
  },
  {
    role: Players[5],
    player: 'cc703ef3-71ec-4514-93c5-b0c280ccb401',
    playerName: 'yellow',
    score: 0,
    damageCounter: 0,
    lastCaptured: null,
    cards: [],
    modifiers: {},
    pieces: [
      {
        board: 'CobblestoneB',
        space: 'a2',
        modifiers: []
      },
      {
        board: 'CobblestoneB',
        space: 'b2',
        modifiers: []
      },
      {
        board: 'CobblestoneB',
        space: 'c2',
        modifiers: []
      }
    ]
  },
  {
    role: Players[6],
    player: '5f48636f-5cc5-4799-8b66-0e40ee15a407',
    playerName: 'Green',
    score: 0,
    damageCounter: 0,
    lastCaptured: null,
    cards: [],
    modifiers: {},
    pieces: [
      {
        board: 'HexB',
        space: 'a4',
        modifiers: []
      },
      {
        board: 'HexB',
        space: 'b3',
        modifiers: []
      },
      {
        board: 'HexB',
        space: 'c2',
        modifiers: []
      }
    ]
  },
  {
    role: Players[7],
    player: 'cffcb782-a0d2-4d63-b9dd-307ee66db78b',
    playerName: 'Orange',
    score: 0,
    damageCounter: 0,
    lastCaptured: null,
    cards: [],
    modifiers: {},
    pieces: [
      {
        board: 'TriangleA',
        space: 'a3',
        modifiers: []
      },
      {
        board: 'TriangleA',
        space: 'b3',
        modifiers: []
      },
      {
        board: 'TriangleA',
        space: 'c2',
        modifiers: []
      }
    ]
  }
];

export { initialGame, initialWorld, initialPlayers };
