import store from '../utils/store';
import {
  appendPrompt,
  discardCard,
  finishAction,
  forceNext,
  shuffleCardDeck,
  updateCanConfirm,
  updateCanDraw,
  updateCurrentAction,
  updateCurrentActionModifier,
  updatePrompt
} from '../actions/game';
import { updateCards, updateDamage } from '../actions/players';
import { restoreFromBackup, updateBackup } from '../actions/backup';
import { cardRequirements, damageToFreeAction } from '../game_config/constants/constants';
import { cardsShouldHave, clearHighlightAndTriggers } from './base_util';
import { confirmAdd } from './add_process';
import { confirmMove } from './move_process';
import { confirmCapture } from './capture_process';

const cancelAction = () => {
  const { backup } = store.getState();
  store.dispatch(restoreFromBackup(backup));
};

const confirmAction = () => {
  const { game, players } = store.getState();
  if (game.getIn(['currentAction', 'type']) === 'ADD') {
    confirmAdd();
  } else if (game.getIn(['currentAction', 'type']) === 'MOVE') {
    confirmMove();
  } else if (game.getIn(['currentAction', 'type']) === 'CAPTURE') {
    confirmCapture();
  } else if (game.getIn(['currentAction', 'type']) === 'SPECIAL') {
    players.getIn([game.get('playerTurn'), 'role', 'special', 'confirm'])();
  } else if (game.hasIn(['currentAction', 'type']) && game.getIn(['currentAction', 'type']).indexOf('CARD_') !== -1) {
    players.getIn([game.get('playerTurn'), 'cards']).find(card => card.get('key') === game.getIn(['currentAction', 'type'])).get('confirm')();
  }

  // we don't count any other type towards action count
  let setFreeAction = () => {};
  if (!game.hasIn(['currentAction', 'type']) || (game.getIn(['currentAction', 'type']) !== 'ADD' && game.getIn(['currentAction', 'type']) !== 'MOVE'
    && game.getIn(['currentAction', 'type']) !== 'CAPTURE' && game.getIn(['currentAction', 'type']) !== 'SPECIAL')) {
    setFreeAction = () => store.dispatch(updateCurrentActionModifier('freeAction', true));
  }

  Promise.resolve(setFreeAction())
    .then(() => store.dispatch(updateCurrentAction({})))
    .then(clearHighlightAndTriggers)
    .then(() => store.dispatch(updateCanConfirm(false)))
    .then(() => {
      const newState = store.getState();
      store.dispatch(finishAction(newState.game, newState.players));
    })
    .then(() => {
      const newState = store.getState();
      return store.dispatch(updateBackup(newState.game, newState.world, newState.players));
    })
    .then(manageCards);
};

const forceNextTurn = () => {
  const { game } = store.getState();
  Promise.resolve(store.dispatch(forceNext(game)))
    .then(manageCards);
};

const useDamageFreeAction = () => {
  const { game, players } = store.getState();

  const activePlayer = players.get(game.get('playerTurn'));
  let damageReq = damageToFreeAction;

  if (activePlayer.get('cards').size > 0) {
    activePlayer.get('cards').forEach((card) => {
      if (card.hasIn(['modifications', 'damageReq'])) {
        damageReq = card.getIn(['modifications', 'damageReq']);
      }
    });
  }

  store.dispatch(updateCurrentActionModifier('freeAction', true));
  store.dispatch(updateDamage(game.get('playerTurn'), (activePlayer.get('damageCounter') - damageReq)));
  store.dispatch(appendPrompt(' You have enabled your free action this turn.'));
};

const manageCards = () => {
  const { game, world, players } = store.getState();

  // First, we shuffle the deck if we are out of cards
  if (game.get('cardDeck').size < 4) {
    store.dispatch(shuffleCardDeck());
  }

  // Evaluate the cards of all the players
  players.forEach((player, playerIndex) => {
    // Get the requirements for the player to hold each card
    let cardCountNeeded = cardRequirements[player.get('score')];

    // Check if the player has a card that lowers their requirements
    if (player.get('cards').size > 0) {
      player.get('cards').forEach((card) => {
        if (card.hasIn(['modifications', 'cardReq'])) {
          cardCountNeeded *= card.getIn(['modifications', 'cardReq']);
        }
      });
    }

    // Find the difference between the number the cards do have and the number they should have
    const cardDifference = cardsShouldHave(world, player, cardCountNeeded) - player.get('cards').size;

    // if the player has too many cards now, need to get rid of some
    if (cardDifference < 0) {
      let newCards = player.get('cards');

      // add top card to the discard pile, and keep updated list of what cards they should keep
      for (let i = 0; i > cardDifference; i -= 1) {
        const discardedCard = newCards.first();
        store.dispatch(discardCard(discardedCard));
        newCards = newCards.shift();
      }

      // update the player's card list with their remaining cards
      store.dispatch(updateCards(playerIndex, newCards));
    }

    // if it is the player's turn, they also have an opportunity to draw cards
    if (playerIndex === game.get('playerTurn')) {
      // if they need more cards, then trigger the buttons to force them to draw
      if (cardDifference > 0) {
        store.dispatch(updateCanDraw(true));
        store.dispatch(updatePrompt('Please draw a card'));
      } else {
        // if they don't need more cards, allow them to pass through and
        // perform a normal action
        store.dispatch(updateCanDraw(false));
        const nextPlayer = players.getIn([game.get('playerTurn'), 'playerName']);

        // Rotate through passive card actions, check if they have been triggered,
        // and trigger them if they haven't.
        // This way, we catch these passive actions at the beginning of turns AND
        // when the card has been drawn the first time.
        player.get('cards').forEach((card) => {
          if (card.hasIn(['records', 'passiveAction']) && card.getIn(['records', 'passiveAction']) !== game.get('totalTurn')) {
            card.get('initAction')();
          }
        });

        store.dispatch(updatePrompt(`${nextPlayer}, please choose an action.`));
      }
    }
  });
};

export { cancelAction, confirmAction, forceNextTurn, useDamageFreeAction, manageCards };
