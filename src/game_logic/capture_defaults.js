import Immutable from 'immutable';
import { allTouchingEnemies, getBordering, getTouching, whichPlayerPiece } from './base_util';
import Boards from '../game_config/boards/boards-enum';
import { captureRequirements, damageToFreeAction } from '../game_config/constants/constants';

const init = (game, world, players) => allTouchingEnemies(game, world, players)
  .filter(enemy => !(players.size > 3 && enemy.get('player') === players.getIn([game.get('playerTurn'), 'lastCaptured'])));

const choose = (game, world, players, board, space) => {
  const activePlayer = players.get(game.get('playerTurn'));
  const opponentIndex = whichPlayerPiece(players, board, space);
  const currentCaptureRequirements = captureRequirements[activePlayer.get('score')];

  const potentialSpaces = getBordering(world, Immutable.fromJS(Boards[board]), space)
    .concat(getTouching(Immutable.fromJS(Boards[board]), space));

  const activePlayerPieces = activePlayer.get('pieces').filter(piece =>
    potentialSpaces.some(potSpace => potSpace.get('board') === piece.get('board') && potSpace.get('space') === piece.get('space'))
  ).map(piece => piece.set('player', game.get('playerTurn')));

  const moves = game.getIn(['currentAction', 'data', 'moves']) || Immutable.List();

  const capReqDiff = capReqModifier(activePlayer, players.get(opponentIndex));

  const ownPieces = hasEnoughOwnPieces(moves, game.get('playerTurn'), (currentCaptureRequirements.own + capReqDiff))
    ? Immutable.List() : activePlayerPieces;

  if (!hasEnoughOtherPieces(moves, game.get('playerTurn'), currentCaptureRequirements.other)) {
    return ownPieces.concat(players.getIn([opponentIndex, 'pieces']).filter(piece =>
      potentialSpaces.some(potSpace => potSpace.get('board') === piece.get('board') && potSpace.get('space') === piece.get('space'))
    ).map(piece => piece.set('player', opponentIndex)));
  }

  return ownPieces;
};

const canCapture = (game, players, activePlayer, targetPlayer) => {
  if (!game.hasIn(['currentAction', 'type']) ||
    game.getIn(['currentAction', 'type']) !== 'CAPTURE' ||
    !game.hasIn(['currentAction', 'data', 'moves'])) {
    return false;
  }

  const currentCaptureRequirements = captureRequirements[players.getIn([game.get('playerTurn'), 'score'])];
  const moves = game.getIn(['currentAction', 'data', 'moves']);

  const capReqDiff = capReqModifier(players.get(activePlayer), players.get(targetPlayer));

  if (!hasEnoughOwnPieces(moves, game.get('playerTurn'), (currentCaptureRequirements.own + capReqDiff))) {
    return false;
  }

  if (!hasEnoughOtherPieces(moves, game.get('playerTurn'), currentCaptureRequirements.other)) {
    return false;
  }

  return true;
};

const capReqModifier = (activePlayer, targetPlayer) => {
  let captureRequirementDifference = 0;

  if (activePlayer.get('cards').size > 0) {
    activePlayer.get('cards').forEach((card) => {
      if (card.hasIn(['modifications', 'captureOther'])) {
        captureRequirementDifference -= card.getIn(['modifications', 'captureOther']);
      }
    });
  }

  if (targetPlayer.get('cards').size > 0) {
    targetPlayer.get('cards').forEach((card) => {
      if (card.hasIn(['modifications', 'captureOwn'])) {
        captureRequirementDifference += card.getIn(['modifications', 'captureOwn']);
      }
    });
  }

  return captureRequirementDifference;
};

const hasEnoughOwnPieces = (moves, playerTurn, captureRequirementsParam) =>
  moves.filter(move => parseInt(move.get('player'), 10) === playerTurn).size >= captureRequirementsParam;

const hasEnoughOtherPieces = (moves, playerTurn, captureRequirementsParam) =>
  moves.filter(move => parseInt(move.get('player'), 10) !== playerTurn).size >= captureRequirementsParam;

const newDamage = (game, players) => {
  const activePlayer = players.get(game.get('playerTurn'));
  const otherCaptureRequirements = captureRequirements[activePlayer.get('score')].other;
  const targetPlayer = game.getIn(['currentAction', 'target', 'player']);
  const newDamageAmt = parseInt(players.getIn([targetPlayer, 'damageCounter']), 10) + otherCaptureRequirements + 1;
  return (newDamageAmt < damageToFreeAction) ? newDamageAmt : damageToFreeAction;
};

const captureDefaults = {
  init,
  choose,
  canCapture,
  newDamage
};

export default captureDefaults;
