import Immutable from 'immutable';
import { getBordering, getCurrentInProgress, isBlocked } from './base_util';

const piecesCanStillMove = (moveLimitParam, game, players) => {
  const playerInfo = players.get(game.get('playerTurn'));
  let moveLimit = moveLimitParam;

  if (playerInfo.get('cards').size > 0) {
    playerInfo.get('cards').forEach((card) => {
      if (card.hasIn(['modifications', 'move'])) {
        moveLimit += card.getIn(['modifications', 'move']);
      }
    });
  }

  if (game.getIn(['currentAction', 'data', 'moves'])
    .filter(move => move.has('from'))
    .size >= moveLimit) {
    return new Immutable.List();
  }

  return playerInfo.get('pieces')
    .filter(piece => piece.get('space') !== 'captured')
    .concat(getCurrentInProgress(game));
};

const init = (game, players) => (
  players.getIn([game.get('playerTurn'), 'pieces'])
    .filter(piece => piece.get('space') !== 'captured')
    .map(piece => Immutable.Map(
      { board: piece.get('board'), space: piece.get('space') }
    ))
);

const available = (game, world, players, board, space) => (
  getBordering(world, board, space)
    .filter(possibleSpace =>
      !isBlocked(
        game,
        players,
        Immutable.Map().set('name', possibleSpace.get('board')),
        possibleSpace.get('space')
      )
    )
);

const blockedAvailable = (game, world, players, board, space) =>
  getBordering(world, board, space)
    .filter(possibleSpace =>
      !isBlocked(
        game,
        Immutable.List([players.get(game.get('playerTurn'))]),
        Immutable.Map().set('name', possibleSpace.get('board')),
        possibleSpace.get('space')
      )
    );

const confirmMove = game => getCurrentInProgress(game).filter(piece => piece.get('player') === game.get('playerTurn'));

const moveDefaults = {
  piecesCanStillMove,
  init,
  available,
  blockedAvailable,
  confirmMove
};

export default moveDefaults;
