import Immutable from 'immutable';
import Boards from '../game_config/boards/boards-enum';
import { addDamage, clearHighlightAndTriggers, getCurrentInProgress, whichPlayerPiece } from './base_util';
import store from '../utils/store';
import { addTriggers, appendCurrentAction, updateCanConfirm, updateCurrentAction, updateHighlighted, updatePrompt } from '../actions/game';
import { updatePieces } from '../actions/players';
import { updateBackup } from '../actions/backup';

const initMove = () => {
  const { game, world, players } = store.getState();
  store.dispatch(updateBackup(game, world, players));
  clearHighlightAndTriggers();
  store.dispatch(updateCurrentAction({ type: 'MOVE', data: {} }));

  const activePlayer = players.get(game.get('playerTurn'));
  const highlightedSpaces = activePlayer.getIn(['role', 'move', 'init'])(game, players);
  store.dispatch(updateHighlighted(highlightedSpaces.map(space => `${space.get('board')}_${space.get('space')}_primary`)));
  let triggers = Immutable.Map();
  highlightedSpaces.forEach((space) => {
    triggers = triggers.set(`${space.get('board')}_${space.get('space')}`, () => choosePieceToMove(`${space.get('board')}_${space.get('space')}`));
  });
  store.dispatch(addTriggers(triggers));
  store.dispatch(updatePrompt('Please choose a piece to move'));
  moveAllPiecesToTemporary();
};

const choosePieceToMove = (originalSpace) => {
  const { game, world, players } = store.getState();
  clearHighlightAndTriggers();
  const spaceArray = originalSpace.split('_');
  const activePlayer = players.get(game.get('playerTurn'));
  const availablePieces = activePlayer.getIn(['role', 'move', 'piecesCanStillMove'])(game, players);

  let highlightOthers = false;

  if (availablePieces.filter(piece => piece.get('board') === spaceArray[0] && piece.get('space') === spaceArray[1]).size > 0) {
    const availableFunction = ((game.hasIn(['currentActionModifiers', 'bluePressure']) && game.getIn(['currentActionModifiers', 'bluePressure'])) ||
      (game.hasIn(['currentActionModifiers', 'orangeDebt']) && game.getIn(['currentActionModifiers', 'orangeDebt']))) ?
      activePlayer.getIn(['role', 'move', 'blockedAvailable']) : activePlayer.getIn(['role', 'move', 'available']);

    const highlightedSpaces = availableFunction(
      game,
      world,
      players,
      Immutable.fromJS(Boards[spaceArray[0]]),
      spaceArray[1]
    );

    const secondaryHighlightedSpaces = availablePieces.filter(piece =>
      !(piece.get('board') === spaceArray[0] && piece.get('space') === spaceArray[1])
    );
    const allHighlightedSpaces = highlightedSpaces.map(space => `${space.get('board')}_${space.get('space')}_primary`)
      .concat(secondaryHighlightedSpaces.map(space => `${space.get('board')}_${space.get('space')}_secondary`));

    store.dispatch(updateHighlighted(allHighlightedSpaces));
    let triggers = Immutable.Map();
    highlightedSpaces.forEach((space) => {
      triggers = triggers.set(`${space.get('board')}_${space.get('space')}`, () => addPotentialMove(originalSpace, `${space.get('board')}_${space.get('space')}`));
    });
    store.dispatch(addTriggers(triggers));
    store.dispatch(updatePrompt('Please choose a space to move this piece, or select a different piece to move.'));
  } else {
    highlightOthers = true;
  }

  const piecesToMove = getCurrentInProgress(game)
    .filter(piece => !(piece.get('board') === spaceArray[0] && piece.get('space') === spaceArray[1]))
    .filter(piece => availablePieces.includes(piece));

  let pieceTriggers = Immutable.Map();
  piecesToMove.forEach((piece) => {
    pieceTriggers = pieceTriggers.set(`${piece.get('board')}_${piece.get('space')}`, () => choosePieceToMove(`${piece.get('board')}_${piece.get('space')}`));
  });
  store.dispatch(addTriggers(pieceTriggers));

  if (highlightOthers) {
    store.dispatch(updateHighlighted(piecesToMove.map(space => `${space.get('board')}_${space.get('space')}_primary`)));
  }
};

const addPotentialMove = (oldSpace, newSpace) => {
  const { game, players } = store.getState();
  const oldSpaceArray = oldSpace.split('_');
  const newSpaceArray = newSpace.split('_');

  const prevOccupier = whichPlayerPiece(players, newSpaceArray[0], newSpaceArray[1]);

  Promise.resolve(
    moveEnemyPlayerToTemp(game, players, prevOccupier, newSpaceArray[0], newSpaceArray[1])
  )
    .then(store.dispatch(appendCurrentAction({
      from: {
        board: oldSpaceArray[0],
        space: oldSpaceArray[1]
      },
      to: {
        board: newSpaceArray[0],
        space: newSpaceArray[1]
      },
      player: game.get('playerTurn')
    })))
  .then(() => clearHighlightAndTriggers())
  .then(() => {
    if (!checkIfOrangeOverlaps()) {
      store.dispatch(updateCanConfirm(true));
    } else {
      store.dispatch(updateCanConfirm(false));
    }
    choosePieceToMove(newSpace);
  });
};

const confirmMove = () => {
  const { game, players } = store.getState();

  if (game.hasIn(['currentActionModifiers', 'bluePressure']) && game.getIn(['currentActionModifiers', 'bluePressure'])) {
    const pieces = getCurrentInProgress(game);

    pieces.filterNot(piece => piece.get('player') === game.get('playerTurn'))
      .countBy(piece => piece.get('player'))
      .forEach((playerDamage, key) => addDamage(key, playerDamage));
  }

  const newPieces = players.getIn([game.get('playerTurn'), 'role', 'move', 'confirmMove'])(game);
  store.dispatch(updatePieces(game.get('playerTurn'), newPieces));
};

const moveEnemyPlayerToTemp = (game, players, prevOccupier, board, space) => {
  if (prevOccupier !== -1 && (!game.hasIn(['currentActionModifiers', 'orangeDebt']) || !game.getIn(['currentActionModifiers', 'orangeDebt']))) {
    const oldPieceIndex = players.getIn([prevOccupier, 'pieces']).findIndex(piece => piece.get('board') === board && piece.get('space') === space);
    return Promise.resolve(store.dispatch(appendCurrentAction({
      to: { board, space },
      player: prevOccupier,
      modifiers: players.getIn([prevOccupier, 'pieces', oldPieceIndex, 'modifiers'])
    })))
    .then(() => store.dispatch(updatePieces(prevOccupier, players.getIn([prevOccupier, 'pieces']).delete(oldPieceIndex))));
  }
  return false;
};

const checkIfOrangeOverlaps = () => {
  const { game, players } = store.getState();
  if (!game.hasIn(['currentActionModifiers', 'orangeDebt']) || !game.getIn(['currentActionModifiers', 'orangeDebt'])) {
    return false;
  }

  const inProcessPieces = getCurrentInProgress(game);

  return players
    .flatMap(player => player.get('pieces'))
    .some(piece => inProcessPieces.some(tPiece => tPiece.get('board') === piece.get('board') && tPiece.get('space') === piece.get('space')));
};

const moveAllPiecesToTemporary = () => {
  const { game, players } = store.getState();
  const activePlayer = players.get(game.get('playerTurn'));
  activePlayer.get('pieces')
    .filter(piece => piece.get('space') !== 'captured')
    .forEach(piece => store.dispatch(appendCurrentAction({
      to: {
        board: piece.get('board'),
        space: piece.get('space')
      },
      player: game.get('playerTurn'),
      modifiers: piece.get('modifiers')
    })));

  store.dispatch(updatePieces(game.get('playerTurn'), activePlayer.get('pieces').filter(piece => piece.get('space') === 'captured')));
};

export { initMove, choosePieceToMove, addPotentialMove, confirmMove };

const moveProcess = { initMove, confirmMove };

export default moveProcess;
