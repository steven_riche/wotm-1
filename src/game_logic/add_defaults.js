import Immutable from 'immutable';
import { getBordering, getCurrentInProgress, isBlocked, isOccupied } from './base_util';
import { pieceLimit } from '../game_config/constants/constants';
import Boards from '../game_config/boards/boards-enum';

/*
  Returns a boolean if current player is able to add another piece
*/
const canAddMore = (addLimitParam, addCheck, game, world, players) => {
  const playerInfo = players.get(game.get('playerTurn'));
  let addLimit = addLimitParam;

  if (playerInfo.get('cards').size > 0) {
    playerInfo.get('cards').forEach((card) => {
      if (card.hasIn(['modifications', 'add'])) {
        addLimit += card.getIn(['modifications', 'add']);
      }
    });
  }

  if (playerInfo.get('pieces').size >= pieceLimit) {
    return false;
  }

  if (game.hasIn(['currentAction', 'data', 'moves'])) {
    if (game.getIn(['currentAction', 'data', 'moves']).filter(move => move.get('player') === game.get('playerTurn')).size >= addLimit) {
      return false;
    }

    if (game.getIn(['currentAction', 'data', 'moves']).filter(move => move.get('player') === game.get('playerTurn')).size
      + playerInfo.get('pieces').size >= pieceLimit) {
      return false;
    }
  }

  if (playerInfo.get('pieces').filter(piece =>
    piece.get('space') !== 'captured'
  ).size === 0) {
    return true;
  }

  return playerInfo.get('pieces')
    .filter(piece =>
      piece.get('space') !== 'captured' &&
      addCheck(game, world, players, Immutable.fromJS(Boards[piece.get('board')]), piece.get('space'))
  ).size > 0;
};

/*
  Returns an Immutable List of pieces on the board that can be added to
*/
const init = (game, players) => {
  const playerInfo = players.get(game.get('playerTurn'));
  const potentialPieces = playerInfo.get('pieces').filter(piece => piece.get('space') !== 'captured');

  return potentialPieces.map(piece => Immutable.Map(
    { board: piece.get('board'), space: piece.get('space') }
  ));
};

/*
  Returns an Immutable List of the blue meditation spaces for the current players' starting room
*/
const initFirstPiece = (game, world, players) => {
  const playerIndex = players.getIn([game.get('playerTurn'), 'player']);
  const startingRoom = world.get('rooms').find(room => room.get('player') === playerIndex);
  let spaces = startingRoom.getIn(['board', 'meditationSpaces'])
  .filter(space => !isOccupied(players, startingRoom.get('board'), space))
  .map(space => Immutable.Map(
    { board: startingRoom.getIn(['board', 'name']), space }
  ));

  if (spaces.size === 0) {
    spaces = startingRoom.getIn(['board', 'type', 'spaces'])
      .filter((spaceInfo, spaceKey) => !isOccupied(players, startingRoom.get('board'), spaceKey))
      .map((spaceInfo, spaceKey) => Immutable.Map(
        { board: startingRoom.getIn(['board', 'name']), space: spaceKey }
      ));
  }

  if (spaces.size === 0) {
    spaces = world.get('rooms').map(room =>
      room.getIn(['board', 'type', 'spaces']).map((spaceInfo, spaceKey) => Immutable.Map(
        { board: room.getIn(['board', 'name']), space: spaceKey }
      ))
    ).flatten()
    .filter(space => !isOccupied(players, Immutable.Map({ name: space.get('board') }), space.get('space')));
  }

  return Immutable.List(spaces);
};

/*
  Returns a list of open bordering spaces
*/
const available = (game, world, players, board, space) =>
  getBordering(world, board, space)
    .filter(possibleSpace => !isBlocked(game, players, Immutable.Map().set('name', possibleSpace.get('board')), possibleSpace.get('space')));

/*
  Returns a list of bordering spaces, assuming that enemy pieces can be destroyed
*/
const blockedAvailable = (game, world, players, board, space) =>
  getBordering(world, board, space)
    .filter(possibleSpace =>
      !isBlocked(
        game,
        Immutable.List([players.get(game.get('playerTurn'))]),
        Immutable.Map().set('name', possibleSpace.get('board')),
        possibleSpace.get('space')
      )
    );

/*
  Returns an updated list of current players pieces after confirming this action
*/
const confirmAdd = (game, players) => {
  const newPieces = getCurrentInProgress(game)
    .filter(piece => piece.get('player') === game.get('playerTurn'))
    .map(piece => piece.set('modifiers', new Immutable.List()));
  return players.getIn([game.get('playerTurn'), 'pieces']).concat(newPieces);
};

const addDefaults = {
  canAddMore,
  init,
  initFirstPiece,
  available,
  blockedAvailable,
  confirmAdd
};

export default addDefaults;
