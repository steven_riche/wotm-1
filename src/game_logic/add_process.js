import Immutable from 'immutable';
import Boards from '../game_config/boards/boards-enum';
import store from '../utils/store';
import { addDamage, clearHighlightAndTriggers, getCurrentInProgress, whichPlayerPiece } from './base_util';
import { addTriggers, appendCurrentAction, updateCanConfirm, updateCurrentAction, updateHighlighted, updatePrompt } from '../actions/game';
import { updatePieces } from '../actions/players';
import { updateBackup } from '../actions/backup';

const initAdd = () => {
  const { game, world, players } = store.getState();
  store.dispatch(updateBackup(game, world, players));
  clearHighlightAndTriggers();
  store.dispatch(updateCurrentAction({ type: 'ADD', data: {} }));
  const activePlayer = players.get(game.get('playerTurn'));

  const highlightedSpaces = activePlayer.getIn(['role', 'add', 'init'])(game, players);

  store.dispatch(updateHighlighted(highlightedSpaces.map(space => `${space.get('board')}_${space.get('space')}_primary`)));

  let triggers = Immutable.Map();
  highlightedSpaces.forEach((space) => {
    triggers = triggers.set(`${space.get('board')}_${space.get('space')}`, () => addOrigin(`${space.get('board')}_${space.get('space')}`));
  });
  store.dispatch(addTriggers(triggers));

  store.dispatch(updatePrompt('Please choose a space to add pieces around'));
};

const addOrigin = (space) => {
  const { game, world, players } = store.getState();
  clearHighlightAndTriggers();
  const spaceArray = space.split('_');
  const activePlayer = players.get(game.get('playerTurn'));
  const availableFunction = ((game.hasIn(['currentActionModifiers', 'bluePressure']) && game.getIn(['currentActionModifiers', 'bluePressure'])) ||
    (game.hasIn(['currentActionModifiers', 'orangeDebt']) && game.getIn(['currentActionModifiers', 'orangeDebt']))) ?
    activePlayer.getIn(['role', 'add', 'blockedAvailable']) : activePlayer.getIn(['role', 'add', 'available']);
  const highlightedSpaces = availableFunction(
    game,
    world,
    players,
    Immutable.fromJS(Boards[spaceArray[0]]),
    spaceArray[1]
  );

  store.dispatch(updateHighlighted(highlightedSpaces.map(hSpace => `${hSpace.get('board')}_${hSpace.get('space')}_primary`)));
  let triggers = Immutable.Map();
  highlightedSpaces.forEach((hSpace) => {
    triggers = triggers.set(`${hSpace.get('board')}_${hSpace.get('space')}`, () => addPiece(`${hSpace.get('board')}_${hSpace.get('space')}`));
  });
  store.dispatch(addTriggers(triggers));
  store.dispatch(updatePrompt('Please choose available spaces to add new pieces'));
};

const addPiece = (space) => {
  const spaceArray = space.split('_');
  const { game, players } = store.getState();

  const prevOccupier = whichPlayerPiece(players, spaceArray[0], spaceArray[1]);

  Promise.resolve(moveEnemyPlayerToTemp(game, players, prevOccupier, spaceArray[0], spaceArray[1]))
    .then(store.dispatch(appendCurrentAction({
      to: {
        board: spaceArray[0],
        space: spaceArray[1]
      },
      player: game.get('playerTurn'),
      modifiers: Immutable.List()
    })))
    .then(() => store.dispatch(updateCanConfirm(true)))
    .then(() => {
      const newState = store.getState();
      if (!players.getIn([game.get('playerTurn'), 'role', 'add', 'canAddMore'])(newState.game, newState.world, newState.players)) {
        clearHighlightAndTriggers();
        store.dispatch(updatePrompt('Please cancel or confirm your selection'));
      }
    });
};

const moveEnemyPlayerToTemp = (game, players, prevOccupier, board, space) => {
  if (prevOccupier !== -1) {
    const oldPieceIndex = players.getIn([prevOccupier, 'pieces']).findIndex(piece => piece.get('board') === board && piece.get('space') === space);
    return Promise.resolve(store.dispatch(appendCurrentAction({
      to: { board, space },
      player: prevOccupier,
      modifiers: players.getIn([prevOccupier, 'pieces', oldPieceIndex, 'modifiers'])
    })))
    .then(() => store.dispatch(updatePieces(prevOccupier, players.getIn([prevOccupier, 'pieces']).delete(oldPieceIndex))));
  }
  return false;
};

const initAddFirstPiece = () => {
  const { game, world, players } = store.getState();
  clearHighlightAndTriggers();
  store.dispatch(updateCurrentAction({ type: 'ADD', data: {} }));
  const activePlayer = players.get(game.get('playerTurn'));

  const highlightedSpaces = activePlayer.getIn(['role', 'add', 'initFirstPiece'])(game, world, players);

  store.dispatch(updateHighlighted(highlightedSpaces.map(space => `${space.get('board')}_${space.get('space')}_primary`)));
  let triggers = Immutable.Map();
  highlightedSpaces.forEach((space) => {
    triggers = triggers.set(`${space.get('board')}_${space.get('space')}`, () => addStartingSpace(`${space.get('board')}_${space.get('space')}`));
  });
  store.dispatch(addTriggers(triggers));
  store.dispatch(updatePrompt('Please choose a starting space to add your first piece'));
};

const addStartingSpace = (space) => {
  const { game } = store.getState();
  clearHighlightAndTriggers();
  const spaceArray = space.split('_');

  store.dispatch(appendCurrentAction({
    to: {
      board: spaceArray[0],
      space: spaceArray[1]
    },
    player: game.get('playerTurn'),
    modifiers: Immutable.List()
  }));

  store.dispatch(updateCanConfirm(true));
  store.dispatch(updatePrompt('Please cancel or confirm your selection'));
};

const confirmAdd = () => {
  const { game, players } = store.getState();

  if ((game.hasIn(['currentActionModifiers', 'bluePressure']) && game.getIn(['currentActionModifiers', 'bluePressure']))) {
    const pieces = getCurrentInProgress(game);

    pieces.filterNot(piece => piece.get('player') === game.get('playerTurn'))
      .countBy(piece => piece.get('player'))
      .forEach((playerDamage, key) => addDamage(key, playerDamage));
  }

  const newPieces = players.getIn([game.get('playerTurn'), 'role', 'add', 'confirmAdd'])(game, players);
  store.dispatch(updatePieces(game.get('playerTurn'), newPieces));
};

const defaultObject = { initAdd, initAddFirstPiece, confirmAdd };

export default defaultObject;

export { initAdd, addOrigin, addPiece, initAddFirstPiece, addStartingSpace, confirmAdd };
