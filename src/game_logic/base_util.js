import Immutable from 'immutable';
import UUID from 'uuidjs';
import store from '../utils/store';
import Boards from '../game_config/boards/boards-enum';
import { damageToFreeAction } from '../game_config/constants/constants';
import { updateHighlighted, clearTriggers } from '../actions/game';
import { updateDamage } from '../actions/players';

/*
 * Triggers adding damage the the player in question, making sure they do not go over the max damage
 */
const addDamage = (player, damage) => {
  const { players } = store.getState();
  const currentDamage = players.getIn([player, 'damageCounter']);
  const newDamage = ((currentDamage + damage) < damageToFreeAction) ?
    (currentDamage + damage)
    : damageToFreeAction;
  store.dispatch(updateDamage(player, newDamage));
};

/*
Returns list of all spaces touching the current player that contain an enemy player
*/
const allTouchingEnemies = (game, world, players) => {
  const activePlayer = players.get(game.get('playerTurn'));
  const enemyPlayers = players.delete(game.get('playerTurn'));

  return activePlayer.get('pieces')
    .filter(piece => piece.get('space') !== 'captured')
    .reduce((a, b) => {
      const board = Immutable.fromJS(Boards[b.get('board')]);
      return a.concat(getBordering(world, board, b.get('space')).concat(getTouching(board, b.get('space'))));
    },
    Immutable.List()
  )
  .groupBy(i => `${i.get('board')}_${i.get('space')}`)
  .map(i => i.first())
  .toList()
  .filter(space => isOccupied(enemyPlayers, Immutable.Map({ name: space.get('board') }), space.get('space')))
  .map(enemy => Immutable.Map({
    board: enemy.get('board'),
    space: enemy.get('space'),
    player: whichPlayerPiece(players, enemy.get('board'), enemy.get('space'))
  }));
};

/*
  Function that returns true if the user can make an additional action
*/
const canTakeAdditionalAction = (game, players) => {
  if (game.hasIn(['currentActionModifiers', 'finalAction']) && game.getIn(['currentActionModifiers', 'finalAction'])) {
    return false;
  }

  return players.getIn([game.get('playerTurn'), 'damageCounter']) >= damageToFreeAction ||
  players.getIn([game.get('playerTurn'), 'cards'])
    .some(card =>
      card.hasIn(['records', 'activeAction']) && card.getIn(['records', 'activeAction']) !== game.get('totalTurn')
    );
};

/*
  Returns number of cards the given player should have given the requirement
  (checks pieces per board)
*/
const cardsShouldHave = (world, player, requirement) => {
  const piecesByBoard = player.get('pieces')
    .filter(piece => piece.get('space') !== 'captured')
    .groupBy(piece => piece.get('board'));

  const medSpaceCounts = piecesByBoard.map(boardPieces =>
    boardPieces.filter((piece) => {
      const room = world.get('rooms').find(eachRoom => eachRoom.getIn(['board', 'name']) === piece.get('board'));
      return room.getIn(['board', 'meditationSpaces']).includes(piece.get('space'));
    })
  .size);

  return medSpaceCounts.filter(roomCount => roomCount >= requirement).size;
};

/*
  Function that clears out all highlighted spaces and all spaces with triggers
*/
const clearHighlightAndTriggers = () => {
  store.dispatch(updateHighlighted([]));
  store.dispatch(clearTriggers());
};

/*
 * Generates a uuid
 */
const generateUUID = () => UUID.generate();

/*
Returns total list of spaces bordering the given space.
Returns an array of objects that give both space and board.
Concatenation of bordering spaces in room and in neighboring room
*/
const getBordering = (world, board, space) => (
  getBorderingSameRoom(board, space).concat(getBorderingNeighbors(world, board, space))
);

/*
Returns a list of spaces that might be bordering the given space from another
room.
*/
const getBorderingNeighbors = (world, board, space) => {
  const doorSide = isDoorSpace(board, space);

  /*
  If this space is not a door, then we don't have to worry about anything
  */
  if (!doorSide) {
    return [];
  }

  /*
  Cycle through cardinal directions to find the index of the bordering room,
  taking the current room's rotation into account.
  */
  const sides = ['n', 'e', 's', 'w'];
  const currentBoardIndex = world.get('rooms').findIndex(room => room.getIn(['board', 'name']) === board.get('name'));
  const neighborIndex = (sides.indexOf(doorSide) + (world.getIn(['rooms', currentBoardIndex, 'rotation']) / 90)) % 4;

  const neighborRoomIndex = world.getIn(['layout', currentBoardIndex, 'neighbors', sides[neighborIndex]]);
  const neighborRoom = world.getIn(['rooms', neighborRoomIndex]);

  const neighborDoorSideIndex = ((4 + (neighborIndex + 2)) - (neighborRoom.get('rotation') / 90)) % 4;

  /*
  Return an array of the neighboring room's door spaces on its matching side
  */
  return neighborRoom.getIn(['board', 'type', 'doors', sides[neighborDoorSideIndex]]).map(nSpace => Immutable.Map(
    { board: neighborRoom.getIn(['board', 'name']), space: nSpace }
  ));
};

/*
Returns a list of spaces in the same room bordering the given space.
*/
const getBorderingSameRoom = (board, space) =>
  board.getIn(['type', 'spaces', space, 'bordering']).map(borderingSpace => Immutable.Map(
    { board: board.get('name'), space: borderingSpace }
  ));

/*
  Loop through moves in current action, and return list of final pieces in state
*/
const getCurrentInProgress = (game) => {
  let pieces = new Immutable.List();

  if (game.hasIn(['currentAction', 'data', 'moves'])) {
    game.getIn(['currentAction', 'data', 'moves']).forEach((move) => {
      let modifiers = move.has('modifiers') ? move.get('modifiers') : Immutable.List();

      if (move.has('from')) {
        const fromIndex = pieces.findIndex(piece => piece.get('board') === move.getIn(['from', 'board']) && piece.get('space') === move.getIn(['from', 'space']) && piece.get('player') === move.get('player'));
        if (fromIndex !== -1) {
          modifiers = pieces.getIn([fromIndex, 'modifiers']);
          pieces = pieces.splice(fromIndex, 1);
        }
      }

      if (move.has('to')) {
        const newPiece = move.get('to').withMutations(piece => piece.set('player', move.get('player')).set('modifiers', modifiers));
        pieces = pieces.push(newPiece);
      }
    });
  }

  return pieces;
};

/*
  Loop through moves in current action, and returns Immutable list of where
  those pieces were originally
*/
const getCurrentInProgressOriginalPosition = (game) => {
  let pieces = new Immutable.List();

  if (game.hasIn(['currentAction', 'data', 'moves'])) {
    game.getIn(['currentAction', 'data', 'moves']).reverse().forEach((move) => {
      if (!move.has('from')) {
        pieces = pieces.push(move.get('to').withMutations(piece => piece.set('player', move.get('player')).set('modifiers', move.get('modifiers'))));
      } else {
        const changingIndex = pieces.findIndex(piece =>
          piece.get('board') === move.getIn(['to', 'board']) &&
          piece.get('space') === move.getIn(['to', 'space']) &&
          piece.get('player') === move.get('player')
        );

        if (changingIndex !== -1) {
          pieces.get(changingIndex).withMutations(piece =>
            piece.set('board', move.getIn(['from', 'board']))
              .set('space', move.getIn(['from', 'space']))
              .set('player', move.get('player'))
              .set('modifiers', move.get('modifiers'))
          );
        }
      }
    });
  }

  return pieces;
};

/*
Returns a list of spaces in the same room touching the given space.
*/
const getTouching = (board, space) =>
  board.getIn(['type', 'spaces', space, 'touching']).map(touchingSpace => Immutable.Map(
    { board: board.get('name'), space: touchingSpace }
  ));

/*
Returns boolean if space is currently blocked
*/
const isBlocked = (game, players, board, space) => {
  /*
  First filter through all current pieces on the board to see if one is on that
  space. Will return the number found
  */
  const blocked = isOccupied(players, board, space);

  /*
  If there is a pending add or move action, we need to check if one of those
  is occupying the space as well (or has just left the space)
  */
  return updateOccupiedInProgress(game, board, space, blocked);
};

/*
Returns a boolean of if the given space is a door space in the given room
*/
const isDoorSpace = (board, space) => (
  board.getIn(['type', 'doors'])
    .keySeq()
    .filter(doorKey => board.getIn(['type', 'doors', doorKey]).includes(space))
    .first()
);

/*
Returns boolean if space is currently occupied
*/
const isOccupied = (players, board, space) =>
  players.some(player =>
    player.get('pieces').filter(piece =>
      piece.get('board') === board.get('name') &&
      piece.get('space') === space
    ).size > 0
  );

/*
Boolean that returns true if two spaces are considered touching
*/
const isTouching = (world, board1, space1, board2, space2) => {
  const allTouchingSpaces = getBordering(world, board1, space1).concat(getTouching(board1, space1));

  return allTouchingSpaces.filter(targetSpace => targetSpace.get('board') === board2.get('name') && targetSpace.get('space') === space2).size > 0;
};

/*
Returns boolean if space is being used in current action
*/
const updateOccupiedInProgress = (game, board, space, blocked) => {
  let isBlockedNow = blocked;

  if (game.hasIn(['currentAction', 'data', 'moves'])) {
    game.getIn(['currentAction', 'data', 'moves']).forEach((move) => {
      /*
      For every piece we move from that space, we set to false
      */
      if (move.has('from') && move.getIn(['from', 'board']) === board.get('name') &&
        move.getIn(['from', 'space']) === space) {
        isBlockedNow = false;
      }

      /*
      For every piece we move to that space, we set to true. Since we are going
      in order, this allows us to account for pieces moving off and on the same
      space
      */
      if (move.getIn(['to', 'board']) === board.get('name') &&
        move.getIn(['to', 'space']) === space) {
        isBlockedNow = true;
      }
    });
  }

  return isBlockedNow;
};

/*
 Function that returns player index for the piece on the given board and space
*/
const whichPlayerPiece = (players, board, space) => (
  players.findIndex(player =>
    player.get('pieces').filter(piece =>
      piece.get('board') === board && piece.get('space') === space
    ).size > 0
  )
);

/*
  Variant of whichPlayerPiece function that searches in progress pieces as well
  if no current pieces are found
*/
const whichPlayerPieceTemp = (game, players, board, space) => {
  const livePiece = whichPlayerPiece(players, board, space);

  if (livePiece !== -1) {
    return livePiece;
  }

  return game.getIn(['currentAction', 'data', 'moves'])
    .findLast(move => move.getIn(['to', 'board']) === board && move.getIn(['to', 'space']) === space)
    .get('player');
};

export {
  addDamage,
  allTouchingEnemies,
  canTakeAdditionalAction,
  cardsShouldHave,
  clearHighlightAndTriggers,
  generateUUID,
  getBordering,
  getBorderingNeighbors,
  getBorderingSameRoom,
  getCurrentInProgress,
  getCurrentInProgressOriginalPosition,
  getTouching,
  isBlocked,
  isDoorSpace,
  isOccupied,
  isTouching,
  whichPlayerPiece,
  whichPlayerPieceTemp
};
