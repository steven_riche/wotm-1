import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from './utils/store';
import MainComponent from './components/main';
import registerServiceWorker from './registerServiceWorker';
import './index.css';

ReactDOM.render(
  (<Provider store={store}>
    <MainComponent />
  </Provider>), document.getElementById('root'));
registerServiceWorker();
