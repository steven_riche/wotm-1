const capturePlus2 = {
  title: 'Capture Own + 2',
  description: 'Enemies must use 2 extra pieces to capture one of your pieces',
  initAction: () => {},
  activateButton: null,
  modifications: {
    captureOwn: 2
  }
};

export default capturePlus2;
