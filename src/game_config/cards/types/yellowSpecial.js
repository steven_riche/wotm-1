import store from '../../../utils/store';
import { yellowSpecialEntryPoint, yellowSpecialConfirm } from '../../players/yellow';
import { updateCurrentAction } from '../../../actions/game';
import { updateCardRecords } from '../../../actions/players';
import { updateBackup } from '../../../actions/backup';

const init = (id) => {
  const { game, world, players } = store.getState();
  store.dispatch(updateBackup(game, world, players));
  store.dispatch(updateCurrentAction({ type: `CARD_YELLOW_SPECIAL_${id}`, data: {} }));
  yellowSpecialEntryPoint();
};

const confirm = (id) => {
  const { game } = store.getState();
  yellowSpecialConfirm();
  store.dispatch(updateCardRecords(game.get('playerTurn'), `CARD_YELLOW_SPECIAL_${id}`, 'activeAction', game.get('totalTurn')));
};

const checkIfDisabled = (id) => {
  const { game, players } = store.getState();
  const lastUsedTurn = players.getIn([game.get('playerTurn'), 'cards']).find(card => card.get('key') === `CARD_YELLOW_SPECIAL_${id}`).getIn(['records', 'activeAction']);
  return lastUsedTurn === game.get('totalTurn');
};

const yellowSpecial = id => ({
  title: 'Free \'First Voltaic Church\' Special each turn',
  description: 'Once per turn, you may move one of your pieces (or an enemy touching one of your pieces) to an open space bordering a matching piece in the same room',
  initAction: () => {},
  activateButton: () => init(id),
  activateButtonDisabled: () => checkIfDisabled(id),
  activateButtonText: 'Move Piece',
  key: `CARD_YELLOW_SPECIAL_${id}`,
  confirm: () => confirm(id),
  modifications: {},
  records: {
    activeAction: null
  }
});

export default yellowSpecial;

export { init, confirm, checkIfDisabled };
