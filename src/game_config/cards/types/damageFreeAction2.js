const damageFreeAction2 = {
  title: 'Gain Free Actions for 2 damage',
  description: 'While holding this card, you only need 2 damage to take a free action. You may still hold up to 5 damage in your counter',
  initAction: () => {},
  activateButton: null,
  modifications: {
    damageReq: 2
  }
};

export default damageFreeAction2;
