import Immutable from 'immutable';
import store from '../../../utils/store';
import Boards from '../../boards/boards-enum';
import { whiteSpecialEntryPoint, whiteSpecialConfirm } from '../../players/white';
import { getBordering, isOccupied } from '../../../game_logic/base_util';
import { updateCurrentAction } from '../../../actions/game';
import { updateCardRecords } from '../../../actions/players';
import { updateBackup } from '../../../actions/backup';

const findAvailablePieces = (epicenter) => {
  const { world, players } = store.getState();
  const epicenterArray = epicenter.split('_');
  return getBordering(world, Immutable.fromJS(Boards[epicenterArray[0]]), epicenterArray[1])
    .map(firstSpace =>
      getBordering(world, Immutable.fromJS(Boards[firstSpace.get('board')]), firstSpace.get('space'))
        .push(firstSpace)
    )
    .first()
    .groupBy(space => `${space.get('board')}_${space.get('space')}`)
    .map(groupedSpaces => groupedSpaces.first())
    .filter(space => !(space.get('board') === epicenterArray[0] && space.get('space') === epicenterArray[1]))
    .filter(space => isOccupied(players, Immutable.fromJS(Boards[space.get('board')]), space.get('space')))
    .toList();
};

const init = (id) => {
  const { game, world, players } = store.getState();
  store.dispatch(updateBackup(game, world, players));
  store.dispatch(updateCurrentAction({ type: `CARD_WHITE_SPECIAL_${id}`, data: {} }));
  whiteSpecialEntryPoint({
    availPieceFunc: findAvailablePieces,
    gale_move_limit: 2
  });
};

const confirm = (id) => {
  const { game } = store.getState();
  store.dispatch(updateCardRecords(game.get('playerTurn'), `CARD_WHITE_SPECIAL_${id}`, 'activeAction', game.get('totalTurn')));
  whiteSpecialConfirm();
};

const checkIfDisabled = (id) => {
  const { game, players } = store.getState();
  const lastUsedTurn = players.getIn([game.get('playerTurn'), 'cards']).find(card => card.get('key') === `CARD_WHITE_SPECIAL_${id}`).getIn(['records', 'activeAction']);
  return lastUsedTurn === game.get('totalTurn');
};

const whiteSpecial = id => ({
  title: 'Free \'Cult of Boreas\' Special each turn',
  description: 'Once per turn, you may use a free action to choose one of your pieces, and move all pieces within 2 spaces up to two spaces in any direction',
  initAction: () => {},
  activateButton: () => init(id),
  activateButtonDisabled: () => checkIfDisabled(id),
  activateButtonText: 'Initiate Gale Force Winds',
  key: `CARD_WHITE_SPECIAL_${id}`,
  confirm: () => confirm(id),
  modifications: {},
  records: {
    activeAction: null
  }
});

export default whiteSpecial;

export { findAvailablePieces, init, confirm, checkIfDisabled };
