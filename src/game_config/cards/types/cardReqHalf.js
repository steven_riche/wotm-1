const cardReqHalf = {
  title: 'Reduce Card Requirement by Half',
  description: 'While holding this card, you have half the requirement (rounded up) to draw and retain cards',
  initAction: () => {},
  activateButton: null,
  modifications: {
    cardReq: 0.5
  }
};

export default cardReqHalf;
