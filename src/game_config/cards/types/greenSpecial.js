import store from '../../../utils/store';
import { greenSpecialEntryPoint, greenSpecialConfirm } from '../../players/green';
import { updateCanConfirm, updateCurrentAction } from '../../../actions/game';
import { modifyCard, updateCardRecords } from '../../../actions/players';
import { updateBackup } from '../../../actions/backup';

const triggerIncrease = (id, type) => {
  const { game, players } = store.getState();
  const lastStat = players.getIn([game.get('playerTurn'), 'cards']).find(card => card.get('key') === `CARD_GREEN_SPECIAL_${id}`).getIn(['modifications', type]);
  return Promise.resolve(store.dispatch(modifyCard(game.get('playerTurn'), `CARD_GREEN_SPECIAL_${id}`, type, (lastStat + 1))))
    .then(() => store.dispatch(updateCanConfirm(true)));
};

const init = (id, type) => {
  const { game, world, players } = store.getState();
  store.dispatch(updateBackup(game, world, players));
  store.dispatch(updateCurrentAction({ type: `CARD_GREEN_SPECIAL_${id}`, data: {} }));
  greenSpecialEntryPoint({ triggerIncrease: () => triggerIncrease(id, type) });
};

const confirm = (id) => {
  const { game } = store.getState();
  greenSpecialConfirm();
  store.dispatch(updateCardRecords(game.get('playerTurn'), `CARD_GREEN_SPECIAL_${id}`, 'activeAction', game.get('totalTurn')));
};

const checkIfDisabled = (id) => {
  const { game, players } = store.getState();
  const lastUsedTurn = players.getIn([game.get('playerTurn'), 'cards']).find(card => card.get('key') === `CARD_GREEN_SPECIAL_${id}`).getIn(['records', 'activeAction']);
  return lastUsedTurn === game.get('totalTurn') || (game.hasIn(['currentAction', 'type']) && game.getIn(['currentAction', 'type']).indexOf('CARD_GREEN_SPECIAL') !== -1);
};

const greenSpecial = id => ({
  title: 'Free \'Kundi Mbegu\' Special each turn',
  description: 'Once per turn, you may eat a piece touching one of yours to boost one of your stats while you own this card',
  initAction: () => {},
  activateButton: () => init(id, 'add'),
  activateButtonDisabled: () => checkIfDisabled(id),
  activateButtonText: 'Eat Piece To Increase Add',
  activateButton2: () => init(id, 'move'),
  activateButtonText2: 'Eat Piece To Increase Move',
  key: `CARD_GREEN_SPECIAL_${id}`,
  confirm: () => confirm(id),
  modifications: {
    add: 0,
    move: 0
  },
  records: {
    activeAction: null
  }
});

export default greenSpecial;

export { triggerIncrease, init, confirm, checkIfDisabled };
