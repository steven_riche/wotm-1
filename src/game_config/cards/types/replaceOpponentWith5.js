import Immutable from 'immutable';
import store from '../../../utils/store';
import { damageToFreeAction } from '../../constants/constants';
import { clearHighlightAndTriggers, whichPlayerPiece } from '../../../game_logic/base_util';
import { addTriggers, appendCurrentAction, updateCanConfirm, updateCurrentAction, updateHighlighted, updatePrompt } from '../../../actions/game';
import { updateCardRecords, updateDamage, updatePieces } from '../../../actions/players';
import { updateBackup } from '../../../actions/backup';

const init = (id) => {
  const { game, world, players } = store.getState();
  store.dispatch(updateBackup(game, world, players));
  clearHighlightAndTriggers();
  store.dispatch(updateCurrentAction({ type: `CARD_REPLACE_OPPONENT_${id}`, data: {} }));

  const potentialTargets = players
    .remove(game.get('playerTurn'))
    .filter(player => player.get('pieces').filter(piece => piece.get('space') !== 'captured').size >= 5)
    .reduce((a, b) => a.concat(b.get('pieces').filter(piece => piece.get('space') !== 'captured')), Immutable.List());

  store.dispatch(updateHighlighted(potentialTargets.map(piece => `${piece.get('board')}_${piece.get('space')}_primary`)));

  let triggers = Immutable.Map();
  potentialTargets.forEach((space) => {
    triggers = triggers.set(`${space.get('board')}_${space.get('space')}`, () => choosePiece(`${space.get('board')}_${space.get('space')}`));
  });
  store.dispatch(addTriggers(triggers));
  store.dispatch(updatePrompt('Please choose a piece to replace'));
};

const choosePiece = (space) => {
  const { game, players } = store.getState();
  clearHighlightAndTriggers();
  const spaceArray = space.split('_');
  const oldPlayerIndex = whichPlayerPiece(players, spaceArray[0], spaceArray[1]);
  const oldModifiers = players.getIn([oldPlayerIndex, 'pieces'])
    .filter(piece => piece.get('board') === spaceArray[0] && piece.get('space') === spaceArray[1])
    .first()
    .get('modifiers');

  Promise.resolve(store.dispatch(updateHighlighted(Immutable.List([`${space}_primary`]))))
    .then(store.dispatch(appendCurrentAction({
      to: {
        board: spaceArray[0],
        space: spaceArray[1]
      },
      player: game.get('playerTurn'),
      formerPlayer: oldPlayerIndex,
      modifiers: oldModifiers
    })))
    .then(store.dispatch(updatePieces(oldPlayerIndex, players.getIn([oldPlayerIndex, 'pieces'])
      .filter(piece => !(piece.get('board') === spaceArray[0] && piece.get('space') === spaceArray[1])))))
    .then(store.dispatch(updateCanConfirm(true)))
    .then(store.dispatch(updatePrompt('Please cancel or confirm your selection')));
};

const checkIfDisabled = (id) => {
  const { game, players } = store.getState();
  const lastUsedTurn = players.getIn([game.get('playerTurn'), 'cards']).find(card => card.get('key') === `CARD_REPLACE_OPPONENT_${id}`).getIn(['records', 'activeAction']);
  return lastUsedTurn === game.get('totalTurn');
};

const confirm = (id) => {
  const { game, players } = store.getState();
  const tempPiece = game.getIn(['currentAction', 'data', 'moves', 0]);
  const targetPlayer = tempPiece.get('formerPlayer');
  let newDamage = parseInt(players.getIn([targetPlayer, 'damageCounter']), 10) + 1;
  newDamage = (newDamage < damageToFreeAction) ? newDamage : damageToFreeAction;

  store.dispatch(updateDamage(targetPlayer, newDamage));

  store.dispatch(updateCardRecords(game.get('playerTurn'), `CARD_REPLACE_OPPONENT_${id}`, 'activeAction', game.get('totalTurn')));

  const newPiece = Immutable.fromJS({
    board: tempPiece.getIn(['to', 'board']),
    space: tempPiece.getIn(['to', 'space']),
    modifiers: Immutable.List()
  });

  store.dispatch(updatePieces(game.get('playerTurn'), players.getIn([game.get('playerTurn'), 'pieces']).push(newPiece)));
};

const replaceOpponentWith5 = id => ({
  title: 'Replace an Opponent\'s Piece',
  description: 'Once per turn, for a free action, you may choose an opponent with more than five pieces on the board and change one of their pieces to yours',
  initAction: () => {},
  activateButton: () => init(id),
  activateButtonDisabled: () => checkIfDisabled(id),
  activateButtonText: 'Replace Opponent Piece',
  key: `CARD_REPLACE_OPPONENT_${id}`,
  confirm: () => confirm(id),
  modifications: {},
  records: {
    activeAction: null
  }
});

export default replaceOpponentWith5;

export { init, choosePiece, checkIfDisabled, confirm };
