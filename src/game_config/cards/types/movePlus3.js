const movePlus3 = {
  title: 'Move + 3 Spaces',
  description: 'Increase the number of spaces you can move on a move action by 3',
  initAction: () => {},
  activateButton: null,
  modifications: {
    move: 3
  }
};

export default movePlus3;
