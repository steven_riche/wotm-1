import store from '../../../utils/store';
import { updateCurrentActionModifier } from '../../../actions/game';
import { modifyCard, updateCardRecords } from '../../../actions/players';
import { updateBackup } from '../../../actions/backup';

const initOnTurn = (id) => {
  const { game, players } = store.getState();
  const currentBalance = players.getIn([game.get('playerTurn'), 'cards']).find(card => card.get('key') === `CARD_ORANGE_SPECIAL_${id}`).getIn(['modifications', 'orangeBalance']);
  store.dispatch(modifyCard(game.get('playerTurn'), `CARD_ORANGE_SPECIAL_${id}`, 'orangeBalance', (parseInt(currentBalance, 10) + 1)));
  store.dispatch(updateCardRecords(game.get('playerTurn'), `CARD_ORANGE_SPECIAL_${id}`, 'passiveAction', game.get('totalTurn')));
};

const checkIfDisabled = (id) => {
  const { game, players } = store.getState();
  const currentBalance = players.getIn([game.get('playerTurn'), 'cards']).find(card => card.get('key') === `CARD_ORANGE_SPECIAL_${id}`).getIn(['modifications', 'orangeBalance']);
  return currentBalance < 2;
};

const init = (id) => {
  const { game, world, players } = store.getState();
  store.dispatch(updateBackup(game, world, players));
  store.dispatch(updateCurrentActionModifier('freeAction', true));
  const currentBalance = players.getIn([game.get('playerTurn'), 'cards']).find(card => card.get('key') === `CARD_ORANGE_SPECIAL_${id}`).getIn(['modifications', 'orangeBalance']);
  store.dispatch(modifyCard(game.get('playerTurn'), `CARD_ORANGE_SPECIAL_${id}`, 'orangeBalance', (parseInt(currentBalance, 10) - 2)));
};

const orangeSpecial = id => ({
  title: 'Free \'Societe Pendulaire\' Special each turn',
  description: 'Once per turn, you will automatically gain one point towards extra actions. Spend two points to take an extra action',
  initAction: () => initOnTurn(id),
  activateButton: () => init(id),
  activateButtonDisabled: () => checkIfDisabled(id),
  activateButtonText: 'Use Action',
  key: `CARD_ORANGE_SPECIAL_${id}`,
  confirm: () => {},
  modifications: {
    orangeBalance: 0
  },
  records: {
    passiveAction: null
  }
});

export default orangeSpecial;

export { initOnTurn, checkIfDisabled, init };
