const addPlus2 = {
  title: 'Add + 2 pieces',
  description: 'Increase the number of pieces you can add on an add action by 2',
  initAction: () => {},
  activateButton: null,
  modifications: {
    add: 2
  }
};

export default addPlus2;
