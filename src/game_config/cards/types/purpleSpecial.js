import store from '../../../utils/store';
import { purpleSpecialEntryPoint, purpleSpecialConfirm } from '../../players/purple';
import { updateCurrentAction } from '../../../actions/game';
import { updateCardRecords } from '../../../actions/players';
import { updateBackup } from '../../../actions/backup';

const init = (id) => {
  const { game, world, players } = store.getState();
  store.dispatch(updateBackup(game, world, players));
  store.dispatch(updateCurrentAction({ type: `CARD_PURPLE_SPECIAL_${id}`, data: {} }));
  purpleSpecialEntryPoint();
};

const confirm = (id) => {
  const { game } = store.getState();
  store.dispatch(updateCardRecords(game.get('playerTurn'), `CARD_PURPLE_SPECIAL_${id}`, 'activeAction', game.get('totalTurn')));
  purpleSpecialConfirm();
};

const checkIfDisabled = (id) => {
  const { game, players } = store.getState();
  const lastUsedTurn = players.getIn([game.get('playerTurn'), 'cards']).find(card => card.get('key') === `CARD_PURPLE_SPECIAL_${id}`).getIn(['records', 'activeAction']);
  return lastUsedTurn === game.get('totalTurn');
};

const purpleSpecial = id => ({
  title: 'Free \'Madrasa Qashani\' Special each turn',
  description: 'Each turn, you can get a free special to teleport away an enemy piece touching one of your pieces to some other open space',
  initAction: () => {},
  activateButton: () => init(id),
  activateButtonDisabled: () => checkIfDisabled(id),
  activateButtonText: 'Teleport Opponent Piece',
  key: `CARD_PURPLE_SPECIAL_${id}`,
  confirm: () => confirm(id),
  modifications: {},
  records: {
    activeAction: null
  }
});

export default purpleSpecial;

export { init, confirm, checkIfDisabled };
