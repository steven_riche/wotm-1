const captureMinus1 = {
  title: 'Capture Opponents - 1',
  description: 'Decrease the number of your own pieces needed to capture an opponent by 1 (must have at least 1)',
  initAction: () => {},
  activateButton: null,
  modifications: {
    captureOther: 1
  }
};

export default captureMinus1;
