import Immutable from 'immutable';
import store from '../../../utils/store';
import { clearHighlightAndTriggers, getCurrentInProgress, isOccupied } from '../../../game_logic/base_util';
import { addTriggers, appendCurrentAction, updateCanConfirm, updateCurrentAction, updateCurrentActionModifier, updateHighlighted, updatePrompt } from '../../../actions/game';
import { updateCardRecords, updatePieces } from '../../../actions/players';
import { updateBackup } from '../../../actions/backup';

const init = (id) => {
  const { game, world, players } = store.getState();
  store.dispatch(updateBackup(game, world, players));
  clearHighlightAndTriggers();
  store.dispatch(updateCurrentAction({ type: `CARD_ADD_PIECE_${id}`, data: {} }));

  const openSpaces = world.get('rooms').reduce((a, b) =>
    a.concat(b.getIn(['board', 'type', 'spaces']).map((spaceInfo, spaceKey) =>
      Immutable.Map({ board: b.getIn(['board', 'name']), space: spaceKey })
    )),
    Immutable.List()
  )
  .filter(space => !isOccupied(players, Immutable.Map({ name: space.get('board') }), space.get('space')));

  store.dispatch(updateHighlighted(openSpaces.map(piece => `${piece.get('board')}_${piece.get('space')}_primary`)));

  let triggers = Immutable.Map();
  openSpaces.forEach((space) => {
    triggers = triggers.set(`${space.get('board')}_${space.get('space')}`, () => chooseSpace(`${space.get('board')}_${space.get('space')}`));
  });
  store.dispatch(addTriggers(triggers));
  store.dispatch(updatePrompt('Please choose a space to add a piece'));
};

const checkIfDisabled = () => {
  const { game } = store.getState();
  return game.get('action') > 0;
};

const chooseSpace = (space) => {
  const { game } = store.getState();
  clearHighlightAndTriggers();
  const spaceArray = space.split('_');
  store.dispatch(updateHighlighted(Immutable.fromJS([`${space}_primary`])));

  store.dispatch(appendCurrentAction({
    to: {
      board: spaceArray[0],
      space: spaceArray[1]
    },
    player: game.get('playerTurn'),
    modifiers: Immutable.List()
  }));
  store.dispatch(updateCanConfirm(true));
  store.dispatch(updatePrompt('Please cancel or confirm your selection'));
};

const confirm = (id) => {
  const { game, players } = store.getState();
  store.dispatch(updateCardRecords(game.get('playerTurn'), `CARD_ADD_PIECE_${id}`, 'activeAction', game.get('totalTurn')));
  const newPieces = getCurrentInProgress(game);
  store.dispatch(updatePieces(game.get('playerTurn'), players.getIn([game.get('playerTurn'), 'pieces']).concat(newPieces)));
  store.dispatch(updateCurrentActionModifier('finalAction', true));
};

const addPieceAnywhere = id => ({
  title: 'Add a Piece in any open space',
  description: 'For a free action, at the end of your turn, you may add a piece on any open space. This must be the last action you take',
  initAction: () => {},
  activateButton: () => init(id),
  activateButtonDisabled: () => checkIfDisabled(id),
  activateButtonText: 'Add piece (final action)',
  key: `CARD_ADD_PIECE_${id}`,
  confirm: () => confirm(id),
  modifications: {},
  records: {
    activeAction: null
  }
});

export default addPieceAnywhere;

export { init, checkIfDisabled, chooseSpace, confirm };
