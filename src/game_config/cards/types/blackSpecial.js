import store from '../../../utils/store';
import { blackSpecialEntryPoint, blackSpecialConfirm } from '../../players/black';
import { updateCurrentAction } from '../../../actions/game';
import { updateCardRecords } from '../../../actions/players';
import { updateBackup } from '../../../actions/backup';

const init = (id) => {
  const { game, world, players } = store.getState();
  store.dispatch(updateBackup(game, world, players));
  store.dispatch(updateCurrentAction({ type: `CARD_BLACK_SPECIAL_${id}`, data: {} }));
  blackSpecialEntryPoint(game, world, players);
};

const confirm = (id) => {
  const { game } = store.getState();
  blackSpecialConfirm();
  store.dispatch(updateCardRecords(game.get('playerTurn'), `CARD_BLACK_SPECIAL_${id}`, 'activeAction', game.get('totalTurn')));
};

const checkIfDisabled = (id) => {
  const { game, players } = store.getState();
  const lastUsedTurn = players.getIn([game.get('playerTurn'), 'cards']).find(card => card.get('key') === `CARD_BLACK_SPECIAL_${id}`).getIn(['records', 'activeAction']);
  return lastUsedTurn === game.get('totalTurn');
};

const blackSpecial = id => ({
  title: 'Free \'The Colony\' Special each turn',
  description: 'Once per turn, you may use a free action to change an opponent\'s piece touching yours to one of your pieces',
  initAction: () => {},
  activateButton: () => init(id),
  activateButtonDisabled: () => checkIfDisabled(id),
  activateButtonText: 'Replace Opponent Piece',
  key: `CARD_BLACK_SPECIAL_${id}`,
  confirm: () => confirm(id),
  modifications: {},
  records: {
    activeAction: null
  }
});

export default blackSpecial;

export { init, confirm, checkIfDisabled };
