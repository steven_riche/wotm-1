import store from '../../../utils/store';
import { updateCurrentActionModifier } from '../../../actions/game';
import { modifyCard, updateCardRecords } from '../../../actions/players';
import { updateBackup } from '../../../actions/backup';

const initOnTurn = (id) => {
  const { game, players } = store.getState();
  const currentPressureCount = players.getIn([game.get('playerTurn'), 'cards']).find(card => card.get('key') === `CARD_BLUE_SPECIAL_${id}`).getIn(['modifications', 'bluePressure']);
  store.dispatch(modifyCard(game.get('playerTurn'), `CARD_BLUE_SPECIAL_${id}`, 'bluePressure', (parseInt(currentPressureCount, 10) + 1)));
  store.dispatch(updateCardRecords(game.get('playerTurn'), `CARD_BLUE_SPECIAL_${id}`, 'passiveAction', game.get('totalTurn')));
};

const checkIfDisabled = (id) => {
  const { game, players } = store.getState();
  const currentPressureCount = players.getIn([game.get('playerTurn'), 'cards']).find(card => card.get('key') === `CARD_BLUE_SPECIAL_${id}`).getIn(['modifications', 'bluePressure']);
  return currentPressureCount === 0;
};

const init = (id) => {
  const { game, world, players } = store.getState();
  store.dispatch(updateBackup(game, world, players));
  store.dispatch(updateCurrentActionModifier('bluePressure', true));
  const currentPressureCount = players.getIn([game.get('playerTurn'), 'cards']).find(card => card.get('key') === `CARD_BLUE_SPECIAL_${id}`).getIn(['modifications', 'bluePressure']);
  store.dispatch(modifyCard(game.get('playerTurn'), `CARD_BLUE_SPECIAL_${id}`, 'bluePressure', (parseInt(currentPressureCount, 10) - 1)));
};

const blueSpecial = id => ({
  title: 'Free \'Knights of the Azure Main\' Special each turn',
  description: 'Once per turn, you will automatically gain one token of pressure. You can use this pressure before an add or move action to destroy blocking enemy pieces',
  initAction: () => initOnTurn(id),
  activateButton: () => init(id),
  activateButtonDisabled: () => checkIfDisabled(id),
  activateButtonText: 'Use Pressure',
  key: `CARD_BLUE_SPECIAL_${id}`,
  confirm: () => {},
  modifications: {
    bluePressure: 0
  },
  records: {
    passiveAction: null
  }
});

export default blueSpecial;

export { initOnTurn, checkIfDisabled, init };
