import store from '../../../utils/store';
import { redSpecialEntryPoint, redSpecialConfirmEntryPoint } from '../../players/red';
import { updateCurrentAction, updateCurrentActionModifier } from '../../../actions/game';
import { modifyCard, updateCardRecords } from '../../../actions/players';
import { updateBackup } from '../../../actions/backup';

const init = (id) => {
  const { game, world, players } = store.getState();
  store.dispatch(updateBackup(game, world, players));
  store.dispatch(updateCurrentAction({ type: `CARD_RED_SPECIAL_${id}`, data: {} }));
  redSpecialEntryPoint();
};

const confirm = (id) => {
  const { game, players } = store.getState();
  store.dispatch(updateCardRecords(game.get('playerTurn'), `CARD_RED_SPECIAL_${id}`, 'activeAction', game.get('totalTurn')));
  const addedFuel = redSpecialConfirmEntryPoint();
  const currentFuel = players.getIn([game.get('playerTurn'), 'cards']).find(card => card.get('key') === `CARD_RED_SPECIAL_${id}`).getIn(['modifications', 'redFuel']);
  store.dispatch(modifyCard(game.get('playerTurn'), `CARD_RED_SPECIAL_${id}`, 'redFuel', (parseInt(currentFuel, 10) + addedFuel)));
};

const checkIfDisabled = (id) => {
  const { game, players } = store.getState();
  const lastUsedTurn = players.getIn([game.get('playerTurn'), 'cards']).find(card => card.get('key') === `CARD_RED_SPECIAL_${id}`).getIn(['records', 'activeAction']);
  return lastUsedTurn === game.get('totalTurn');
};

const useFuel = (id) => {
  const { game, players } = store.getState();
  const currentFuel = players.getIn([game.get('playerTurn'), 'cards']).find(card => card.get('key') === `CARD_RED_SPECIAL_${id}`).getIn(['modifications', 'redFuel']);
  store.dispatch(modifyCard(game.get('playerTurn'), `CARD_RED_SPECIAL_${id}`, 'redFuel', (parseInt(currentFuel, 10) - 2)));
  store.dispatch(updateCurrentActionModifier('freeAction', true));
};

const redSpecial = id => ({
  title: 'Free \'Ignis Rex\' Special each turn',
  description: 'Once per turn, you may detonate a group of pieces, gaining fuel from enemy pieces destroyed. For each 2 fuel points, you can take an additional action.',
  initAction: () => {},
  activateButton: () => init(id),
  activateButtonDisabled: () => checkIfDisabled(id),
  activateButtonText: 'Detonate',
  key: `CARD_RED_SPECIAL_${id}`,
  confirm: () => confirm(id),
  useFuel: () => useFuel(id),
  modifications: {
    redFuel: 0
  },
  records: {
    activeAction: null
  }
});

export default redSpecial;

export { init, confirm, checkIfDisabled, useFuel };
