import Immutable from 'immutable';
import { init, confirm, checkIfDisabled } from '../blackSpecial';

jest.unmock('../blackSpecial');
jest.unmock('../../../../actions/game');
const gameActions = require('../../../../actions/game');

jest.unmock('../../../../actions/players');
const playerActions = require('../../../../actions/players');

jest.unmock('../../../../actions/backup');
const backupActions = require('../../../../actions/backup');

jest.unmock('../../../players/black');
const blackPlayerActions = require('../../../players/black');

const mocks = {
  updateCurrentAction: jest.spyOn(gameActions, 'updateCurrentAction'),
  updateCardRecords: jest.spyOn(playerActions, 'updateCardRecords'),
  updateBackup: jest.spyOn(backupActions, 'updateBackup'),
  blackSpecialEntryPoint: jest.spyOn(blackPlayerActions, 'blackSpecialEntryPoint').mockImplementation(() => true),
  blackSpecialConfirm: jest.spyOn(blackPlayerActions, 'blackSpecialConfirm').mockImplementation(() => true)
};

const mockGame = Immutable.fromJS({
  playerTurn: 0,
  totalTurn: 5
});
const mockWorld = Immutable.fromJS({});
const mockPlayers = Immutable.fromJS([
  {
    cards: [
      { key: 'CARD_BLACK_SPECIAL_foo', records: { activeAction: 5 } }
    ]
  }
]);

const mockState = {
  game: mockGame,
  world: mockWorld,
  players: mockPlayers
};

jest.mock('../../../../utils/store', () => ({
  getState: () => mockState,
  dispatch: () => {}
}));

describe('init', () => {
  beforeAll(() => init('foo'));

  it('should call updateBackup', () =>
    expect(mocks.updateBackup).toHaveBeenCalledWith(mockGame, mockWorld, mockPlayers));

  it('should call updateCurrentAction', () =>
    expect(mocks.updateCurrentAction).toHaveBeenCalledWith({ type: 'CARD_BLACK_SPECIAL_foo', data: {} }));

  it('should call blackSpecialEntryPoint', () =>
    expect(mocks.blackSpecialEntryPoint).toHaveBeenCalledWith(mockGame, mockWorld, mockPlayers));

  afterAll(() => {
    mocks.updateCurrentAction.mockReset();
    mocks.updateBackup.mockReset();
    mocks.blackSpecialEntryPoint.mockReset();
  });
});

describe('confirm', () => {
  beforeAll(() => confirm('foo'));

  it('should call blackSpecialConfirm', () =>
    expect(mocks.blackSpecialConfirm).toHaveBeenCalled());

  it('should call updateCardRecords', () =>
    expect(mocks.updateCardRecords).toHaveBeenCalledWith(0, 'CARD_BLACK_SPECIAL_foo', 'activeAction', 5));

  afterAll(() => {
    mocks.updateCardRecords.mockReset();
    mocks.blackSpecialConfirm.mockReset();
  });
});

describe('checkIfDisabled', () => {
  it('should return true because it has been used this turn', () =>
    expect(checkIfDisabled('foo')).toBe(true));
});
