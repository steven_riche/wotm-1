import Immutable from 'immutable';
import { findAvailablePieces, init, confirm, checkIfDisabled } from '../whiteSpecial';

jest.unmock('../whiteSpecial');
jest.unmock('../../../../game_logic/base_util');
const baseUtil = require('../../../../game_logic/base_util');

jest.unmock('../../../../actions/game');
const gameActions = require('../../../../actions/game');

jest.unmock('../../../../actions/players');
const playerActions = require('../../../../actions/players');

jest.unmock('../../../../actions/backup');
const backupActions = require('../../../../actions/backup');

jest.unmock('../../../players/white');
const whitePlayerActions = require('../../../players/white');

const mockGetBordering = (world, board, space) => {
  if (space === 'bar') {
    return Immutable.fromJS([{ board: 'foo', space: 'baz' }]);
  } else if (space === 'baz') {
    return Immutable.fromJS([{ board: 'foo', space: 'boom' }]);
  }

  return Immutable.fromJS([{ board: 'foo', space: 'bing' }]);
};

const mocks = {
  getBordering: jest.spyOn(baseUtil, 'getBordering').mockImplementation(mockGetBordering),
  isOccupied: jest.spyOn(baseUtil, 'isOccupied').mockImplementation(() => true),
  updateCurrentAction: jest.spyOn(gameActions, 'updateCurrentAction'),
  updateCardRecords: jest.spyOn(playerActions, 'updateCardRecords'),
  updateBackup: jest.spyOn(backupActions, 'updateBackup'),
  whiteSpecialEntryPoint: jest.spyOn(whitePlayerActions, 'whiteSpecialEntryPoint').mockImplementation(() => true),
  whiteSpecialConfirm: jest.spyOn(whitePlayerActions, 'whiteSpecialConfirm').mockImplementation(() => true)
};

const mockGame = Immutable.fromJS({
  playerTurn: 0,
  totalTurn: 5
});
const mockWorld = Immutable.fromJS({});
const mockPlayers = Immutable.fromJS([
  {
    cards: [
      { key: 'CARD_WHITE_SPECIAL_foo', records: { activeAction: 5 } }
    ]
  }
]);

const mockState = {
  game: mockGame,
  world: mockWorld,
  players: mockPlayers
};

jest.mock('../../../../utils/store', () => ({
  getState: () => mockState,
  dispatch: () => {}
}));

jest.mock('../../../../game_logic/base_util', () => ({
  getBordering: () => mocks.getBordering,
  isOccupied: () => mocks.isOccupied
}));

describe('findAvailablePieces', () => {
  it('should return correct available pieces', () =>
    expect(findAvailablePieces('foo_bar')).toEqual(Immutable.fromJS([{ board: 'foo', space: 'boom' }, { board: 'foo', space: 'baz' }])));
});

describe('init', () => {
  beforeAll(() => init('foo'));

  it('should call updateBackup', () =>
    expect(mocks.updateBackup).toHaveBeenCalledWith(mockGame, mockWorld, mockPlayers));

  it('should call updateCurrentAction', () =>
    expect(mocks.updateCurrentAction).toHaveBeenCalledWith({ type: 'CARD_WHITE_SPECIAL_foo', data: {} }));

  it('should call whiteSpecialEntryPoint', () =>
    expect(mocks.whiteSpecialEntryPoint).toHaveBeenCalledWith({
      availPieceFunc: expect.any(Function), gale_move_limit: 2
    }));

  afterAll(() => {
    mocks.updateCurrentAction.mockReset();
    mocks.updateBackup.mockReset();
    mocks.whiteSpecialEntryPoint.mockReset();
  });
});

describe('confirm', () => {
  beforeAll(() => confirm('foo'));

  it('should call whiteSpecialConfirm', () =>
    expect(mocks.whiteSpecialConfirm).toHaveBeenCalled());

  it('should call updateCardRecords', () =>
    expect(mocks.updateCardRecords).toHaveBeenCalledWith(0, 'CARD_WHITE_SPECIAL_foo', 'activeAction', 5));

  afterAll(() => {
    mocks.updateCardRecords.mockReset();
    mocks.whiteSpecialConfirm.mockReset();
  });
});

describe('checkIfDisabled', () => {
  it('should return true because it has been used this turn', () =>
    expect(checkIfDisabled('foo')).toBe(true));
});
