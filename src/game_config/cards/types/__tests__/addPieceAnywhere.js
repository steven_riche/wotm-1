import Immutable from 'immutable';
import { init, checkIfDisabled, chooseSpace, confirm } from '../addPieceAnywhere';

jest.unmock('../addPieceAnywhere');
jest.unmock('../../../../game_logic/base_util');
const baseUtil = require('../../../../game_logic/base_util');

jest.unmock('../../../../actions/game');
const gameActions = require('../../../../actions/game');

jest.unmock('../../../../actions/players');
const playerActions = require('../../../../actions/players');

jest.unmock('../../../../actions/backup');
const backupActions = require('../../../../actions/backup');

const mocks = {
  clearHighlightAndTriggers: jest.spyOn(baseUtil, 'clearHighlightAndTriggers'),
  getCurrentInProgress: jest.spyOn(baseUtil, 'getCurrentInProgress').mockImplementation(() => Immutable.fromJS([{ board: 'foo', space: 'bar' }])),
  isOccupied: jest.spyOn(baseUtil, 'isOccupied').mockImplementation((players, board, space) => space === 'baz'),
  addTriggers: jest.spyOn(gameActions, 'addTriggers'),
  appendCurrentAction: jest.spyOn(gameActions, 'appendCurrentAction'),
  updateCanConfirm: jest.spyOn(gameActions, 'updateCanConfirm'),
  updateCurrentAction: jest.spyOn(gameActions, 'updateCurrentAction'),
  updateCurrentActionModifier: jest.spyOn(gameActions, 'updateCurrentActionModifier'),
  updateHighlighted: jest.spyOn(gameActions, 'updateHighlighted'),
  updatePrompt: jest.spyOn(gameActions, 'updatePrompt'),
  updateCardRecords: jest.spyOn(playerActions, 'updateCardRecords'),
  updatePieces: jest.spyOn(playerActions, 'updatePieces'),
  updateBackup: jest.spyOn(backupActions, 'updateBackup')
};

const mockGame = Immutable.fromJS({
  action: 1,
  playerTurn: 0,
  totalTurn: 5
});
const mockWorld = Immutable.fromJS({
  rooms: [
    {
      board: {
        name: 'foo',
        type: {
          spaces: { bar: {}, baz: {} }
        }
      }
    }
  ]
});
const mockPlayers = Immutable.fromJS([
  {
    pieces: [{ board: 'foo', space: 'baz' }]
  }
]);

const mockState = {
  game: mockGame,
  world: mockWorld,
  players: mockPlayers
};

jest.mock('../../../../utils/store', () => ({
  getState: () => mockState,
  dispatch: () => {}
}));

jest.mock('../../../../game_logic/base_util', () => ({
  clearHighlightAndTriggers: () => mocks.clearHighlightAndTriggers,
  getCurrentInProgress: () => mocks.getCurrentInProgress,
  isOccupied: () => mocks.isOccupied
}));

describe('init', () => {
  beforeAll(() => init('foo'));

  it('should call updateBackup', () =>
    expect(mocks.updateBackup).toHaveBeenCalledWith(mockGame, mockWorld, mockPlayers));

  it('should call clearHighlightAndTriggers', () =>
    expect(mocks.clearHighlightAndTriggers).toHaveBeenCalled());

  it('should call updateCurrentAction', () =>
    expect(mocks.updateCurrentAction).toHaveBeenCalledWith({ type: 'CARD_ADD_PIECE_foo', data: {} }));

  it('should call updateHighlighted', () =>
    expect(mocks.updateHighlighted).toHaveBeenCalledWith(Immutable.List(['foo_bar_primary'])));

  it('should call addTriggers', () =>
    expect(mocks.addTriggers.mock.calls[0][0])
      .toEqual(Immutable.Map({ foo_bar: expect.any(Function) })));

  it('should call updatePrompt', () =>
    expect(mocks.updatePrompt).toHaveBeenCalled());

  afterAll(() => {
    mocks.clearHighlightAndTriggers.mockReset();
    mocks.addTriggers.mockReset();
    mocks.updateCurrentAction.mockReset();
    mocks.updateHighlighted.mockReset();
    mocks.updatePrompt.mockReset();
    mocks.updateBackup.mockReset();
  });
});

describe('checkIfDisabled', () => {
  it('should show disabled since there are still actions remaining', () =>
    expect(checkIfDisabled('foo')).toBe(true));
});

describe('chooseSpace', () => {
  beforeAll(() => chooseSpace('foo_bar'));

  it('should call clearHighlightAndTriggers', () =>
    expect(mocks.clearHighlightAndTriggers).toHaveBeenCalled());

  it('should call updateHighlighted', () =>
    expect(mocks.updateHighlighted).toHaveBeenCalledWith(Immutable.List(['foo_bar_primary'])));

  it('should call appendCurrentAction', () =>
    expect(mocks.appendCurrentAction).toHaveBeenCalledWith({ to: { board: 'foo', space: 'bar' }, player: 0, modifiers: Immutable.List() }));

  it('should call updateCanConfirm', () =>
    expect(mocks.updateCanConfirm).toHaveBeenCalledWith(true));

  it('should call updatePrompt', () =>
    expect(mocks.updatePrompt).toHaveBeenCalled());

  afterAll(() => {
    mocks.clearHighlightAndTriggers.mockReset();
    mocks.appendCurrentAction.mockReset();
    mocks.updateCanConfirm.mockReset();
    mocks.updateHighlighted.mockReset();
    mocks.updatePrompt.mockReset();
  });
});

describe('confirm', () => {
  beforeAll(() => confirm('foo'));

  it('should call updateCardRecords', () =>
    expect(mocks.updateCardRecords).toHaveBeenCalledWith(0, 'CARD_ADD_PIECE_foo', 'activeAction', 5));

  it('should call updatePieces', () => {
    expect(mocks.updatePieces.mock.calls[0][0]).toEqual(0);
    expect(mocks.updatePieces.mock.calls[0][1]).toEqual(Immutable.fromJS([{ board: 'foo', space: 'baz' }, { board: 'foo', space: 'bar' }]));
  });

  it('should call updateCurrentActionModifier', () =>
    expect(mocks.updateCurrentActionModifier).toHaveBeenCalledWith('finalAction', true));

  afterAll(() => {
    mocks.updateCurrentActionModifier.mockReset();
    mocks.updateCardRecords.mockReset();
    mocks.updatePieces.mockReset();
  });
});
