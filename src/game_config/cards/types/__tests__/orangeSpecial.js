import Immutable from 'immutable';
import { initOnTurn, checkIfDisabled, init } from '../orangeSpecial';

jest.unmock('../orangeSpecial');
jest.unmock('../../../../actions/game');
const gameActions = require('../../../../actions/game');

jest.unmock('../../../../actions/players');
const playerActions = require('../../../../actions/players');

jest.unmock('../../../../actions/backup');
const backupActions = require('../../../../actions/backup');

const mocks = {
  updateCurrentActionModifier: jest.spyOn(gameActions, 'updateCurrentActionModifier'),
  modifyCard: jest.spyOn(playerActions, 'modifyCard'),
  updateCardRecords: jest.spyOn(playerActions, 'updateCardRecords'),
  updateBackup: jest.spyOn(backupActions, 'updateBackup')
};

const mockGame = Immutable.fromJS({
  playerTurn: 0,
  totalTurn: 5
});
const mockWorld = Immutable.fromJS({});
const mockPlayers = Immutable.fromJS([
  {
    cards: [
      { key: 'CARD_ORANGE_SPECIAL_foo', modifications: { orangeBalance: 3 } }
    ]
  }
]);

const mockState = {
  game: mockGame,
  world: mockWorld,
  players: mockPlayers
};

jest.mock('../../../../utils/store', () => ({
  getState: () => mockState,
  dispatch: () => {}
}));

describe('initOnTurn', () => {
  beforeAll(() => initOnTurn('foo'));

  it('should call modifyCard', () =>
    expect(mocks.modifyCard).toHaveBeenCalledWith(0, 'CARD_ORANGE_SPECIAL_foo', 'orangeBalance', 4));

  it('should call updateCardRecords', () =>
    expect(mocks.updateCardRecords).toHaveBeenCalledWith(0, 'CARD_ORANGE_SPECIAL_foo', 'passiveAction', 5));

  afterAll(() => {
    mocks.modifyCard.mockReset();
    mocks.updateCardRecords.mockReset();
  });
});

describe('checkIfDisabled', () => {
  it('should return false because it has pressure', () =>
    expect(checkIfDisabled('foo')).toBe(false));
});

describe('init', () => {
  beforeAll(() => init('foo'));

  it('should call updateBackup', () =>
    expect(mocks.updateBackup).toHaveBeenCalledWith(mockGame, mockWorld, mockPlayers));

  it('should call updateCurrentActionModifier', () =>
    expect(mocks.updateCurrentActionModifier).toHaveBeenCalledWith('freeAction', true));

  it('should call modifyCard', () =>
    expect(mocks.modifyCard).toHaveBeenCalledWith(0, 'CARD_ORANGE_SPECIAL_foo', 'orangeBalance', 1));

  afterAll(() => {
    mocks.updateCurrentActionModifier.mockReset();
    mocks.modifyCard.mockReset();
    mocks.updateBackup.mockReset();
  });
});
