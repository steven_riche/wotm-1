import Immutable from 'immutable';
import { init, confirm, checkIfDisabled, useFuel } from '../redSpecial';

jest.unmock('../redSpecial');
jest.unmock('../../../../actions/game');
const gameActions = require('../../../../actions/game');

jest.unmock('../../../../actions/players');
const playerActions = require('../../../../actions/players');

jest.unmock('../../../../actions/backup');
const backupActions = require('../../../../actions/backup');

jest.unmock('../../../players/red');
const redPlayerActions = require('../../../players/red');

const mocks = {
  updateCurrentAction: jest.spyOn(gameActions, 'updateCurrentAction'),
  updateCurrentActionModifier: jest.spyOn(gameActions, 'updateCurrentActionModifier'),
  modifyCard: jest.spyOn(playerActions, 'modifyCard'),
  updateCardRecords: jest.spyOn(playerActions, 'updateCardRecords'),
  updateBackup: jest.spyOn(backupActions, 'updateBackup'),
  redSpecialEntryPoint: jest.spyOn(redPlayerActions, 'redSpecialEntryPoint').mockImplementation(() => true),
  redSpecialConfirmEntryPoint: jest.spyOn(redPlayerActions, 'redSpecialConfirmEntryPoint').mockImplementation(() => 2)
};

const mockGame = Immutable.fromJS({
  playerTurn: 0,
  totalTurn: 5
});
const mockWorld = Immutable.fromJS({});
const mockPlayers = Immutable.fromJS([
  {
    cards: [
      { key: 'CARD_RED_SPECIAL_foo', records: { activeAction: 5 }, modifications: { redFuel: 2 } }
    ]
  }
]);

const mockState = {
  game: mockGame,
  world: mockWorld,
  players: mockPlayers
};

jest.mock('../../../../utils/store', () => ({
  getState: () => mockState,
  dispatch: () => {}
}));

describe('init', () => {
  beforeAll(() => init('foo'));

  it('should call updateBackup', () =>
    expect(mocks.updateBackup).toHaveBeenCalledWith(mockGame, mockWorld, mockPlayers));

  it('should call updateCurrentAction', () =>
    expect(mocks.updateCurrentAction).toHaveBeenCalledWith({ type: 'CARD_RED_SPECIAL_foo', data: {} }));

  it('should call redSpecialEntryPoint', () =>
    expect(mocks.redSpecialEntryPoint).toHaveBeenCalled());

  afterAll(() => {
    mocks.updateCurrentAction.mockReset();
    mocks.updateBackup.mockReset();
    mocks.redSpecialEntryPoint.mockReset();
  });
});

describe('confirm', () => {
  beforeAll(() => confirm('foo'));

  it('should call redSpecialConfirmEntryPoint', () =>
    expect(mocks.redSpecialConfirmEntryPoint).toHaveBeenCalled());

  it('should call updateCardRecords', () =>
    expect(mocks.updateCardRecords).toHaveBeenCalledWith(0, 'CARD_RED_SPECIAL_foo', 'activeAction', 5));

  it('should call modifyCard', () =>
    expect(mocks.modifyCard).toHaveBeenCalledWith(0, 'CARD_RED_SPECIAL_foo', 'redFuel', 4));

  afterAll(() => {
    mocks.modifyCard.mockReset();
    mocks.updateCardRecords.mockReset();
    mocks.redSpecialConfirmEntryPoint.mockReset();
  });
});

describe('checkIfDisabled', () => {
  it('should return true because it has been used this turn', () =>
    expect(checkIfDisabled('foo')).toBe(true));
});

describe('useFuel', () => {
  beforeAll(() => useFuel('foo'));

  it('should call modifyCard', () =>
    expect(mocks.modifyCard).toHaveBeenCalledWith(0, 'CARD_RED_SPECIAL_foo', 'redFuel', 0));

  it('should call updateCurrentActionModifier', () =>
    expect(mocks.updateCurrentActionModifier).toHaveBeenCalledWith('freeAction', true));

  afterAll(() => {
    mocks.updateCurrentActionModifier.mockReset();
    mocks.modifyCard.mockReset();
  });
});
