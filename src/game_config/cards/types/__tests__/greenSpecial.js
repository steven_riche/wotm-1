import Immutable from 'immutable';
import { triggerIncrease, init, confirm, checkIfDisabled } from '../greenSpecial';

jest.unmock('../greenSpecial');
jest.unmock('../../../../actions/game');
const gameActions = require('../../../../actions/game');

jest.unmock('../../../../actions/players');
const playerActions = require('../../../../actions/players');

jest.unmock('../../../../actions/backup');
const backupActions = require('../../../../actions/backup');

jest.unmock('../../../players/black');
const greenPlayerActions = require('../../../players/green');

const mocks = {
  updateCanConfirm: jest.spyOn(gameActions, 'updateCanConfirm'),
  updateCurrentAction: jest.spyOn(gameActions, 'updateCurrentAction'),
  modifyCard: jest.spyOn(playerActions, 'modifyCard'),
  updateCardRecords: jest.spyOn(playerActions, 'updateCardRecords'),
  updateBackup: jest.spyOn(backupActions, 'updateBackup'),
  greenSpecialEntryPoint: jest.spyOn(greenPlayerActions, 'greenSpecialEntryPoint').mockImplementation(() => true),
  greenSpecialConfirm: jest.spyOn(greenPlayerActions, 'greenSpecialConfirm').mockImplementation(() => true)
};

const mockGame = Immutable.fromJS({
  playerTurn: 0,
  totalTurn: 5
});
const mockWorld = Immutable.fromJS({});
const mockPlayers = Immutable.fromJS([
  {
    cards: [
      { key: 'CARD_GREEN_SPECIAL_foo', records: { activeAction: 5 }, modifications: { bar: 3 } }
    ]
  }
]);

const mockState = {
  game: mockGame,
  world: mockWorld,
  players: mockPlayers
};

jest.mock('../../../../utils/store', () => ({
  getState: () => mockState,
  dispatch: () => {}
}));

describe('triggerIncrease', () => {
  beforeAll(() => triggerIncrease('foo', 'bar'));

  it('should call modifyCard', () =>
    expect(mocks.modifyCard).toHaveBeenCalledWith(0, 'CARD_GREEN_SPECIAL_foo', 'bar', 4));

  it('should call updateCanConfirm', () =>
    expect(mocks.updateCanConfirm).toHaveBeenCalledWith(true));

  afterAll(() => {
    mocks.updateCanConfirm.mockReset();
    mocks.modifyCard.mockReset();
  });
});

describe('init', () => {
  beforeAll(() => init('foo'));

  it('should call updateBackup', () =>
    expect(mocks.updateBackup).toHaveBeenCalledWith(mockGame, mockWorld, mockPlayers));

  it('should call updateCurrentAction', () =>
    expect(mocks.updateCurrentAction).toHaveBeenCalledWith({ type: 'CARD_GREEN_SPECIAL_foo', data: {} }));

  it('should call greenSpecialEntryPoint', () =>
    expect(mocks.greenSpecialEntryPoint).toHaveBeenCalledWith({
      triggerIncrease: expect.any(Function)
    }));

  afterAll(() => {
    mocks.updateCurrentAction.mockReset();
    mocks.updateBackup.mockReset();
    mocks.greenSpecialEntryPoint.mockReset();
  });
});

describe('confirm', () => {
  beforeAll(() => confirm('foo'));

  it('should call greenSpecialConfirm', () =>
    expect(mocks.greenSpecialConfirm).toHaveBeenCalled());

  it('should call updateCardRecords', () =>
    expect(mocks.updateCardRecords).toHaveBeenCalledWith(0, 'CARD_GREEN_SPECIAL_foo', 'activeAction', 5));

  afterAll(() => {
    mocks.updateCardRecords.mockReset();
    mocks.greenSpecialConfirm.mockReset();
  });
});

describe('checkIfDisabled', () => {
  it('should return true because it has been used this turn', () =>
    expect(checkIfDisabled('foo')).toBe(true));
});
