import Immutable from 'immutable';
import { init, choosePiece, checkIfDisabled, confirm } from '../replaceOpponentWith5';

jest.unmock('../replaceOpponentWith5');
jest.unmock('../../../../game_logic/base_util');
const baseUtil = require('../../../../game_logic/base_util');

jest.unmock('../../../../actions/game');
const gameActions = require('../../../../actions/game');

jest.unmock('../../../../actions/players');
const playerActions = require('../../../../actions/players');

jest.unmock('../../../../actions/backup');
const backupActions = require('../../../../actions/backup');

const mocks = {
  clearHighlightAndTriggers: jest.spyOn(baseUtil, 'clearHighlightAndTriggers'),
  whichPlayerPiece: jest.spyOn(baseUtil, 'whichPlayerPiece').mockImplementation(() => 1),
  addTriggers: jest.spyOn(gameActions, 'addTriggers'),
  appendCurrentAction: jest.spyOn(gameActions, 'appendCurrentAction'),
  updateCanConfirm: jest.spyOn(gameActions, 'updateCanConfirm'),
  updateCurrentAction: jest.spyOn(gameActions, 'updateCurrentAction'),
  updateHighlighted: jest.spyOn(gameActions, 'updateHighlighted'),
  updatePrompt: jest.spyOn(gameActions, 'updatePrompt'),
  updateCardRecords: jest.spyOn(playerActions, 'updateCardRecords'),
  updateDamage: jest.spyOn(playerActions, 'updateDamage'),
  updatePieces: jest.spyOn(playerActions, 'updatePieces'),
  updateBackup: jest.spyOn(backupActions, 'updateBackup')
};

const mockGame = Immutable.fromJS({
  playerTurn: 0,
  totalTurn: 5,
  currentAction: {
    data: {
      moves: [
        {
          to: { board: 'foo', space: 'bar' },
          formerPlayer: 1
        }
      ]
    }
  }
});
const mockWorld = Immutable.fromJS({});
const mockPlayers = Immutable.fromJS([
  {
    pieces: [{ board: 'foo', space: 'baz', modifiers: [] }],
    cards: [
      { key: 'CARD_REPLACE_OPPONENT_foo', records: { activeAction: 5 } }
    ]
  },
  {
    pieces: [
      { board: 'foo', space: 'f1', modifiers: [] },
      { board: 'foo', space: 'f2', modifiers: [] },
      { board: 'foo', space: 'f3', modifiers: [] },
      { board: 'foo', space: 'f4', modifiers: [] },
      { board: 'foo', space: 'f5', modifiers: [] }
    ],
    damageCounter: 3
  }
]);

const mockState = {
  game: mockGame,
  world: mockWorld,
  players: mockPlayers
};

jest.mock('../../../../utils/store', () => ({
  getState: () => mockState,
  dispatch: () => {}
}));

jest.mock('../../../../game_logic/base_util', () => ({
  clearHighlightAndTriggers: () => mocks.clearHighlightAndTriggers,
  whichPlayerPiece: () => mocks.whichPlayerPiece
}));

describe('init', () => {
  beforeAll(() => init('foo'));

  it('should call updateBackup', () =>
    expect(mocks.updateBackup).toHaveBeenCalledWith(mockGame, mockWorld, mockPlayers));

  it('should call clearHighlightAndTriggers', () =>
    expect(mocks.clearHighlightAndTriggers).toHaveBeenCalled());

  it('should call updateCurrentAction', () =>
    expect(mocks.updateCurrentAction).toHaveBeenCalledWith({ type: 'CARD_REPLACE_OPPONENT_foo', data: {} }));

  it('should call updateHighlighted', () =>
    expect(mocks.updateHighlighted).toHaveBeenCalledWith(Immutable.List(['foo_f1_primary', 'foo_f2_primary', 'foo_f3_primary', 'foo_f4_primary', 'foo_f5_primary'])));

  it('should call addTriggers', () =>
    expect(mocks.addTriggers.mock.calls[0][0]).toEqual(Immutable.Map({
      foo_f1: expect.any(Function),
      foo_f2: expect.any(Function),
      foo_f3: expect.any(Function),
      foo_f4: expect.any(Function),
      foo_f5: expect.any(Function)
    })));

  it('should call updatePrompt', () =>
    expect(mocks.updatePrompt).toHaveBeenCalled());

  afterAll(() => {
    mocks.clearHighlightAndTriggers.mockReset();
    mocks.addTriggers.mockReset();
    mocks.updateCurrentAction.mockReset();
    mocks.updateHighlighted.mockReset();
    mocks.updatePrompt.mockReset();
    mocks.updateBackup.mockReset();
  });
});

describe('choosePiece', () => {
  beforeAll(() => choosePiece('foo_f3'));

  const newPieces = Immutable.fromJS([
    { board: 'foo', space: 'f1', modifiers: [] },
    { board: 'foo', space: 'f2', modifiers: [] },
    { board: 'foo', space: 'f4', modifiers: [] },
    { board: 'foo', space: 'f5', modifiers: [] }
  ]);

  it('should call clearHighlightAndTriggers', () =>
    expect(mocks.clearHighlightAndTriggers).toHaveBeenCalled());

  it('should call updateHighlighted', () =>
    expect(mocks.updateHighlighted).toHaveBeenCalledWith(Immutable.List(['foo_f3_primary'])));

  it('should call appendCurrentAction', () =>
    expect(mocks.appendCurrentAction).toHaveBeenCalledWith({ to: { board: 'foo', space: 'f3' }, player: 0, formerPlayer: 1, modifiers: Immutable.List() }));

  it('should call updatePieces', () => {
    expect(mocks.updatePieces.mock.calls[0][0]).toEqual(1);
    expect(mocks.updatePieces.mock.calls[0][1]).toEqual(newPieces);
  });

  it('should call updateCanConfirm', () =>
    expect(mocks.updateCanConfirm).toHaveBeenCalledWith(true));

  it('should call updatePrompt', () =>
    expect(mocks.updatePrompt).toHaveBeenCalled());

  afterAll(() => {
    mocks.clearHighlightAndTriggers.mockReset();
    mocks.appendCurrentAction.mockReset();
    mocks.updateCanConfirm.mockReset();
    mocks.updateHighlighted.mockReset();
    mocks.updatePrompt.mockReset();
    mocks.updatePieces.mockReset();
  });
});

describe('checkIfDisabled', () => {
  it('should show disabled', () =>
    expect(checkIfDisabled('foo')).toBe(true));
});

describe('confirm', () => {
  beforeAll(() => confirm('foo'));

  it('should call updateDamage', () =>
    expect(mocks.updateDamage).toHaveBeenCalledWith(1, 4));

  it('should call updateCardRecords', () =>
    expect(mocks.updateCardRecords).toHaveBeenCalledWith(0, 'CARD_REPLACE_OPPONENT_foo', 'activeAction', 5));

  it('should call updatePieces', () => {
    expect(mocks.updatePieces.mock.calls[0][0]).toEqual(0);
    expect(mocks.updatePieces.mock.calls[0][1]).toEqual(Immutable.fromJS([{ board: 'foo', space: 'baz', modifiers: [] }, { board: 'foo', space: 'bar', modifiers: [] }]));
  });

  afterAll(() => {
    mocks.updateCardRecords.mockReset();
    mocks.updateDamage.mockReset();
    mocks.updatePieces.mockReset();
  });
});
