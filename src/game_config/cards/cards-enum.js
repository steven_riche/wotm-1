import movePlus3 from './types/movePlus3';
import addPlus2 from './types/addPlus2';
import captureMinus1 from './types/captureMinus1';
import capturePlus2 from './types/capturePlus2';
import cardReqHalf from './types/cardReqHalf';
import damageFreeAction2 from './types/damageFreeAction2';
import replaceOpponentWith5 from './types/replaceOpponentWith5';
import addPieceAnywhere from './types/addPieceAnywhere';
import blackSpecial from './types/blackSpecial';
import purpleSpecial from './types/purpleSpecial';
import whiteSpecial from './types/whiteSpecial';
import blueSpecial from './types/blueSpecial';
import redSpecial from './types/redSpecial';
import yellowSpecial from './types/yellowSpecial';
import greenSpecial from './types/greenSpecial';
import orangeSpecial from './types/orangeSpecial';

const Cards = [
  movePlus3, movePlus3, movePlus3,
  addPlus2, addPlus2, addPlus2,
  captureMinus1, captureMinus1, captureMinus1,
  capturePlus2, capturePlus2, capturePlus2,
  cardReqHalf, cardReqHalf, cardReqHalf,
  damageFreeAction2, damageFreeAction2, damageFreeAction2,
  replaceOpponentWith5('A'), replaceOpponentWith5('B'), replaceOpponentWith5('C'),
  addPieceAnywhere('A'), addPieceAnywhere('B'), addPieceAnywhere('C'),
  blackSpecial('A'), blackSpecial('B'),
  purpleSpecial('A'), purpleSpecial('B'),
  whiteSpecial('A'), whiteSpecial('B'),
  blueSpecial('A'), blueSpecial('B'),
  redSpecial('A'), redSpecial('B'),
  yellowSpecial('A'), yellowSpecial('B'),
  greenSpecial('A'), greenSpecial('B'),
  orangeSpecial('A'), orangeSpecial('B')
];

export default Cards;
