import parse from 'parse-svg-path';
import scale from 'scale-svg-path';
import serialize from 'serialize-svg-path';
import Boards from '../boards/boards-enum';
import store from '../../utils/store';
import OrangeBalance from '../../components/player-specific/orange-balance';
import { updateModifier } from '../../actions/players';
import { updateCanConfirm, updateCurrentAction, updatePrompt } from '../../actions/game';
import addDefaults from '../../game_logic/add_defaults';
import moveDefaults from '../../game_logic/move_defaults';
import captureDefaults from '../../game_logic/capture_defaults';

const ADD_LIMIT = 2;
const MOVE_LIMIT = 3;

const orangeSpecialInit = () => {
  Promise.resolve(store.dispatch(updatePrompt('Click confirm to save time')))
    .then(store.dispatch(updateCurrentAction({ type: 'SPECIAL', role: 'Societe Pendulaire', data: {} })))
    .then(store.dispatch(updateCanConfirm(true)));
};

const orangeSpecialConfirm = () => {
  const { game, players } = store.getState();
  const currentBalance = (players.hasIn([game.get('playerTurn'), 'modifiers', 'orangeBalance'])) ? players.getIn([game.get('playerTurn'), 'modifiers', 'orangeBalance']) : 0;
  store.dispatch(updateModifier(game.get('playerTurn'), 'orangeBalance', (currentBalance + 1)));
};

const add = Object.assign({}, addDefaults,
  {
    canAddMore: (...args) => addDefaults.canAddMore(ADD_LIMIT, addDefaults.available, ...args)
  }
);

const move = Object.assign({}, moveDefaults,
  {
    piecesCanStillMove: (...args) => moveDefaults.piecesCanStillMove(MOVE_LIMIT, ...args)
  }
);

const special = {
  init: orangeSpecialInit,
  confirm: orangeSpecialConfirm
};

const capture = captureDefaults;

const path = serialize(scale(parse(`M1632 1600q14 0 23 9t9 23v128q0 14-9 23t-23 9h-1472q-14 0-23-9t-9-23v-128q0-14 9-23t23-9h1472z
  m-1374-64q3-55 16-107t30-95 46-87 53.5-76 64.5-69.5 66-60 70.5-55 66.5-47.5 65-43q-43-28-65-43t-66.5-47.5-70.5-55-66-60-64.5-69.5-53.5
  -76-46-87-30-95-16-107h1276q-3 55-16 107t-30 95-46 87-53.5 76-64.5 69.5-66 60-70.5 55-66.5 47.5-65 43q43 28 65 43t66.5 47.5 70.5 55 66
  60 64.5 69.5 53.5 76 46 87 30 95 16 107h-1276zm1374-1536q14 0 23 9t9 23v128q0 14-9 23t-23 9h-1472q-14 0-23-9t-9-23v-128q0-14 9-23t23-9h1472z`), (20 / 1792)));

const Orange = {
  name: 'Societe Pendulaire',
  boards: [Boards.TriangleA, Boards.KiteB],
  scoreAdd: ADD_LIMIT,
  scoreMove: MOVE_LIMIT,
  scoreSpecial: 'Save Time',
  scoreComponent: OrangeBalance,
  primaryColor: '#f9b411',
  secondaryColor: '#333333',
  tabKey: 'orange',
  path,
  add,
  move,
  special,
  capture
};

export default Orange;

export { orangeSpecialInit, orangeSpecialConfirm };
