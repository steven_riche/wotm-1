import Immutable from 'immutable';
import {
  greenSpecialInit,
  greenSpecialChoosePiece,
  greenSpecialChooseTarget,
  greenSpecialConfirm,
  triggerIncrease,
  additionalAdd,
  additionalMove,
  additionalIncrease
} from '../green';

jest.unmock('../green');
jest.unmock('../../../game_logic/base_util');
const baseUtil = require('../../../game_logic/base_util');

jest.unmock('../../../actions/game');
const gameActions = require('../../../actions/game');

jest.unmock('../../../actions/players');
const playerActions = require('../../../actions/players');

const mocks = {
  triggerIncrease: jest.fn(),
  clearHighlightAndTriggers: jest.spyOn(baseUtil, 'clearHighlightAndTriggers'),
  getBordering: jest.spyOn(baseUtil, 'getBordering').mockImplementation(() => Immutable.fromJS([{ board: 'foo', space: 'baz' }, { board: 'foo', space: 'boo' }])),
  isOccupied: jest.spyOn(baseUtil, 'isOccupied').mockImplementation((players, board, space) => space === 'baz'),
  whichPlayerPiece: jest.spyOn(baseUtil, 'whichPlayerPiece').mockImplementation(() => 1),
  addTriggers: jest.spyOn(gameActions, 'addTriggers'),
  appendCurrentAction: jest.spyOn(gameActions, 'appendCurrentAction'),
  updateCurrentAction: jest.spyOn(gameActions, 'updateCurrentAction'),
  updateHighlighted: jest.spyOn(gameActions, 'updateHighlighted'),
  updatePrompt: jest.spyOn(gameActions, 'updatePrompt'),
  updateSpecialActionNeeded: jest.spyOn(gameActions, 'updateSpecialActionNeeded'),
  updateDamage: jest.spyOn(playerActions, 'updateDamage'),
  updateModifier: jest.spyOn(playerActions, 'updateModifier'),
  updatePieces: jest.spyOn(playerActions, 'updatePieces')
};

const mockGame = Immutable.fromJS({
  playerTurn: 0,
  currentAction: {
    data: {
      moves: [{
        to: { board: 'foo', space: 'baz' },
        player: 0,
        formerPlayer: 1,
        modifiers: []
      }]
    }
  }
});
const mockWorld = Immutable.fromJS({});
const mockPlayers = Immutable.fromJS([
  {
    pieces: [
      { board: 'foo', space: 'bar', modifiers: [] }
    ],
    modifiers: {
      greenAdd: 4,
      greenMove: 5,
      greenIncrease: 6
    }
  },
  {
    pieces: [
      { board: 'foo', space: 'baz', modifiers: [] }
    ],
    damageCounter: 1
  }
]);

const mockState = {
  game: mockGame,
  world: mockWorld,
  players: mockPlayers
};

jest.mock('../../../utils/store', () => ({
  getState: () => mockState,
  dispatch: () => {}
}));

jest.mock('../../../game_logic/base_util', () => ({
  clearHighlightAndTriggers: () => mocks.clearHighlightAndTriggers,
  getBordering: () => mocks.getBordering,
  isOccupied: () => mocks.isOccupied,
  whichPlayerPiece: () => mocks.whichPlayerPiece
}));

describe('greenSpecialInit & greenSpecialEntryPoint', () => {
  beforeAll(() => greenSpecialInit());

  it('should call updateCurrentAction', () =>
    expect(mocks.updateCurrentAction).toHaveBeenCalledWith({ type: 'SPECIAL', role: 'Kundi Mbegu', data: {} }));

  it('should call updateHighlighted', () =>
    expect(mocks.updateHighlighted).toHaveBeenCalledWith(Immutable.List(['foo_bar_primary'])));

  it('should call addTriggers', () =>
    expect(mocks.addTriggers.mock.calls[0][0])
      .toEqual(Immutable.Map({ foo_bar: expect.any(Function) })));

  it('should call updatePrompt', () =>
    expect(mocks.updatePrompt).toHaveBeenCalled());

  afterAll(() => {
    mocks.addTriggers.mockReset();
    mocks.updateCurrentAction.mockReset();
    mocks.updateHighlighted.mockReset();
    mocks.updatePrompt();
  });
});

describe('greenSpecialChoosePiece', () => {
  beforeAll(() => greenSpecialChoosePiece('foo_bar', { triggerIncrease: mocks.triggerIncrease }));

  it('should call clearclearHighlightAndTriggers', () =>
    expect(mocks.clearHighlightAndTriggers).toHaveBeenCalled());

  it('should call getBordering', () =>
    expect(mocks.getBordering).toHaveBeenCalledWith(mockWorld, undefined, 'bar'));

  it('should call isOccupied', () =>
    expect(mocks.isOccupied).toHaveBeenCalled());

  it('should call updateHighlighted', () =>
    expect(mocks.updateHighlighted.mock.calls[0][0]).toEqual(Immutable.List(['foo_baz_primary', 'foo_bar_secondary'])));

  it('should call addTriggers', () =>
    expect(mocks.addTriggers.mock.calls[0][0])
      .toEqual(Immutable.Map({ foo_baz: expect.any(Function) })));

  it('should call updatePrompt', () =>
    expect(mocks.updatePrompt).toHaveBeenCalled());

  afterAll(() => {
    mocks.clearHighlightAndTriggers.mockReset();
    mocks.addTriggers.mockReset();
    mocks.updateHighlighted.mockReset();
    mocks.updatePrompt();
  });
});

describe('greenSpecialChooseTarget', () => {
  beforeAll(() => greenSpecialChooseTarget('foo_baz', 'foo_bar', { triggerIncrease: mocks.triggerIncrease }));

  it('should call clearclearHighlightAndTriggers', () =>
    expect(mocks.clearHighlightAndTriggers).toHaveBeenCalled());

  it('should call whichPlayerPiece', () =>
    expect(mocks.whichPlayerPiece).toHaveBeenCalledWith(mockPlayers, 'foo', 'baz'));

  it('should call appendCurrentAction', () =>
    expect(mocks.appendCurrentAction).toHaveBeenCalledWith({ from: { board: 'foo', space: 'bar' }, to: { board: 'foo', space: 'baz' }, player: 0, formerPlayer: 1, modifiers: Immutable.List() }));

  it('should call updatePieces', () =>
    expect(mocks.updatePieces).toHaveBeenCalledWith(1, Immutable.List()));

  it('should call triggerIncrease', () =>
    expect(mocks.triggerIncrease).toHaveBeenCalled());

  it('should call updatePrompt', () =>
    expect(mocks.updatePrompt).toHaveBeenCalled());

  afterAll(() => {
    mocks.triggerIncrease.mockReset();
    mocks.clearHighlightAndTriggers.mockReset();
    mocks.appendCurrentAction.mockReset();
    mocks.updatePrompt.mockReset();
    mocks.updatePieces.mockReset();
  });
});

describe('greenSpecialConfirm', () => {
  beforeAll(() => greenSpecialConfirm());

  it('should call updateDamage', () =>
    expect(mocks.updateDamage).toHaveBeenCalledWith(1, 2));

  it('should call updatePieces', () => {
    expect(mocks.updatePieces.mock.calls[0][0]).toEqual(0);
    expect(mocks.updatePieces.mock.calls[0][1]).toEqual(Immutable.fromJS([{ board: 'foo', space: 'bar', modifiers: [] }, { board: 'foo', space: 'baz', modifiers: [] }]));
  });

  afterAll(() => {
    mocks.updateDamage.mockReset();
    mocks.updatePieces.mockReset();
  });
});

describe('triggerIncrease', () => {
  beforeAll(() => triggerIncrease(mockGame));

  it('should call updateModifier', () =>
    expect(mocks.updateModifier).toHaveBeenCalledWith(0, 'greenFuel', 6));

  it('should call updateSpecialActionNeeded', () =>
    expect(mocks.updateSpecialActionNeeded).toHaveBeenCalledWith(true));

  afterAll(() => {
    mocks.updateSpecialActionNeeded.mockReset();
    mocks.updateModifier.mockReset();
  });
});

describe('additionalAdd', () => {
  it('should correctly get the add value', () =>
    expect(additionalAdd()).toBe(4));
});

describe('additionalMove', () => {
  it('should correctly get the move value', () =>
    expect(additionalMove()).toBe(5));
});

describe('additionalIncrease', () => {
  it('should correctly get the increase value', () =>
    expect(additionalIncrease()).toBe(6));
});
