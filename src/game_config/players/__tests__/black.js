import Immutable from 'immutable';
import {
  blackPiecesCanStillMove,
  blackSpecialInit,
  blackSpecialChoosePiece,
  blackSpecialConfirm
} from '../black';

jest.unmock('../black');
jest.unmock('../../../game_logic/base_util');
const baseUtil = require('../../../game_logic/base_util');

jest.unmock('../../../actions/game');
const gameActions = require('../../../actions/game');

jest.unmock('../../../actions/players');
const playerActions = require('../../../actions/players');

const mocks = {
  allTouchingEnemies: jest.spyOn(baseUtil, 'allTouchingEnemies').mockImplementation(() => Immutable.fromJS([{ board: 'foo', space: 'bar' }])),
  clearHighlightAndTriggers: jest.spyOn(baseUtil, 'clearHighlightAndTriggers'),
  whichPlayerPiece: jest.spyOn(baseUtil, 'whichPlayerPiece').mockImplementation(() => 1),
  addTriggers: jest.spyOn(gameActions, 'addTriggers'),
  appendCurrentAction: jest.spyOn(gameActions, 'appendCurrentAction'),
  updateCanConfirm: jest.spyOn(gameActions, 'updateCanConfirm'),
  updateCurrentAction: jest.spyOn(gameActions, 'updateCurrentAction'),
  updateHighlighted: jest.spyOn(gameActions, 'updateHighlighted'),
  updatePrompt: jest.spyOn(gameActions, 'updatePrompt'),
  updateDamage: jest.spyOn(playerActions, 'updateDamage'),
  updatePieces: jest.spyOn(playerActions, 'updatePieces')
};

const mockGame = Immutable.fromJS({
  playerTurn: 0,
  currentAction: {
    data: {
      moves: [
        { to: { board: 'foo', space: 'bar' }, formerPlayer: 1, modifiers: [] }
      ]
    }
  }
});
const mockWorld = Immutable.Map();
const mockPlayers = Immutable.fromJS([
  {
    pieces: [],
    cards: []
  },
  {
    damageCounter: 3,
    pieces: [
      { board: 'foo', space: 'bar', modifiers: [] }
    ]
  }
]);
const mockState = {
  game: mockGame,
  world: mockWorld,
  players: mockPlayers
};

jest.mock('../../../utils/store', () => ({
  getState: () => mockState,
  dispatch: () => {}
}));

jest.mock('../../../game_logic/base_util', () => ({
  allTouchingEnemies: () => mocks.allTouchingEnemies,
  clearHighlightAndTriggers: () => mocks.clearHighlightAndTriggers,
  whichPlayerPiece: () => mocks.whichPlayerPiece
}));

describe('blackPiecesCanStillMove', () => {
  it('should correctly return list of pieces that can still move', () =>
    expect(blackPiecesCanStillMove(2, mockGame, mockPlayers)).toEqual(Immutable.fromJS([{ board: 'foo', space: 'bar', player: 0, modifiers: Immutable.List() }])));
});

describe('blackSpecialInit & blackSpecialEntryPoint', () => {
  beforeAll(() => blackSpecialInit());

  it('should call updateCurrentAction', () =>
    expect(mocks.updateCurrentAction).toHaveBeenCalledWith({ type: 'SPECIAL', role: 'The Colony', data: {} }));

  it('should call allTouchingEnemies', () =>
    expect(mocks.allTouchingEnemies).toHaveBeenCalledWith(mockGame, mockWorld, mockPlayers));

  it('should call updateHighlighted', () =>
    expect(mocks.updateHighlighted).toHaveBeenCalledWith(Immutable.List(['foo_bar_primary'])));

  it('should call addTriggers', () =>
    expect(mocks.addTriggers.mock.calls[0][0])
      .toEqual(Immutable.Map({ foo_bar: expect.any(Function) })));

  it('should update the prompt', () =>
    expect(mocks.updatePrompt).toHaveBeenCalled());

  afterAll(() => {
    mocks.updateCurrentAction.mockReset();
    mocks.updateHighlighted.mockReset();
    mocks.addTriggers.mockReset();
    mocks.updatePrompt.mockReset();
  });
});

describe('blackSpecialChoosePiece', () => {
  beforeAll(() => blackSpecialChoosePiece('foo_bar'));

  it('should clear highlights', () =>
    expect(mocks.clearHighlightAndTriggers).toHaveBeenCalled());

  it('should call whichPlayerPiece', () =>
    expect(mocks.whichPlayerPiece).toHaveBeenCalledWith(mockPlayers, 'foo', 'bar'));

  it('should call updateHighlighted', () =>
    expect(mocks.updateHighlighted).toHaveBeenCalledWith(['foo_bar_primary']));

  it('should call appendCurrentAction', () =>
    expect(mocks.appendCurrentAction).toHaveBeenCalledWith({ to: { board: 'foo', space: 'bar' }, player: 0, formerPlayer: 1, modifiers: Immutable.List() }));

  it('should call updatePieces', () =>
    expect(mocks.updatePieces).toHaveBeenCalledWith(1, Immutable.List()));

  it('should call canConfirm', () =>
    expect(mocks.updateCanConfirm).toHaveBeenCalledWith(true));

  it('should update the prompt', () =>
    expect(mocks.updatePrompt).toHaveBeenCalled());

  afterAll(() => {
    mocks.clearHighlightAndTriggers.mockReset();
    mocks.updateHighlighted.mockReset();
    mocks.appendCurrentAction.mockReset();
    mocks.updatePieces.mockReset();
    mocks.updateCanConfirm.mockReset();
    mocks.updatePrompt.mockReset();
  });
});

describe('blackSpecialConfirm', () => {
  beforeAll(() => blackSpecialConfirm());

  it('should call updateDamage', () =>
    expect(mocks.updateDamage).toHaveBeenCalledWith(1, 4));

  it('should call updatePieces', () => {
    expect(mocks.updatePieces.mock.calls[0][0]).toEqual(0);
    expect(mocks.updatePieces.mock.calls[0][1]).toEqual(Immutable.fromJS([{ board: 'foo', space: 'bar', modifiers: Immutable.List() }]));
  });

  afterAll(() => {
    mocks.updateDamage.mockReset();
    mocks.updatePieces.mockReset();
  });
});
