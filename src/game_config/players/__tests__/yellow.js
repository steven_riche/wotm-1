import Immutable from 'immutable';

import {
  yellowMoveBlockedAvailable,
  yellowSpecialInit,
  yellowChoosePiece,
  yellowChooseSpace,
  yellowSpecialConfirm
} from '../yellow';

jest.unmock('../yellow');
jest.unmock('../../../game_logic/base_util');
const baseUtil = require('../../../game_logic/base_util');

jest.unmock('../../../actions/game');
const gameActions = require('../../../actions/game');

jest.unmock('../../../actions/players');
const playerActions = require('../../../actions/players');

const mockGetBordering = (world, board, space) => {
  if (space === 'bar') {
    return Immutable.fromJS([{ board: 'foo', space: 'baz' }]);
  } else if (space === 'baz') {
    return Immutable.fromJS([{ board: 'foo', space: 'bada' }]);
  } else if (space === 'bada') {
    return Immutable.fromJS([{ board: 'foo', space: 'bing' }]);
  }

  return Immutable.fromJS([{ board: 'foo', space: 'boom' }]);
};

const mockIsBlocked = (game, players, board, space) => (space === 'baz' || space === 'bada' || space === 'bing');

const mocks = {
  blockedAvailable: Immutable.fromJS([{ board: 'foo', space: 'bozo' }]),
  allTouchingEnemies: jest.spyOn(baseUtil, 'allTouchingEnemies').mockImplementation(() => Immutable.fromJS([{ board: 'foo', space: 'boo' }, { board: 'foo', space: 'booo' }])),
  clearHighlightAndTriggers: jest.spyOn(baseUtil, 'clearHighlightAndTriggers'),
  getBordering: jest.spyOn(baseUtil, 'getBordering').mockImplementation(mockGetBordering),
  getCurrentInProgress: jest.spyOn(baseUtil, 'getCurrentInProgress').mockImplementation(() => Immutable.fromJS([{ board: 'foo', space: 'bang', player: 1, modifiers: [] }])),
  isBlocked: jest.spyOn(baseUtil, 'isBlocked').mockImplementation(mockIsBlocked),
  whichPlayerPiece: jest.spyOn(baseUtil, 'whichPlayerPiece').mockImplementation(() => 1),
  whichPlayerPieceTemp: jest.spyOn(baseUtil, 'whichPlayerPieceTemp').mockImplementation(() => 1),
  addTriggers: jest.spyOn(gameActions, 'addTriggers'),
  appendCurrentAction: jest.spyOn(gameActions, 'appendCurrentAction'),
  updateCanConfirm: jest.spyOn(gameActions, 'updateCanConfirm'),
  updateCurrentAction: jest.spyOn(gameActions, 'updateCurrentAction'),
  updateHighlighted: jest.spyOn(gameActions, 'updateHighlighted'),
  updatePrompt: jest.spyOn(gameActions, 'updatePrompt'),
  updatePieces: jest.spyOn(playerActions, 'updatePieces')
};

const mockGame = Immutable.fromJS({
  playerTurn: 0
});
const mockWorld = Immutable.fromJS({});
const mockPlayers = Immutable.fromJS([
  {
    pieces: [
      { board: 'foo', space: 'baz', modifiers: [] },
      { board: 'foo', space: 'bada', modifiers: [] }
    ]
  },
  {
    pieces: [
      { board: 'foo', space: 'bar', modifiers: [] },
      { board: 'foo', space: 'bing', modifiers: [] }
    ]
  }
]);

const mockState = {
  game: mockGame,
  world: mockWorld,
  players: mockPlayers
};

jest.mock('../../../utils/store', () => ({
  getState: () => mockState,
  dispatch: () => {}
}));

jest.mock('../../../game_logic/base_util', () => ({
  allTouchingEnemies: () => mocks.allTouchingEnemies,
  clearHighlightAndTriggers: () => mocks.clearHighlightAndTriggers,
  getBordering: () => mocks.getBordering,
  getCurrentInProgress: () => mocks.getCurrentInProgress,
  isBlocked: () => mocks.isBlocked,
  whichPlayerPiece: () => mocks.whichPlayerPiece,
  whichPlayerPieceTemp: () => mocks.whichPlayerPieceTemp
}));

jest.mock('../../../game_logic/move_defaults', () => ({
  blockedAvailable: () => mocks.blockedAvailable
}));

jest.mock('../../boards/boards-enum', () => ({
  foo: { get: () => 'foo' }
}));

describe('yellowMoveAvailable & yellowMoveBlockedAvailable', () => {
  const result = Immutable.fromJS([{ board: 'foo', space: 'bozo' }, { board: 'foo', space: 'boom' }]);

  it('should return correct list of available spaces', () =>
    expect(yellowMoveBlockedAvailable(mockGame, mockWorld, mockPlayers, { get: () => 'foo' }, 'bar')).toEqual(result));
});

describe('yellowSpecialInit & yellowSpecialEntryPoint', () => {
  beforeAll(() => yellowSpecialInit());

  it('should call updateCurrentAction', () =>
    expect(mocks.updateCurrentAction).toHaveBeenCalledWith({ type: 'SPECIAL', role: 'The First Voltaic Church', data: {} }));

  it('should call updateHighlighted', () =>
    expect(mocks.updateHighlighted).toHaveBeenCalledWith(Immutable.fromJS(['foo_baz_primary', 'foo_bada_primary', 'foo_boo_primary', 'foo_booo_primary'])));

  it('should call addTriggers', () =>
    expect(mocks.addTriggers.mock.calls[0][0])
      .toEqual(Immutable.Map({
        foo_baz: expect.any(Function),
        foo_bada: expect.any(Function),
        foo_boo: expect.any(Function),
        foo_booo: expect.any(Function)
      })));

  it('should call updatePrompt', () =>
    expect(mocks.updatePrompt).toHaveBeenCalled());

  afterAll(() => {
    mocks.addTriggers.mockReset();
    mocks.updateCurrentAction.mockReset();
    mocks.updateHighlighted.mockReset();
    mocks.updatePrompt.mockReset();
  });
});

describe('yellowChoosePiece', () => {
  beforeAll(() => yellowChoosePiece('foo_bar'));

  it('should call clearHighlightAndTriggers', () =>
    expect(mocks.clearHighlightAndTriggers).toHaveBeenCalled());

  it('should call appendCurrentAction', () =>
    expect(mocks.appendCurrentAction).toHaveBeenCalledWith({ to: { board: 'foo', space: 'bar' }, player: 1, modifiers: Immutable.List() }));

  it('should call updatePieces', () => {
    expect(mocks.updatePieces.mock.calls[0][0]).toEqual(1);
    expect(mocks.updatePieces.mock.calls[0][1]).toEqual(Immutable.fromJS([{ board: 'foo', space: 'bing', modifiers: [] }]));
  });

  it('should call updateHighlighted', () =>
    expect(mocks.updateHighlighted.mock.calls[0][0]).toEqual(Immutable.List(['foo_boom_primary', 'foo_bar_secondary'])));

  it('should call addTriggers', () =>
    expect(mocks.addTriggers.mock.calls[0][0])
      .toEqual(Immutable.Map({ foo_boom: expect.any(Function) })));

  it('should call updatePrompt', () =>
    expect(mocks.updatePrompt).toHaveBeenCalled());

  afterAll(() => {
    mocks.clearHighlightAndTriggers.mockReset();
    mocks.addTriggers.mockReset();
    mocks.appendCurrentAction.mockReset();
    mocks.updateHighlighted.mockReset();
    mocks.updatePrompt.mockReset();
    mocks.updatePieces.mockReset();
  });
});

describe('yellowChooseSpace', () => {
  beforeAll(() => yellowChooseSpace('foo_bar', 'foo_baz'));

  it('should call clearHighlightAndTriggers', () =>
    expect(mocks.clearHighlightAndTriggers).toHaveBeenCalled());

  it('should call appendCurrentAction', () =>
    expect(mocks.appendCurrentAction).toHaveBeenCalledWith({ from: { board: 'foo', space: 'baz' }, to: { board: 'foo', space: 'bar' }, player: 1 }));

  it('should call updateCanConfirm', () =>
    expect(mocks.updateCanConfirm).toHaveBeenCalledWith(true));

  it('should call updatePrompt', () =>
    expect(mocks.updatePrompt).toHaveBeenCalled());

  afterAll(() => {
    mocks.clearHighlightAndTriggers.mockReset();
    mocks.appendCurrentAction.mockReset();
    mocks.updateCanConfirm.mockReset();
    mocks.updatePrompt.mockReset();
  });
});

describe('yellowSpecialConfirm', () => {
  beforeAll(() => yellowSpecialConfirm());

  const result = Immutable.fromJS([
    { board: 'foo', space: 'bar', modifiers: [] },
    { board: 'foo', space: 'bing', modifiers: [] },
    { board: 'foo', space: 'bang', player: 1, modifiers: [] }
  ]);

  it('should call updatePieces', () => {
    expect(mocks.updatePieces.mock.calls[0][0]).toEqual(1);
    expect(mocks.updatePieces.mock.calls[0][1]).toEqual(result);
  });

  afterAll(() => {
    mocks.updatePieces.mockReset();
  });
});
