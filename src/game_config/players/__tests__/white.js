import Immutable from 'immutable';
import {
  whiteSpecialPiecesCanStillMove,
  findAvailablePieces,
  whiteSpecialMovePiecesToTemporary,
  whiteSpecialInit,
  whiteSpecialChooseEye,
  whiteSpecialChoosePiece,
  whiteSpecialAddPotentialMove,
  whiteSpecialConfirm
} from '../white';

jest.unmock('../white');
jest.unmock('../../../game_logic/base_util');
const baseUtil = require('../../../game_logic/base_util');

jest.unmock('../../../actions/game');
const gameActions = require('../../../actions/game');

jest.unmock('../../../actions/players');
const playerActions = require('../../../actions/players');

const mockGetBordering = (world, board, space) => {
  if (space === 'bang') {
    return Immutable.fromJS([
      { board: 'foo', space: 'bar' }
    ]);
  } else if (space === 'bar') {
    return Immutable.fromJS([
      { board: 'foo', space: 'bang' },
      { board: 'foo', space: 'baz' },
      { board: 'foo', space: 'boo' }
    ]);
  } else if (space === 'baz') {
    return Immutable.fromJS([
      { board: 'foo', space: 'bar' },
      { board: 'foo', space: 'boo' }
    ]);
  } else if (space === 'boo') {
    return Immutable.fromJS([
      { board: 'foo', space: 'bar' },
      { board: 'foo', space: 'baz' }
    ]);
  }

  return Immutable.fromJS([{ board: 'foo', space: 'bing' }]);
};

const mocks = {
  clearHighlightAndTriggers: jest.spyOn(baseUtil, 'clearHighlightAndTriggers'),
  getBordering: jest.spyOn(baseUtil, 'getBordering').mockImplementation(mockGetBordering),
  getCurrentInProgress: jest.spyOn(baseUtil, 'getCurrentInProgress').mockImplementation(() => Immutable.fromJS([{ board: 'foo', space: 'bang', player: 1 }])),
  isOccupied: jest.spyOn(baseUtil, 'isOccupied').mockImplementation(() => true),
  whichPlayerPiece: jest.spyOn(baseUtil, 'whichPlayerPiece').mockImplementation(() => 1),
  available: Immutable.fromJS([{ board: 'foo', space: 'bar' }]),
  addTriggers: jest.spyOn(gameActions, 'addTriggers'),
  appendCurrentAction: jest.spyOn(gameActions, 'appendCurrentAction'),
  updateCanConfirm: jest.spyOn(gameActions, 'updateCanConfirm'),
  updateCurrentAction: jest.spyOn(gameActions, 'updateCurrentAction'),
  updateHighlighted: jest.spyOn(gameActions, 'updateHighlighted'),
  updatePrompt: jest.spyOn(gameActions, 'updatePrompt'),
  updatePiecesGroups: jest.spyOn(playerActions, 'updatePiecesGroups')
};

const mockGame = Immutable.fromJS({
  playerTurn: 0,
  currentAction: {
    data: {
      moves: [
        { to: { board: 'foo', space: 'bar' }, player: 0, modifiers: [] },
        { from: { board: 'foo', space: 'bar' }, to: { board: 'foo', space: 'baz' }, player: 0, modifiers: [] },
        { from: { board: 'foo', space: 'baz' }, to: { board: 'foo', space: 'bam' }, player: 0, modifiers: [] },
        { from: { board: 'foo', space: 'bam' }, to: { board: 'foo', space: 'boom' }, player: 0, modifiers: [] },
        { to: { board: 'foo', space: 'bing' }, player: 0, modifiers: [] },
        { from: { board: 'foo', space: 'bing' }, to: { board: 'foo', space: 'bang' }, player: 0, modifiers: [] }
      ]
    }
  }
});
const mockWorld = Immutable.fromJS({});
const mockPlayers = Immutable.fromJS([
  {
    pieces: [
      { board: 'foo', space: 'bada', modifiers: [] }
    ]
  },
  {
    pieces: [
      { board: 'foo', space: 'bar', modifiers: [] },
      { board: 'foo', space: 'bang', modifiers: [] }
    ]
  }
]);

const mockState = {
  game: mockGame,
  world: mockWorld,
  players: mockPlayers
};

jest.mock('../../../utils/store', () => ({
  getState: () => mockState,
  dispatch: () => {}
}));

jest.mock('../../../game_logic/base_util', () => ({
  clearHighlightAndTriggers: () => mocks.clearHighlightAndTriggers,
  getBordering: () => mocks.getBordering,
  getCurrentInProgress: () => mocks.getCurrentInProgress,
  isOccupied: () => mocks.isOccupied,
  whichPlayerPiece: () => mocks.whichPlayerPiece
}));

jest.mock('../../../game_logic/move_defaults', () => ({
  available: () => mocks.available
}));

describe('whiteSpecialPiecesCanStillMove', () => {
  it('should return correct list of pieces', () =>
    expect(whiteSpecialPiecesCanStillMove(3)).toEqual(Immutable.fromJS([{ board: 'foo', space: 'bang', player: 0, modifiers: [] }])));
});

describe('findAvailablePieces', () => {
  it('should return correct list of pieces', () =>
    expect(findAvailablePieces('foo_bang')).toEqual(Immutable.fromJS([{ board: 'foo', space: 'bar' }, { board: 'foo', space: 'boo' }, { board: 'foo', space: 'baz' }])));
});

describe('whiteSpecialMovePiecesToTemporary', () => {
  beforeAll(() => whiteSpecialMovePiecesToTemporary(Immutable.fromJS([{ board: 'foo', space: 'bar' }])));

  it('should call appendCurrentAction', () =>
    expect(mocks.appendCurrentAction).toHaveBeenCalledWith({ to: { board: 'foo', space: 'bar' }, player: 1, modifiers: Immutable.List() }));

  it('should call updatePiecesGroups', () =>
    expect(mocks.updatePiecesGroups.mock.calls[0][0].get(1)).toEqual(Immutable.fromJS([{ board: 'foo', space: 'bang', modifiers: [], player: 1 }])));

  afterAll(() => {
    mocks.appendCurrentAction.mockReset();
    mocks.updatePiecesGroups.mockReset();
  });
});

describe('whiteSpecialInit & whiteSpecialEntryPoint', () => {
  beforeAll(() => whiteSpecialInit());

  it('should call updateCurrentAction', () =>
    expect(mocks.updateCurrentAction).toHaveBeenCalledWith({ type: 'SPECIAL', role: 'Cult of Boreas', data: {} }));

  it('should call updateHighlighted', () =>
    expect(mocks.updateHighlighted).toHaveBeenCalledWith(Immutable.List(['foo_bada_primary'])));

  it('should call addTriggers', () =>
    expect(mocks.addTriggers.mock.calls[0][0])
      .toEqual(Immutable.Map({ foo_bada: expect.any(Function) })));

  it('should call updatePrompt', () =>
    expect(mocks.updatePrompt).toHaveBeenCalled());

  afterAll(() => {
    mocks.addTriggers.mockReset();
    mocks.updateCurrentAction.mockReset();
    mocks.updateHighlighted.mockReset();
    mocks.updatePrompt.mockReset();
  });
});

describe('whiteSpecialChooseEye', () => {
  beforeAll(() => whiteSpecialChooseEye('foo', { availPieceFunc: () => Immutable.fromJS([{ board: 'foo', space: 'bar' }]) }));

  it('should call clearHighlightAndTriggers', () =>
    expect(mocks.clearHighlightAndTriggers).toHaveBeenCalled());

  it('should call updateHighlighted', () =>
    expect(mocks.updateHighlighted).toHaveBeenCalledWith(Immutable.List(['foo_bar_primary'])));

  it('should call addTriggers', () =>
    expect(mocks.addTriggers.mock.calls[0][0])
      .toEqual(Immutable.Map({ foo_bar: expect.any(Function) })));

  it('should call updatePrompt', () =>
    expect(mocks.updatePrompt).toHaveBeenCalled());

  afterAll(() => {
    mocks.clearHighlightAndTriggers.mockReset();
    mocks.addTriggers.mockReset();
    mocks.updateHighlighted.mockReset();
    mocks.updatePrompt.mockReset();
  });
});

describe('whiteSpecialChoosePiece', () => {
  beforeAll(() => whiteSpecialChoosePiece('foo_bang', { gale_move_limit: 3 }));

  it('should call clearHighlightAndTriggers', () =>
    expect(mocks.clearHighlightAndTriggers).toHaveBeenCalled());

  it('should call updateHighlighted', () =>
    expect(mocks.updateHighlighted).toHaveBeenCalledWith(Immutable.List(['foo_bar_primary'])));

  it('should call addTriggers', () =>
    expect(mocks.addTriggers.mock.calls[0][0])
      .toEqual(Immutable.Map({ foo_bar: expect.any(Function) })));

  it('should call updatePrompt', () =>
    expect(mocks.updatePrompt).toHaveBeenCalled());

  afterAll(() => {
    mocks.clearHighlightAndTriggers.mockReset();
    mocks.addTriggers.mockReset();
    mocks.updateHighlighted.mockReset();
    mocks.updatePrompt.mockReset();
  });
});

describe('whiteSpecialAddPotentialMove', () => {
  beforeAll(() => whiteSpecialAddPotentialMove('foo_bang', 'foo_bar', {}));

  it('should call appendCurrentAction', () =>
    expect(mocks.appendCurrentAction).toHaveBeenCalledWith({ from: { board: 'foo', space: 'bang' }, to: { board: 'foo', space: 'bar' }, player: 1 }));

  it('should call clearHighlightAndTriggers', () =>
    expect(mocks.clearHighlightAndTriggers).toHaveBeenCalled());

  it('should call updateCanConfirm', () =>
    expect(mocks.updateCanConfirm).toHaveBeenCalledWith(true));

  afterAll(() => {
    mocks.clearHighlightAndTriggers.mockReset();
    mocks.appendCurrentAction.mockReset();
    mocks.updateCanConfirm.mockReset();
  });
});

describe('whiteSpecialConfirm', () => {
  beforeAll(() => whiteSpecialConfirm());

  it('should call updatePiecesGroups', () =>
    expect(mocks.updatePiecesGroups).toHaveBeenCalled());

  afterAll(() => {
    mocks.updatePiecesGroups.mockReset();
  });
});
