import Immutable from 'immutable';
import {
  purpleAvailableSpaces,
  purpleBlockedAvailableSpaces,
  purpleSpecialInit,
  purpleSpecialChoosePiece,
  purpleSpecialChooseSpace,
  purpleSpecialConfirm
} from '../purple';

jest.unmock('../purple');
jest.unmock('../../../game_logic/base_util');
const baseUtil = require('../../../game_logic/base_util');

jest.unmock('../../../actions/game');
const gameActions = require('../../../actions/game');

jest.unmock('../../../actions/players');
const playerActions = require('../../../actions/players');

const mocks = {
  allTouchingEnemies: jest.spyOn(baseUtil, 'allTouchingEnemies').mockImplementation(() => Immutable.fromJS([{ board: 'foo', space: 'bar' }])),
  clearHighlightAndTriggers: jest.spyOn(baseUtil, 'clearHighlightAndTriggers'),
  getBordering: jest.spyOn(baseUtil, 'getBordering').mockImplementation(() => Immutable.fromJS([{ board: 'foo', space: 'baz' }])),
  getCurrentInProgress: jest.spyOn(baseUtil, 'getCurrentInProgress').mockImplementation(() => Immutable.fromJS([{ player: 1, board: 'foo', space: 'baz' }])),
  getTouching: jest.spyOn(baseUtil, 'getTouching').mockImplementation(() => Immutable.fromJS([{ board: 'foo', space: 'boo' }])),
  isBlocked: jest.spyOn(baseUtil, 'isBlocked').mockImplementation(() => false),
  isOccupied: jest.spyOn(baseUtil, 'isOccupied').mockImplementation(() => false),
  isTouching: jest.spyOn(baseUtil, 'isTouching').mockImplementation(() => false),
  whichPlayerPiece: jest.spyOn(baseUtil, 'whichPlayerPiece').mockImplementation(() => 1),
  addTriggers: jest.spyOn(gameActions, 'addTriggers'),
  appendCurrentAction: jest.spyOn(gameActions, 'appendCurrentAction'),
  updateCanConfirm: jest.spyOn(gameActions, 'updateCanConfirm'),
  updateCurrentAction: jest.spyOn(gameActions, 'updateCurrentAction'),
  updateHighlighted: jest.spyOn(gameActions, 'updateHighlighted'),
  updatePrompt: jest.spyOn(gameActions, 'updatePrompt'),
  updatePieces: jest.spyOn(playerActions, 'updatePieces')
};

const mockGame = Immutable.fromJS({
  playerTurn: 0,
  currentAction: {
    data: {
      moves: [
        { to: { board: 'foo', space: 'baz' }, player: 1, modifiers: [] }
      ]
    }
  }
});
const mockWorld = Immutable.fromJS({
  rooms: [{
    board: {
      name: 'foo',
      type: {
        spaces: { a1: {} }
      }
    }
  }]
});
const mockPlayers = Immutable.fromJS([
  {
    pieces: []
  },
  {
    pieces: [{
      board: 'foo',
      space: 'bar',
      modifiers: []
    }]
  }
]);

const mockState = {
  game: mockGame,
  world: mockWorld,
  players: mockPlayers
};

jest.mock('../../../utils/store', () => ({
  getState: () => mockState,
  dispatch: () => {}
}));

jest.mock('../../../game_logic/base_util', () => ({
  allTouchingEnemies: () => mocks.allTouchingEnemies,
  clearHighlightAndTriggers: () => mocks.clearHighlightAndTriggers,
  getBordering: () => mocks.getBordering,
  getCurrentInProgress: () => mocks.getCurrentInProgress,
  getTouching: () => mocks.getTouching,
  isBlocked: () => mocks.isBlocked,
  isOccupied: () => mocks.isOccupied,
  isTouching: () => mocks.isTouching,
  whichPlayerPiece: () => mocks.whichPlayerPiece
}));

describe('purpleAvailableSpaces', () => {
  const results = Immutable.fromJS([
    { board: 'foo', space: 'baz' },
    { board: 'foo', space: 'boo' }
  ]);

  it('should call isBlocked with all players', () => {
    purpleAvailableSpaces(mockGame, mockWorld, mockPlayers, 'foo', 'bar');
    expect(mocks.isBlocked.mock.calls[0][0]).toEqual(mockGame);
    expect(mocks.isBlocked.mock.calls[0][1]).toEqual(mockPlayers);
    expect(mocks.isBlocked.mock.calls[0][2]).toEqual(Immutable.Map({ name: 'foo' }));
    expect(mocks.isBlocked.mock.calls[0][3]).toEqual('baz');
  });

  it('should return correct available spaces', () =>
    expect(purpleAvailableSpaces(mockGame, mockWorld, mockPlayers, 'foo', 'bar')).toEqual(results));

  afterAll(() => {
    mocks.isBlocked.mockReset();
  });
});

describe('purpleBlockedAvailableSpaces', () => {
  const results = Immutable.fromJS([
    { board: 'foo', space: 'baz' },
    { board: 'foo', space: 'boo' }
  ]);

  it('should call isBlocked with first player', () => {
    purpleBlockedAvailableSpaces(mockGame, mockWorld, mockPlayers, 'foo', 'bar');
    expect(mocks.isBlocked.mock.calls[0][0]).toEqual(mockGame);
    expect(mocks.isBlocked.mock.calls[0][1]).toEqual(Immutable.List([mockPlayers.get(0)]));
    expect(mocks.isBlocked.mock.calls[0][2]).toEqual(Immutable.Map({ name: 'foo' }));
    expect(mocks.isBlocked.mock.calls[0][3]).toEqual('baz');
  });

  it('should return correct available spaces', () =>
    expect(purpleBlockedAvailableSpaces(mockGame, mockWorld, mockPlayers, 'foo', 'bar')).toEqual(results));

  afterAll(() => {
    mocks.isBlocked.mockReset();
  });
});

describe('purpleSpecialInit & purpleSpecialEntryPoint', () => {
  beforeAll(() => purpleSpecialInit());

  it('should call updateCurrentAction', () =>
    expect(mocks.updateCurrentAction).toHaveBeenCalledWith({ type: 'SPECIAL', role: 'Madrasa Qashani', data: {} }));

  it('should call allTouchingEnemies', () =>
    expect(mocks.allTouchingEnemies).toHaveBeenCalledWith(mockGame, mockWorld, mockPlayers));

  it('should call updateHighlighted', () =>
    expect(mocks.updateHighlighted).toHaveBeenCalledWith(Immutable.List(['foo_bar_primary'])));

  it('should call addTriggers', () =>
    expect(mocks.addTriggers.mock.calls[0][0])
      .toEqual(Immutable.Map({ foo_bar: expect.any(Function) })));

  it('should update the prompt', () =>
    expect(mocks.updatePrompt).toHaveBeenCalled());

  afterAll(() => {
    mocks.addTriggers.mockReset();
    mocks.updateCurrentAction.mockReset();
    mocks.updateHighlighted.mockReset();
    mocks.updatePrompt.mockReset();
  });
});

describe('purpleSpecialChoosePiece', () => {
  beforeAll(() => purpleSpecialChoosePiece('foo_bar'));

  it('should clear highlights', () =>
    expect(mocks.clearHighlightAndTriggers).toHaveBeenCalled());

  it('should call whichPlayerPiece', () =>
    expect(mocks.whichPlayerPiece).toHaveBeenCalledWith(mockPlayers, 'foo', 'bar'));

  it('should call updateHighlighted', () =>
    expect(mocks.updateHighlighted.mock.calls[0][0]).toEqual(Immutable.List(['foo_a1_primary', 'foo_bar_secondary'])));

  it('should call addTriggers', () =>
    expect(mocks.addTriggers.mock.calls[0][0])
      .toEqual(Immutable.Map({ foo_a1: expect.any(Function) })));

  it('should call appendCurrentAction', () =>
    expect(mocks.appendCurrentAction).toHaveBeenCalledWith({ to: { board: 'foo', space: 'bar' }, player: 1, modifiers: Immutable.List() }));

  it('should call updatePieces', () =>
    expect(mocks.updatePieces).toHaveBeenCalledWith(1, Immutable.List()));

  it('should update the prompt', () =>
    expect(mocks.updatePrompt).toHaveBeenCalled());

  afterAll(() => {
    mocks.clearHighlightAndTriggers.mockReset();
    mocks.addTriggers.mockReset();
    mocks.appendCurrentAction.mockReset();
    mocks.updateHighlighted.mockReset();
    mocks.updatePieces.mockReset();
    mocks.updatePrompt.mockReset();
  });
});

describe('purpleSpecialChooseSpace', () => {
  beforeAll(() => purpleSpecialChooseSpace('foo_bar'));

  it('should clear highlights', () =>
    expect(mocks.clearHighlightAndTriggers).toHaveBeenCalled());

  it('should call appendCurrentAction', () =>
    expect(mocks.appendCurrentAction).toHaveBeenCalledWith({ from: { board: 'foo', space: 'baz' }, to: { board: 'foo', space: 'bar' }, player: 1 }));

  it('should call updateCanConfirm', () =>
    expect(mocks.updateCanConfirm).toHaveBeenCalledWith(true));

  it('should update the prompt', () =>
    expect(mocks.updatePrompt).toHaveBeenCalled());

  afterAll(() => {
    mocks.clearHighlightAndTriggers.mockReset();
    mocks.appendCurrentAction.mockReset();
    mocks.updateCanConfirm.mockReset();
    mocks.updatePrompt.mockReset();
  });
});

describe('purpleSpecialConfirm', () => {
  beforeAll(() => purpleSpecialConfirm());

  it('should call updatePieces', () =>
    expect(mocks.updatePieces).toHaveBeenCalledWith(1, Immutable.fromJS([{ board: 'foo', space: 'bar', modifiers: [] }, { player: 1, board: 'foo', space: 'baz' }])));

  afterAll(() => {
    mocks.updatePieces.mockReset();
  });
});
