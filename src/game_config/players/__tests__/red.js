import Immutable from 'immutable';
import {
  redSpecialGetOwnBordering,
  redSpecialGetEnemyTouching,
  redSpecialInit,
  redSpecialChoosePiece,
  redSpecialConfirm
} from '../red';

jest.unmock('../red');

jest.unmock('../../../game_logic/base_util');
const baseUtil = require('../../../game_logic/base_util');

jest.unmock('../../../actions/game');
const gameActions = require('../../../actions/game');

jest.unmock('../../../actions/players');
const playerActions = require('../../../actions/players');

const mocks = {
  addDamage: jest.spyOn(baseUtil, 'addDamage'),
  clearHighlightAndTriggers: jest.spyOn(baseUtil, 'clearHighlightAndTriggers'),
  getBordering: jest.spyOn(baseUtil, 'getBordering').mockImplementation(() => Immutable.fromJS([{ board: 'foo', space: 'boo' }])),
  getTouching: jest.spyOn(baseUtil, 'getTouching').mockImplementation(() => Immutable.fromJS([{ board: 'foo', space: 'bam' }])),
  isOccupied: jest.spyOn(baseUtil, 'isOccupied').mockImplementation(() => true),
  whichPlayerPiece: jest.spyOn(baseUtil, 'whichPlayerPiece').mockImplementation(() => 1),
  addTriggers: jest.spyOn(gameActions, 'addTriggers'),
  appendCurrentAction: jest.spyOn(gameActions, 'appendCurrentAction'),
  updateCanConfirm: jest.spyOn(gameActions, 'updateCanConfirm'),
  updateCurrentAction: jest.spyOn(gameActions, 'updateCurrentAction'),
  updateHighlighted: jest.spyOn(gameActions, 'updateHighlighted'),
  updatePrompt: jest.spyOn(gameActions, 'updatePrompt'),
  updateModifier: jest.spyOn(playerActions, 'updateModifier'),
  updatePieces: jest.spyOn(playerActions, 'updatePieces')
};

const mockGame = Immutable.fromJS({
  playerTurn: 0,
  currentAction: {
    data: {
      moves: [
        { to: { board: 'foo', space: 'bar' } },
        { to: { board: 'foo', space: 'baz' } }
      ]
    }
  }
});
const mockWorld = Immutable.fromJS({});
const mockPlayers = Immutable.fromJS([
  {
    pieces: [
      { board: 'foo', space: 'bing', modifiers: [] }
    ],
    modifiers: { redFuel: 1 }
  },
  {
    pieces: [
      { board: 'foo', space: 'boo', modifiers: [] },
      { board: 'foo', space: 'bam', modifiers: [] }
    ]
  }
]);

const mockState = {
  game: mockGame,
  world: mockWorld,
  players: mockPlayers
};

jest.mock('../../../utils/store', () => ({
  getState: () => mockState,
  dispatch: () => {}
}));

jest.mock('../../../game_logic/base_util', () => ({
  addDamage: () => mocks.addDamage,
  clearHighlightAndTriggers: () => mocks.clearHighlightAndTriggers,
  getBordering: () => mocks.getBordering,
  getTouching: () => mocks.getTouching,
  isOccupied: () => mocks.isOccupied,
  whichPlayerPiece: () => mocks.whichPlayerPiece
}));

describe('redSpecialGetOwnBordering', () => {
  const result = Immutable.OrderedMap({ foo_boo: Immutable.Map({ board: 'foo', space: 'boo' }) });

  it('should return bordering spaces', () =>
    expect(redSpecialGetOwnBordering()).toEqual(result));
});

describe('redSpecialGetEnemyTouching', () => {
  const result = Immutable.OrderedMap({
    foo_boo: Immutable.Map({ board: 'foo', space: 'boo' }),
    foo_bam: Immutable.Map({ board: 'foo', space: 'bam' })
  });

  it('should return bordering spaces', () =>
    expect(redSpecialGetEnemyTouching()).toEqual(result));
});

describe('redSpecialInit & redSpecialEntryPoint', () => {
  beforeAll(() => redSpecialInit());

  it('should call updateCurrentAction', () =>
    expect(mocks.updateCurrentAction).toHaveBeenCalledWith({ type: 'SPECIAL', role: 'Ignis Rex', data: {} }));

  it('should call updateHighlighted', () =>
    expect(mocks.updateHighlighted).toHaveBeenCalledWith(Immutable.List(['foo_bing_primary'])));

  it('should call addTriggers', () =>
    expect(mocks.addTriggers.mock.calls[0][0])
      .toEqual(Immutable.Map({ foo_bing: expect.any(Function) })));

  it('should call updatePrompt', () =>
    expect(mocks.updatePrompt).toHaveBeenCalled());

  afterAll(() => {
    mocks.addTriggers.mockReset();
    mocks.updateCurrentAction.mockReset();
    mocks.updateHighlighted.mockReset();
    mocks.updatePrompt.mockReset();
  });
});

describe('redSpecialChoosePiece', () => {
  beforeAll(() => redSpecialChoosePiece('foo_bar'));

  it('should call clearHighlightAndTriggers', () =>
    expect(mocks.clearHighlightAndTriggers).toHaveBeenCalled());

  it('should call appendCurrentAction', () =>
    expect(mocks.appendCurrentAction).toHaveBeenCalledWith({ to: { board: 'foo', space: 'bar' }, player: 0, modifiers: [] }));

  it('should call updatePieces', () => {
    expect(mocks.updatePieces.mock.calls[0][0]).toBe(0);
    expect(mocks.updatePieces.mock.calls[0][1]).toEqual(Immutable.fromJS([{ board: 'foo', space: 'bing', modifiers: [] }]));
  });

  it('should call addTriggers', () =>
    expect(mocks.addTriggers.mock.calls[0][0])
      .toEqual(Immutable.Map({ foo_boo: expect.any(Function) })));

  it('should call updateHighlighted', () =>
    expect(mocks.updateHighlighted).toHaveBeenCalledWith(Immutable.List(['foo_boo_primary', 'foo_boo_secondary', 'foo_bam_secondary'])));

  it('should call updateCanConfirm', () =>
    expect(mocks.updateCanConfirm).toHaveBeenCalledWith(true));

  afterAll(() => {
    mocks.clearHighlightAndTriggers.mockReset();
    mocks.addTriggers.mockReset();
    mocks.appendCurrentAction.mockReset();
    mocks.updateCanConfirm.mockReset();
    mocks.updateHighlighted.mockReset();
    mocks.updatePieces.mockReset();
  });
});

describe('redSpecialConfirm & redSpecialConfirmEntryPoint', () => {
  beforeAll(() => redSpecialConfirm());

  it('should call updatePieces', () =>
    expect(mocks.updatePieces).toHaveBeenCalledWith(1, Immutable.List()));

  it('should call addDamage', () =>
    expect(mocks.addDamage).toHaveBeenCalledWith(1, 2));

  it('should call updateModifier', () =>
    expect(mocks.updateModifier).toHaveBeenCalledWith(0, 'redFuel', 3));

  afterAll(() => {
    mocks.addDamage.mockReset();
    mocks.updatePieces.mockReset();
    mocks.updateModifier.mockReset();
  });
});
