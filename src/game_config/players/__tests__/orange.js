import Immutable from 'immutable';
import { orangeSpecialInit, orangeSpecialConfirm } from '../orange';

jest.unmock('../orange');

jest.unmock('../../../actions/game');
const gameActions = require('../../../actions/game');

jest.unmock('../../../actions/players');
const playerActions = require('../../../actions/players');

const mocks = {
  updateCanConfirm: jest.spyOn(gameActions, 'updateCanConfirm'),
  updateCurrentAction: jest.spyOn(gameActions, 'updateCurrentAction'),
  updatePrompt: jest.spyOn(gameActions, 'updatePrompt'),
  updateModifier: jest.spyOn(playerActions, 'updateModifier')
};

const mockGame = Immutable.fromJS({
  playerTurn: 0
});
const mockWorld = Immutable.fromJS({});
const mockPlayers = Immutable.fromJS([
  {
    modifiers: {
      orangeBalance: 1
    }
  }
]);

const mockState = {
  game: mockGame,
  world: mockWorld,
  players: mockPlayers
};

jest.mock('../../../utils/store', () => ({
  getState: () => mockState,
  dispatch: () => {}
}));

describe('orangeSpecialInit', () => {
  beforeAll(() => orangeSpecialInit());

  it('should update the prompt', () =>
    expect(mocks.updatePrompt).toHaveBeenCalled());

  it('should call updateCurrentAction', () =>
    expect(mocks.updateCurrentAction).toHaveBeenCalledWith({ type: 'SPECIAL', role: 'Societe Pendulaire', data: {} }));

  it('should call updateCanConfirm', () =>
    expect(mocks.updateCanConfirm).toHaveBeenCalledWith(true));

  afterAll(() => {
    mocks.updateCanConfirm.mockReset();
    mocks.updateCurrentAction.mockReset();
    mocks.updatePrompt.mockReset();
  });
});

describe('orangeSpecialConfirm', () => {
  beforeAll(() => orangeSpecialConfirm());

  it('should call updateModifier', () =>
    expect(mocks.updateModifier).toHaveBeenCalledWith(0, 'orangeBalance', 2));

  afterAll(() => {
    mocks.updateModifer.mockReset();
  });
});
