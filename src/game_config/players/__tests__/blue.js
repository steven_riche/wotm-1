import Immutable from 'immutable';
import { blueSpecialInit, blueSpecialConfirm } from '../blue';

jest.unmock('../blue');

jest.unmock('../../../actions/game');
const gameActions = require('../../../actions/game');

jest.unmock('../../../actions/players');
const playerActions = require('../../../actions/players');

const mocks = {
  updateCanConfirm: jest.spyOn(gameActions, 'updateCanConfirm'),
  updateCurrentAction: jest.spyOn(gameActions, 'updateCurrentAction'),
  updatePrompt: jest.spyOn(gameActions, 'updatePrompt'),
  updateModifier: jest.spyOn(playerActions, 'updateModifier')
};

const mockGame = Immutable.fromJS({
  playerTurn: 0
});
const mockWorld = Immutable.Map();
const mockPlayers = Immutable.fromJS([
  {
    modifiers: { bluePressure: 1 }
  }
]);

const mockState = {
  game: mockGame,
  world: mockWorld,
  players: mockPlayers
};

jest.mock('../../../utils/store', () => ({
  getState: () => mockState,
  dispatch: () => {}
}));

describe('blueSpecialInit', () => {
  beforeAll(() => blueSpecialInit());

  it('should call updatePrompt', () =>
    expect(mocks.updatePrompt).toHaveBeenCalled());

  it('should call updateCurrentAction', () =>
    expect(mocks.updateCurrentAction).toHaveBeenCalledWith({ type: 'SPECIAL', role: 'Knights of the Azure Main', data: {} }));

  it('should call updateCanConfirm', () =>
    expect(mocks.updateCanConfirm).toHaveBeenCalledWith(true));

  afterAll(() => {
    mocks.updatePrompt.mockReset();
    mocks.updateCurrentAction.mockReset();
    mocks.updateCanConfirm.mockReset();
  });
});

describe('blueSpecialConfirm', () => {
  beforeAll(() => blueSpecialConfirm());

  it('should call updateModifier', () =>
    expect(mocks.updateModifier).toHaveBeenCalledWith(0, 'bluePressure', 3));

  afterAll(() => {
    mocks.updateModifier.mockReset();
  });
});
