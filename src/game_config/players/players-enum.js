import Black from './black';
import White from './white';
import Purple from './purple';
import Blue from './blue';
import Red from './red';
import Yellow from './yellow';
import Green from './green';
import Orange from './orange';

export default [
  Black, White, Purple, Blue, Red, Yellow, Green, Orange
];
