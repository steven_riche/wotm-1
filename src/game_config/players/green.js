import Immutable from 'immutable';
import parse from 'parse-svg-path';
import scale from 'scale-svg-path';
import serialize from 'serialize-svg-path';
import store from '../../utils/store';
import GreenStats from '../../components/player-specific/green-stats';
import {
  addTriggers,
  appendCurrentAction,
  updateCurrentAction,
  updateHighlighted,
  updatePrompt,
  updateSpecialActionNeeded
} from '../../actions/game';
import { updateDamage, updateModifier, updatePieces } from '../../actions/players';
import Boards from '../boards/boards-enum';
import { clearHighlightAndTriggers, getBordering, isOccupied, whichPlayerPiece } from '../../game_logic/base_util';
import addDefaults from '../../game_logic/add_defaults';
import moveDefaults from '../../game_logic/move_defaults';
import captureDefaults from '../../game_logic/capture_defaults';
import { damageToFreeAction } from '../constants/constants';

const greenSpecialInit = () => {
  const { game } = store.getState();
  store.dispatch(updateCurrentAction({ type: 'SPECIAL', role: 'Kundi Mbegu', data: {} }));
  greenSpecialEntryPoint({ triggerIncrease: () => triggerIncrease(game) });
};

const greenSpecialEntryPoint = (config) => {
  const { game, players } = store.getState();
  const highlightedSpaces = players.getIn([game.get('playerTurn'), 'pieces'])
    .filter(piece => piece.get('space') !== 'captured');
  store.dispatch(updateHighlighted(highlightedSpaces.map(space => `${space.get('board')}_${space.get('space')}_primary`)));

  let triggers = Immutable.Map();
  highlightedSpaces.forEach((space) => {
    triggers = triggers.set(`${space.get('board')}_${space.get('space')}`, () => greenSpecialChoosePiece(`${space.get('board')}_${space.get('space')}`, config));
  });
  store.dispatch(addTriggers(triggers));
  store.dispatch(updatePrompt('Please choose a piece that will attack and eat another piece'));
};

const greenSpecialChoosePiece = (space, config) => {
  const { world, players } = store.getState();
  clearHighlightAndTriggers();
  const spaceArray = space.split('_');
  const targetPieces = getBordering(world, Immutable.fromJS(Boards[spaceArray[0]]), spaceArray[1])
    .filter(fSpace => isOccupied(players, Immutable.fromJS(Boards[fSpace.get('board')]), fSpace.get('space')));

  store.dispatch(updateHighlighted(targetPieces.map(tSpace => `${tSpace.get('board')}_${tSpace.get('space')}_primary`).push(`${space}_secondary`)));

  let triggers = Immutable.Map();
  targetPieces.forEach((tSpace) => {
    triggers = triggers.set(`${tSpace.get('board')}_${tSpace.get('space')}`, () => greenSpecialChooseTarget(`${tSpace.get('board')}_${tSpace.get('space')}`, space, config));
  });
  store.dispatch(addTriggers(triggers));
  store.dispatch(updatePrompt('Please choose a target to eat'));
};

const greenSpecialChooseTarget = (targetSpace, attackingSpace, config) => {
  const { game, players } = store.getState();
  clearHighlightAndTriggers();
  const targetSpaceArray = targetSpace.split('_');
  const attackingSpaceArray = attackingSpace.split('_');
  const targetPlayer = whichPlayerPiece(players, targetSpaceArray[0], targetSpaceArray[1]);

  Promise.resolve(
    store.dispatch(appendCurrentAction({
      from: {
        board: attackingSpaceArray[0],
        space: attackingSpaceArray[1]
      },
      to: {
        board: targetSpaceArray[0],
        space: targetSpaceArray[1]
      },
      player: game.get('playerTurn'),
      formerPlayer: targetPlayer,
      modifiers: players.getIn([game.get('playerTurn'), 'pieces']).find(piece => piece.get('board') === attackingSpaceArray[0] && piece.get('space') === attackingSpaceArray[1]).get('modifiers')
    }))
  )
  .then(() =>
    store.dispatch(updatePieces(
      game.get('playerTurn'),
      players.getIn([game.get('playerTurn'), 'pieces']).filter(piece => !(piece.get('board') === attackingSpaceArray[0] && piece.get('space') === attackingSpaceArray[1]))
    ))
  )
  .then(() => {
    const newState = store.getState();
    return store.dispatch(updatePieces(
      targetPlayer,
      newState.players.getIn([targetPlayer, 'pieces']).filter(piece => !(piece.get('board') === targetSpaceArray[0] && piece.get('space') === targetSpaceArray[1]))
    ));
  })
  .then(config.triggerIncrease)
  .then(() => store.dispatch(updatePrompt('Please update your stats')));
};

const greenSpecialConfirm = () => {
  const { game, players } = store.getState();
  const tempPiece = game.getIn(['currentAction', 'data', 'moves', 0]);
  const targetPlayer = tempPiece.get('formerPlayer');

  if (targetPlayer !== game.get('playerTurn')) {
    let newDamage = parseInt(players.getIn([targetPlayer, 'damageCounter']), 10) + 1;
    newDamage = (newDamage < damageToFreeAction) ? newDamage : damageToFreeAction;
    store.dispatch(updateDamage(targetPlayer, newDamage));
  }

  const newPiece = Immutable.fromJS({
    board: tempPiece.getIn(['to', 'board']),
    space: tempPiece.getIn(['to', 'space']),
    modifiers: tempPiece.get('modifiers')
  });

  store.dispatch(updatePieces(game.get('playerTurn'), players.getIn([game.get('playerTurn'), 'pieces']).push(newPiece)));
};

const triggerIncrease = (game) => {
  store.dispatch(updateModifier(game.get('playerTurn'), 'greenFuel', additionalIncrease()));
  store.dispatch(updateSpecialActionNeeded(true));
};

const additionalAdd = () => {
  const { game, players } = store.getState();
  return players.getIn([game.get('playerTurn'), 'modifiers', 'greenAdd']) || 1;
};

const additionalMove = () => {
  const { game, players } = store.getState();
  return players.getIn([game.get('playerTurn'), 'modifiers', 'greenMove']) || 2;
};

const additionalIncrease = () => {
  const { game, players } = store.getState();
  return players.getIn([game.get('playerTurn'), 'modifiers', 'greenIncrease']) || 3;
};

const add = Object.assign({}, addDefaults,
  {
    canAddMore: (...args) => addDefaults.canAddMore(additionalAdd(), addDefaults.available, ...args)
  }
);

const move = Object.assign({}, moveDefaults,
  {
    piecesCanStillMove: (...args) => moveDefaults.piecesCanStillMove(additionalMove(), ...args)
  }
);

const special = {
  init: greenSpecialInit,
  confirm: greenSpecialConfirm
};

const capture = captureDefaults;

const path = serialize(scale(parse(`M1280 704q0-26-19-45t-45-19q-172 0-318 49.5t-259.5 134-235.5 219.5q-19 21-19 45 0 26 19 45t45 19
  q24 0 45-19 27-24 74-71t67-66q137-124 268.5-176t313.5-52q26 0 45-19t19-45zm512-198q0 95-20 193-46 224-184.5 383t-357.5 268q-214 108-438 108
  -148 0-286-47-15-5-88-42t-96-37q-16 0-39.5 32t-45 70-52.5 70-60 32q-43 0-63.5-17.5t-45.5-59.5q-2-4-6-11t-5.5-10-3-9.5-1.5-13.5q0-35 31-73.5
  t68-65.5 68-56 31-48q0-4-14-38t-16-44q-9-51-9-104 0-115 43.5-220t119-184.5 170.5-139 204-95.5q55-18 145-25.5t179.5-9 178.5-6 163.5-24
  113.5-56.5l29.5-29.5 29.5-28 27-20 36.5-16 43.5-4.5q39 0 70.5 46t47.5 112 24 124 8 96z`), (20 / 1792)));

const Green = {
  name: 'Kundi Mbegu',
  boards: [Boards.HexB, Boards.CobblestoneA],
  scoreAdd: additionalAdd,
  scoreAddUnique: true,
  scoreMove: additionalMove,
  scoreMoveUnique: true,
  scoreSpecial: 'Consume',
  scoreComponent: GreenStats,
  primaryColor: '#007700',
  secondaryColor: '#FFFFFF',
  tabKey: 'green',
  path,
  add,
  move,
  capture,
  special
};

export default Green;

export {
  greenSpecialInit,
  greenSpecialEntryPoint,
  greenSpecialChoosePiece,
  greenSpecialChooseTarget,
  greenSpecialConfirm,
  triggerIncrease,
  additionalAdd,
  additionalMove,
  additionalIncrease
};
