import Immutable from 'immutable';
import parse from 'parse-svg-path';
import scale from 'scale-svg-path';
import serialize from 'serialize-svg-path';
import Boards from '../boards/boards-enum';
import store from '../../utils/store';
import { addTriggers, appendCurrentAction, updateCanConfirm, updateCurrentAction, updateHighlighted, updatePrompt } from '../../actions/game';
import { updatePiecesGroups } from '../../actions/players';
import { clearHighlightAndTriggers, getBordering, getCurrentInProgress, isOccupied, whichPlayerPiece } from '../../game_logic/base_util';
import addDefaults from '../../game_logic/add_defaults';
import moveDefaults from '../../game_logic/move_defaults';
import captureDefaults from '../../game_logic/capture_defaults';

const ADD_LIMIT = 2;
const MOVE_LIMIT = 8;
const GALE_MOVE_LIMIT = 3;

const whiteSpecialPiecesCanStillMove = (galeMoveLimit) => {
  const { game } = store.getState();

  let availablePieces = Immutable.List();

  game.getIn(['currentAction', 'data', 'moves']).forEach((move) => {
    if (!move.has('from') ||
    availablePieces.filter(avPiece =>
      avPiece.getIn(['move', 'board']) === move.getIn(['from', 'board']) &&
      avPiece.getIn(['move', 'space']) === move.getIn(['from', 'space'])
    ).size === 0) {
      availablePieces = availablePieces.push(
        Immutable.Map([['move', move.get('to')], ['number', 0], ['player', move.get('player')], ['modifiers', move.get('modifiers')]])
      );
    } else {
      const availPieceIndex = availablePieces.findIndex(avPiece =>
        avPiece.getIn(['move', 'board']) === move.getIn(['from', 'board']) &&
        avPiece.getIn(['move', 'space']) === move.getIn(['from', 'space'])
      );

      const currentNum = availablePieces.getIn([availPieceIndex, 'number']);

      const changedMove = availablePieces.get(availPieceIndex).withMutations(avPiece =>
        avPiece.set('move', move.get('to')).set('number', (currentNum + 1)));
      availablePieces = availablePieces.set(availPieceIndex, changedMove);
    }
  });

  return availablePieces
    .filter(avPiece => avPiece.get('number') < galeMoveLimit)
    .map(avPiece => avPiece.get('move').withMutations(piece =>
      piece.set('player', avPiece.get('player')).set('modifiers', avPiece.get('modifiers'))
    ));
};

const findAvailablePieces = (epicenter) => {
  const { world, players } = store.getState();
  const epicenterArray = epicenter.split('_');

  return getBordering(world, Immutable.fromJS(Boards[epicenterArray[0]]), epicenterArray[1])
    .reduce((a, firstSpace) =>
      a.concat(
        getBordering(world, Immutable.fromJS(Boards[firstSpace.get('board')]), firstSpace.get('space'))
          .reduce((b, secondSpace) =>
            b.concat(getBordering(world, Immutable.fromJS(Boards[secondSpace.get('board')]), secondSpace.get('space'))),
            Immutable.List()
          )
      ),
      Immutable.List()
    )
    .groupBy(space => `${space.get('board')}_${space.get('space')}`)
    .map(groupedSpaces => groupedSpaces.first())
    .filter(space => !(space.get('board') === epicenterArray[0] && space.get('space') === epicenterArray[1]))
    .filter(space => isOccupied(players, Immutable.fromJS(Boards[space.get('board')]), space.get('space')))
    .toList();
};

const whiteSpecialMovePiecesToTemporary = (availablePieces) => {
  const { players } = store.getState();
  const avPiecesHasPlayer = {};

  availablePieces.forEach((piece) => {
    const player = whichPlayerPiece(players, piece.get('board'), piece.get('space'));
    avPiecesHasPlayer[player] = true;
    const modifiers = players.getIn([player, 'pieces'])
      .find(curPiece => curPiece.get('board') === piece.get('board') && curPiece.get('space') === piece.get('space'))
      .get('modifiers');

    store.dispatch(appendCurrentAction({
      to: {
        board: piece.get('board'),
        space: piece.get('space')
      },
      player,
      modifiers
    }));
  });

  let allPlayerPieces = players.flatMap((player, pKey) => player.get('pieces').map(piece => piece.set('player', pKey)))
    .filter(piece => piece.get('space') !== 'captured')
    .filter(piece => !availablePieces.some(avPiece => avPiece.get('board') === piece.get('board') && avPiece.get('space') === piece.get('space')))
    .groupBy(piece => piece.get('player'));

  Object.keys(avPiecesHasPlayer).forEach((k) => {
    const intK = parseInt(k, 10);
    if (!allPlayerPieces.has(intK)) {
      allPlayerPieces = allPlayerPieces.set(intK, Immutable.List());
    }
  });

  store.dispatch(updatePiecesGroups(allPlayerPieces));
};

const whiteSpecialInit = () => {
  store.dispatch(updateCurrentAction({ type: 'SPECIAL', role: 'Cult of Boreas', data: {} }));
  whiteSpecialEntryPoint({
    availPieceFunc: findAvailablePieces,
    gale_move_limit: GALE_MOVE_LIMIT
  });
};

const whiteSpecialEntryPoint = (config) => {
  const { game, players } = store.getState();
  const highlightedSpaces = players.getIn([game.get('playerTurn'), 'pieces']).filter(piece => piece.get('space') !== 'captured');

  store.dispatch(updateHighlighted(highlightedSpaces.map(space => `${space.get('board')}_${space.get('space')}_primary`)));

  let triggers = Immutable.Map();
  highlightedSpaces.forEach((space) => {
    triggers = triggers.set(`${space.get('board')}_${space.get('space')}`, () => whiteSpecialChooseEye(`${space.get('board')}_${space.get('space')}`, config));
  });
  store.dispatch(addTriggers(triggers));
  store.dispatch(updatePrompt('Please choose an epicenter for your gale force winds'));
};

const whiteSpecialChooseEye = (epicenter, config) => {
  clearHighlightAndTriggers();
  const availablePieces = config.availPieceFunc(epicenter);

  store.dispatch(updateHighlighted(availablePieces.map(space => `${space.get('board')}_${space.get('space')}_primary`)));
  let triggers = Immutable.Map();
  availablePieces.forEach((space) => {
    triggers = triggers.set(`${space.get('board')}_${space.get('space')}`, () => whiteSpecialChoosePiece(`${space.get('board')}_${space.get('space')}`, config));
  });
  store.dispatch(addTriggers(triggers));
  whiteSpecialMovePiecesToTemporary(availablePieces);
  store.dispatch(updatePrompt('You may move any of the highlighted pieces up to three spaces each'));
};

const whiteSpecialChoosePiece = (originalSpace, config) => {
  const { game, world, players } = store.getState();
  clearHighlightAndTriggers();
  const spaceArray = originalSpace.split('_');
  const availablePieces = whiteSpecialPiecesCanStillMove(config.gale_move_limit);

  let highlightOthers = false;

  if (availablePieces.filter(piece => piece.get('board') === spaceArray[0] && piece.get('space') === spaceArray[1]).size > 0) {
    const highlightedSpaces = moveDefaults.available(
      game,
      world,
      players,
      Immutable.fromJS(Boards[spaceArray[0]]),
      spaceArray[1]
    );

    const secondaryHighlightedSpaces = availablePieces.filter(piece =>
      !(piece.get('board') === spaceArray[0] && piece.get('space') === spaceArray[1])
    );
    const allHighlightedSpaces = highlightedSpaces.map(space => `${space.get('board')}_${space.get('space')}_primary`)
      .concat(secondaryHighlightedSpaces.map(space => `${space.get('board')}_${space.get('space')}_secondary`));

    store.dispatch(updateHighlighted(allHighlightedSpaces));
    let triggers = Immutable.Map();
    highlightedSpaces.forEach((space) => {
      triggers = triggers.set(`${space.get('board')}_${space.get('space')}`, () => whiteSpecialAddPotentialMove(originalSpace, `${space.get('board')}_${space.get('space')}`, config));
    });
    store.dispatch(addTriggers(triggers));
    store.dispatch(updatePrompt('Please choose a space to move this piece, or select a different piece to move.'));
  } else {
    highlightOthers = true;
  }

  const piecesToMove = getCurrentInProgress(game)
    .filter(piece => !(piece.get('board') === spaceArray[0] && piece.get('space') === spaceArray[1]))
    .filter(piece => availablePieces.some(avPiece =>
      avPiece.get('board') === piece.get('board') && avPiece.get('space') === piece.get('space')
    ));

  let triggers = Immutable.Map();
  piecesToMove.forEach((piece) => {
    triggers = triggers.set(`${piece.get('board')}_${piece.get('space')}`, () => whiteSpecialChoosePiece(`${piece.get('board')}_${piece.get('space')}`, config));
  });
  store.dispatch(addTriggers(triggers));

  if (highlightOthers) {
    store.dispatch(updateHighlighted(piecesToMove.map(space => `${space.get('board')}_${space.get('space')}_primary`)));
  }
};

const whiteSpecialAddPotentialMove = (oldSpace, newSpace, config) => {
  const { game } = store.getState();
  const oldSpaceArray = oldSpace.split('_');
  const newSpaceArray = newSpace.split('_');

  const player = getCurrentInProgress(game)
    .find(piece => piece.get('board') === oldSpaceArray[0] && piece.get('space') === oldSpaceArray[1])
    .get('player');

  Promise.resolve(store.dispatch(appendCurrentAction({
    from: {
      board: oldSpaceArray[0],
      space: oldSpaceArray[1]
    },
    to: {
      board: newSpaceArray[0],
      space: newSpaceArray[1]
    },
    player
  })))
  .then(() => clearHighlightAndTriggers())
  .then(() => {
    store.dispatch(updateCanConfirm(true));
    whiteSpecialChoosePiece(newSpace, config);
  });
};

const whiteSpecialConfirm = () => {
  const { game, players } = store.getState();
  const piecesToUpdate = getCurrentInProgress(game).groupBy(piece => piece.get('player'));
  const allPieces = piecesToUpdate.map((group, player) => players.getIn([player, 'pieces']).concat(group));
  store.dispatch(updatePiecesGroups(allPieces));
};

const add = Object.assign({}, addDefaults,
  {
    canAddMore: (...args) => addDefaults.canAddMore(ADD_LIMIT, addDefaults.available, ...args)
  }
);

const move = Object.assign({}, moveDefaults,
  {
    piecesCanStillMove: (...args) => moveDefaults.piecesCanStillMove(MOVE_LIMIT, ...args)
  }
);

const special = {
  init: whiteSpecialInit,
  confirm: whiteSpecialConfirm
};

const capture = captureDefaults;

const path = serialize(scale(parse(`M4.5 0c-1.21 0-2.27.86-2.5 2-1.1 0-2 .9-2 2s.9 2 2 2h4.5c.83
  0 1.5-.67 1.5-1.5 0-.65-.42-1.29-1-1.5v-.5c0-1.38-1.12-2.5-2.5-2.5z`), (20 / 8)));

const White = {
  name: 'Cult of Boreas',
  boards: [Boards.OctA, Boards.KiteA],
  scoreAdd: ADD_LIMIT,
  scoreMove: MOVE_LIMIT,
  scoreSpecial: 'Gale Force',
  primaryColor: '#FFFFFF',
  secondaryColor: '#000000',
  tabKey: 'white',
  path,
  add,
  move,
  capture,
  special
};

export default White;

export {
  whiteSpecialPiecesCanStillMove,
  findAvailablePieces,
  whiteSpecialMovePiecesToTemporary,
  whiteSpecialInit,
  whiteSpecialEntryPoint,
  whiteSpecialChooseEye,
  whiteSpecialChoosePiece,
  whiteSpecialAddPotentialMove,
  whiteSpecialConfirm
};
