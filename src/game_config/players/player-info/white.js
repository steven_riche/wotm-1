import React from 'react';

const rulesTab = () => (<div className="tab-container">
  <h3>Adding</h3>
  <p>For one action, this player may add up to <strong>2 pieces</strong></p>

  <h3>Moving</h3>
  <p>
    For one action, this player may move pieces up to <strong>8 spaces</strong> total
  </p>

  <h3>Special - Gale Force</h3>
  <p>
    For one action, this player may choose one piece to be the source of the storm.
    That piece must stay stationary, but any piece (own or enemy) within
    <strong> 3 spaces</strong> may be moved up to <strong>3 spaces each</strong>,
    in any direction, in any order.
  </p>
</div>);

const backstoryTab = () => (<div className="tab-container">
  <p><em>
    Arrogant, exclusive, and powerful, the <strong>Cult of Boreas</strong> is today
    almost as occupied with political games and debauchery as it is ancient alchemy.
    Although their magic specializes in manipulating wind and storms to tremendous effect,
    their focus is squarely on power in any form.
  </em></p>

  <p><em>
    With their membership restricted to the same few dynastic families for generations,
    they believe they have bred into themselves the right and privilege to wield their
    power over lesser peoples. This arrogance would not be so grating if they were not
    also so successful in being first in everything they do.
  </em></p>

  <p><em>
    Now, with the Library finally revealed, it is no surprise the Cult of Boreas will
    stop at nothing to ensure they stay on top. But, with this much power at stake,
    they might have the furthest to fall.
  </em></p>
</div>);

const strategyTab = () => (<div className="tab-container">
  <p>
    While the Cult of Boreas is usually the fasted moving player, it is one of the
    slowest to add new pieces. Plan ahead to make having enough pieces a priority.
    Then, keep your distance from enemies until you are ready to strike. Your special
    is excellent for both keeping enemies from attacking you and for drawing in enemies
    for easy captures.
  </p>
</div>);

const White = {
  rulesTab: rulesTab(),
  backstoryTab: backstoryTab(),
  strategyTab: strategyTab()
};

export default White;
