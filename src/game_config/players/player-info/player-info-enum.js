import Black from './black';
import Blue from './blue';
import Green from './green';
import Orange from './orange';
import Purple from './purple';
import Red from './red';
import White from './white';
import Yellow from './yellow';

const PlayerInfoEnum = {
  black: Black,
  blue: Blue,
  green: Green,
  orange: Orange,
  purple: Purple,
  red: Red,
  white: White,
  yellow: Yellow
};

export default PlayerInfoEnum;
