import React from 'react';

const rulesTab = () => (<div className="tab-container">
  <h3>Adding</h3>
  <p>For one action, this player may add up to <strong>3 pieces</strong>.</p>

  <h3>Moving</h3>
  <p>
    For one action, this player may move pieces up to <strong>2 jumps</strong> total.
    A jump is defined as either moving to an open space, or moving <em>through</em>
    any continual group of the same color pieces (including your own) and landing on
    an open space.
  </p>

  <h3>Special - Attraction</h3>
  <p>
    For one action, this player may either move one of his pieces to any open space
    touching another of his pieces in the same room, or he may move an enemy piece
    that is touching one of his to any open piece touching another of that same enemy
    pieces in the same room.
  </p>
</div>);

const backstoryTab = () => (<div className="tab-container">
  <p><em>
    Most magical cabals are secretive, found on the fringes of society. Not so with
    <strong> The First Voltaic Church</strong> - they are society. Charismatic, charming,
    and beautiful, they stroll into a room and immediately command all eyes on themselves.
    Effortless in conversation and social politics, they flow through crowds drawing
    admirers in their wake.
  </em></p>

  <p><em>
    One moment they might be lightly suggesting policy to a magistrate, the next they
    may be flirting with an important ambassador, each person they speak with feeling
    at that moment that they are now the center of the universe.
  </em></p>

  <p><em>
    And with each person, there are later the typical secret meetings in back alleys,
    the promises of &ldquo;Why of course, I am sure that you would make an excellent
    addition to the Church - you just need to prove your loyalty!&rdquo;
  </em></p>

  <p><em>
    And all the while they smile, with a reassuring arm around your shoulders, until
    they have accomplished what they needed from you and can finally drive in the
    knife and leave you abandoned with the rest of their garbage.
  </em></p>
</div>);

const strategyTab = () => (<div className="tab-container">
  <p>
    The strengths from <strong>The First Voltaic Church</strong> come from staying
    connected with itself. Just as a wire is useless once it is cut, if pieces are
    separated they become very weak. On the other hand, if you connect your pieces
    together so they stretch with lots of surface area, then you are incredibly
    maneuverable. Similarly, if you are in an area that is greatly dominated by one
    player, you can quickly cover a lot of area. Just keep in mind the positions of
    other pieces on the board and stay connected.
  </p>
</div>);

const Yellow = {
  rulesTab: rulesTab(),
  backstoryTab: backstoryTab(),
  strategyTab: strategyTab()
};

export default Yellow;
