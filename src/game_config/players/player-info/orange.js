import React from 'react';

const rulesTab = () => (<div className="tab-container">
  <h3>Adding</h3>
  <p>For one action, this player may add up to <strong>2 pieces</strong>.</p>

  <h3>Moving</h3>
  <p>
    For one action, this player may move pieces up to <strong>3 spaces</strong> total.
  </p>

  <h3>Special - Save Time</h3>
  <p>
    For one action, this player may set aside his action for future use, spending
    these saved actions in the future. He may also borrow actions from the future -
    without spending any actions this turn, he may add or move pieces (even move pieces
    through blocked pieces), but at the cost of increasing his action debt by 2.
    Even if this player captures enough opponents to win, he must still pay of any
    remaining action debt before the win is official.
  </p>
</div>);

const backstoryTab = () => (<div className="tab-container">
  <p><em>
    The <strong>Societe Pendulaire</strong> are the self-styled borrowers and lenders
    of time itself. A look into their membership would show nothing but fresh-faced
    children and graying elders, with nothing in-between. This is because the very
    timelines of their own lives are warped and stretched for their own ends.
  </em></p>

  <p><em>
    For most of a lifetime, a member of the Societe Pendulaire might live only moments
    of every hour, staying in near-stasis until it is time to act. Then, at the moment
    of acting, they will live 40 or 50 years in the blink of an eye, appearing to age
    immediately as the world around them suddenly changes with a lifetime of work
    compressed into a moment.
  </em></p>
</div>);

const strategyTab = () => (<div className="tab-container">
  <p>
    Within any given turn, <strong>Societe Pendulaire</strong> is one of the weakest
    players. However, the ability to save actions for later can be incredibly powerful.
    If you save up as many actions as possible, then you are able to unleash them all
    at once and attack other players without giving them a chance to defend themselves.
  </p>

  <p>
    Borrowing actions from the future can be very useful in a tight bind as well,
    but be very careful in using this, as it takes two equivalent actions to pay off.
  </p>
</div>);

const Orange = {
  rulesTab: rulesTab(),
  backstoryTab: backstoryTab(),
  strategyTab: strategyTab()
};

export default Orange;
