import React from 'react';

const rulesTab = () => (<div className="tab-container">
  <h3>Adding</h3>
  <p>
    For one action, this player may initially only add <strong>1 piece</strong>.
    However, after using the <strong>Consume</strong> special, this number can grow.
  </p>

  <h3>Moving</h3>
  <p>
    For one action, this player may move pieces up to <strong>2 spaces</strong> total.
    However, after using the <strong>Consume</strong> special, this number can grow.
  </p>

  <h3>Special - Consume</h3>
  <p>
    For one action, a player may move onto a blocked space and consume the piece that
    is there (this could be a Kundi Mbegu piece or an enemy piece). Then, this player
    gains fuel they can use to grow their other statistics. Initially, they will receive
    3 units of fuel for consuming a piece, but this number could grow as well. For
    instance, this player does a special action, consuming a piece and gaining 3 fuel.
    This player then grows their Add number from 1 to 2, their Move number from 2 to 3,
    and their Increase number from 3 to 4. Now, this player could add up to 2 pieces,
    could move up to 3 spaces, and when they perform the Consume special they will gain
    4 units of fuel. There is no upper limit to how much they can increase these
    statistics.
  </p>
</div>);

const backstoryTab = () => (<div className="tab-container">
  <p><em>
    Being the most powerful often means having the largest target on your back. The
    members of <strong>Kundi Mbegu</strong> have learned to walk humbly and feign
    weakness, slipping under the radar of those who would mean them harm.
  </em></p>

  <p><em>
    Meanwhile, they attack from the shadows, picking off enemies in secret. With
    each victory, they grow stronger.
  </em></p>

  <p><em>
    Eventually, the strongest of the magicians will look around and realize they are
    no longer the top predator - in their foolishness they have allowed a more dangerous
    animal to take their places, and it will eat them alive.
  </em></p>
</div>);

const strategyTab = () => (<div className="tab-container">
  <p>
    <strong>Kundi Mbegu</strong> is a very difficult role to play because it depends
    largely on good timing. At the beginning of the game, this player is at a clear
    disadvantage and incredibly weak. However, as he starts becoming more powerful,
    the temptation will be to focus on becoming more powerful at the expense of gaining
    points, so while you may be the strongest player at the end of the game you are
    not the winner.
  </p>

  <p>
    You should definitely try to get stronger as soon as possible, but you will need
    the discernment to know when to stop growing and start playing to win.
  </p>
</div>);

const Green = {
  rulesTab: rulesTab(),
  backstoryTab: backstoryTab(),
  strategyTab: strategyTab()
};

export default Green;
