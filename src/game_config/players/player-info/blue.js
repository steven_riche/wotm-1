import React from 'react';

const rulesTab = () => (<div className="tab-container">
  <h3>Adding</h3>
  <p>
    For one action, this player may add up to <strong>3 pieces</strong>. If you are
    using <strong>Pressure</strong> when adding pieces (see below), you may add pieces
    to spaces occupied by enemy pieces, destroying those pieces.
  </p>

  <h3>Moving</h3>
  <p>
    For one action, this player may move pieces up to <strong>5 spaces</strong> total.
    If you are using <strong>Pressure</strong> when moving pieces (see below), you
    may move pieces onto spaces occupied by enemy pieces, destroying those pieces.
  </p>

  <h3>Special - Build Pressure</h3>
  <p>
    For one action, you may increase your <strong>Pressure</strong> 2 units. This has
    no effect for this action, but you may choose to use one of these Pressure units
    in a future action to destroy enemy pieces.
  </p>
</div>);

const backstoryTab = () => (<div className="tab-container">
  <p><em>
    Cold and collected, the <strong>Knights of the Azure Main</strong> are often criticized
    for being passive and passionless. With folded hands and forced smiles, they seem to
    acquiesce quickly to the demands of more aggressive organizations. One could be forgiven
    for even thinking they were incapable of defending themselves, and merely supported
    others with their water-based magics because they were too weak to accomplish much
    on their own.
  </em></p>

  <p><em>
    But such a belief would likely find you leagues underwater the moment you let down
    your guard, crushed by the colossal pressure before you had time to drown. For the
    Knights of the Azure Main do not make alliances, they do not compromise, and they
    do not forget insults. They simply wait. And for those who would mistake their
    patience as weakness, their destruction will be as thorough as it will be sudden.
  </em></p>
</div>);

const strategyTab = () => (<div className="tab-container">
  <p>
    The Knights of the Azure Main is a fairly well-rounded player, with faster-than-average
    movement. At first glance, they may appear to have a very weak special ability
    since it does not pay off immediately. However, if used properly, they can easily
    be the most destructive player, bulldozing their way around the board and destroying
    the plans of any opponents in their way. The keys to win are patience and timing -
    when you can afford it, build up pressure, and wait until the right time to unleash it.
  </p>
</div>);

const Blue = {
  rulesTab: rulesTab(),
  backstoryTab: backstoryTab(),
  strategyTab: strategyTab()
};

export default Blue;
