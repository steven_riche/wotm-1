import React from 'react';

const rulesTab = () => (<div className="tab-container">
  <h3>Adding</h3>
  <p>
    For one action, this player may add up to <strong>3 pieces</strong>, and place
    them on either open spaces bordering the original piece, or touching the original
    piece by corner.
  </p>

  <h3>Moving</h3>
  <p>
    For one action, this player may move pieces up to <strong>3 spaces</strong> total.
    This player may move to spaces bordering the previous space or touching the
    previous space by corner.
  </p>

  <h3>Special - Teleport</h3>
  <p>
    For one action, this player may move an enemy piece that is touching one if their
    pieces by side or corner. The enemy piece may be moved to any open space anywhere
    on the board that is <strong>not</strong> bordering or touching a Madrasa Qashani
    piece.
  </p>
</div>);

const backstoryTab = () => (<div className="tab-container">
  <p><em>
    Around the world, occassionally a security camera will pick up an unusual sight, all
    too familiar to a small number of befuddled elite security analysts. In a supposedly
    secure room, impregnable, locked on all sides, a bookish man or woman will come
    stumbling into view as if stepping in from behind a corner that is not actually
    there.
  </em></p>

  <p><em>
    Muttering under their breath like eccentric, distracted professors, these individuals
    will sometimes wonder a bit aimlessly through the priceless artwork or gold bars
    (or whatever other incidental priceless item this particular vault is protecting),
    and act as though they are trying to get their bearings after a wrong turn.
  </em></p>

  <p><em>
    Then, inevitably, the piece of chalk comes out, and formulas and figures are drawn
    on the walls. The individuals then leave as they came, passing out of sight as if
    turning down a corridor only they can see, sometimes casually taking a souvenir
    and sometimes not. Afterwards, of course, experts of all types will be brought in
    to analyze the chalk scrawlings, which look more like pseudo-calculus than magic
    incantations, but might as well be arcana for all the sense they make.
  </em></p>

  <p><em>
    In highly classified reports that will be inevitably heavily redacted afterwards,
    the highly-paid analysts often agree on one thing - the most frightening thing
    is not that a group is penetrating the most secure locations on the planet with
    ease. It is the indifference with which they do it, as though out for an afternoon
    stroll.
  </em></p>
</div>);

const strategyTab = () => (<div className="tab-container">
  <p>
    The strengths or weaknesses of Madrasa Qashani depend largely on the shape of the
    spaces in the room the pieces are in. In spaces that have lots of sides (such as a
    hexagon room), this is one of the slowest players. However, in spaces that have
    few sides but touch many other spaces on the corners (such as a triangle room),
    this is not only one of the fastest players, it has incredibly agility and
    flexibility compared to players that must stay on bordering spaces. So, try to
    stay in rooms where you are the strongest, and use your cornering to outmaneuver
    opponents.
  </p>
</div>);

const Purple = {
  rulesTab: rulesTab(),
  backstoryTab: backstoryTab(),
  strategyTab: strategyTab()
};

export default Purple;
