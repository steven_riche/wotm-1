import React from 'react';

const rulesTab = () => (<div className="tab-container">
  <h3>Adding</h3>
  <p>For one action, this player may add up to <strong>3 pieces</strong></p>

  <h3>Moving</h3>
  <p>
    For one action, this player may move <strong>each</strong> of his pieces up
    to <strong>2 spaces</strong>
  </p>

  <h3>Special - Infect</h3>
  <p>
    For one action, this player may transform an enemy piece touching one of his
    pieces <strong>by side or by corner</strong> into one of his own pieces.
  </p>
</div>);

const backstoryTab = () => (<div className="tab-container">
  <p><em>
    Few things frighten devotees of one of the hidden cabals of magic. Those who
    have mastery over the elements themselves are typically fearless and confident
    in their godlike power.
  </em></p>

  <p><em>
    However, among the most experienced practitioners, the spectre of
    <strong> The Colony</strong> hangs like a recurring nightmare. Silent and unblinking,
    they appear en masse unexpectedly, hordes of black-robed figures moving in perfect
    unison to carry out some sinister plan. Among those unfortunate enough to encounter
    them in person, witnesses tell of their blank stares, sewn lips, and stilted movement.
    More often, remains of members are found left behind after a mission, discarded
    like broken tools and looking far too decayed for magicians so recently deceased.
  </em></p>

  <p><em>
    These deceased members are often recognized by others as missing from their own
    cabals, thought dead years before. It is thought that The Colony is entirely
    composed of members somehow coerced or brainwashed, lacking any will of their
    own. Of course, The Colony never speaks for itself - its very name was given
    to it by outsiders watching its mysterious hive-like behavior.
  </em></p>

  <p><em>
    In the end, only two things about The Colony are known for sure: its members
    somehow simultaneously obey a single willpower regardless of who they might
    have been before, and The Colony is always looking for new recruits.
  </em></p>
</div>);

const strategyTab = () => (<div className="tab-container">
  <p>
    As The Colony adds more pieces on the board, the stronger it becomes. Take advantage
    of the ability to add pieces quickly to build your strength and surround your
    enemies. If you are able to get many pieces nearby an enemy, you will be unstoppable.
    The last thing you will want to do is find your self with only a few pieces widely
    separated from others.
  </p>
</div>);

const Black = {
  rulesTab: rulesTab(),
  backstoryTab: backstoryTab(),
  strategyTab: strategyTab()
};

export default Black;
