import React from 'react';

const rulesTab = () => (<div className="tab-container">
  <h3>Adding</h3>
  <p>For one action, this player may add up to <strong>3 pieces</strong>.</p>

  <h3>Moving</h3>
  <p>
    For one action, this player may move pieces up to <strong>4 spaces</strong> total.
  </p>

  <h3>Special - Explode</h3>
  <p>
    For one action, this player may choose a piece (or number of bordering pieces) to
    self-destruct. If more than one piece is chosen, then all enemy pieces touching
    any of the exploded pieces are also destroyed. For every enemy piece destroyed,
    Ignis Rex gets a unit of fuel. At any time during his turn, Ignis Rex can spend
    2 units of fuel to take a free action.
  </p>
</div>);

const backstoryTab = () => (<div className="tab-container">
  <p><em>
    Perhaps the most devoted of all orders of magic, <strong>Ignis Rex</strong> condemns
    the lack of focus they see in other cabals as symptomatic of a larger disease -
    a cancer that they are singularly qualified to remove.
  </em></p>

  <p><em>
    Rich in rhetoric, they speak of bringing utopia by self-sacrifice and cleansing the
    world, yet always seem vague about what this utopia should look like or why most of
    humanity is undeserving. Some outsiders cynically think that Ignis Rex has no real
    end in mind, that they just enjoy wanton destruction and have created the flimsiest
    of excuses to justify it.
  </em></p>

  <p><em>
    One thing is for sure - they seem to thrive and feed off of violence itself, each
    explosion creating a chain reaction of more destruction. What may at first seem
    like a senseless sacrifice may just be lighting the fuse for a fire that will
    consume the battlefield entirely.
  </em></p>
</div>);

const strategyTab = () => (<div className="tab-container">
  <p>
    <strong>Ignis Rex</strong> can be a difficult role to play, since it can be misleading.
    Since the special power involves destroying enemy pieces, players will often make
    the mistake of exploding pieces every change they can get to little effect, leaving
    themselves perpetually weak.
  </p>

  <p>
    Instead, you must recognize how costly it is to destroy your own pieces, and
    patiently try to set up situations where you can gain lots of fuel for one special.
    Once you start gaining fuel, you can use the extra actions to more easily set
    up other big explosions, like a chain reaction. Most importantly, though, remember
    that the point of the game is to capture enemy pieces, not destroy them. Do not
    get so caught up in the chaos that you never make a plan.
  </p>
</div>);

const Red = {
  rulesTab: rulesTab(),
  backstoryTab: backstoryTab(),
  strategyTab: strategyTab()
};

export default Red;
