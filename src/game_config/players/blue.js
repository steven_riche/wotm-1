import parse from 'parse-svg-path';
import scale from 'scale-svg-path';
import serialize from 'serialize-svg-path';
import Boards from '../boards/boards-enum';
import store from '../../utils/store';
import BluePressure from '../../components/player-specific/blue-pressure';
import { updateModifier } from '../../actions/players';
import { updateCanConfirm, updateCurrentAction, updatePrompt } from '../../actions/game';
import addDefaults from '../../game_logic/add_defaults';
import moveDefaults from '../../game_logic/move_defaults';
import captureDefaults from '../../game_logic/capture_defaults';

const ADD_LIMIT = 3;
const MOVE_LIMIT = 5;

const blueSpecialInit = () => {
  Promise.resolve(store.dispatch(updatePrompt('Click confirm to add pressure')))
    .then(store.dispatch(updateCurrentAction({ type: 'SPECIAL', role: 'Knights of the Azure Main', data: {} })))
    .then(store.dispatch(updateCanConfirm(true)));
};

const blueSpecialConfirm = () => {
  const { game, players } = store.getState();
  const currentPressure = (players.hasIn([game.get('playerTurn'), 'modifiers', 'bluePressure'])) ? players.getIn([game.get('playerTurn'), 'modifiers', 'bluePressure']) : 0;
  store.dispatch(updateModifier(game.get('playerTurn'), 'bluePressure', (currentPressure + 2)));
};

const add = Object.assign({}, addDefaults,
  {
    canAddMore: (...args) => addDefaults.canAddMore(ADD_LIMIT, addDefaults.available, ...args)
  }
);

const move = Object.assign({}, moveDefaults,
  {
    piecesCanStillMove: (...args) => moveDefaults.piecesCanStillMove(MOVE_LIMIT, ...args)
  }
);

const special = {
  init: blueSpecialInit,
  confirm: blueSpecialConfirm
};

const capture = captureDefaults;

const path = serialize(scale(parse(`M3 0l-.34.34c-.11.11-2.66 2.69-2.66 4.88 0 1.65 1.35 3 3 3s3-1.35
  3-3c0-2.18-2.55-4.77-2.66-4.88l-.34-.34zm-1.5 4.72c.28 0 .5.22.5.5 0 .55.45 1 1 1 .28 0 .5.22.5.5s-.22.5
  -.5.5c-1.1 0-2-.9-2-2 0-.28.22-.5.5-.5z`), (20 / 8)));

const Blue = {
  name: 'Knights of the Azure Main',
  boards: [Boards.SnowflakeB, Boards.HexA],
  scoreAdd: ADD_LIMIT,
  scoreMove: MOVE_LIMIT,
  scoreSpecial: 'Build Pressure',
  scoreComponent: BluePressure,
  primaryColor: '#1455bc',
  secondaryColor: '#76a6f2',
  tabKey: 'blue',
  path,
  add,
  move,
  special,
  capture
};

export default Blue;

export { blueSpecialInit, blueSpecialConfirm };
