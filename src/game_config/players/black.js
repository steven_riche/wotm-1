import Immutable from 'immutable';
import parse from 'parse-svg-path';
import scale from 'scale-svg-path';
import serialize from 'serialize-svg-path';
import store from '../../utils/store';
import { addTriggers, appendCurrentAction, updateCanConfirm, updateCurrentAction, updateHighlighted, updatePrompt } from '../../actions/game';
import { updateDamage, updatePieces } from '../../actions/players';
import Boards from '../boards/boards-enum';
import { allTouchingEnemies, clearHighlightAndTriggers, whichPlayerPiece } from '../../game_logic/base_util';
import addDefaults from '../../game_logic/add_defaults';
import moveDefaults from '../../game_logic/move_defaults';
import captureDefaults from '../../game_logic/capture_defaults';
import { damageToFreeAction } from '../constants/constants';

const ADD_LIMIT = 3;
const MOVE_LIMIT = 2;

const blackPiecesCanStillMove = (MOVE_LIMIT_LOCAL, game, players) => {
  let availablePieces = Immutable.List();

  const playerInfo = players.get(game.get('playerTurn'));
  let moveLimit = MOVE_LIMIT_LOCAL;

  if (playerInfo.get('cards').size > 0) {
    playerInfo.get('cards').forEach((card) => {
      if (card.hasIn(['modifications', 'move'])) {
        moveLimit += card.getIn(['modifications', 'move']);
      }
    });
  }

  game.getIn(['currentAction', 'data', 'moves']).forEach((move) => {
    if (!move.has('from') ||
    availablePieces.filter(avPiece =>
      avPiece.getIn(['move', 'board']) === move.getIn(['from', 'board']) &&
      avPiece.getIn(['move', 'space']) === move.getIn(['from', 'space'])
    ).size === 0) {
      availablePieces = availablePieces.push(Immutable.Map([['move', move.get('to')], ['number', 0], ['modifiers', move.get('modifiers')]]));
    } else {
      const availPieceIndex = availablePieces.findIndex(avPiece =>
        avPiece.getIn(['move', 'board']) === move.getIn(['from', 'board']) && avPiece.getIn(['move', 'space']) === move.getIn(['from', 'space'])
      );

      const currentNum = availablePieces.getIn([availPieceIndex, 'number']);

      const changedMove = availablePieces.get(availPieceIndex).withMutations(avPiece => avPiece.set('move', move.get('to')).set('number', (currentNum + 1)));
      availablePieces = availablePieces.set(availPieceIndex, changedMove);
    }
  });

  return availablePieces
    .filter(avPiece => avPiece.get('number') < moveLimit)
    .map(avPiece => avPiece.get('move').withMutations(piece => piece.set('player', game.get('playerTurn')).set('modifiers', avPiece.get('modifiers'))));
};

const blackSpecialInit = () => {
  store.dispatch(updateCurrentAction({ type: 'SPECIAL', role: 'The Colony', data: {} }));
  blackSpecialEntryPoint();
};

const blackSpecialEntryPoint = () => {
  const { game, world, players } = store.getState();
  const highlightedSpaces = allTouchingEnemies(game, world, players);
  store.dispatch(updateHighlighted(highlightedSpaces.map(space => `${space.get('board')}_${space.get('space')}_primary`)));
  let triggers = Immutable.Map();
  highlightedSpaces.forEach((space) => {
    triggers = triggers.set(`${space.get('board')}_${space.get('space')}`, () => blackSpecialChoosePiece(`${space.get('board')}_${space.get('space')}`));
  });
  store.dispatch(addTriggers(triggers));
  store.dispatch(updatePrompt('Please choose an enemy piece to transform into one of yours'));
};

const blackSpecialChoosePiece = (space) => {
  const { game, players } = store.getState();
  clearHighlightAndTriggers();
  const spaceArray = space.split('_');
  const oldPlayerIndex = whichPlayerPiece(players, spaceArray[0], spaceArray[1]);
  const oldModifiers = players.getIn([oldPlayerIndex, 'pieces'])
    .filter(piece => piece.get('board') === spaceArray[0] && piece.get('space') === spaceArray[1])
    .first()
    .get('modifiers');

  Promise.resolve(store.dispatch(updateHighlighted([`${space}_primary`])))
    .then(store.dispatch(appendCurrentAction({
      to: {
        board: spaceArray[0],
        space: spaceArray[1]
      },
      player: game.get('playerTurn'),
      formerPlayer: oldPlayerIndex,
      modifiers: oldModifiers
    })))
    .then(store.dispatch(updatePieces(oldPlayerIndex, players.getIn([oldPlayerIndex, 'pieces'])
      .filter(piece => !(piece.get('board') === spaceArray[0] && piece.get('space') === spaceArray[1])))))
    .then(store.dispatch(updateCanConfirm(true)))
    .then(store.dispatch(updatePrompt('Please cancel or confirm your selection')));
};

const blackSpecialConfirm = () => {
  const { game, players } = store.getState();
  const tempPiece = game.getIn(['currentAction', 'data', 'moves', 0]);
  const targetPlayer = tempPiece.get('formerPlayer');
  let newDamage = parseInt(players.getIn([targetPlayer, 'damageCounter']), 10) + 1;
  newDamage = (newDamage < damageToFreeAction) ? newDamage : damageToFreeAction;

  store.dispatch(updateDamage(targetPlayer, newDamage));

  const newPiece = Immutable.fromJS({
    board: tempPiece.getIn(['to', 'board']),
    space: tempPiece.getIn(['to', 'space']),
    modifiers: Immutable.List()
  });

  store.dispatch(updatePieces(game.get('playerTurn'), players.getIn([game.get('playerTurn'), 'pieces']).push(newPiece)));
};

const add = Object.assign({}, addDefaults,
  {
    canAddMore: (...args) => addDefaults.canAddMore(ADD_LIMIT, addDefaults.available, ...args)
  }
);

const move = Object.assign({}, moveDefaults,
  {
    piecesCanStillMove: (...args) => blackPiecesCanStillMove(MOVE_LIMIT, ...args)
  }
);

const special = {
  init: blackSpecialInit,
  confirm: blackSpecialConfirm
};

const capture = captureDefaults;

const path = serialize(scale(parse(`M3.5 0c-1.19 0-1.98 1.69-1.19 2.5-.09.07-.2.14-.28.22l-1.31-.66a.5.5 0
  0 0-.34-.06.5.5 0 0 0-.09.94l1.16.56c-.09.16-.19.33-.25.5h-.69a.5.5 0 0 0-.09 0 .5.5 0 1 0 .09 1h.5c0
  .23.02.45.06.66l-.78.41a.5.5 0 1 0 .44.88l.66-.34c.25.46.62.85 1.03 1.09.35-.19.59-.44.59-.72v-1.44a.5.5
  0 0 0 0-.09v-.81a.5.5 0 0 0 0-.22c.05-.23.26-.41.5-.41.28 0 .5.22.5.5v.88a.5.5 0 0 0 0 .09v.06a.5.5 0 0
  0 0 .09v1.34c0 .27.24.53.59.72.41-.25.79-.63 1.03-1.09l.66.34a.5.5 0 1 0 .44-.88l-.78-.41c.04-.21.06-.43
  .06-.66h.5a.5.5 0 1 0 0-1h-.69c-.06-.17-.16-.34-.25-.5l1.16-.56a.5.5 0 0 0-.31-.94.5.5 0 0 0-.13.06l-1.31
  .66c-.09-.08-.19-.15-.28-.22.78-.83 0-2.5-1.19-2.5z`), (22 / 8)));

const Black = {
  name: 'The Colony',
  boards: [Boards.PentagonA, Boards.DiamondA],
  scoreAdd: ADD_LIMIT,
  scoreMove: MOVE_LIMIT,
  scoreMoveUnique: true,
  scoreSpecial: 'Infect',
  primaryColor: '#000000',
  secondaryColor: '#FFFFFF',
  tabKey: 'black',
  path,
  add,
  move,
  capture,
  special
};

export default Black;

export {
  blackPiecesCanStillMove,
  blackSpecialInit,
  blackSpecialEntryPoint,
  blackSpecialChoosePiece,
  blackSpecialConfirm
};
