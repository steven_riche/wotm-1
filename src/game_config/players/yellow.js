import Immutable from 'immutable';
import parse from 'parse-svg-path';
import scale from 'scale-svg-path';
import serialize from 'serialize-svg-path';
import store from '../../utils/store';
import { addTriggers, appendCurrentAction, updateCanConfirm, updateCurrentAction, updateHighlighted, updatePrompt } from '../../actions/game';
import { updatePieces } from '../../actions/players';
import Boards from '../boards/boards-enum';
import {
  allTouchingEnemies,
  clearHighlightAndTriggers,
  getBordering,
  getCurrentInProgress,
  isBlocked,
  whichPlayerPiece,
  whichPlayerPieceTemp
} from '../../game_logic/base_util';
import addDefaults from '../../game_logic/add_defaults';
import moveDefaults from '../../game_logic/move_defaults';
import captureDefaults from '../../game_logic/capture_defaults';

const ADD_LIMIT = 3;
const MOVE_LIMIT = 2;

const yellowMoveAvailable = (
  game,
  world,
  players,
  board,
  space,
  connectingPlayer,
  alreadySearched = Immutable.List()
) => {
  let availableSpaces = Immutable.List();
  const alreadySearchedPlus = alreadySearched.push(`${board.get('name')}_${space}`);

  getBordering(world, board, space).forEach((possibleSpace) => {
    if (!isBlocked(game, players, Immutable.Map().set('name', possibleSpace.get('board')), possibleSpace.get('space'))) {
      availableSpaces = availableSpaces.push(possibleSpace);
    } else if (!alreadySearchedPlus.includes(`${possibleSpace.get('board')}_${possibleSpace.get('space')}`) &&
      (typeof connectingPlayer === 'undefined' || whichPlayerPieceTemp(game, players, possibleSpace.get('board'), possibleSpace.get('space')) === connectingPlayer)) {
      availableSpaces = availableSpaces.concat(
        yellowMoveAvailable(
          game,
          world,
          players,
          Immutable.fromJS(Boards[possibleSpace.get('board')]),
          possibleSpace.get('space'),
          whichPlayerPieceTemp(game, players, possibleSpace.get('board'), possibleSpace.get('space')),
          alreadySearchedPlus
        )
      );
    }
  });

  return availableSpaces
    .groupBy(piece => `${piece.get('board')}_${piece.get('space')}`)
    .map(x => x.first())
    .toList();
};

const yellowMoveBlockedAvailable = (game, world, players, board, space) => (
  moveDefaults.blockedAvailable(game, world, players, board, space)
    .concat(yellowMoveAvailable(game, world, players, board, space))
    .groupBy(piece => `${piece.get('board')}_${piece.get('space')}`)
    .map(x => x.first())
    .toList()
);

const yellowSpecialInit = () => {
  store.dispatch(updateCurrentAction({ type: 'SPECIAL', role: 'The First Voltaic Church', data: {} }));
  yellowSpecialEntryPoint();
};

const yellowSpecialEntryPoint = () => {
  const { game, world, players } = store.getState();
  const ownPieces = players.getIn([game.get('playerTurn'), 'pieces'])
    .filter(piece => piece.get('space') !== 'captured')
    .groupBy(piece => piece.get('board'))
    .filter(pieceGroup => pieceGroup.size > 1)
    .reduce((a, b) => a.push(b), Immutable.List())
    .first();

  const otherPieces = allTouchingEnemies(game, world, players)
    .groupBy(piece => `${piece.get('board')}_${whichPlayerPiece(players, piece.get('board'), piece.get('space'))}`)
    .filter(pieceGroup => pieceGroup.size > 1)
    .reduce((a, b) => a.push(b), Immutable.List())
    .first() || Immutable.List();

  const highlightedSpaces = ownPieces.concat(otherPieces);

  store.dispatch(updateHighlighted(highlightedSpaces.map(space => `${space.get('board')}_${space.get('space')}_primary`)));
  let triggers = Immutable.Map();
  highlightedSpaces.forEach((space) => {
    triggers = triggers.set(`${space.get('board')}_${space.get('space')}`, () => yellowChoosePiece(`${space.get('board')}_${space.get('space')}`));
  });
  store.dispatch(addTriggers(triggers));
  store.dispatch(updatePrompt('Please choose a piece to move'));
};

const yellowChoosePiece = (space) => {
  const { game, world, players } = store.getState();
  clearHighlightAndTriggers();
  const spaceArray = space.split('_');
  const thisPlayer = whichPlayerPiece(players, spaceArray[0], spaceArray[1]);
  const oldModifiers = players.getIn([thisPlayer, 'pieces'])
    .filter(piece => piece.get('board') === spaceArray[0] && piece.get('space') === spaceArray[1])
    .first()
    .get('modifiers');

  store.dispatch(appendCurrentAction({
    to: {
      board: spaceArray[0],
      space: spaceArray[1]
    },
    player: thisPlayer,
    modifiers: oldModifiers
  }));

  store.dispatch(updatePieces(thisPlayer, players.getIn([thisPlayer, 'pieces']).filter(piece => !(piece.get('board') === spaceArray[0] && piece.get('space') === spaceArray[1]))));

  const availableSpaces = players.getIn([thisPlayer, 'pieces'])
    .filter(piece => piece.get('board') === spaceArray[0] && piece.get('space') !== 'captured' && piece.get('space') !== spaceArray[1])
    .flatMap(piece => getBordering(world, Immutable.fromJS(Boards[piece.get('board')]), piece.get('space')))
    .filter(piece => !isBlocked(game, players, Immutable.Map().set('name', piece.get('board')), piece.get('space')))
    .groupBy(piece => `${piece.get('board')}_${piece.get('space')}`)
    .map(x => x.first())
    .toList();

  store.dispatch(updateHighlighted(availableSpaces.map(avSpace => `${avSpace.get('board')}_${avSpace.get('space')}_primary`).push(`${space}_secondary`)));

  let triggers = Immutable.Map();
  availableSpaces.forEach((avSpace) => {
    triggers = triggers.set(`${avSpace.get('board')}_${avSpace.get('space')}`, () => yellowChooseSpace(`${avSpace.get('board')}_${avSpace.get('space')}`, space));
  });
  store.dispatch(addTriggers(triggers));
  store.dispatch(updatePrompt('Please choose a space to move to'));
};

const yellowChooseSpace = (newSpace, oldSpace) => {
  const { game, players } = store.getState();

  const newSpaceArray = newSpace.split('_');
  const oldSpaceArray = oldSpace.split('_');
  const playerIndex = whichPlayerPieceTemp(game, players, oldSpaceArray[0], oldSpaceArray[1]);

  clearHighlightAndTriggers();
  store.dispatch(appendCurrentAction({
    from: {
      board: oldSpaceArray[0],
      space: oldSpaceArray[1]
    },
    to: {
      board: newSpaceArray[0],
      space: newSpaceArray[1]
    },
    player: playerIndex
  }));
  store.dispatch(updateCanConfirm(true));
  store.dispatch(updatePrompt('Please cancel or confirm your action'));
};

const yellowSpecialConfirm = () => {
  const { game, players } = store.getState();
  const movedPiece = getCurrentInProgress(game).first();
  const thisPlayer = movedPiece.get('player');
  store.dispatch(updatePieces(thisPlayer, players.getIn([thisPlayer, 'pieces']).push(movedPiece)));
};

const add = Object.assign({}, addDefaults,
  {
    canAddMore: (...args) => addDefaults.canAddMore(ADD_LIMIT, addDefaults.available, ...args)
  }
);

const move = Object.assign({}, moveDefaults,
  {
    piecesCanStillMove: (...args) => moveDefaults.piecesCanStillMove(MOVE_LIMIT, ...args),
    available: (...args) => yellowMoveAvailable(...args),
    blockedAvailable: (...args) => yellowMoveBlockedAvailable(...args)
  }
);

const special = {
  init: yellowSpecialInit,
  confirm: yellowSpecialConfirm
};

const capture = captureDefaults;

const path = serialize(scale(parse(`M1333 566q18 20 7 44l-540 1157q-13 25-42 25-4 0-14-2-17-5-25.5-19t-4.5-30l197-808-406 101
  q-4 1-12 1-18 0-31-11-18-15-13-39l201-825q4-14 16-23t28-9h328q19 0 32 12.5t13 29.5q0 8-5 18l-171 463 396-98q8-2 12-2 19 0 34 15z`), (20 / 1792)));

const Yellow = {
  name: 'The First Voltaic Church',
  boards: [Boards.CobblestoneB, Boards.DiamondB],
  scoreAdd: ADD_LIMIT,
  scoreMove: MOVE_LIMIT,
  scoreMoveUnique: true,
  scoreSpecial: 'Attraction',
  primaryColor: '#FFFF00',
  secondaryColor: '#333333',
  tabKey: 'yellow',
  path,
  add,
  move,
  capture,
  special
};

export default Yellow;

export {
  yellowMoveAvailable,
  yellowMoveBlockedAvailable,
  yellowSpecialInit,
  yellowSpecialEntryPoint,
  yellowChoosePiece,
  yellowChooseSpace,
  yellowSpecialConfirm
};
