import Immutable from 'immutable';
import parse from 'parse-svg-path';
import scale from 'scale-svg-path';
import serialize from 'serialize-svg-path';
import store from '../../utils/store';
import { addTriggers, appendCurrentAction, updateCanConfirm, updateCurrentAction, updateHighlighted, updatePrompt } from '../../actions/game';
import { updatePieces } from '../../actions/players';
import Boards from '../boards/boards-enum';
import {
  allTouchingEnemies,
  clearHighlightAndTriggers,
  getBordering,
  getCurrentInProgress,
  getTouching,
  isBlocked,
  isOccupied,
  isTouching,
  whichPlayerPiece
} from '../../game_logic/base_util';
import addDefaults from '../../game_logic/add_defaults';
import moveDefaults from '../../game_logic/move_defaults';
import captureDefaults from '../../game_logic/capture_defaults';

const ADD_LIMIT = 3;
const MOVE_LIMIT = 3;

const purpleAvailableSpaces = (game, world, players, board, space) =>
  getBordering(world, board, space).concat(getTouching(board, space))
    .filter(possibleSpace =>
      !isBlocked(game, players, Immutable.Map().set('name', possibleSpace.get('board')), possibleSpace.get('space'))
    );

const purpleBlockedAvailableSpaces = (game, world, players, board, space) =>
  getBordering(world, board, space).concat(getTouching(board, space))
    .filter(possibleSpace =>
      !isBlocked(game, Immutable.List([players.get(game.get('playerTurn'))]), Immutable.Map().set('name', possibleSpace.get('board')), possibleSpace.get('space'))
    );

const purpleSpecialInit = () => {
  store.dispatch(updateCurrentAction({ type: 'SPECIAL', role: 'Madrasa Qashani', data: {} }));
  purpleSpecialEntryPoint();
};

const purpleSpecialEntryPoint = () => {
  const { game, world, players } = store.getState();
  const highlightedSpaces = allTouchingEnemies(game, world, players);
  store.dispatch(updateHighlighted(highlightedSpaces.map(space => `${space.get('board')}_${space.get('space')}_primary`)));
  let triggers = Immutable.Map();
  highlightedSpaces.forEach((space) => {
    triggers = triggers.set(`${space.get('board')}_${space.get('space')}`, () => purpleSpecialChoosePiece(`${space.get('board')}_${space.get('space')}`));
  });
  store.dispatch(addTriggers(triggers));
  store.dispatch(updatePrompt('Please choose an enemy piece to teleport'));
};

const purpleSpecialChoosePiece = (space) => {
  const { game, world, players } = store.getState();
  clearHighlightAndTriggers();
  const spaceArray = space.split('_');
  const oldPlayerIndex = whichPlayerPiece(players, spaceArray[0], spaceArray[1]);
  const oldModifiers = players.getIn([oldPlayerIndex, 'pieces'])
    .filter(piece => piece.get('board') === spaceArray[0] && piece.get('space') === spaceArray[1])
    .first()
    .get('modifiers');

  const openSpaces = world.get('rooms').reduce((a, b) =>
    a.concat(b.getIn(['board', 'type', 'spaces']).map((spaceInfo, spaceKey) =>
      Immutable.Map({ board: b.getIn(['board', 'name']), space: spaceKey })
    )),
    Immutable.List()
  )
  .filter(fSpace => !isOccupied(players, Immutable.Map({ name: fSpace.get('board') }), fSpace.get('space')))
  .filter(fSpace => !players.getIn([game.get('playerTurn'), 'pieces']).some(pplPiece =>
    isTouching(world, Immutable.fromJS(Boards[fSpace.get('board')]), fSpace.get('space'), Immutable.fromJS(Boards[pplPiece.get('board')]), pplPiece.get('space'))));

  store.dispatch(updateHighlighted(openSpaces.map(openSpace => `${openSpace.get('board')}_${openSpace.get('space')}_primary`).push(`${space}_secondary`)));
  let triggers = Immutable.Map();
  openSpaces.forEach((openSpace) => {
    triggers = triggers.set(`${openSpace.get('board')}_${openSpace.get('space')}`, () => purpleSpecialChooseSpace(`${openSpace.get('board')}_${openSpace.get('space')}`));
  });
  store.dispatch(addTriggers(triggers));

  store.dispatch(appendCurrentAction({
    to: {
      board: spaceArray[0],
      space: spaceArray[1]
    },
    player: oldPlayerIndex,
    modifiers: oldModifiers
  }));
  store.dispatch(updatePieces(oldPlayerIndex, players.getIn([oldPlayerIndex, 'pieces'])
    .filter(piece => !(piece.get('board') === spaceArray[0] && piece.get('space') === spaceArray[1]))));
  store.dispatch(updatePrompt('Please choose a space to teleport their piece to'));
};

const purpleSpecialChooseSpace = (space) => {
  const { game } = store.getState();
  clearHighlightAndTriggers();
  const spaceArray = space.split('_');
  const firstMove = game.getIn(['currentAction', 'data', 'moves', 0]);

  store.dispatch(updateHighlighted(`${space}_primary`));
  store.dispatch(appendCurrentAction({
    from: {
      board: firstMove.getIn(['to', 'board']),
      space: firstMove.getIn(['to', 'space'])
    },
    to: {
      board: spaceArray[0],
      space: spaceArray[1]
    },
    player: firstMove.get('player')
  }));
  store.dispatch(updateCanConfirm(true));
  store.dispatch(updatePrompt('Please cancel or confirm your selection'));
};

const purpleSpecialConfirm = () => {
  const { game, players } = store.getState();
  const newPieces = getCurrentInProgress(game);
  const oldPlayer = newPieces.getIn([0, 'player']);
  store.dispatch(updatePieces(oldPlayer, players.getIn([oldPlayer, 'pieces']).concat(newPieces)));
};

const add = Object.assign({}, addDefaults,
  {
    canAddMore: (...args) => addDefaults.canAddMore(ADD_LIMIT, purpleAvailableSpaces, ...args),
    available: purpleAvailableSpaces,
    blockedAvailable: purpleBlockedAvailableSpaces
  }
);

const move = Object.assign({}, moveDefaults,
  {
    piecesCanStillMove: (...args) => moveDefaults.piecesCanStillMove(MOVE_LIMIT, ...args),
    available: purpleAvailableSpaces,
    blockedAvailable: purpleBlockedAvailableSpaces
  }
);

const capture = captureDefaults;

const special = {
  init: purpleSpecialInit,
  confirm: purpleSpecialConfirm
};

const path = serialize(scale(parse('M3 0v1h-2l-1 1 1 1h2v5h1v-4h2l1-1-1-1h-2v-2h-1z'), (22 / 8)));

const Purple = {
  name: 'Madrasa Qashani',
  boards: [Boards.TriangleB, Boards.RhombusA],
  scoreAdd: ADD_LIMIT,
  scoreAddUnique: true,
  scoreMove: MOVE_LIMIT,
  scoreMoveUnique: true,
  scoreSpecial: 'Teleport',
  primaryColor: '#840bbc',
  secondaryColor: '#3a2842',
  tabKey: 'purple',
  path,
  add,
  move,
  capture,
  special
};

export default Purple;

export {
  purpleAvailableSpaces,
  purpleBlockedAvailableSpaces,
  purpleSpecialInit,
  purpleSpecialEntryPoint,
  purpleSpecialChoosePiece,
  purpleSpecialChooseSpace,
  purpleSpecialConfirm
};
