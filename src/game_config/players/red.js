import Immutable from 'immutable';
import parse from 'parse-svg-path';
import scale from 'scale-svg-path';
import serialize from 'serialize-svg-path';
import Boards from '../boards/boards-enum';
import store from '../../utils/store';
import RedFuel from '../../components/player-specific/red-fuel';
import { updateModifier, updatePieces } from '../../actions/players';
import { addTriggers, appendCurrentAction, updateCanConfirm, updateCurrentAction, updateHighlighted, updatePrompt } from '../../actions/game';
import { addDamage, clearHighlightAndTriggers, getBordering, getTouching, isOccupied, whichPlayerPiece } from '../../game_logic/base_util';
import addDefaults from '../../game_logic/add_defaults';
import moveDefaults from '../../game_logic/move_defaults';
import captureDefaults from '../../game_logic/capture_defaults';

const ADD_LIMIT = 3;
const MOVE_LIMIT = 4;

const redSpecialGetOwnBordering = () => {
  const { game, world, players } = store.getState();
  const currentPlayer = players.get(game.get('playerTurn'));

  const primedPieces = game.getIn(['currentAction', 'data', 'moves']).map(piece => piece.get('to'));

  return primedPieces.flatMap(piece =>
    getBordering(world, Immutable.fromJS(Boards[piece.get('board')]), piece.get('space'))
      .filter(fPiece =>
        isOccupied(
          Immutable.List([currentPlayer]),
          Immutable.fromJS(Boards[fPiece.get('board')]),
          fPiece.get('space')
        )
      )
  )
  .groupBy(piece => `${piece.get('board')}_${piece.get('space')}`)
  .map(x => x.first());
};

const redSpecialGetEnemyTouching = () => {
  const { game, world, players } = store.getState();
  const primedPieces = game.getIn(['currentAction', 'data', 'moves']).map(piece => piece.get('to'));

  if (primedPieces.size < 2) {
    return Immutable.List();
  }

  return primedPieces.flatMap(piece =>
    getBordering(world, Immutable.fromJS(Boards[piece.get('board')]), piece.get('space'))
      .concat(getTouching(Immutable.fromJS(Boards[piece.get('board')]), piece.get('space')))
      .filter(fPiece => isOccupied(players.remove(game.get('playerTurn')), Immutable.fromJS(Boards[fPiece.get('board')]), fPiece.get('space')))
  )
  .groupBy(piece => `${piece.get('board')}_${piece.get('space')}`)
  .map(x => x.first());
};

const redSpecialInit = () => {
  store.dispatch(updateCurrentAction({ type: 'SPECIAL', role: 'Ignis Rex', data: {} }));
  redSpecialEntryPoint();
};

const redSpecialEntryPoint = () => {
  const { game, players } = store.getState();
  const highlightedSpaces = players.getIn([game.get('playerTurn'), 'pieces']).filter(piece => piece.get('space') !== 'captured');
  store.dispatch(updateHighlighted(highlightedSpaces.map(space => `${space.get('board')}_${space.get('space')}_primary`)));
  let triggers = Immutable.Map();
  highlightedSpaces.forEach((space) => {
    triggers = triggers.set(`${space.get('board')}_${space.get('space')}`, () => redSpecialChoosePiece(`${space.get('board')}_${space.get('space')}`));
  });
  store.dispatch(addTriggers(triggers));
  store.dispatch(updatePrompt('Please choose pieces to detonate'));
};

const redSpecialChoosePiece = (space) => {
  const { game, players } = store.getState();
  const spaceArray = space.split('_');
  clearHighlightAndTriggers();
  store.dispatch(appendCurrentAction({
    to: {
      board: spaceArray[0],
      space: spaceArray[1]
    },
    player: game.get('playerTurn'),
    modifiers: []
  }));

  const updatedPieces = players.getIn([game.get('playerTurn'), 'pieces']).filter(piece => !(piece.get('board') === spaceArray[0] && piece.get('space') === spaceArray[1]));
  store.dispatch(updatePieces(game.get('playerTurn'), updatedPieces));

  const primaryHighlightedSpaces = redSpecialGetOwnBordering().toList();
  const secondaryHighlightedSpaces = redSpecialGetEnemyTouching().map(piece => `${piece.get('board')}_${piece.get('space')}_secondary`).toList();

  let triggers = Immutable.Map();
  primaryHighlightedSpaces.forEach((hSpace) => {
    triggers = triggers.set(`${hSpace.get('board')}_${hSpace.get('space')}`, () => redSpecialChoosePiece(`${hSpace.get('board')}_${hSpace.get('space')}`));
  });
  store.dispatch(addTriggers(triggers));
  store.dispatch(updateHighlighted(primaryHighlightedSpaces.map(hSpace => `${hSpace.get('board')}_${hSpace.get('space')}_primary`).concat(secondaryHighlightedSpaces)));
  store.dispatch(updateCanConfirm(true));
};

const redSpecialConfirmEntryPoint = () => {
  const { players } = store.getState();
  const enemyPieces = redSpecialGetEnemyTouching();

  const sortedEnemies = enemyPieces.map(piece =>
    Immutable.Map({
      board: piece.get('board'),
      space: piece.get('space'),
      player: whichPlayerPiece(players, piece.get('board'), piece.get('space'))
    })
  )
    .groupBy(piece => piece.get('player'));

  sortedEnemies.forEach((destroyedPieces, playerIndex) => {
    const remainingPieces = players.getIn([playerIndex, 'pieces']).filter(piece =>
      !destroyedPieces.some(dPiece => dPiece.get('board') === piece.get('board') && dPiece.get('space') === piece.get('space'))
    );

    store.dispatch(updatePieces(playerIndex, remainingPieces));
    addDamage(playerIndex, destroyedPieces.size);
  });
  return enemyPieces.size;
};

const redSpecialConfirm = () => {
  const { game, players } = store.getState();
  const addedFuel = redSpecialConfirmEntryPoint();
  const currentFuel = (players.hasIn([game.get('playerTurn'), 'modifiers', 'redFuel'])) ? players.getIn([game.get('playerTurn'), 'modifiers', 'redFuel']) : 0;
  store.dispatch(updateModifier(game.get('playerTurn'), 'redFuel', (currentFuel + addedFuel)));
};

const add = Object.assign({}, addDefaults,
  {
    canAddMore: (...args) => addDefaults.canAddMore(ADD_LIMIT, addDefaults.available, ...args)
  }
);

const move = Object.assign({}, moveDefaults,
  {
    piecesCanStillMove: (...args) => moveDefaults.piecesCanStillMove(MOVE_LIMIT, ...args)
  }
);

const special = {
  init: redSpecialInit,
  confirm: redSpecialConfirm
};

const capture = captureDefaults;

const path = serialize(scale(parse(`M571 589q-10-25-34-35t-49 0q-108 44-191 127t-127 191q-10 25 0 49t35 34q13
  5 24 5 42 0 60-40 34-84 98.5-148.5t148.5-98.5q25-11 35-35t0-49zm942-356l46 46-244 243 68 68q19 19 19 45.5t-19
  45.5l-64 64q89 161 89 343 0 143-55.5 273.5t-150 225-225 150-273.5 55.5-273.5-55.5-225-150-150-225-55.5-273.5 55.5-273.5
  150-225 225-150 273.5-55.5q182 0 343 89l64-64q19-19 45.5-19t45.5 19l68 68zm8-56q-10 10-22 10-13 0-23-10l-91-90q-9-10-9-23
  t9-23q10-9 23-9t23 9l90 91q10 9 10 22.5t-10 22.5zm230 230q-11 9-23 9t-23-9l-90-91q-10-9-10-22.5t10-22.5q9-10 22.5-10t22.5
  10l91 90q9 10 9 23t-9 23zm41-183q0 14-9 23t-23 9h-96q-14 0-23-9t-9-23 9-23 23-9h96q14 0 23 9t9 23zm-192-192v96q0 14-9 23
  t-23 9-23-9-9-23v-96q0-14 9-23t23-9 23 9 9 23zm151 55l-91 90q-10 10-22 10-13 0-23-10-10-9-10-22.5t10-22.5l90-91q10-9 23-9
  t23 9q9 10 9 23t-9 23z`), (20 / 1792)));

const Red = {
  name: 'Ignis Rex',
  boards: [Boards.SnowflakeA, Boards.RhombusB],
  scoreAdd: ADD_LIMIT,
  scoreMove: MOVE_LIMIT,
  scoreSpecial: 'Explode',
  scoreComponent: RedFuel,
  primaryColor: '#db0a00',
  secondaryColor: '#ffffff',
  tabKey: 'red',
  path,
  add,
  move,
  special,
  capture
};

export default Red;

export {
  redSpecialGetOwnBordering,
  redSpecialGetEnemyTouching,
  redSpecialInit,
  redSpecialEntryPoint,
  redSpecialChoosePiece,
  redSpecialConfirmEntryPoint,
  redSpecialConfirm
};
