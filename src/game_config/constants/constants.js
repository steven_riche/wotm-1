/*
  The number of pieces required (both of your own and the other player's) to capture
*/
export const captureRequirements = [
  { own: 2, other: 0 },
  { own: 3, other: 0 },
  { own: 3, other: 1 },
  { own: 4, other: 0 }
];

/*
  The number of pieces required on blue spaces to draw a card for each score
*/
export const cardRequirements = [2, 3, 4, 5];

/*
  The number of points required to win the game
*/
export const winningScore = 4;

/*
  The amount of damage required to receive a free action
*/
export const damageToFreeAction = 5;

/*
  The total number of pieces each player can have in play at once
*/
export const pieceLimit = 15;
