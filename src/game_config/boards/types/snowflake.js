const SnowflakeBoard = {
  name: 'Snowflake',
  doors: {
    n: ['wh1a', 'wh1c'],
    e: ['wh3c', 'wh2a'],
    s: ['wh4a', 'wh4c'],
    w: ['wh5a', 'wh6c']
  },
  spaces: {
    wh1: {
      centerX: 115.84,
      centerY: 79.2,
      points: [{
        x: 120,
        y: 48
      }, {
        x: 140.8,
        y: 84
      }, {
        x: 120,
        y: 120
      }, {
        x: 99.2,
        y: 84
      }, {
        x: 99.2,
        y: 60
      }],
      bordering: ['wh1a', 'wh1c', 'wh6a', 'wh2', 'wh6'],
      touching: ['wh3', 'wh4', 'wh5']
    },
    wh2: {
      centerX: 153.28,
      centerY: 96,
      points: [{
        x: 140.8,
        y: 84
      }, {
        x: 120,
        y: 120
      }, {
        x: 161.6,
        y: 120
      }, {
        x: 182.4,
        y: 84
      }, {
        x: 161.6,
        y: 72
      }],
      bordering: ['wh2a', 'wh2c', 'wh1a', 'wh3', 'wh1'],
      touching: ['wh4', 'wh5', 'wh6']
    },
    wh3: {
      centerX: 157.44,
      centerY: 136.8,
      points: [{
        x: 161.6,
        y: 120
      }, {
        x: 120,
        y: 120
      }, {
        x: 140.8,
        y: 156
      }, {
        x: 182.4,
        y: 156
      }, {
        x: 182.4,
        y: 132
      }],
      bordering: ['wh3a', 'wh3c', 'wh2a', 'wh2', 'wh4'],
      touching: ['wh5', 'wh6', 'wh1']
    },
    wh4: {
      centerX: 124.16,
      centerY: 160.8,
      points: [{
        x: 120,
        y: 192
      }, {
        x: 99.2,
        y: 156
      }, {
        x: 120,
        y: 120
      }, {
        x: 140.8,
        y: 156
      }, {
        x: 140.8,
        y: 180
      }],
      bordering: ['wh4a', 'wh4c', 'wh3a', 'wh3', 'wh5'],
      touching: ['wh6', 'wh1', 'wh2']
    },
    wh5: {
      centerX: 86.72,
      centerY: 144,
      points: [{
        x: 57.6,
        y: 156
      }, {
        x: 78.4,
        y: 120
      }, {
        x: 120,
        y: 120
      }, {
        x: 99.2,
        y: 156
      }, {
        x: 78.4,
        y: 168
      }],
      bordering: ['wh5a', 'wh5c', 'wh4a', 'wh4', 'wh6'],
      touching: ['wh1', 'wh2', 'wh3']
    },
    wh6: {
      centerX: 82.56,
      centerY: 103.2,
      points: [{
        x: 57.6,
        y: 84
      }, {
        x: 99.2,
        y: 84
      }, {
        x: 120,
        y: 120
      }, {
        x: 78.4,
        y: 120
      }, {
        x: 57.6,
        y: 108
      }],
      bordering: ['wh6a', 'wh6c', 'wh5a', 'wh5', 'wh1'],
      touching: ['wh2', 'wh3', 'wh4']
    },
    wh1a: {
      centerX: 144.96,
      centerY: 52.8,
      points: [{
        x: 140.8,
        y: 84
      }, {
        x: 120,
        y: 48
      }, {
        x: 140.8,
        y: 12
      }, {
        x: 161.6,
        y: 48
      }, {
        x: 161.6,
        y: 72
      }],
      bordering: ['wh1', 'wh1c', 'wh2', 'wh2c'],
      touching: ['wh2d']
    },
    wh2a: {
      centerX: 190.72,
      centerY: 108,
      points: [{
        x: 161.6,
        y: 120
      }, {
        x: 182.4,
        y: 84
      }, {
        x: 224,
        y: 84
      }, {
        x: 203.2,
        y: 120
      }, {
        x: 182.4,
        y: 132
      }],
      bordering: ['wh2', 'wh2c', 'wh3', 'wh3c'],
      touching: []
    },
    wh3a: {
      centerX: 165.76,
      centerY: 175.2,
      points: [{
        x: 140.8,
        y: 156
      }, {
        x: 182.4,
        y: 156
      }, {
        x: 203.2,
        y: 192
      }, {
        x: 161.6,
        y: 192
      }, {
        x: 140.8,
        y: 180
      }],
      bordering: ['wh3', 'wh3b', 'wh3c', 'wh4', 'wh4c'],
      touching: []
    },
    wh4a: {
      centerX: 95.04,
      centerY: 187.2,
      points: [{
        x: 78.4,
        y: 192
      }, {
        x: 99.2,
        y: 228
      }, {
        x: 120,
        y: 192
      }, {
        x: 99.2,
        y: 156
      }, {
        x: 78.4,
        y: 168
      }],
      bordering: ['wh4', 'wh4c', 'wh5', 'wh5c'],
      touching: ['wh5d']
    },
    wh5a: {
      centerX: 49.28,
      centerY: 132,
      points: [{
        x: 36.8,
        y: 120
      }, {
        x: 16,
        y: 156
      }, {
        x: 57.6,
        y: 156
      }, {
        x: 78.4,
        y: 120
      }, {
        x: 57.6,
        y: 108
      }],
      bordering: ['wh5', 'wh5c', 'wh6', 'wh6c'],
      touching: []
    },
    wh6a: {
      centerX: 74.24,
      centerY: 64.8,
      points: [{
        x: 78.4,
        y: 48
      }, {
        x: 36.8,
        y: 48
      }, {
        x: 57.6,
        y: 84
      }, {
        x: 99.2,
        y: 84
      }, {
        x: 99.2,
        y: 60
      }],
      bordering: ['wh6', 'wh6b', 'wh6c', 'wh1', 'wh1c'],
      touching: []
    },
    wh3b: {
      centerX: 199.04,
      centerY: 208.8,
      points: [{
        x: 203.2,
        y: 192
      }, {
        x: 161.6,
        y: 192
      }, {
        x: 182.4,
        y: 228
      }, {
        x: 224,
        y: 228
      }, {
        x: 224,
        y: 204
      }],
      bordering: ['wh3a'],
      touching: ['wh3c', 'wh4c']
    },
    wh6b: {
      centerX: 40.96,
      centerY: 31.2,
      points: [{
        x: 36.8,
        y: 48
      }, {
        x: 78.4,
        y: 48
      }, {
        x: 57.6,
        y: 12
      }, {
        x: 16,
        y: 12
      }, {
        x: 16,
        y: 36
      }],
      bordering: ['wh6a'],
      touching: ['wh6c', 'wh1c']
    },
    wh1c: {
      centerX: 107.52,
      centerY: 36,
      points: [{
        x: 78.4,
        y: 48
      }, {
        x: 99.2,
        y: 12
      }, {
        x: 140.8,
        y: 12
      }, {
        x: 120,
        y: 48
      }, {
        x: 99.2,
        y: 60
      }],
      bordering: ['wh1', 'wh1a', 'wh6a'],
      touching: ['wh6b']
    },
    wh2c: {
      centerX: 186.56,
      centerY: 66.4,
      points: [{
        x: 161.6,
        y: 48
      }, {
        x: 203.2,
        y: 48
      }, {
        x: 224,
        y: 84
      }, {
        x: 182.4,
        y: 84
      }, {
        x: 161.6,
        y: 72
      }],
      bordering: ['wh2', 'wh2a', 'wh2d', 'wh1a'],
      touching: []
    },
    wh3c: {
      centerX: 199.04,
      centerY: 151.2,
      points: [{
        x: 182.4,
        y: 156
      }, {
        x: 203.2,
        y: 192
      }, {
        x: 224,
        y: 156
      }, {
        x: 203.2,
        y: 120
      }, {
        x: 182.4,
        y: 132
      }],
      bordering: ['wh3', 'wh3a', 'wh2a'],
      touching: ['wh3b']
    },
    wh4c: {
      centerX: 132.48,
      centerY: 204,
      points: [{
        x: 120,
        y: 192
      }, {
        x: 99.2,
        y: 228
      }, {
        x: 140.8,
        y: 228
      }, {
        x: 161.6,
        y: 192
      }, {
        x: 140.8,
        y: 180
      }],
      bordering: ['wh4', 'wh4a', 'wh3a'],
      touching: ['wh3b']
    },
    wh5c: {
      centerX: 53.44,
      centerY: 172.8,
      points: [{
        x: 57.6,
        y: 156
      }, {
        x: 16,
        y: 156
      }, {
        x: 36.8,
        y: 192
      }, {
        x: 78.4,
        y: 192
      }, {
        x: 78.4,
        y: 168
      }],
      bordering: ['wh5', 'wh5a', 'wh5d', 'wh4a'],
      touching: []
    },
    wh6c: {
      centerX: 40.96,
      centerY: 88.8,
      points: [{
        x: 57.6,
        y: 84
      }, {
        x: 36.8,
        y: 48
      }, {
        x: 16,
        y: 84
      }, {
        x: 36.8,
        y: 120
      }, {
        x: 57.6,
        y: 108
      }],
      bordering: ['wh6', 'wh6a', 'wh5a'],
      touching: ['wh6b']
    },
    wh2d: {
      centerX: 194.88,
      centerY: 24,
      points: [{
        x: 182.4,
        y: 12
      }, {
        x: 161.6,
        y: 48
      }, {
        x: 203.2,
        y: 48
      }, {
        x: 224,
        y: 12
      }, {
        x: 203.2,
        y: 0
      }],
      bordering: ['wh2c'],
      touching: ['wh1a']
    },
    wh5d: {
      centerX: 45.12,
      centerY: 216,
      points: [{
        x: 16,
        y: 228
      }, {
        x: 36.8,
        y: 192
      }, {
        x: 78.4,
        y: 192
      }, {
        x: 57.6,
        y: 228
      }, {
        x: 36.8,
        y: 240
      }],
      bordering: ['wh5c'],
      touching: ['wh4a']
    }
  }
};

export default SnowflakeBoard;
