const DiamondBoard = {
  name: 'Diamond',
  doors: {
    n: ['a2', 'a3'],
    e: ['c4', 'e4'],
    s: ['g2', 'g3'],
    w: ['c1', 'e1']
  },
  spaces: {
    a1: {
      centerX: 32.91,
      centerY: 32.91,
      points: [{
        x: 3.87,
        y: 32.91
      }, {
        x: 32.91,
        y: 3.87
      }, {
        x: 61.95,
        y: 32.91
      }, {
        x: 32.91,
        y: 61.95
      }],
      bordering: ['b1'],
      touching: ['a2', 'c1']
    },
    a2: {
      centerX: 90.99,
      centerY: 32.91,
      points: [{
        x: 61.95,
        y: 32.91
      }, {
        x: 90.99,
        y: 3.87
      }, {
        x: 120.03,
        y: 32.91
      }, {
        x: 90.99,
        y: 61.95
      }],
      bordering: ['b1', 'b2'],
      touching: ['a1', 'a3', 'c2']
    },
    a3: {
      centerX: 149.07,
      centerY: 32.91,
      points: [{
        x: 120.03,
        y: 32.91
      }, {
        x: 149.07,
        y: 3.87
      }, {
        x: 178.11,
        y: 32.91
      }, {
        x: 149.07,
        y: 61.95
      }],
      bordering: ['b2', 'b3'],
      touching: ['a2', 'a4', 'c3']
    },
    a4: {
      centerX: 207.15,
      centerY: 32.91,
      points: [{
        x: 178.11,
        y: 32.91
      }, {
        x: 207.15,
        y: 3.87
      }, {
        x: 236.19,
        y: 32.91
      }, {
        x: 207.15,
        y: 61.95
      }],
      bordering: ['b3'],
      touching: ['a3', 'c4']
    },
    b1: {
      centerX: 61.95,
      centerY: 61.95,
      points: [{
        x: 32.91,
        y: 61.95
      }, {
        x: 61.95,
        y: 32.91
      }, {
        x: 90.99,
        y: 61.95
      }, {
        x: 61.95,
        y: 90.99
      }],
      bordering: ['a1', 'a2', 'c1', 'c2'],
      touching: ['b2', 'd1']
    },
    b2: {
      centerX: 120.03,
      centerY: 61.95,
      points: [{
        x: 90.99,
        y: 61.95
      }, {
        x: 120.03,
        y: 32.91
      }, {
        x: 149.07,
        y: 61.95
      }, {
        x: 120.03,
        y: 90.99
      }],
      bordering: ['a2', 'a3', 'c2', 'c3'],
      touching: ['b1', 'b3', 'd2']
    },
    b3: {
      centerX: 178.11,
      centerY: 61.95,
      points: [{
        x: 149.07,
        y: 61.95
      }, {
        x: 178.11,
        y: 32.91
      }, {
        x: 207.15,
        y: 61.95
      }, {
        x: 178.11,
        y: 90.99
      }],
      bordering: ['a3', 'a4', 'c3', 'c4'],
      touching: ['b2', 'd3']
    },
    c1: {
      centerX: 32.91,
      centerY: 90.99,
      points: [{
        x: 3.87,
        y: 90.99
      }, {
        x: 32.91,
        y: 61.95
      }, {
        x: 61.95,
        y: 90.99
      }, {
        x: 32.91,
        y: 120.03
      }],
      bordering: ['b1', 'd1'],
      touching: ['a1', 'c2', 'e1']
    },
    c2: {
      centerX: 90.99,
      centerY: 90.99,
      points: [{
        x: 61.95,
        y: 90.99
      }, {
        x: 90.99,
        y: 61.95
      }, {
        x: 120.03,
        y: 90.99
      }, {
        x: 90.99,
        y: 120.03
      }],
      bordering: ['b1', 'b2', 'd1', 'd2'],
      touching: ['c1', 'c3', 'a2', 'e2']
    },
    c3: {
      centerX: 149.07,
      centerY: 90.99,
      points: [{
        x: 120.03,
        y: 90.99
      }, {
        x: 149.07,
        y: 61.95
      }, {
        x: 178.11,
        y: 90.99
      }, {
        x: 149.07,
        y: 120.03
      }],
      bordering: ['b2', 'b3', 'd2', 'd3'],
      touching: ['a3', 'c2', 'c4', 'e3']
    },
    c4: {
      centerX: 207.15,
      centerY: 90.99,
      points: [{
        x: 178.11,
        y: 90.99
      }, {
        x: 207.15,
        y: 61.95
      }, {
        x: 236.19,
        y: 90.99
      }, {
        x: 207.15,
        y: 120.03
      }],
      bordering: ['b3', 'd3'],
      touching: ['a4', 'c3', 'e4']
    },
    d1: {
      centerX: 61.95,
      centerY: 120.03,
      points: [{
        x: 32.91,
        y: 120.03
      }, {
        x: 61.95,
        y: 90.99
      }, {
        x: 90.99,
        y: 120.03
      }, {
        x: 61.95,
        y: 149.07
      }],
      bordering: ['c1', 'c2', 'e1', 'e2'],
      touching: ['b1', 'd2', 'f1']
    },
    d2: {
      centerX: 120.03,
      centerY: 120.03,
      points: [{
        x: 90.99,
        y: 120.03
      }, {
        x: 120.03,
        y: 90.99
      }, {
        x: 149.07,
        y: 120.03
      }, {
        x: 120.03,
        y: 149.07
      }],
      bordering: ['c2', 'c3', 'e2', 'e3'],
      touching: ['b2', 'd1', 'd3', 'f2']
    },
    d3: {
      centerX: 178.11,
      centerY: 120.03,
      points: [{
        x: 149.07,
        y: 120.03
      }, {
        x: 178.11,
        y: 90.99
      }, {
        x: 207.15,
        y: 120.03
      }, {
        x: 178.11,
        y: 149.07
      }],
      bordering: ['c3', 'c4', 'e3', 'e4'],
      touching: ['b3', 'd2', 'f3']
    },
    e1: {
      centerX: 32.91,
      centerY: 149.07,
      points: [{
        x: 3.87,
        y: 149.07
      }, {
        x: 32.91,
        y: 120.03
      }, {
        x: 61.95,
        y: 149.07
      }, {
        x: 32.91,
        y: 178.11
      }],
      bordering: ['d1', 'f1'],
      touching: ['c1', 'e2', 'g1']
    },
    e2: {
      centerX: 90.99,
      centerY: 149.07,
      points: [{
        x: 61.95,
        y: 149.07
      }, {
        x: 90.99,
        y: 120.03
      }, {
        x: 120.03,
        y: 149.07
      }, {
        x: 90.99,
        y: 178.11
      }],
      bordering: ['d1', 'd2', 'f1', 'f2'],
      touching: ['e1', 'e3', 'c2', 'g2']
    },
    e3: {
      centerX: 149.07,
      centerY: 149.07,
      points: [{
        x: 120.03,
        y: 149.07
      }, {
        x: 149.07,
        y: 120.03
      }, {
        x: 178.11,
        y: 149.07
      }, {
        x: 149.07,
        y: 178.11
      }],
      bordering: ['d2', 'd3', 'f2', 'f3'],
      touching: ['c3', 'e2', 'e4', 'g3']
    },
    e4: {
      centerX: 207.15,
      centerY: 149.07,
      points: [{
        x: 178.11,
        y: 149.07
      }, {
        x: 207.15,
        y: 120.03
      }, {
        x: 236.19,
        y: 149.07
      }, {
        x: 207.15,
        y: 178.11
      }],
      bordering: ['d3', 'f3'],
      touching: ['c4', 'e3', 'g4']
    },
    f1: {
      centerX: 61.95,
      centerY: 178.11,
      points: [{
        x: 32.91,
        y: 178.11
      }, {
        x: 61.95,
        y: 149.07
      }, {
        x: 90.99,
        y: 178.11
      }, {
        x: 61.95,
        y: 207.15
      }],
      bordering: ['e1', 'e2', 'g1', 'g2'],
      touching: ['d1', 'f2']
    },
    f2: {
      centerX: 120.03,
      centerY: 178.11,
      points: [{
        x: 90.99,
        y: 178.11
      }, {
        x: 120.03,
        y: 149.07
      }, {
        x: 149.07,
        y: 178.11
      }, {
        x: 120.03,
        y: 207.15
      }],
      bordering: ['e2', 'e3', 'g2', 'g3'],
      touching: ['d2', 'f1', 'f3']
    },
    f3: {
      centerX: 178.11,
      centerY: 178.11,
      points: [{
        x: 149.07,
        y: 178.11
      }, {
        x: 178.11,
        y: 149.07
      }, {
        x: 207.15,
        y: 178.11
      }, {
        x: 178.11,
        y: 207.15
      }],
      bordering: ['e3', 'e4', 'g3', 'g4'],
      touching: ['d3', 'f2']
    },
    g1: {
      centerX: 32.91,
      centerY: 207.15,
      points: [{
        x: 3.87,
        y: 207.15
      }, {
        x: 32.91,
        y: 178.11
      }, {
        x: 61.95,
        y: 207.15
      }, {
        x: 32.91,
        y: 236.19
      }],
      bordering: ['f1'],
      touching: ['e1', 'g2']
    },
    g2: {
      centerX: 90.99,
      centerY: 207.15,
      points: [{
        x: 61.95,
        y: 207.15
      }, {
        x: 90.99,
        y: 178.11
      }, {
        x: 120.03,
        y: 207.15
      }, {
        x: 90.99,
        y: 236.19
      }],
      bordering: ['f1', 'f2'],
      touching: ['g1', 'g3', 'e2']
    },
    g3: {
      centerX: 149.07,
      centerY: 207.15,
      points: [{
        x: 120.03,
        y: 207.15
      }, {
        x: 149.07,
        y: 178.11
      }, {
        x: 178.11,
        y: 207.15
      }, {
        x: 149.07,
        y: 236.19
      }],
      bordering: ['f2', 'f3'],
      touching: ['e3', 'g2', 'g4']
    },
    g4: {
      centerX: 207.15,
      centerY: 207.15,
      points: [{
        x: 178.11,
        y: 207.15
      }, {
        x: 207.15,
        y: 178.11
      }, {
        x: 236.19,
        y: 207.15
      }, {
        x: 207.15,
        y: 236.19
      }],
      bordering: ['f3'],
      touching: ['e4', 'g3']
    }
  }
};

export default DiamondBoard;
