const CobblestoneBoard = {
  name: 'Cobblestone',
  doors: {
    n: ['a1', 'a2', 'b2'],
    e: ['c4', 'e3', 'g2'],
    s: ['j2', 'i1', 'j3'],
    w: ['d1', 'f1', 'h1']
  },
  spaces: {
    a1: {
      centerX: 84.8,
      centerY: 23.2,
      points: [{
        x: 65,
        y: -1
      }, {
        x: 65,
        y: 43
      }, {
        x: 109,
        y: 43
      }, {
        x: 109,
        y: 21
      }, {
        x: 87,
        y: -1
      }],
      bordering: ['b1', 'b2', 'c1', 'c2'],
      touching: []
    },
    a2: {
      centerX: 177.2,
      centerY: 23.2,
      points: [{
        x: 153,
        y: 21
      }, {
        x: 153,
        y: 43
      }, {
        x: 197,
        y: 43
      }, {
        x: 197,
        y: -1
      }, {
        x: 175,
        y: -1
      }],
      bordering: ['a3', 'b2', 'c3'],
      touching: ['c4']
    },
    a3: {
      centerX: 216.8,
      centerY: 23.2,
      points: [{
        x: 197,
        y: -1
      }, {
        x: 197,
        y: 43
      }, {
        x: 241,
        y: 43
      }, {
        x: 241,
        y: 21
      }, {
        x: 219,
        y: -1
      }],
      bordering: ['a2', 'c4'],
      touching: ['c3']
    },
    b1: {
      centerX: 40.8,
      centerY: 40.8,
      points: [{
        x: 21,
        y: 65
      }, {
        x: 21,
        y: 21
      }, {
        x: 65,
        y: 21
      }, {
        x: 65,
        y: 43
      }, {
        x: 43,
        y: 65
      }],
      bordering: ['a1', 'c1', 'd1'],
      touching: []
    },
    b2: {
      centerX: 133.2,
      centerY: 40.8,
      points: [{
        x: 109,
        y: 21
      }, {
        x: 153,
        y: 21
      }, {
        x: 153,
        y: 65
      }, {
        x: 131,
        y: 65
      }, {
        x: 109,
        y: 43
      }],
      bordering: ['a1', 'a2', 'c2', 'c3', 'd2'],
      touching: []
    },
    c1: {
      centerX: 67.2,
      centerY: 67.2,
      points: [{
        x: 43,
        y: 65
      }, {
        x: 65,
        y: 43
      }, {
        x: 87,
        y: 43
      }, {
        x: 87,
        y: 87
      }, {
        x: 43,
        y: 87
      }],
      bordering: ['a1', 'b1', 'c2', 'd1', 'e1'],
      touching: ['e2']
    },
    c2: {
      centerX: 106.8,
      centerY: 67.2,
      points: [{
        x: 87,
        y: 43
      }, {
        x: 87,
        y: 87
      }, {
        x: 131,
        y: 87
      }, {
        x: 131,
        y: 65
      }, {
        x: 109,
        y: 43
      }],
      bordering: ['a1', 'b2', 'c1', 'd2', 'e2'],
      touching: ['e1']
    },
    c3: {
      centerX: 177.2,
      centerY: 62.8,
      points: [{
        x: 153,
        y: 43
      }, {
        x: 197,
        y: 43
      }, {
        x: 197,
        y: 87
      }, {
        x: 175,
        y: 87
      }, {
        x: 153,
        y: 65
      }],
      bordering: ['a2', 'b2', 'c4', 'd2', 'e3'],
      touching: ['a3']
    },
    c4: {
      centerX: 216.8,
      centerY: 62.8,
      points: [{
        x: 197,
        y: 43
      }, {
        x: 197,
        y: 87
      }, {
        x: 219,
        y: 87
      }, {
        x: 241,
        y: 65
      }, {
        x: 241,
        y: 43
      }],
      bordering: ['a3', 'c3', 'e3'],
      touching: ['a2']
    },
    d1: {
      centerX: 23.2,
      centerY: 84.8,
      points: [{
        x: -1,
        y: 65
      }, {
        x: 43,
        y: 65
      }, {
        x: 43,
        y: 109
      }, {
        x: 21,
        y: 109
      }, {
        x: -1,
        y: 87
      }],
      bordering: ['b1', 'c1', 'e1', 'f1'],
      touching: []
    },
    d2: {
      centerX: 150.8,
      centerY: 89.2,
      points: [{
        x: 131,
        y: 65
      }, {
        x: 153,
        y: 65
      }, {
        x: 175,
        y: 87
      }, {
        x: 175,
        y: 109
      }, {
        x: 131,
        y: 109
      }],
      bordering: ['b2', 'c2', 'c3', 'e2', 'e3', 'f2', 'f3'],
      touching: []
    },
    e1: {
      centerX: 67.2,
      centerY: 106.8,
      points: [{
        x: 43,
        y: 87
      }, {
        x: 87,
        y: 87
      }, {
        x: 87,
        y: 131
      }, {
        x: 65,
        y: 131
      }, {
        x: 43,
        y: 109
      }],
      bordering: ['c1', 'd1', 'e2', 'f1', 'g1'],
      touching: ['c2']
    },
    e2: {
      centerX: 106.8,
      centerY: 106.8,
      points: [{
        x: 87,
        y: 87
      }, {
        x: 87,
        y: 131
      }, {
        x: 109,
        y: 131
      }, {
        x: 131,
        y: 109
      }, {
        x: 131,
        y: 87
      }],
      bordering: ['c2', 'd2', 'e1', 'f2', 'g1'],
      touching: ['c1']
    },
    e3: {
      centerX: 199.2,
      centerY: 106.8,
      points: [{
        x: 175,
        y: 87
      }, {
        x: 175,
        y: 109
      }, {
        x: 197,
        y: 131
      }, {
        x: 219,
        y: 131
      }, {
        x: 219,
        y: 87
      }],
      bordering: ['c3', 'c4', 'd2', 'f3', 'g2'],
      touching: []
    },
    f1: {
      centerX: 40.8,
      centerY: 133.2,
      points: [{
        x: 21,
        y: 109
      }, {
        x: 43,
        y: 109
      }, {
        x: 65,
        y: 131
      }, {
        x: 65,
        y: 153
      }, {
        x: 21,
        y: 153
      }],
      bordering: ['d1', 'e1', 'g1', 'h1', 'h2'],
      touching: []
    },
    f2: {
      centerX: 133.2,
      centerY: 133.2,
      points: [{
        x: 131,
        y: 109
      }, {
        x: 153,
        y: 109
      }, {
        x: 153,
        y: 153
      }, {
        x: 109,
        y: 153
      }, {
        x: 109,
        y: 131
      }],
      bordering: ['d2', 'e2', 'f3', 'g1', 'h3'],
      touching: ['h4']
    },
    f3: {
      centerX: 172.8,
      centerY: 133.2,
      points: [{
        x: 153,
        y: 109
      }, {
        x: 153,
        y: 153
      }, {
        x: 197,
        y: 153
      }, {
        x: 197,
        y: 131
      }, {
        x: 175,
        y: 109
      }],
      bordering: ['d2', 'e3', 'f2', 'g2', 'h4'],
      touching: ['h3']
    },
    g1: {
      centerX: 89.2,
      centerY: 150.8,
      points: [{
        x: 65,
        y: 131
      }, {
        x: 109,
        y: 131
      }, {
        x: 109,
        y: 175
      }, {
        x: 87,
        y: 175
      }, {
        x: 65,
        y: 153
      }],
      bordering: ['e1', 'e2', 'f1', 'f2', 'h2', 'h3', 'i1'],
      touching: []
    },
    g2: {
      centerX: 216.8,
      centerY: 155.2,
      points: [{
        x: 197,
        y: 131
      }, {
        x: 219,
        y: 131
      }, {
        x: 241,
        y: 153
      }, {
        x: 241,
        y: 175
      }, {
        x: 197,
        y: 175
      }],
      bordering: ['e3', 'f3', 'h4', 'i2'],
      touching: []
    },
    h1: {
      centerX: 23.2,
      centerY: 177.2,
      points: [{
        x: 21,
        y: 153
      }, {
        x: 43,
        y: 153
      }, {
        x: 43,
        y: 197
      }, {
        x: -1,
        y: 197
      }, {
        x: -1,
        y: 175
      }],
      bordering: ['f1', 'h2', 'j1'],
      touching: ['j2']
    },
    h2: {
      centerX: 62.8,
      centerY: 177.2,
      points: [{
        x: 43,
        y: 153
      }, {
        x: 43,
        y: 197
      }, {
        x: 87,
        y: 197
      }, {
        x: 87,
        y: 175
      }, {
        x: 65,
        y: 153
      }],
      bordering: ['f1', 'g1', 'h1', 'i1', 'j2'],
      touching: ['j1']
    },
    h3: {
      centerX: 133.2,
      centerY: 172.8,
      points: [{
        x: 109,
        y: 153
      }, {
        x: 153,
        y: 153
      }, {
        x: 153,
        y: 197
      }, {
        x: 131,
        y: 197
      }, {
        x: 109,
        y: 175
      }],
      bordering: ['f2', 'g1', 'h4', 'i1', 'j3'],
      touching: ['f3']
    },
    h4: {
      centerX: 172.8,
      centerY: 172.8,
      points: [{
        x: 153,
        y: 153
      }, {
        x: 153,
        y: 197
      }, {
        x: 175,
        y: 197
      }, {
        x: 197,
        y: 175
      }, {
        x: 197,
        y: 153
      }],
      bordering: ['f3', 'h3', 'g2', 'i2', 'j3'],
      touching: ['f2']
    },
    i1: {
      centerX: 106.8,
      centerY: 199.2,
      points: [{
        x: 87,
        y: 175
      }, {
        x: 109,
        y: 175
      }, {
        x: 131,
        y: 197
      }, {
        x: 131,
        y: 219
      }, {
        x: 87,
        y: 219
      }],
      bordering: ['g1', 'h2', 'h3', 'j2', 'j3'],
      touching: []
    },
    i2: {
      centerX: 199.2,
      centerY: 199.2,
      points: [{
        x: 175,
        y: 197
      }, {
        x: 197,
        y: 175
      }, {
        x: 219,
        y: 175
      }, {
        x: 219,
        y: 219
      }, {
        x: 175,
        y: 219
      }],
      bordering: ['g2', 'h4', 'j3'],
      touching: []
    },
    j1: {
      centerX: 23.2,
      centerY: 216.8,
      points: [{
        x: -1,
        y: 197
      }, {
        x: 43,
        y: 197
      }, {
        x: 43,
        y: 241
      }, {
        x: 21,
        y: 241
      }, {
        x: -1,
        y: 219
      }],
      bordering: ['h1', 'j2'],
      touching: ['h2']
    },
    j2: {
      centerX: 62.8,
      centerY: 216.8,
      points: [{
        x: 43,
        y: 197
      }, {
        x: 43,
        y: 241
      }, {
        x: 65,
        y: 241
      }, {
        x: 87,
        y: 219
      }, {
        x: 87,
        y: 197
      }],
      bordering: ['h2', 'i1', 'j1'],
      touching: ['h1']
    },
    j3: {
      centerX: 155.2,
      centerY: 216.8,
      points: [{
        x: 131,
        y: 197
      }, {
        x: 175,
        y: 197
      }, {
        x: 175,
        y: 241
      }, {
        x: 153,
        y: 241
      }, {
        x: 131,
        y: 219
      }],
      bordering: ['h3', 'h4', 'i1', 'i2'],
      touching: []
    }
  }
};

export default CobblestoneBoard;
