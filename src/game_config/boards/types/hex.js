const HexBoard = {
  name: 'Hexagon',
  doors: {
    n: ['a3'],
    e: ['c5'],
    s: ['e3'],
    w: ['c1']
  },
  spaces: {
    a1: {
      centerX: 42,
      centerY: 30,
      points: [{
        x: 16,
        y: 30
      }, {
        x: 29,
        y: 7.5
      }, {
        x: 55,
        y: 7.5
      }, {
        x: 68,
        y: 30
      }, {
        x: 55,
        y: 52.5
      }, {
        x: 29,
        y: 52.5
      }],
      bordering: ['a2', 'b1'],
      touching: []
    },
    a2: {
      centerX: 81,
      centerY: 52.5,
      points: [{
        x: 55,
        y: 52.5
      }, {
        x: 68,
        y: 30
      }, {
        x: 94,
        y: 30
      }, {
        x: 107,
        y: 52.5
      }, {
        x: 94,
        y: 75
      }, {
        x: 68,
        y: 75
      }],
      bordering: ['a1', 'a3', 'b1', 'b2', 'b3'],
      touching: []
    },
    a3: {
      centerX: 120,
      centerY: 30,
      points: [{
        x: 94,
        y: 30
      }, {
        x: 107,
        y: 7.5
      }, {
        x: 133,
        y: 7.5
      }, {
        x: 146,
        y: 30
      }, {
        x: 133,
        y: 52.5
      }, {
        x: 107,
        y: 52.5
      }],
      bordering: ['a2', 'a4', 'b3'],
      touching: []
    },
    a4: {
      centerX: 159,
      centerY: 52.5,
      points: [{
        x: 133,
        y: 52.5
      }, {
        x: 146,
        y: 30
      }, {
        x: 172,
        y: 30
      }, {
        x: 185,
        y: 52.5
      }, {
        x: 172,
        y: 75
      }, {
        x: 146,
        y: 75
      }],
      bordering: ['a3', 'a5', 'b3', 'b4', 'b5'],
      touching: []
    },
    a5: {
      centerX: 198,
      centerY: 30,
      points: [{
        x: 172,
        y: 30
      }, {
        x: 185,
        y: 7.5
      }, {
        x: 211,
        y: 7.5
      }, {
        x: 224,
        y: 30
      }, {
        x: 211,
        y: 52.5
      }, {
        x: 185,
        y: 52.5
      }],
      bordering: ['a4', 'b5'],
      touching: []
    },
    b1: {
      centerX: 42,
      centerY: 75,
      points: [{
        x: 16,
        y: 75
      }, {
        x: 29,
        y: 52.5
      }, {
        x: 55,
        y: 52.5
      }, {
        x: 68,
        y: 75
      }, {
        x: 55,
        y: 97.5
      }, {
        x: 29,
        y: 97.5
      }],
      bordering: ['a1', 'a2', 'b2', 'c1'],
      touching: []
    },
    b2: {
      centerX: 81,
      centerY: 97.5,
      points: [{
        x: 55,
        y: 97.5
      }, {
        x: 68,
        y: 75
      }, {
        x: 94,
        y: 75
      }, {
        x: 107,
        y: 97.5
      }, {
        x: 94,
        y: 120
      }, {
        x: 68,
        y: 120
      }],
      bordering: ['a2', 'b1', 'b3', 'c1', 'c2', 'c3'],
      touching: []
    },
    b3: {
      centerX: 120,
      centerY: 75,
      points: [{
        x: 94,
        y: 75
      }, {
        x: 107,
        y: 52.5
      }, {
        x: 133,
        y: 52.5
      }, {
        x: 146,
        y: 75
      }, {
        x: 133,
        y: 97.5
      }, {
        x: 107,
        y: 97.5
      }],
      bordering: ['a2', 'a3', 'a4', 'b2', 'b4', 'c3'],
      touching: []
    },
    b4: {
      centerX: 159,
      centerY: 97.5,
      points: [{
        x: 133,
        y: 97.5
      }, {
        x: 146,
        y: 75
      }, {
        x: 172,
        y: 75
      }, {
        x: 185,
        y: 97.5
      }, {
        x: 172,
        y: 120
      }, {
        x: 146,
        y: 120
      }],
      bordering: ['a4', 'b3', 'b5', 'c3', 'c4', 'c5'],
      touching: []
    },
    b5: {
      centerX: 198,
      centerY: 75,
      points: [{
        x: 172,
        y: 75
      }, {
        x: 185,
        y: 52.5
      }, {
        x: 211,
        y: 52.5
      }, {
        x: 224,
        y: 75
      }, {
        x: 211,
        y: 97.5
      }, {
        x: 185,
        y: 97.5
      }],
      bordering: ['a4', 'a5', 'b4', 'c5'],
      touching: []
    },
    c1: {
      centerX: 42,
      centerY: 120,
      points: [{
        x: 16,
        y: 120
      }, {
        x: 29,
        y: 97.5
      }, {
        x: 55,
        y: 97.5
      }, {
        x: 68,
        y: 120
      }, {
        x: 55,
        y: 142.5
      }, {
        x: 29,
        y: 142.5
      }],
      bordering: ['b1', 'b2', 'c2', 'd1'],
      touching: []
    },
    c2: {
      centerX: 81,
      centerY: 142.5,
      points: [{
        x: 55,
        y: 142.5
      }, {
        x: 68,
        y: 120
      }, {
        x: 94,
        y: 120
      }, {
        x: 107,
        y: 142.5
      }, {
        x: 94,
        y: 165
      }, {
        x: 68,
        y: 165
      }],
      bordering: ['b2', 'c1', 'c3', 'd1', 'd2', 'd3'],
      touching: []
    },
    c3: {
      centerX: 120,
      centerY: 120,
      points: [{
        x: 94,
        y: 120
      }, {
        x: 107,
        y: 97.5
      }, {
        x: 133,
        y: 97.5
      }, {
        x: 146,
        y: 120
      }, {
        x: 133,
        y: 142.5
      }, {
        x: 107,
        y: 142.5
      }],
      bordering: ['b2', 'b3', 'b4', 'c2', 'c4', 'd3'],
      touching: []
    },
    c4: {
      centerX: 159,
      centerY: 142.5,
      points: [{
        x: 133,
        y: 142.5
      }, {
        x: 146,
        y: 120
      }, {
        x: 172,
        y: 120
      }, {
        x: 185,
        y: 142.5
      }, {
        x: 172,
        y: 165
      }, {
        x: 146,
        y: 165
      }],
      bordering: ['b4', 'c3', 'c5', 'd3', 'd4', 'd5'],
      touching: []
    },
    c5: {
      centerX: 198,
      centerY: 120,
      points: [{
        x: 172,
        y: 120
      }, {
        x: 185,
        y: 97.5
      }, {
        x: 211,
        y: 97.5
      }, {
        x: 224,
        y: 120
      }, {
        x: 211,
        y: 142.5
      }, {
        x: 185,
        y: 142.5
      }],
      bordering: ['b4', 'b5', 'c4', 'd5'],
      touching: []
    },
    d1: {
      centerX: 42,
      centerY: 165,
      points: [{
        x: 16,
        y: 165
      }, {
        x: 29,
        y: 142.5
      }, {
        x: 55,
        y: 142.5
      }, {
        x: 68,
        y: 165
      }, {
        x: 55,
        y: 187.5
      }, {
        x: 29,
        y: 187.5
      }],
      bordering: ['c1', 'c2', 'd2', 'e1'],
      touching: []
    },
    d2: {
      centerX: 81,
      centerY: 187.5,
      points: [{
        x: 55,
        y: 187.5
      }, {
        x: 68,
        y: 165
      }, {
        x: 94,
        y: 165
      }, {
        x: 107,
        y: 187.5
      }, {
        x: 94,
        y: 210
      }, {
        x: 68,
        y: 210
      }],
      bordering: ['c2', 'd1', 'd3', 'e1', 'e3'],
      touching: []
    },
    d3: {
      centerX: 120,
      centerY: 165,
      points: [{
        x: 94,
        y: 165
      }, {
        x: 107,
        y: 142.5
      }, {
        x: 133,
        y: 142.5
      }, {
        x: 146,
        y: 165
      }, {
        x: 133,
        y: 187.5
      }, {
        x: 107,
        y: 187.5
      }],
      bordering: ['c2', 'c3', 'c4', 'd2', 'd4', 'e3'],
      touching: []
    },
    d4: {
      centerX: 159,
      centerY: 187.5,
      points: [{
        x: 133,
        y: 187.5
      }, {
        x: 146,
        y: 165
      }, {
        x: 172,
        y: 165
      }, {
        x: 185,
        y: 187.5
      }, {
        x: 172,
        y: 210
      }, {
        x: 146,
        y: 210
      }],
      bordering: ['c4', 'd3', 'd5', 'e3', 'e5'],
      touching: []
    },
    d5: {
      centerX: 198,
      centerY: 165,
      points: [{
        x: 172,
        y: 165
      }, {
        x: 185,
        y: 142.5
      }, {
        x: 211,
        y: 142.5
      }, {
        x: 224,
        y: 165
      }, {
        x: 211,
        y: 187.5
      }, {
        x: 185,
        y: 187.5
      }],
      bordering: ['c4', 'c5', 'd4', 'e5'],
      touching: []
    },
    e1: {
      centerX: 42,
      centerY: 210,
      points: [{
        x: 16,
        y: 210
      }, {
        x: 29,
        y: 187.5
      }, {
        x: 55,
        y: 187.5
      }, {
        x: 68,
        y: 210
      }, {
        x: 55,
        y: 232.5
      }, {
        x: 29,
        y: 232.5
      }],
      bordering: ['d1', 'd2'],
      touching: []
    },
    e3: {
      centerX: 120,
      centerY: 210,
      points: [{
        x: 94,
        y: 210
      }, {
        x: 107,
        y: 187.5
      }, {
        x: 133,
        y: 187.5
      }, {
        x: 146,
        y: 210
      }, {
        x: 133,
        y: 232.5
      }, {
        x: 107,
        y: 232.5
      }],
      bordering: ['d2', 'd3', 'd4'],
      touching: []
    },
    e5: {
      centerX: 198,
      centerY: 210,
      points: [{
        x: 172,
        y: 210
      }, {
        x: 185,
        y: 187.5
      }, {
        x: 211,
        y: 187.5
      }, {
        x: 224,
        y: 210
      }, {
        x: 211,
        y: 232.5
      }, {
        x: 185,
        y: 232.5
      }],
      bordering: ['d4', 'd5'],
      touching: []
    }
  }
};

export default HexBoard;
