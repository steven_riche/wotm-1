const KiteBoard = {
  name: 'Kite',
  doors: {
    n: ['a2', 'a3'],
    e: ['c4', 'e4'],
    s: ['g2', 'g3'],
    w: ['d1']
  },
  spaces: {
    a1: {
      centerX: 50.98,
      centerY: 37.75,
      points: [{
        x: 21,
        y: 30.6
      }, {
        x: 70.5,
        y: 2
      }, {
        x: 70.5,
        y: 59.2
      }, {
        x: 41.9,
        y: 59.2
      }],
      bordering: ['a2', 'c1'],
      touching: ['c2']
    },
    a2: {
      centerX: 90.03,
      centerY: 37.75,
      points: [{
        x: 70.5,
        y: 2
      }, {
        x: 70.5,
        y: 59.2
      }, {
        x: 99.1,
        y: 59.2
      }, {
        x: 120,
        y: 30.6
      }],
      bordering: ['a1', 'b1', 'c2'],
      touching: ['a3', 'c1']
    },
    a3: {
      centerX: 149.98,
      centerY: 23.45,
      points: [{
        x: 120,
        y: 30.6
      }, {
        x: 140.9,
        y: 2
      }, {
        x: 169.5,
        y: 2
      }, {
        x: 169.5,
        y: 59.2
      }],
      bordering: ['b1', 'a4'],
      touching: ['a2', 'c3', 'b2', 'c4']
    },
    a4: {
      centerX: 189.03,
      centerY: 23.45,
      points: [{
        x: 169.5,
        y: 2
      }, {
        x: 169.5,
        y: 59.2
      }, {
        x: 219,
        y: 30.6
      }, {
        x: 198.1,
        y: 2
      }],
      bordering: ['a3', 'b2'],
      touching: ['b1', 'c3', 'c4']
    },
    b1: {
      centerX: 127.15,
      centerY: 59.2,
      points: [{
        x: 99.1,
        y: 59.2
      }, {
        x: 120,
        y: 30.6
      }, {
        x: 169.5,
        y: 59.2
      }, {
        x: 120,
        y: 87.8
      }],
      bordering: ['a2', 'a3', 'c2', 'c3'],
      touching: ['a4', 'b2', 'c4', 'd2']
    },
    b2: {
      centerX: 211.85,
      centerY: 59.2,
      points: [{
        x: 169.5,
        y: 59.2
      }, {
        x: 219,
        y: 30.6
      }, {
        x: 239.9,
        y: 59.2
      }, {
        x: 219,
        y: 87.8
      }],
      bordering: ['a4', 'c4'],
      touching: ['a3', 'b1', 'c3']
    },
    c1: {
      centerX: 50.98,
      centerY: 80.65,
      points: [{
        x: 21,
        y: 87.8
      }, {
        x: 41.9,
        y: 59.2
      }, {
        x: 70.5,
        y: 59.2
      }, {
        x: 70.5,
        y: 116.4
      }],
      bordering: ['a1', 'c2', 'd1'],
      touching: ['a2', 'd2', 'e1', 'e2']
    },
    c2: {
      centerX: 90.03,
      centerY: 80.65,
      points: [{
        x: 70.5,
        y: 59.2
      }, {
        x: 70.5,
        y: 116.4
      }, {
        x: 120,
        y: 87.8
      }, {
        x: 99.1,
        y: 59.2
      }],
      bordering: ['a2', 'b1', 'c1', 'd2'],
      touching: ['a1', 'c3', 'd1', 'e1', 'e2']
    },
    c3: {
      centerX: 149.98,
      centerY: 94.95,
      points: [{
        x: 120,
        y: 87.8
      }, {
        x: 169.5,
        y: 59.2
      }, {
        x: 169.5,
        y: 116.4
      }, {
        x: 140.9,
        y: 116.4
      }],
      bordering: ['b1', 'c4', 'd2', 'e3'],
      touching: ['a3', 'a4', 'b2', 'c2', 'e4']
    },
    c4: {
      centerX: 189.03,
      centerY: 94.95,
      points: [{
        x: 169.5,
        y: 59.2
      }, {
        x: 219,
        y: 87.8
      }, {
        x: 198.1,
        y: 116.4
      }, {
        x: 169.5,
        y: 116.4
      }],
      bordering: ['b2', 'c3', 'e4'],
      touching: ['a3', 'a4', 'b1', 'e3']
    },
    d1: {
      centerX: 28.15,
      centerY: 116.4,
      points: [{
        x: 0.1,
        y: 116.4
      }, {
        x: 21,
        y: 87.8
      }, {
        x: 70.5,
        y: 116.4
      }, {
        x: 21,
        y: 145
      }],
      bordering: ['c1', 'e1'],
      touching: ['c2', 'd2', 'e2']
    },
    d2: {
      centerX: 112.85,
      centerY: 116.4,
      points: [{
        x: 70.5,
        y: 116.4
      }, {
        x: 120,
        y: 87.8
      }, {
        x: 140.9,
        y: 116.4
      }, {
        x: 120,
        y: 145
      }],
      bordering: ['c2', 'c3', 'e2', 'e3'],
      touching: ['b1', 'c1', 'd1', 'e1', 'f1']
    },
    e1: {
      centerX: 50.98,
      centerY: 152.15,
      points: [{
        x: 21,
        y: 145
      }, {
        x: 70.5,
        y: 116.4
      }, {
        x: 70.5,
        y: 173.6
      }, {
        x: 41.9,
        y: 173.6
      }],
      bordering: ['d1', 'e2', 'g1'],
      touching: ['c1', 'c2', 'd2', 'g2']
    },
    e2: {
      centerX: 90.03,
      centerY: 152.15,
      points: [{
        x: 70.5,
        y: 116.4
      }, {
        x: 70.5,
        y: 173.6
      }, {
        x: 99.1,
        y: 173.6
      }, {
        x: 120,
        y: 145
      }],
      bordering: ['d2', 'e1', 'f1', 'g2'],
      touching: ['c1', 'c2', 'd1', 'e3', 'g1']
    },
    e3: {
      centerX: 149.98,
      centerY: 137.85,
      points: [{
        x: 120,
        y: 145
      }, {
        x: 140.9,
        y: 116.4
      }, {
        x: 169.5,
        y: 116.4
      }, {
        x: 169.5,
        y: 173.6
      }],
      bordering: ['c3', 'd2', 'e4', 'f1'],
      touching: ['c4', 'e2', 'f2', 'g3', 'g4']
    },
    e4: {
      centerX: 189.03,
      centerY: 137.85,
      points: [{
        x: 169.5,
        y: 116.4
      }, {
        x: 169.5,
        y: 173.6
      }, {
        x: 219,
        y: 145
      }, {
        x: 198.1,
        y: 116.4
      }],
      bordering: ['c4', 'e3', 'f2'],
      touching: ['c3', 'f1', 'g3', 'g4']
    },
    f1: {
      centerX: 127.15,
      centerY: 173.6,
      points: [{
        x: 99.1,
        y: 173.6
      }, {
        x: 120,
        y: 145
      }, {
        x: 169.5,
        y: 173.6
      }, {
        x: 120,
        y: 202.2
      }],
      bordering: ['e2', 'e3', 'g2', 'g3'],
      touching: ['d2', 'e4', 'f2', 'g4']
    },
    f2: {
      centerX: 211.85,
      centerY: 173.6,
      points: [{
        x: 169.5,
        y: 173.6
      }, {
        x: 219,
        y: 145
      }, {
        x: 239.9,
        y: 173.6
      }, {
        x: 219,
        y: 202.2
      }],
      bordering: ['e4', 'g4'],
      touching: ['e3', 'f1', 'g3']
    },
    g1: {
      centerX: 50.98,
      centerY: 195.05,
      points: [{
        x: 21,
        y: 202.2
      }, {
        x: 41.9,
        y: 173.6
      }, {
        x: 70.5,
        y: 173.6
      }, {
        x: 70.5,
        y: 230.8
      }],
      bordering: ['e1', 'g2'],
      touching: ['e2']
    },
    g2: {
      centerX: 90.03,
      centerY: 195.05,
      points: [{
        x: 70.5,
        y: 173.6
      }, {
        x: 70.5,
        y: 230.8
      }, {
        x: 120,
        y: 202.2
      }, {
        x: 99.1,
        y: 173.6
      }],
      bordering: ['e2', 'f1', 'g1'],
      touching: ['e1', 'g3']
    },
    g3: {
      centerX: 149.98,
      centerY: 209.35,
      points: [{
        x: 120,
        y: 202.2
      }, {
        x: 169.5,
        y: 173.6
      }, {
        x: 169.5,
        y: 230.8
      }, {
        x: 140.9,
        y: 230.8
      }],
      bordering: ['f1', 'g4'],
      touching: ['e3', 'e4', 'f2', 'g2']
    },
    g4: {
      centerX: 189.03,
      centerY: 209.35,
      points: [{
        x: 169.5,
        y: 173.6
      }, {
        x: 219,
        y: 202.2
      }, {
        x: 198.1,
        y: 230.8
      }, {
        x: 169.5,
        y: 230.8
      }],
      bordering: ['f2', 'g3'],
      touching: ['e3', 'e4', 'f1']
    }
  }
};

export default KiteBoard;
