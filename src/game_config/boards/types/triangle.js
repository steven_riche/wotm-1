const TriangleBoard = {
  name: 'Triangle',
  doors: {
    n: ['a2', 'a3'],
    e: ['c3', 'f3'],
    s: ['h2', 'h3'],
    w: ['d1', 'e1']
  },
  spaces: {
    a1: {
      centerX: 37.5,
      centerY: 23.13,
      points: [{
        x: 5,
        y: 4.38
      }, {
        x: 70,
        y: 4.38
      }, {
        x: 37.5,
        y: 60.63
      }],
      bordering: ['b1'],
      touching: ['a2', 'c1', 'd1']
    },
    a2: {
      centerX: 102.5,
      centerY: 23.13,
      points: [{
        x: 70,
        y: 4.38
      }, {
        x: 135,
        y: 4.38
      }, {
        x: 102.5,
        y: 60.63
      }],
      bordering: ['b1', 'b2'],
      touching: ['a1', 'a3', 'c1', 'c2', 'd2']
    },
    a3: {
      centerX: 167.5,
      centerY: 23.13,
      points: [{
        x: 135,
        y: 4.38
      }, {
        x: 200,
        y: 4.38
      }, {
        x: 167.5,
        y: 60.63
      }],
      bordering: ['b2', 'b3'],
      touching: ['a2', 'c2', 'c3', 'd3']
    },
    b1: {
      centerX: 70,
      centerY: 41.88,
      points: [{
        x: 70,
        y: 4.38
      }, {
        x: 37.5,
        y: 60.63
      }, {
        x: 102.5,
        y: 60.63
      }],
      bordering: ['a1', 'a2', 'c1'],
      touching: ['b2', 'c2', 'd1', 'd2']
    },
    b2: {
      centerX: 135,
      centerY: 41.88,
      points: [{
        x: 135,
        y: 4.38
      }, {
        x: 102.5,
        y: 60.63
      }, {
        x: 167.5,
        y: 60.63
      }],
      bordering: ['a2', 'a3', 'c2'],
      touching: ['b1', 'b3', 'c1', 'c3', 'd2', 'd3']
    },
    b3: {
      centerX: 200,
      centerY: 41.88,
      points: [{
        x: 200,
        y: 4.38
      }, {
        x: 167.5,
        y: 60.63
      }, {
        x: 232.5,
        y: 60.63
      }],
      bordering: ['a3', 'c3'],
      touching: ['b2', 'c2', 'd3']
    },
    c1: {
      centerX: 70,
      centerY: 79.38,
      points: [{
        x: 37.5,
        y: 60.63
      }, {
        x: 102.5,
        y: 60.63
      }, {
        x: 70,
        y: 116.88
      }],
      bordering: ['b1', 'd1', 'd2'],
      touching: ['a1', 'a2', 'b2', 'c2', 'e1', 'e2', 'f1']
    },
    c2: {
      centerX: 135,
      centerY: 79.38,
      points: [{
        x: 102.5,
        y: 60.63
      }, {
        x: 167.5,
        y: 60.63
      }, {
        x: 135,
        y: 116.88
      }],
      bordering: ['b2', 'd2', 'd3'],
      touching: ['a2', 'a3', 'b1', 'b3', 'c1', 'c3', 'e2', 'e3', 'f2']
    },
    c3: {
      centerX: 200,
      centerY: 79.38,
      points: [{
        x: 167.5,
        y: 60.63
      }, {
        x: 232.5,
        y: 60.63
      }, {
        x: 200,
        y: 116.88
      }],
      bordering: ['b3', 'd3'],
      touching: ['a3', 'b2', 'c2', 'e3', 'f3']
    },
    d1: {
      centerX: 37.5,
      centerY: 98.13,
      points: [{
        x: 37.5,
        y: 60.63
      }, {
        x: 5,
        y: 116.88
      }, {
        x: 70,
        y: 116.88
      }],
      bordering: ['c1', 'e1'],
      touching: ['a1', 'b1', 'd2', 'e2', 'f1']
    },
    d2: {
      centerX: 102.5,
      centerY: 98.13,
      points: [{
        x: 102.5,
        y: 60.63
      }, {
        x: 70,
        y: 116.88
      }, {
        x: 135,
        y: 116.88
      }],
      bordering: ['c1', 'c2', 'e2'],
      touching: ['a2', 'b1', 'b2', 'd1', 'd3', 'e1', 'e3', 'f1', 'f2']
    },
    d3: {
      centerX: 167.5,
      centerY: 98.13,
      points: [{
        x: 167.5,
        y: 60.63
      }, {
        x: 135,
        y: 116.88
      }, {
        x: 200,
        y: 116.88
      }],
      bordering: ['c2', 'c3', 'e3'],
      touching: ['a3', 'b2', 'b3', 'd2', 'e2', 'f2', 'f3']
    },
    e1: {
      centerX: 37.5,
      centerY: 135.63,
      points: [{
        x: 5,
        y: 116.88
      }, {
        x: 70,
        y: 116.88
      }, {
        x: 37.5,
        y: 173.13
      }],
      bordering: ['d1', 'f1'],
      touching: ['c1', 'd2', 'e2', 'g1', 'h1']
    },
    e2: {
      centerX: 102.5,
      centerY: 135.63,
      points: [{
        x: 70,
        y: 116.88
      }, {
        x: 135,
        y: 116.88
      }, {
        x: 102.5,
        y: 173.13
      }],
      bordering: ['d2', 'f1', 'f2'],
      touching: ['c1', 'c2', 'd1', 'd3', 'e1', 'e3', 'g1', 'g2', 'h2']
    },
    e3: {
      centerX: 167.5,
      centerY: 135.63,
      points: [{
        x: 135,
        y: 116.88
      }, {
        x: 200,
        y: 116.88
      }, {
        x: 167.5,
        y: 173.13
      }],
      bordering: ['d3', 'f2', 'f3'],
      touching: ['c2', 'c3', 'd2', 'e2', 'g2', 'g3', 'h3']
    },
    f1: {
      centerX: 70,
      centerY: 154.38,
      points: [{
        x: 70,
        y: 116.88
      }, {
        x: 37.5,
        y: 173.13
      }, {
        x: 102.5,
        y: 173.13
      }],
      bordering: ['e1', 'e2', 'g1'],
      touching: ['c1', 'd1', 'd2', 'f2', 'g2', 'h1', 'h2']
    },
    f2: {
      centerX: 135,
      centerY: 154.38,
      points: [{
        x: 135,
        y: 116.88
      }, {
        x: 102.5,
        y: 173.13
      }, {
        x: 167.5,
        y: 173.13
      }],
      bordering: ['e2', 'e3', 'g2'],
      touching: ['c2', 'd2', 'd3', 'f1', 'f3', 'g1', 'g3', 'h2', 'h3']
    },
    f3: {
      centerX: 200,
      centerY: 154.38,
      points: [{
        x: 200,
        y: 116.88
      }, {
        x: 167.5,
        y: 173.13
      }, {
        x: 232.5,
        y: 173.13
      }],
      bordering: ['e3', 'g3'],
      touching: ['c3', 'd3', 'f2', 'g2', 'h3']
    },
    g1: {
      centerX: 70,
      centerY: 191.88,
      points: [{
        x: 37.5,
        y: 173.13
      }, {
        x: 102.5,
        y: 173.13
      }, {
        x: 70,
        y: 229.38
      }],
      bordering: ['f1', 'h1', 'h2'],
      touching: ['e1', 'e2', 'f2', 'g2']
    },
    g2: {
      centerX: 135,
      centerY: 191.88,
      points: [{
        x: 102.5,
        y: 173.13
      }, {
        x: 167.5,
        y: 173.13
      }, {
        x: 135,
        y: 229.38
      }],
      bordering: ['f2', 'h2', 'h3'],
      touching: ['e2', 'e3', 'f1', 'f3', 'g1', 'g3']
    },
    g3: {
      centerX: 200,
      centerY: 191.88,
      points: [{
        x: 167.5,
        y: 173.13
      }, {
        x: 232.5,
        y: 173.13
      }, {
        x: 200,
        y: 229.38
      }],
      bordering: ['f3', 'h3'],
      touching: ['e3', 'f2', 'g2']
    },
    h1: {
      centerX: 37.5,
      centerY: 210.63,
      points: [{
        x: 37.5,
        y: 173.13
      }, {
        x: 5,
        y: 229.38
      }, {
        x: 70,
        y: 229.38
      }],
      bordering: ['g1'],
      touching: ['e1', 'f1', 'h2']
    },
    h2: {
      centerX: 102.5,
      centerY: 210.63,
      points: [{
        x: 102.5,
        y: 173.13
      }, {
        x: 70,
        y: 229.38
      }, {
        x: 135,
        y: 229.38
      }],
      bordering: ['g1', 'g2'],
      touching: ['e2', 'f1', 'f2', 'h1', 'h3']
    },
    h3: {
      centerX: 167.5,
      centerY: 210.63,
      points: [{
        x: 167.5,
        y: 173.13
      }, {
        x: 135,
        y: 229.38
      }, {
        x: 200,
        y: 229.38
      }],
      bordering: ['g2', 'g3'],
      touching: ['e3', 'f2', 'f3', 'h2']
    }
  }
};

export default TriangleBoard;
