const RhombusBoard = {
  name: 'Rhombus',
  doors: {
    n: ['a2'],
    e: ['d2', 'h2'],
    s: ['k2'],
    w: ['e1', 'g1']
  },
  spaces: {
    a1: {
      centerX: 45.11,
      centerY: 26.31,
      points: [{
        x: 12.5,
        y: 7.5
      }, {
        x: 34.24,
        y: 45.12
      }, {
        x: 77.71,
        y: 45.12
      }, {
        x: 55.98,
        y: 7.5
      }],
      bordering: ['c1'],
      touching: ['a2', 'c2', 'd1']
    },
    a2: {
      centerX: 110.32,
      centerY: 26.31,
      points: [{
        x: 77.71,
        y: 45.12
      }, {
        x: 99.45,
        y: 7.5
      }, {
        x: 142.92,
        y: 7.5
      }, {
        x: 121.18,
        y: 45.12
      }],
      bordering: ['b1', 'c2'],
      touching: ['a1', 'a3', 'c1', 'd1']
    },
    a3: {
      centerX: 175.52,
      centerY: 26.31,
      points: [{
        x: 142.92,
        y: 7.5
      }, {
        x: 164.66,
        y: 45.12
      }, {
        x: 208.13,
        y: 45.12
      }, {
        x: 186.39,
        y: 7.5
      }],
      bordering: ['b1', 'c3'],
      touching: ['a2', 'd2']
    },
    b1: {
      centerX: 142.92,
      centerY: 45.12,
      points: [{
        x: 142.92,
        y: 7.5
      }, {
        x: 121.18,
        y: 45.12
      }, {
        x: 142.92,
        y: 82.74
      }, {
        x: 164.66,
        y: 45.12
      }],
      bordering: ['a2', 'a3', 'c2', 'c3'],
      touching: ['e2', 'e3', 'f1']
    },
    c1: {
      centerX: 45.11,
      centerY: 63.93,
      points: [{
        x: 12.5,
        y: 82.74
      }, {
        x: 34.24,
        y: 45.12
      }, {
        x: 77.71,
        y: 45.12
      }, {
        x: 55.98,
        y: 82.74
      }],
      bordering: ['a1', 'd1', 'e1'],
      touching: ['a2', 'c2']
    },
    c2: {
      centerX: 110.32,
      centerY: 63.93,
      points: [{
        x: 77.71,
        y: 45.12
      }, {
        x: 99.45,
        y: 82.74
      }, {
        x: 142.92,
        y: 82.74
      }, {
        x: 121.18,
        y: 45.12
      }],
      bordering: ['a2', 'b1', 'd1', 'e2'],
      touching: ['a1', 'c1', 'c3', 'e3', 'f1']
    },
    c3: {
      centerX: 175.52,
      centerY: 63.93,
      points: [{
        x: 142.92,
        y: 82.74
      }, {
        x: 164.66,
        y: 45.12
      }, {
        x: 208.13,
        y: 45.12
      }, {
        x: 186.39,
        y: 82.74
      }],
      bordering: ['a3', 'b1', 'd2', 'e3'],
      touching: ['c2', 'e2', 'f1']
    },
    d1: {
      centerX: 77.71,
      centerY: 82.74,
      points: [{
        x: 77.71,
        y: 45.12
      }, {
        x: 55.98,
        y: 82.74
      }, {
        x: 77.71,
        y: 120.36
      }, {
        x: 99.45,
        y: 82.74
      }],
      bordering: ['c1', 'c2', 'e1', 'e2'],
      touching: ['a1', 'a2', 'g1', 'g2', 'h1']
    },
    d2: {
      centerX: 208.13,
      centerY: 82.74,
      points: [{
        x: 208.13,
        y: 45.12
      }, {
        x: 186.39,
        y: 82.74
      }, {
        x: 208.13,
        y: 120.36
      }, {
        x: 229.86,
        y: 82.74
      }],
      bordering: ['c3', 'e3'],
      touching: ['a3', 'g3', 'h2']
    },
    e1: {
      centerX: 45.11,
      centerY: 101.55,
      points: [{
        x: 12.5,
        y: 82.74
      }, {
        x: 34.24,
        y: 120.36
      }, {
        x: 77.71,
        y: 120.36
      }, {
        x: 55.98,
        y: 82.74
      }],
      bordering: ['c1', 'd1', 'g1'],
      touching: ['e2', 'g2', 'h1']
    },
    e2: {
      centerX: 110.32,
      centerY: 101.55,
      points: [{
        x: 77.71,
        y: 120.36
      }, {
        x: 99.45,
        y: 82.74
      }, {
        x: 142.92,
        y: 82.74
      }, {
        x: 121.18,
        y: 120.36
      }],
      bordering: ['d1', 'c2', 'f1', 'g2'],
      touching: ['b1', 'c3', 'e1', 'e3', 'g1', 'h1']
    },
    e3: {
      centerX: 175.52,
      centerY: 101.55,
      points: [{
        x: 142.92,
        y: 82.74
      }, {
        x: 164.66,
        y: 120.36
      }, {
        x: 208.13,
        y: 120.36
      }, {
        x: 186.39,
        y: 82.74
      }],
      bordering: ['c3', 'd2', 'f1', 'g3'],
      touching: ['b1', 'c2', 'e2', 'h2']
    },
    f1: {
      centerX: 142.92,
      centerY: 120.36,
      points: [{
        x: 142.92,
        y: 82.74
      }, {
        x: 121.18,
        y: 120.36
      }, {
        x: 142.92,
        y: 157.98
      }, {
        x: 164.66,
        y: 120.36
      }],
      bordering: ['e2', 'e3', 'g2', 'g3'],
      touching: ['c2', 'c3', 'b1', 'i2', 'i3', 'j1']
    },
    g1: {
      centerX: 45.11,
      centerY: 139.17,
      points: [{
        x: 12.5,
        y: 157.98
      }, {
        x: 34.24,
        y: 120.36
      }, {
        x: 77.71,
        y: 120.36
      }, {
        x: 55.98,
        y: 157.98
      }],
      bordering: ['e1', 'h1', 'i1'],
      touching: ['d1', 'e2', 'g2']
    },
    g2: {
      centerX: 110.32,
      centerY: 139.17,
      points: [{
        x: 77.71,
        y: 120.36
      }, {
        x: 99.45,
        y: 157.98
      }, {
        x: 142.92,
        y: 157.98
      }, {
        x: 121.18,
        y: 120.36
      }],
      bordering: ['e2', 'f1', 'h1', 'i2'],
      touching: ['d1', 'e1', 'g1', 'g3', 'i3', 'j1']
    },
    g3: {
      centerX: 175.52,
      centerY: 139.17,
      points: [{
        x: 142.92,
        y: 157.98
      }, {
        x: 164.66,
        y: 120.36
      }, {
        x: 208.13,
        y: 120.36
      }, {
        x: 186.39,
        y: 157.98
      }],
      bordering: ['e3', 'f1', 'h2', 'i3'],
      touching: ['d2', 'g2', 'i2', 'j1']
    },
    h1: {
      centerX: 77.71,
      centerY: 157.98,
      points: [{
        x: 77.71,
        y: 120.36
      }, {
        x: 55.98,
        y: 157.98
      }, {
        x: 77.71,
        y: 195.6
      }, {
        x: 99.45,
        y: 157.98
      }],
      bordering: ['g1', 'g2', 'i1', 'i2'],
      touching: ['d1', 'e1', 'e2', 'k1', 'k2']
    },
    h2: {
      centerX: 208.13,
      centerY: 157.98,
      points: [{
        x: 208.13,
        y: 120.36
      }, {
        x: 186.39,
        y: 157.98
      }, {
        x: 208.13,
        y: 195.6
      }, {
        x: 229.86,
        y: 157.98
      }],
      bordering: ['g3', 'i3'],
      touching: ['d2', 'e3', 'k3']
    },
    i1: {
      centerX: 45.11,
      centerY: 176.79,
      points: [{
        x: 12.5,
        y: 157.98
      }, {
        x: 34.24,
        y: 195.6
      }, {
        x: 77.71,
        y: 195.6
      }, {
        x: 55.98,
        y: 157.98
      }],
      bordering: ['g1', 'h1', 'k1'],
      touching: ['i2', 'k2']
    },
    i2: {
      centerX: 110.32,
      centerY: 176.79,
      points: [{
        x: 77.71,
        y: 195.6
      }, {
        x: 99.45,
        y: 157.98
      }, {
        x: 142.92,
        y: 157.98
      }, {
        x: 121.18,
        y: 195.6
      }],
      bordering: ['h1', 'g2', 'j1', 'k2'],
      touching: ['f1', 'g3', 'i1', 'i3', 'k1']
    },
    i3: {
      centerX: 175.52,
      centerY: 176.79,
      points: [{
        x: 142.92,
        y: 157.98
      }, {
        x: 164.66,
        y: 195.6
      }, {
        x: 208.13,
        y: 195.6
      }, {
        x: 186.39,
        y: 157.98
      }],
      bordering: ['g3', 'h2', 'j1', 'k3'],
      touching: ['f1', 'g2', 'i2']
    },
    j1: {
      centerX: 142.92,
      centerY: 195.6,
      points: [{
        x: 142.92,
        y: 157.98
      }, {
        x: 121.18,
        y: 195.6
      }, {
        x: 142.92,
        y: 233.22
      }, {
        x: 164.66,
        y: 195.6
      }],
      bordering: ['i2', 'i3', 'k2', 'k3'],
      touching: ['f1', 'g2', 'g3']
    },
    k1: {
      centerX: 45.11,
      centerY: 214.41,
      points: [{
        x: 12.5,
        y: 233.22
      }, {
        x: 34.24,
        y: 195.6
      }, {
        x: 77.71,
        y: 195.6
      }, {
        x: 55.98,
        y: 233.22
      }],
      bordering: ['i1'],
      touching: ['h1', 'i2', 'k2']
    },
    k2: {
      centerX: 110.32,
      centerY: 214.41,
      points: [{
        x: 77.71,
        y: 195.6
      }, {
        x: 99.45,
        y: 233.22
      }, {
        x: 142.92,
        y: 233.22
      }, {
        x: 121.18,
        y: 195.6
      }],
      bordering: ['i2', 'j1'],
      touching: ['h1', 'i1', 'k1', 'k3']
    },
    k3: {
      centerX: 175.52,
      centerY: 214.41,
      points: [{
        x: 142.92,
        y: 233.22
      }, {
        x: 164.66,
        y: 195.6
      }, {
        x: 208.13,
        y: 195.6
      }, {
        x: 186.39,
        y: 233.22
      }],
      bordering: ['i3', 'j1'],
      touching: ['h2', 'k2']
    }
  }
};

export default RhombusBoard;
