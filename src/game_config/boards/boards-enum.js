import CobblestoneBoard from './types/cobblestone';
import DiamondBoard from './types/diamond';
import HexBoard from './types/hex';
import KiteBoard from './types/kite';
import PentagonBoard from './types/pentagon';
import RhombusBoard from './types/rhombus';
import SnowflakeBoard from './types/snowflake';
import TriangleBoard from './types/triangle';
import OctBoard from './types/octagon';

const Boards = {
  TriangleA: {
    type: TriangleBoard,
    name: 'TriangleA',
    meditationSpaces: ['d2', 'd3', 'e2', 'e3', 'f2', 'c2']
  },
  TriangleB: {
    type: TriangleBoard,
    name: 'TriangleB',
    meditationSpaces: ['b2', 'b3', 'd2', 'd3', 'f1', 'f2']
  },
  TriangleC: {
    type: TriangleBoard,
    name: 'TriangleC',
    meditationSpaces: ['a1', 'b3', 'd2', 'e2', 'h1', 'g3']
  },
  TriangleD: {
    type: TriangleBoard,
    name: 'TriangleD',
    meditationSpaces: ['a1', 'b2', 'd2', 'e3', 'f1', 'g3']
  },
  DiamondA: {
    type: DiamondBoard,
    name: 'DiamondA',
    meditationSpaces: ['b2', 'c2', 'c3', 'd2', 'e2', 'e3']
  },
  DiamondB: {
    type: DiamondBoard,
    name: 'DiamondB',
    meditationSpaces: ['b2', 'c2', 'c3', 'd1', 'd2', 'd3']
  },
  DiamondC: {
    type: DiamondBoard,
    name: 'DiamondC',
    meditationSpaces: ['b2', 'b3', 'd1', 'd2', 'd3', 'f2']
  },
  DiamondD: {
    type: DiamondBoard,
    name: 'DiamondD',
    meditationSpaces: ['b2', 'c2', 'c3', 'e2', 'e3', 'f2']
  },
  RhombusA: {
    type: RhombusBoard,
    name: 'RhombusA',
    meditationSpaces: ['d1', 'e2', 'e3', 'g2', 'g3', 'h1']
  },
  RhombusB: {
    type: RhombusBoard,
    name: 'RhombusB',
    meditationSpaces: ['b1', 'c2', 'c3', 'e2', 'e3', 'f1']
  },
  RhombusC: {
    type: RhombusBoard,
    name: 'RhombusC',
    meditationSpaces: ['b1', 'c2', 'c3', 'i2', 'i3', 'j1']
  },
  RhombusD: {
    type: RhombusBoard,
    name: 'RhombusD',
    meditationSpaces: ['c1', 'd1', 'e2', 'g2', 'h1', 'i1']
  },
  KiteA: {
    type: KiteBoard,
    name: 'KiteA',
    meditationSpaces: ['b1', 'c2', 'c3', 'e2', 'e3', 'f1']
  },
  KiteB: {
    type: KiteBoard,
    name: 'KiteB',
    meditationSpaces: ['b1', 'c1', 'c2', 'e1', 'e2', 'f1']
  },
  KiteC: {
    type: KiteBoard,
    name: 'KiteC',
    meditationSpaces: ['b1', 'b2', 'c1', 'e1', 'f1', 'f2']
  },
  KiteD: {
    type: KiteBoard,
    name: 'KiteD',
    meditationSpaces: ['a1', 'b2', 'c3', 'e3', 'f2', 'g1']
  },
  PentagonA: {
    type: PentagonBoard,
    name: 'PentagonA',
    meditationSpaces: ['b2', 'b3', 'c2', 'e2', 'f2', 'f3']
  },
  PentagonB: {
    type: PentagonBoard,
    name: 'PentagonB',
    meditationSpaces: ['b2', 'c2', 'd1', 'd2', 'e2', 'f3']
  },
  PentagonC: {
    type: PentagonBoard,
    name: 'PentagonC',
    meditationSpaces: ['c2', 'c3', 'd1', 'd2', 'e2', 'e3']
  },
  PentagonD: {
    type: PentagonBoard,
    name: 'PentagonD',
    meditationSpaces: ['b3', 'b4', 'c3', 'e3', 'f3', 'f4']
  },
  CobblestoneA: {
    type: CobblestoneBoard,
    name: 'CobblestoneA',
    meditationSpaces: ['c1', 'c2', 'e1', 'f3', 'h3', 'h4']
  },
  CobblestoneB: {
    type: CobblestoneBoard,
    name: 'CobblestoneB',
    meditationSpaces: ['c3', 'd2', 'e2', 'f2', 'g1', 'h2']
  },
  CobblestoneC: {
    type: CobblestoneBoard,
    name: 'CobblestoneC',
    meditationSpaces: ['a3', 'b1', 'd2', 'g1', 'i2', 'j1']
  },
  CobblestoneD: {
    type: CobblestoneBoard,
    name: 'CobblestoneD',
    meditationSpaces: ['b1', 'c3', 'd2', 'g1', 'h2', 'i2']
  },
  SnowflakeA: {
    type: SnowflakeBoard,
    name: 'SnowflakeA',
    meditationSpaces: ['wh1', 'wh2', 'wh3', 'wh4', 'wh5', 'wh6']
  },
  SnowflakeB: {
    type: SnowflakeBoard,
    name: 'SnowflakeB',
    meditationSpaces: ['wh1', 'wh6', 'wh4', 'wh3', 'wh6a', 'wh3a']
  },
  SnowflakeC: {
    type: SnowflakeBoard,
    name: 'SnowflakeC',
    meditationSpaces: ['wh2', 'wh5', 'wh2d', 'wh5d', 'wh6b', 'wh3b']
  },
  SnowflakeD: {
    type: SnowflakeBoard,
    name: 'SnowflakeD',
    meditationSpaces: ['wh2', 'wh5', 'wh2c', 'wh5c', 'wh6a', 'wh3a']
  },
  HexA: {
    type: HexBoard,
    name: 'HexA',
    meditationSpaces: ['c2', 'c3', 'c4', 'd1', 'd3', 'd5']
  },
  HexB: {
    type: HexBoard,
    name: 'HexB',
    meditationSpaces: ['b2', 'b3', 'b4', 'c2', 'c4', 'd3']
  },
  HexC: {
    type: HexBoard,
    name: 'HexC',
    meditationSpaces: ['a2', 'a4', 'b3', 'd2', 'd3', 'd4']
  },
  HexD: {
    type: HexBoard,
    name: 'HexD',
    meditationSpaces: ['b1', 'b2', 'b4', 'c2', 'c4', 'd5']
  },
  OctA: {
    type: OctBoard,
    name: 'OctA',
    meditationSpaces: ['b3', 'b4', 'c2', 'c4', 'd2', 'd3']
  }
};

export default Boards;
