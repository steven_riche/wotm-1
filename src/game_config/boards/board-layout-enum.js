import B_W from './board-size';

export const A3 = [
  { x: 0, y: 0, neighbors: { n: 2, e: 0, s: 1, w: 0 } },
  { x: 0, y: B_W, neighbors: { n: 0, e: 1, s: 2, w: 1 } },
  { x: 0, y: (B_W * 2), neighbors: { n: 1, e: 2, s: 0, w: 2 } }
];

export const B3 = [
  { x: 0, y: 0, neighbors: { n: 1, e: 0, s: 1, w: 0 } },
  { x: 0, y: B_W, neighbors: { n: 0, e: 2, s: 0, w: 2 } },
  { x: B_W, y: B_W, neighbors: { n: 2, e: 1, s: 2, w: 1 } }
];

export const A4 = [
  { x: 0, y: 0, neighbors: { n: 2, e: 1, s: 2, w: 1 } },
  { x: B_W, y: 0, neighbors: { n: 3, e: 0, s: 3, w: 0 } },
  { x: 0, y: B_W, neighbors: { n: 0, e: 3, s: 0, w: 3 } },
  { x: B_W, y: B_W, neighbors: { n: 1, e: 2, s: 1, w: 2 } }
];

export const B4 = [
  { x: 0, y: 0, neighbors: { n: 2, e: 0, s: 1, w: 0 } },
  { x: 0, y: B_W, neighbors: { n: 0, e: 1, s: 2, w: 1 } },
  { x: 0, y: (B_W * 2), neighbors: { n: 1, e: 3, s: 0, w: 3 } },
  { x: B_W, y: (B_W * 2), neighbors: { n: 3, e: 2, s: 3, w: 2 } }
];

export const A5 = [
  { x: 0, y: 0, neighbors: { n: 3, e: 1, s: 3, w: 2 } },
  { x: B_W, y: 0, neighbors: { n: 1, e: 2, s: 1, w: 0 } },
  { x: (B_W * 2), y: 0, neighbors: { n: 4, e: 0, s: 4, w: 1 } },
  { x: 0, y: B_W, neighbors: { n: 0, e: 4, s: 0, w: 4 } },
  { x: (B_W * 2), y: B_W, neighbors: { n: 2, e: 3, s: 2, w: 3 } }
];

export const B5 = [
  { x: 0, y: 0, neighbors: { n: 3, e: 0, s: 1, w: 0 } },
  { x: 0, y: B_W, neighbors: { n: 0, e: 2, s: 3, w: 2 } },
  { x: B_W, y: B_W, neighbors: { n: 4, e: 1, s: 4, w: 1 } },
  { x: 0, y: (B_W * 2), neighbors: { n: 1, e: 4, s: 0, w: 4 } },
  { x: B_W, y: (B_W * 2), neighbors: { n: 2, e: 3, s: 2, w: 3 } }
];

export const A6 = [
  { x: 0, y: 0, neighbors: { n: 3, e: 0, s: 1, w: 0 } },
  { x: 0, y: B_W, neighbors: { n: 0, e: 2, s: 3, w: 2 } },
  { x: B_W, y: B_W, neighbors: { n: 4, e: 1, s: 4, w: 1 } },
  { x: 0, y: (B_W * 2), neighbors: { n: 1, e: 4, s: 0, w: 5 } },
  { x: B_W, y: (B_W * 2), neighbors: { n: 2, e: 5, s: 2, w: 3 } },
  { x: (B_W * 2), y: (B_W * 2), neighbors: { n: 5, e: 3, s: 5, w: 4 } }
];

export const B6 = [
  { x: 0, y: 0, neighbors: { n: 4, e: 1, s: 2, w: 1 } },
  { x: B_W, y: 0, neighbors: { n: 5, e: 0, s: 3, w: 0 } },
  { x: 0, y: B_W, neighbors: { n: 0, e: 3, s: 4, w: 3 } },
  { x: B_W, y: B_W, neighbors: { n: 1, e: 2, s: 5, w: 2 } },
  { x: 0, y: (B_W * 2), neighbors: { n: 2, e: 5, s: 0, w: 5 } },
  { x: B_W, y: (B_W * 2), neighbors: { n: 3, e: 4, s: 1, w: 4 } }
];

export const A7 = [
  { x: 0, y: 0, neighbors: { n: 2, e: 1, s: 2, w: 1 } },
  { x: B_W, y: 0, neighbors: { n: 5, e: 0, s: 3, w: 0 } },
  { x: 0, y: B_W, neighbors: { n: 0, e: 3, s: 0, w: 4 } },
  { x: B_W, y: B_W, neighbors: { n: 1, e: 4, s: 5, w: 2 } },
  { x: (B_W * 2), y: B_W, neighbors: { n: 6, e: 2, s: 6, w: 3 } },
  { x: B_W, y: (B_W * 2), neighbors: { n: 3, e: 6, s: 1, w: 6 } },
  { x: (B_W * 2), y: (B_W * 2), neighbors: { n: 4, e: 5, s: 4, w: 5 } }
];

export const B7 = [
  { x: 0, y: 0, neighbors: { n: 3, e: 1, s: 3, w: 2 } },
  { x: B_W, y: 0, neighbors: { n: 5, e: 2, s: 5, w: 0 } },
  { x: (B_W * 2), y: 0, neighbors: { n: 6, e: 0, s: 4, w: 1 } },
  { x: 0, y: B_W, neighbors: { n: 0, e: 4, s: 0, w: 4 } },
  { x: (B_W * 2), y: B_W, neighbors: { n: 2, e: 3, s: 6, w: 3 } },
  { x: B_W, y: (B_W * 2), neighbors: { n: 1, e: 6, s: 1, w: 6 } },
  { x: (B_W * 2), y: (B_W * 2), neighbors: { n: 4, e: 5, s: 2, w: 5 } }
];

export const A8 = [
  { x: 0, y: 0, neighbors: { n: 5, e: 1, s: 3, w: 2 } },
  { x: B_W, y: 0, neighbors: { n: 6, e: 2, s: 6, w: 0 } },
  { x: (B_W * 2), y: 0, neighbors: { n: 7, e: 0, s: 4, w: 1 } },
  { x: 0, y: B_W, neighbors: { n: 0, e: 4, s: 5, w: 4 } },
  { x: (B_W * 2), y: B_W, neighbors: { n: 2, e: 3, s: 7, w: 3 } },
  { x: 0, y: (B_W * 2), neighbors: { n: 3, e: 6, s: 0, w: 7 } },
  { x: B_W, y: (B_W * 2), neighbors: { n: 1, e: 7, s: 1, w: 5 } },
  { x: (B_W * 2), y: (B_W * 2), neighbors: { n: 4, e: 5, s: 2, w: 6 } }
];

export const B8 = [
  { x: 0, y: 0, neighbors: { n: 6, e: 1, s: 3, w: 2 } },
  { x: B_W, y: 0, neighbors: { n: 7, e: 2, s: 4, w: 0 } },
  { x: (B_W * 2), y: 0, neighbors: { n: 5, e: 0, s: 5, w: 1 } },
  { x: 0, y: B_W, neighbors: { n: 0, e: 4, s: 6, w: 5 } },
  { x: B_W, y: B_W, neighbors: { n: 1, e: 5, s: 7, w: 3 } },
  { x: (B_W * 2), y: B_W, neighbors: { n: 2, e: 3, s: 2, w: 4 } },
  { x: 0, y: (B_W * 2), neighbors: { n: 3, e: 7, s: 0, w: 7 } },
  { x: B_W, y: (B_W * 2), neighbors: { n: 4, e: 6, s: 1, w: 6 } }
];

const layoutInfo = {
  rotateReset: (B_W / 2),
  layouts: [
    {
      numPlayers: 3,
      layouts: [A3, B3]
    },
    {
      numPlayers: 4,
      layouts: [A4, B4]
    },
    {
      numPlayers: 5,
      layouts: [A5, B5]
    },
    {
      numPlayers: 6,
      layouts: [A6, B6]
    },
    {
      numPlayers: 7,
      layouts: [A7, B7]
    },
    {
      numPlayers: 8,
      layouts: [A8, B8]
    }
  ]
};

export default layoutInfo;
