import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Immutable from 'immutable';
import Avatar from 'material-ui/Avatar';
import Table, { TableBody, TableCell, TableHead, TableRow } from 'material-ui/Table';
import { captureRequirements } from '../../game_config/constants/constants';

import './scoreboard.scss';

function mapStateToProps(state) {
  return {
    players: state.players
  };
}

export class Scoreboard extends React.PureComponent {

  static propTypes = {
    players: PropTypes.instanceOf(Immutable.List).isRequired,
    setGameOver: PropTypes.func.isRequired
  };

  playersAtScore = (score) => {
    const filteredPlayers = this.props.players.filter(player => player.get('score') === score);
    return filteredPlayers.map(player =>
      <Avatar
        style={{
          backgroundColor: player.getIn(['role', 'primaryColor']),
          float: 'left',
          width: '15px',
          height: '15px'
        }}
        key={`avatar-score-${player.get('player')}`}
      />
    );
  };

  outerCellStyling = {
    width: '75px',
    padding: 0,
    textAlign: 'center'
  };

  innerCellStyling = {
    width: '148px',
    padding: 0,
    textAlign: 'center'
  };

  rowStyling = {
    height: '33px'
  };

  gameOverCheck = players => players.some(player =>
    player.get('score') >= captureRequirements.length &&
    (!player.hasIn(['modifiers', 'orangeBalance']) || player.getIn(['modifiers', 'orangeBalance']) >= 0));

  componentWillUpdate(newProps) {
    if (newProps.players && this.gameOverCheck(newProps.players)) {
      this.props.setGameOver(newProps.players);
    }
  }

  render() {
    return (
      <div className="scoreboard">
        <Table>
          <TableHead>
            <TableRow style={this.rowStyling}>
              <TableCell style={this.outerCellStyling}>Capture</TableCell>
              <TableCell style={this.innerCellStyling}>Players</TableCell>
              <TableCell style={this.outerCellStyling}>Card</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow style={this.rowStyling}>
              <TableCell style={this.outerCellStyling}>2</TableCell>
              <TableCell style={this.innerCellStyling}>{this.playersAtScore(0)}</TableCell>
              <TableCell style={this.outerCellStyling}>2</TableCell>
            </TableRow>
            <TableRow style={this.rowStyling}>
              <TableCell style={this.outerCellStyling}>3</TableCell>
              <TableCell style={this.innerCellStyling}>{this.playersAtScore(1)}</TableCell>
              <TableCell style={this.outerCellStyling}>3</TableCell>
            </TableRow>
            <TableRow style={this.rowStyling}>
              <TableCell style={this.outerCellStyling}>3/1</TableCell>
              <TableCell style={this.innerCellStyling}>{this.playersAtScore(2)}</TableCell>
              <TableCell style={this.outerCellStyling}>4</TableCell>
            </TableRow>
            <TableRow style={this.rowStyling}>
              <TableCell style={this.outerCellStyling}>4</TableCell>
              <TableCell style={this.innerCellStyling}>{this.playersAtScore(3)}</TableCell>
              <TableCell style={this.outerCellStyling}>5</TableCell>
            </TableRow>
            <TableRow style={this.rowStyling}>
              <TableCell style={this.outerCellStyling} />
              <TableCell style={this.innerCellStyling}>FINISH</TableCell>
              <TableCell style={this.outerCellStyling} />
            </TableRow>
          </TableBody>
        </Table>
      </div>
    );
  }
}

export default connect(mapStateToProps)(Scoreboard);
