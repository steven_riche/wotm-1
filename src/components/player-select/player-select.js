import React from 'react';
import PropTypes from 'prop-types';
import Immutable from 'immutable';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Avatar from 'material-ui/Avatar';
import Button from 'material-ui/Button';
import Chip from 'material-ui/Chip';
import TextField from 'material-ui/TextField';
import PlayerTypes from '../../game_config/players/players-enum';
import { addPlayer } from '../../actions/players';
import PlayerInfoComponent from '../player-info/player-info';
import PlayerRoleInfoComponent from '../player-role-info/player-role-info';

import './player-select.scss';

function mapStateToProps(state) {
  return {
    players: state.players
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({
      addPlayer
    }, dispatch)
  };
}

export class PlayerSelect extends React.PureComponent {
  static propTypes = {
    players: PropTypes.instanceOf(Immutable.List).isRequired,
    actions: PropTypes.shape({
      addPlayer: PropTypes.func
    }).isRequired,
    playersReady: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      role: '',
      playerName: ''
    };
  }

  getPlayerType = (player) => {
    const { players } = this.props;
    const playerTaken = players.some(chosenPlayer => chosenPlayer.getIn(['role', 'name']) === player.name);
    const primaryColor = (playerTaken) ? 'gray' : player.primaryColor;
    const secondaryColor = (playerTaken) ? 'black' : player.secondaryColor;
    return (
      <div
        className="player-select-tile"
        style={{ backgroundColor: primaryColor, border: `1px solid ${secondaryColor}` }}
      >
        <div className="player-select-headers">

          <h3 style={{ color: secondaryColor }}>{player.name}</h3>

          <Button
            className="header-button"
            onClick={() => this.updateRole(player.name)}
            disabled={playerTaken}
          >
            <i
              className="fa fa-check fa-2x"
              style={{
                lineHeight: '24px',
                borderRadius: '12px',
                color: (player.name === this.state.role) ? primaryColor : secondaryColor,
                backgroundColor: (player.name === this.state.role) ? secondaryColor : 'transparent'
              }}
            />
          </Button>

          <PlayerRoleInfoComponent player={Immutable.fromJS(player)} />

        </div>

        <div className="player-select-stats">
          <Chip
            avatar={<Avatar style={{ backgroundColor: secondaryColor, color: primaryColor }}>
              <i className="fa fa-user-plus fa-lg fa-center-icon" />
            </Avatar>}
            label={player.scoreAdd}
            classes={{
              label: 'fontSize18'
            }}
            className="player-chip"
          />

          <Chip
            avatar={<Avatar style={{ backgroundColor: secondaryColor, color: primaryColor }}>
              <i className="fa fa-arrows fa-lg fa-center-icon" />
            </Avatar>}
            label={player.scoreMove}
            classes={{
              label: 'fontSize18'
            }}
            className="player-chip"
          />

          <Chip
            avatar={<Avatar style={{ backgroundColor: secondaryColor, color: primaryColor }}>
              <i className="fa fa-magic fa-lg fa-center-icon" />
            </Avatar>}
            label={player.scoreSpecial}
            classes={{
              label: 'fontSize12'
            }}
            className="player-chip"
          />
        </div>

      </div>
    );
  }

  setupWorld = () => {
    this.props.playersReady(this.props.players.size);
  }

  addPlayer = () => {
    const role = PlayerTypes.find(playerType => playerType.name === this.state.role);
    this.props.actions.addPlayer(role, this.state.playerName);
    this.setState({
      role: '',
      playerName: ''
    });
  };

  updateName = (e) => {
    this.setState({
      playerName: e.target.value
    });
  }

  updateRole = (name) => {
    this.setState({
      role: name
    });
  }

  render() {
    const { players } = this.props;

    const addPlayerButton = (<Button
      raised
      className="main-button"
      onClick={this.addPlayer}
      disabled={this.state.role === '' || this.state.playerName === ''}
    >
      <i className="fa fa-user-plus fa-lg" /> Add Player
    </Button>);
    const setupWorldButton = (<Button
      raised
      className="main-button"
      onClick={this.setupWorld}
      disabled={players.size <= 2}
    >
      <i className="fa fa-globe fa-lg" /> Set-up World
    </Button>);

    return (
      <div>
        <PlayerInfoComponent alwaysOpen />
        <div className="player-form" style={{ marginLeft: '650px' }}>
          <h3 className="select-title">Select Your Players</h3>
          <TextField label="Player Name" onChange={this.updateName} value={this.state.playerName} />
          {addPlayerButton}
          {setupWorldButton}
          <div className="player-type-container" style={{ backgroundColor: '#91e278' }} >
            <p>These players are recommended if you are new to the game</p>
            {this.getPlayerType(PlayerTypes[0])}
            {this.getPlayerType(PlayerTypes[1])}
            {this.getPlayerType(PlayerTypes[2])}
            {this.getPlayerType(PlayerTypes[3])}
          </div>
          <div className="player-type-container" style={{ backgroundColor: '#f4f266' }} >
            <p>These players are recommended if you are at least a little familiar with the game</p>
            {this.getPlayerType(PlayerTypes[4])}
            {this.getPlayerType(PlayerTypes[5])}
            {this.getPlayerType(PlayerTypes[6])}
            {this.getPlayerType(PlayerTypes[7])}
          </div>
        </div>
      </div>
    );
  }

}

export default connect(mapStateToProps, mapDispatchToProps)(PlayerSelect);
