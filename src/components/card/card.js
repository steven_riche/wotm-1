import React from 'react';
import PropTypes from 'prop-types';
import Button from 'material-ui/Button';
import Paper from 'material-ui/Paper';

import './card.scss';

const Card = ({ cardTitle, cardDesc, buttonOnClick, buttonDisabled, buttonText, addlContent }) =>
  (<div className="card-paper-outer">
    <Paper elevation={4} className="card-paper">
      <div className="card-paper-inner">
        <h3>{cardTitle}</h3>
        <p>{cardDesc}</p>
        {(buttonOnClick) ?
          <Button
            raised
            className="main-button card-button"
            onClick={buttonOnClick}
            disabled={buttonDisabled}
          >
            {buttonText}
          </Button>
          : null
        }
        {addlContent || null}
      </div>
    </Paper>
  </div>);

Card.propTypes = {
  cardTitle: PropTypes.string.isRequired,
  cardDesc: PropTypes.string.isRequired,
  buttonOnClick: PropTypes.func.isRequired,
  buttonDisabled: PropTypes.bool.isRequired,
  buttonText: PropTypes.string.isRequired,
  addlContent: PropTypes.node
};

Card.defaultProps = {
  addlContent: null
};

export default Card;
