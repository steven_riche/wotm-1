import React from 'react';
import PropTypes from 'prop-types';
import Immutable from 'immutable';
import AppBar from 'material-ui/AppBar';
import Button from 'material-ui/Button';
import Dialog from 'material-ui/Dialog';
import Tabs, { Tab } from 'material-ui/Tabs';
import PlayerInfoEnum from '../../game_config/players/player-info/player-info-enum';

import './player-role-info.scss';

export class PlayerRoleInfo extends React.PureComponent {
  static propTypes = {
    player: PropTypes.instanceOf(Immutable.Map).isRequired
  }

  constructor(props) {
    super(props);
    this.state = {
      activeTab: 'rules',
      open: false
    };
  }

  toggleDialog = () => this.setState({ open: !this.state.open });

  changeTab = (e, v) => this.setState({ activeTab: v });

  render() {
    const { player } = this.props;
    return (
      <div className="player-role-dialog-container">
        <Button className="info-button" onClick={this.toggleDialog}>
          <i className="fa fa-info-circle fa-2x" style={{ color: player.get('secondaryColor') }} />
        </Button>
        <Dialog className="player-role-dialog" open={this.state.open} onRequestClose={this.toggleDialog} fullWidth maxWidth="md">
          <h3 className="dialog-title">{player.get('name')}</h3>
          <AppBar position="static">
            <Tabs value={this.state.activeTab} onChange={this.changeTab}>
              <Tab value="rules" label="Rules" />
              <Tab value="backstory" label="Backstory" />
              <Tab value="strategy" label="Strategy" />
            </Tabs>
          </AppBar>
          {this.state.activeTab === 'rules' && PlayerInfoEnum[player.get('tabKey')].rulesTab}
          {this.state.activeTab === 'backstory' && PlayerInfoEnum[player.get('tabKey')].backstoryTab}
          {this.state.activeTab === 'strategy' && PlayerInfoEnum[player.get('tabKey')].strategyTab}
        </Dialog>
      </div>
    );
  }
}

export default PlayerRoleInfo;
