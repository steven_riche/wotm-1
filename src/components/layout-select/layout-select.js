import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Immutable from 'immutable';
import Button from 'material-ui/Button';
import Layouts from '../../game_config/boards/board-layout-enum';
import B_W from '../../game_config/boards/board-size';
import { updateActivePlayer } from '../../actions/game';
import { setLayout } from '../../actions/world';
import PlayerInfoComponent from '../player-info/player-info';

import './layout-select.scss';

function mapStateToProps(state) {
  return {
    players: state.players
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({
      updateActivePlayer,
      setLayout
    }, dispatch)
  };
}

export class LayoutSelect extends React.PureComponent {
  static propTypes = {
    players: PropTypes.instanceOf(Immutable.List).isRequired,
    actions: PropTypes.shape({
      updateActivePlayer: PropTypes.func,
      setLayout: PropTypes.func
    }).isRequired,
    worldReady: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      chosenLayout: '',
      layoutRendering: null
    };
  }

  componentWillMount() {
    this.props.actions.updateActivePlayer(this.props.players.size - 1);
  }

  setLayout = (e) => {
    const { players } = this.props;

    const potentialLayouts = Layouts.layouts
      .find(layout => layout.numPlayers === players.size).layouts;

    let layoutRendering = null;

    if (e.target.value !== '') {
      const chosenLayout = potentialLayouts[e.target.value];

      layoutRendering = (<div className="gameBoard" style={{ padding: '20px' }}>
        <svg height={((3 * B_W) / 3)} width={((3 * B_W) / 3)} x="0" y="0">
          {this.drawRooms(chosenLayout)}
        </svg>
      </div>);
    }

    this.setState({
      chosenLayout: e.target.value,
      layoutRendering
    });
  }

  drawRooms = layout => layout.map(room => (
    <g
      key={`room-${room.x}-${room.y}`}
      width={B_W / 3}
      height={B_W / 3}
      x={room.x / 3}
      y={room.x / 3}
    >
      <rect
        x={room.x / 3}
        y={room.y / 3}
        width={B_W / 3}
        height={B_W / 3}
        fill="#CCC"
        stroke="black"
      />
    </g>
  ));

  createLayout = (i) => {
    const potentialLayouts = Layouts.layouts
      .find(layout => layout.numPlayers === this.props.players.size).layouts;

    return (<div className="gameBoard" style={{ padding: '20px' }}>
      <svg height={((3 * B_W) / 3)} width={((3 * B_W) / 3)} x="0" y="0">
        {this.drawRooms(potentialLayouts[i])}
      </svg>

      <Button
        className="layout-button"
        onClick={() => this.chooseLayout(i)}
      >
        <i
          className="fa fa-check fa-2x"
          style={{
            lineHeight: '24px',
            borderRadius: '12px',
            color: (this.state.chosenLayout === i) ? 'white' : 'black',
            backgroundColor: (this.state.chosenLayout === i) ? 'green' : 'transparent'
          }}
        />
      </Button>
    </div>);
  }

  chooseLayout = (layout) => {
    this.setState({
      chosenLayout: layout
    });
  }

  worldReady = () => {
    const { players, actions } = this.props;
    const chosenLayout = Layouts.layouts
      .find(layout => layout.numPlayers === players.size).layouts[this.state.chosenLayout];

    actions.setLayout(chosenLayout);
    this.props.worldReady();
  }

  render() {
    const { players } = this.props;
    const lastPlayerName = players.last().get('playerName');
    return (<div>
      <PlayerInfoComponent />
      <div className="layoutSelect" style={{ marginLeft: '650px' }}>
        <h3 className="select-title">Choose a Board Layout</h3>
        <p>{lastPlayerName}, please select a board layout</p>
        {this.createLayout(0)}
        {this.createLayout(1)}
        <Button
          raised
          className="main-button"
          disabled={this.state.chosenLayout === ''}
          onClick={this.worldReady}
        >
          Next <i className="fa fa-angle-double-right fa-lg" />
        </Button>

      </div>
    </div>);
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LayoutSelect);
