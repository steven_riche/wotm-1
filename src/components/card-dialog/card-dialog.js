import React from 'react';
import PropTypes from 'prop-types';
import Immutable from 'immutable';
import Dialog from 'material-ui/Dialog';

const CardDialog = ({ open, onRequestClose, cards, bottomButton }) => {
  const cardRowCount = (cards) ? Math.ceil(cards.length / 3) : 0;
  const cardRows = [];
  for (let iii = 0; iii < cardRowCount; iii += 1) {
    const card1 = (cards.length >= (iii * 3)) ? cards[(iii * 3)] : null;
    const card2 = (cards.length >= ((iii * 3) + 1)) ? cards[(iii * 3) + 1] : null;
    const card3 = (cards.length >= ((iii * 3) + 2)) ? cards[(iii * 3) + 2] : null;
    cardRows.push(<div className="rowFlex" key={`card-row-${iii}`}>{card1}{card2}{card3}</div>);
  }

  return (
    <Dialog open={open} onRequestClose={onRequestClose} fullWidth maxWidth="md">
      {cardRows}
      {bottomButton || null}
    </Dialog>
  );
};

CardDialog.propTypes = {
  open: PropTypes.bool.isRequired,
  onRequestClose: PropTypes.func.isRequired,
  cards: PropTypes.arrayOf(Immutable.Map),
  bottomButton: PropTypes.element
};

CardDialog.defaultProps = {
  cards: [],
  bottomButton: null
};

export default CardDialog;
