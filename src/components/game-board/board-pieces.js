import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Immutable from 'immutable';
import { drawPieces } from '../../utils/drawing';

function mapStateToProps(state) {
  return {
    players: state.players
  };
}

const BoardPieces = ({ world, players, trigger }) => (<g>{drawPieces(world, players, trigger)}</g>);

BoardPieces.propTypes = {
  players: PropTypes.instanceOf(Immutable.List).isRequired,
  world: PropTypes.instanceOf(Immutable.Map).isRequired,
  trigger: PropTypes.func.isRequired
};

export default connect(mapStateToProps)(BoardPieces);
