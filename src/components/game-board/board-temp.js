import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Immutable from 'immutable';
import { drawTemporaryPieces } from '../../utils/drawing';

function mapStateToProps(state) {
  return {
    game: state.game,
    players: state.players
  };
}

const BoardTemp = ({ game, players, world, trigger }) =>
  (<g>{drawTemporaryPieces(game, world, players, trigger)}</g>);

BoardTemp.propTypes = {
  game: PropTypes.instanceOf(Immutable.Map).isRequired,
  players: PropTypes.instanceOf(Immutable.List).isRequired,
  world: PropTypes.instanceOf(Immutable.Map).isRequired,
  trigger: PropTypes.func.isRequired
};

export default connect(mapStateToProps)(BoardTemp);
