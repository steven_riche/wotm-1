import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Immutable from 'immutable';
import BoardPiecesComponent from './board-pieces';
import BoardTempPiecesComponent from './board-temp';
import { drawShapeOverlays } from '../../utils/drawing';

function mapStateToProps(state) {
  return {
    game: state.game
  };
}

const GameRenderer = ({ game, world }) => {
  const triggerAction = (space) => {
    if (game.hasIn(['triggerableSpaces', space])) {
      game.getIn(['triggerableSpaces', space])();
    }
  };

  return (
    <g>
      <BoardPiecesComponent world={world} trigger={triggerAction} />
      <BoardTempPiecesComponent world={world} trigger={triggerAction} />
      {drawShapeOverlays(game, world, triggerAction)}
    </g>
  );
};

GameRenderer.propTypes = {
  game: PropTypes.instanceOf(Immutable.Map).isRequired,
  world: PropTypes.instanceOf(Immutable.Map).isRequired
};

export default connect(mapStateToProps)(GameRenderer);
