import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Immutable from 'immutable';
import B_W from '../../game_config/boards/board-size';
import { drawRooms } from '../../utils/drawing';
import GameRendererComponent from './game-renderer';

function mapStateToProps(state) {
  return {
    world: state.world
  };
}

const GameBoard = ({ world }) => (
  <div className="gameBoard" style={{ padding: '20px', marginLeft: '375px' }}>
    <svg height={(3 * B_W)} width={(3 * B_W)} x="0" y="0">
      {drawRooms(world)}
      <GameRendererComponent world={world} />
    </svg>
  </div>
);

GameBoard.propTypes = {
  world: PropTypes.instanceOf(Immutable.Map).isRequired
};

export default connect(mapStateToProps)(GameBoard);
