import React from 'react';
import AppBar from 'material-ui/AppBar';
import Button from 'material-ui/Button';
import Dialog from 'material-ui/Dialog';
import Tabs, { Tab } from 'material-ui/Tabs';

import './how-to-play.scss';

export class HowToPlay extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: 'intro',
      open: false
    };
  }

  introTab = () => (<div className="tab-container">
    <p><em>
      The year is 1901. A new world is dawning, but in the hidden recesses of the old
      world, secret cabals of alchemists and thaumaturgists are at civil war, each
      claiming to be the true inheritors of the traditions of magic.
    </em></p>

    <p><em>
      A rumor spreads like wildfire. Overnight, even the greenest novices have heard:
      the legendary Library of Lemuria has been discovered and exhumed. Once the seat
      of magical power, its secrets have been lost for generations. Every school of
      magic has descended on the Library, racing to be the first to unlock the true
      secrets of magic and win the War of the Mystics once and for all, no matter the cost.
    </em></p>

    <p>
      War of the Mystics is a tactical board game for 3 to 8 players. Play time typically
      takes 45 to 90 minutes, depending on the number of players. The game is assymetric,
      meaning that each player will have a very different play style and will need to use
      a different strategy to win.
    </p>

    <p>
      The goal of the game is to be the first player to capture 4 enemy pieces.
    </p>
  </div>);

  basicsTab = () => (<div className="tab-container">
    <p>
      The goal of the game is to be the first player to capture 4 enemy pieces.
    </p>

    <p>
      Each player represents the leader of a mysterious cult or cabal that practices
      a branch of magic. As the leader, you have an endless supply of loyal devotees
      that will venture into the Library and potentially sacrifice themselves in
      order for your organization to secure the secrets to the most powerful magic.
    </p>

    <p>
      At the beginning of the game, each player will add a room to the board, representing
      the mystical Library of Lemuria, existing in a pocket dimension that takes shape
      depending on the individuals that enter it. Each room has a unique pattern of
      spaces with a different number of spaces that border or touch each other space -
      e.g. in a room with triangle spaces, each space only borders 3 others but touches
      up to 9 other spaces on the corners. In contrast, in a room with hexagon spaces,
      each space borders 6 others but does not touch any additional spaces on the
      corners. These differences in room layout may make big differences for different
      strategies or the different stengths and weaknesses of the battling cabals.
    </p>

    <p>
      Each room also contains at least one green colored space on each of the four
      sides - these green spaces represent passageways between rooms, and the green
      spaces on two touching rooms are considered to be bordering one another. Furthermore,
      due to the odd physics of being inside a pocket dimension, any green space on
      the edge of the board is considered bordering the green spaces on the exact
      opposite of the board.
    </p>

    <p>
      During the game, players will take turns performing two actions each (see the
      <strong> &ldquo;Game Actions&rdquo;</strong> tab). During the course of the
      game, a player might draw a card that allows for extra actions (see the
      <strong> &ldquo;Cards&rdquo;</strong> tab), or might gain an extra action by
      having pieces destroyed. Every time a player has a piece destroyed (completely
      removed from the board), that player gains a unit in their free action meter.
      After 5 units, the player has a free action they can spend.
    </p>

  </div>);

  setupTab = () => (<div className="tab-container">
    <p>
      At the beginning of the game, each player will choose a different player type
      to play. Each player type represents a different cabal racing to outmaneuver
      their enemies and unlock the secrets of power of the Library, and each has
      its unique type of magic. Each type has unique strengths and weaknesses, and
      will need to employ different strategies to succeed.
    </p>

    <p>
      Once each player has chosen a player type and entered his or her name, the
      last player will be given a choice of two board layouts to choose from.
    </p>

    <p>
      Once the board layout has been determined, each player will be able to choose
      a room to add from one of two choices. They will then choose which remaining
      spot in the layout that room will occupy, and can rotate that room to whichever
      orientation they would like.
    </p>

    <p>
      Once each player has their room chosen and placed in the board, the game is
      ready to begin!
    </p>
  </div>);

  actionsTab = () => (<div className="tab-container">
    <p>
      On each turn, a player will take two actions from the following choices. He or
      she may perform the same action twice, or two different actions.
    </p>

    <h3>1. Adding more pieces</h3>
    <p>
      A player may add more pieces to the board. <strong>If a player does not have any
      other pieces currently on the board (such as at the beginning of the game, or
        if all of his or her pieces are destroyed), their next action must be adding
        one piece to an open blue space in their original room.</strong> If there
        are not any open blue spaces in their original room, they may add a piece on
        any open space in their original room. If all of those spaces are also occupied,
        they may add a single piece on any open space on the board.
    </p>

    <p>
      If a player does have pieces on the board, he or she may choose one of their
      exisitng pieces and add more pieces around it. That is because once there is a
      devotee (piece) inside the Library dimension, it is easier for them to hold
      open a portal from their side to allow others to join them. Each different
      player type has a maximum number of pieces they can add for one action. For
      a normal add action, a player will choose an existing piece, and then add up
      to their maximum of additional pieces onto unoccupied spaces that are bordering
      the original piece. Keep in mind that different rooms have different layouts
      that may make it easier or more difficult to add pieces in them.
    </p>

    <p>
      Although there is no limit to the total number of pieces that could be added to
      the board over the course of a game, each player may only have 15 pieces in play
      at any one time (including pieces being held by other players as their most
      recently captured piece - see <strong>Capturing a piece</strong> below).
    </p>

    <p>
      Note that some player types have unique ways of adding pieces that might differ
      from the rules given here.
    </p>

    <h3>2. Moving pieces</h3>
    <p>
      If a player has pieces on the board, he or she may take an action to move their
      existing pieces. Each player role has a maximum limit of total spaces they may
      move their pieces during an action. These spaces may be divvied up among pieces
      however a player would like. For instance, a player playing as &ldquoThe Order
      of the Azure Main&rdquo may move a maximum of 5 spaces, so the player might move
      one piece 2 spaces, and three other pieces 1 space each.
    </p>

    <p>
      When moving, one space is considered moving over to one unoccupied space that is
      bordering the previous space by a line. A piece cannot normally move onto or
      through a space that is occupied with another piece.
    </p>

    <p>
      Note that some player types have unique ways of moving that might differ from
      the rules given here.
    </p>

    <h3>3. Special abilities</h3>
    <p>
      Each player type has a unique special ability that they can perform that uses
      their unique magical abilities. See the information on that player type for
      more information.
    </p>

    <h3>4. Capturing a piece</h3>
    <p>
      The fourth and final type of action - and the means to winning the game - is
      capturing an enemy piece. Capturing a piece is distinct from other actions that
      might destroy enemy pieces. In capturing a piece, a player must surround an
      enemy piece with a required number of their own pieces, then destroy both their
      pieces and the enemy piece. While adding or moving pieces generally require spaces
      that are bordering on sides, capturing pieces can be done by bordering or by
      touching on a corner. So, while a room with triangle-shaped spaces may be
      difficult to move or add pieces in because each space only borders 3 others,
      it is very easy to capture another piece in because each space touches a total
      of 12 other spaces.
    </p>

    <p>
      In the upper right of the play area, there is a scoreboard that displays the
      requirements for capturing enemy pieces. At the beginning of the game, a player
      must only surround an enemy with <strong>2</strong> of their own pieces. After
      successfully capturing an enemy, their next capture must use <strong>3</strong>
      of their own pieces. Their third capture must use <strong>3</strong> of their
      pieces and <strong>1</strong> of that same type of enemy piece, all surrounding
      their target piece. Finally, to win the game, they must make a fourth capture
      that uses <strong>4</strong> of their pieces to capture an enemy.
    </p>

    <p>
      <strong>In a game of more than 3 players, a player cannot capture the same enemy
      twice in a row.</strong> Players will retain their &ldquo;last captured&rdquo;
      piece (to remind them of who is temporarily immune from capturing). These
      &ldquo;last captured&rdquo; pieces count toward the 15 piece limit for each player.
    </p>
  </div>);

  cardsTab = () => (<div className="tab-container">
    <p>
      In addition to the player-type-specific special abilities each player possesses,
      he or she will also get the chance to draw cards that give additional abilities.
    </p>

    <p>
      Cards grant varying abilities, ranging from allowing a player to move further
      on a turn, to granting them special abilities from other players they can use
      for free every turn, to abilities that allow them to add extra pieces around
      the board. Each card has a positive effect, but some may be more powerful
      than others, and some may be better in certain situations than others.
    </p>

    <p>
      Each room on the board has 6 blue spaces. These blue spaces represent
      &ldquo;meditation areas&rduo; in the Library. With enough continued focus, a
      player may gain an additional ability by drawing a card. In other words, if a
      player has enough pieces on blue spaces in a given room, that player can draw
      a card and it is immediately active. If at any time, a player has too few pieces
      on blue spaces in a room, they will immediately lose a card. This might be
      because they move pieces, because some of their pieces are captured or destroyed,
      or because they gain points and the requirement to keep a card rises.
    </p>

    <p>
      In the top right of the play area, there is a scoreboard that shows the current
      requirement for each player to draw or keep a card. As a player gains more points,
      it becomes more difficult to gain or keep cards. In the beginning of the game,
      a player must only occupy at least 2 blue spaces in a room to draw or keep a card.
      However, when a player has 3 points and is close to winning, they must occupy at
      least 5 blue spaces in a room to draw or keep a card.
    </p>

    <p>
      A player may take one card for every room in the board. In other words, in a four-player game,
      there will be four rooms in the board, and it will be possible for a player to have four cards
      at once. If a player has multiple cards and loses a card, they will automatically lose their
      oldest card, regardless of which room they drew each card.
    </p>

    <p>
      When drawing a card, a player has their choice from the top three cards on the
      deck face up, or they can blindly draw the next card face down.
    </p>

    <p>
      If a player has multiples of the same card, the effects of those cards do stack.
      In other words, if a player has two different cards each granting +3 spaces on
      a move action, the player now has a +6 on a move action.
    </p>

    <p>
      Whether a player gains, keeps, or loses a card is evaluated at the end of each turn,
      so you may move a piece off of a blue space and back on a blue space in the same
      action without losing a card.
    </p>
  </div>);

  toggleDialog = () => this.setState({ open: !this.state.open });

  changeTab = (e, v) => this.setState({ activeTab: v });

  render() {
    return (
      <div className="how-to-play-container">
        <Button fab className="help-button" onClick={this.toggleDialog}>
          <i className="fa fa-question-circle fa-2x" />
        </Button>
        <Dialog className="how-to-play-dialog" open={this.state.open} onRequestClose={this.toggleDialog} fullWidth maxWidth="md">
          <h3 className="dialog-title">How To Play</h3>
          <AppBar position="static">
            <Tabs value={this.state.activeTab} onChange={this.changeTab}>
              <Tab value="intro" label="Introduction" />
              <Tab value="basics" label="Game Basics" />
              <Tab value="setup" label="Game Setup" />
              <Tab value="actions" label="Game Actions" />
              <Tab value="cards" label="Cards" />
            </Tabs>
          </AppBar>
          {this.state.activeTab === 'intro' && this.introTab()}
          {this.state.activeTab === 'basics' && this.basicsTab()}
          {this.state.activeTab === 'setup' && this.setupTab()}
          {this.state.activeTab === 'actions' && this.actionsTab()}
          {this.state.activeTab === 'cards' && this.cardsTab()}
        </Dialog>
      </div>
    );
  }
}

export default HowToPlay;
