import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import GameBoardComponent from '../game-board/game-board';
import PlayerActionsComponent from '../player-actions/player-actions';
import PlayerInfoComponent from '../player-info/player-info';
import PromptComponent from '../prompt/prompt';
import CardDeckComponent from '../cardDeck/cardDeck';
import ScoreboardComponent from '../scoreboard/scoreboard';
import HowToPlayComponent from '../how-to-play/how-to-play';
import Cards from '../../game_config/cards/cards-enum';
import { loadCardDeck, shuffleCardDeck } from '../../actions/game';

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({
      loadCardDeck,
      shuffleCardDeck
    }, dispatch)
  };
}

export class GamePlay extends React.PureComponent {

  static propTypes = {
    setGameOver: PropTypes.func.isRequired,
    actions: PropTypes.shape({
      loadCardDeck: PropTypes.func,
      shuffleCardDeck: PropTypes.func
    }).isRequired
  };

  componentWillMount() {
    const { actions } = this.props;
    Promise.resolve(actions.loadCardDeck(Cards))
      .then(actions.shuffleCardDeck);
  }

  render() {
    return (
      <div>
        <PlayerInfoComponent />
        <GameBoardComponent />
        <ScoreboardComponent setGameOver={this.props.setGameOver} />
        <PlayerActionsComponent />
        <PromptComponent />
        <CardDeckComponent />
        <HowToPlayComponent />
      </div>
    );
  }
}

export default connect(null, mapDispatchToProps)(GamePlay);
