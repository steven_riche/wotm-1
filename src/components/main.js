import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Immutable from 'immutable';
import PlayerSelectComponent from './player-select/player-select';
import LayoutSelectComponent from './layout-select/layout-select';
import RoomSelectComponent from './room-select/room-select';
import GameOverComponent from './game-over/game-over';
import GamePlayComponent from './game-play/game-play';
import { initializeGame, initializeWorld, initializePlayers } from '../actions/initStore';
import { updatePlayerCount, updatePrompt } from '../actions/game';

import './main.scss';

const LOAD_DEMO_GAME = false;

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({
      initializeGame,
      initializeWorld,
      initializePlayers,
      updatePlayerCount,
      updatePrompt
    }, dispatch)
  };
}

export class Main extends React.PureComponent {
  static propTypes = {
    actions: PropTypes.shape({
      initializeGame: PropTypes.func,
      initializeWorld: PropTypes.func,
      initializePlayers: PropTypes.func,
      updatePlayerCount: PropTypes.func,
      updatePrompt: PropTypes.func
    }).isRequired
  };

  constructor(props) {
    super(props);
    props.actions.initializeGame();
    if (LOAD_DEMO_GAME) {
      props.actions.initializeWorld();
      props.actions.initializePlayers();
    }
    this.state = {
      setupPlayers: LOAD_DEMO_GAME,
      setupWorld: LOAD_DEMO_GAME,
      setupRooms: LOAD_DEMO_GAME,
      gameOver: false,
      gameOverPlayers: Immutable.List()
    };
  }

  setupPlayers = () => <PlayerSelectComponent playersReady={this.playersReady} />;

  setupWorld = () => <LayoutSelectComponent worldReady={this.worldReady} />;

  setupRooms = () => <RoomSelectComponent roomsReady={this.roomsReady} />;

  worldReady = () => {
    this.setState({
      setupWorld: true
    });
  }

  playersReady = (playerSize) => {
    this.props.actions.updatePlayerCount(playerSize);
    this.setState({
      setupPlayers: true
    });
  }

  roomsReady = (promptMessage) => {
    Promise.resolve(this.props.actions.updatePrompt(promptMessage))
      .then(() => this.setState({
        setupRooms: true
      }));
  }

  setGameOver = (gameOverPlayers) => {
    this.setState({
      gameOver: true,
      gameOverPlayers
    });
  };

  gameOver = () => <GameOverComponent players={this.state.gameOverPlayers} />;

  playGame = () => <GamePlayComponent setGameOver={this.setGameOver} />;

  render() {
    if (this.state.gameOver) {
      return this.gameOver();
    } else if (!this.state.setupPlayers) {
      return this.setupPlayers();
    } else if (!this.state.setupWorld) {
      return this.setupWorld();
    } else if (!this.state.setupRooms) {
      return this.setupRooms();
    }

    return this.playGame();
  }
}

export default connect(null, mapDispatchToProps)(Main);
