import React from 'react';
import PropTypes from 'prop-types';
import Immutable from 'immutable';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Button from 'material-ui/Button';
import { FormControl } from 'material-ui/Form';
import Input, { InputLabel } from 'material-ui/Input';
import { MenuItem } from 'material-ui/Menu';
import Select from 'material-ui/Select';
import { updateActivePlayer } from '../../actions/game';
import { setRoom } from '../../actions/world';
import B_W from '../../game_config/boards/board-size';
import PlayerInfoComponent from '../player-info/player-info';

import './room-select.scss';

function mapStateToProps(state) {
  return {
    world: state.world,
    players: state.players
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({
      updateActivePlayer,
      setRoom
    }, dispatch)
  };
}

export class RoomSelect extends React.PureComponent {
  static propTypes = {
    world: PropTypes.instanceOf(Immutable.Map).isRequired,
    players: PropTypes.instanceOf(Immutable.List).isRequired,
    actions: PropTypes.shape({
      updateActivePlayer: PropTypes.func,
      setRoom: PropTypes.func
    }).isRequired,
    roomsReady: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      currentPlayerIndex: 0,
      currentChosenRoomIndex: null,
      currentChosenRoom: 0,
      currentChosenRotation: 0
    };
  }

  componentWillMount() {
    this.props.actions.updateActivePlayer(0);
  }

  setChosenRoomIndex = (i) => {
    this.setState({
      currentChosenRoomIndex: i
    });
  }

  setRoom = (e) => {
    this.setState({
      currentChosenRoom: e.target.value
    });
  }

  drawShapes = (board, offset) => {
    const doorSpaces = board.getIn(['type', 'doors']).keySeq().reduce((a, b) => a.concat(board.getIn(['type', 'doors', b])), Immutable.List());

    return board.getIn(['type', 'spaces']).keySeq().map((space) => {
      let spaceColor = 'white';
      if (doorSpaces.includes(space)) {
        spaceColor = 'rgba(0, 255, 0, .3)';
      } else if (board.get('meditationSpaces').includes(space)) {
        spaceColor = 'rgba(0, 0, 255, .3)';
      }

      const points = board.getIn(['type', 'spaces', space, 'points']).reduce((a, b) => `${a}${b.get('x') + offset.x}, ${b.get('y') + offset.y}\n`, '');

      const spaceString = `${board.get('name')}_${space}`;
      const className = 'room-space';

      return (
        <polygon
          key={spaceString}
          id={spaceString}
          className={className}
          stroke="black"
          fill={spaceColor}
          points={points}
        />
      );
    });
  }

  drawRooms = (world, players) => world.get('layout').map((room, i) => {
    let transform = `rotate(0 ${(B_W / 2) + room.get('x')} ${(B_W / 2) + room.get('y')})`;
    let shapes = null;
    let trigger = () => this.setChosenRoomIndex(i);

    if (world.hasIn(['rooms', i]) && world.getIn(['rooms', i]) !== undefined) {
      transform = `rotate(${world.getIn(['rooms', i, 'rotation'])} ${(B_W / 2) + room.get('x')} ${(B_W / 2) + room.get('y')})`;
      shapes = this.drawShapes(world.getIn(['rooms', i, 'board']), { x: room.get('x'), y: room.get('y') });
      trigger = () => {};
    } else if (this.state.currentChosenRoomIndex === i) {
      transform = `rotate(${this.state.currentChosenRotation} ${(B_W / 2) + room.get('x')} ${(B_W / 2) + room.get('y')})`;
      shapes = this.drawShapes(players.getIn([this.state.currentPlayerIndex, 'role', 'boards', this.state.currentChosenRoom]), { x: room.get('x'), y: room.get('y') });
    }
    return (<g
      key={`room-${room.get('x')}-${room.get('y')}`}
      width={B_W}
      height={B_W}
      x={room.get('x')}
      y={room.get('y')}
      transform={transform}
    >
      <rect
        x={room.get('x')}
        y={room.get('y')}
        width={B_W}
        height={B_W}
        fill="#CCC"
        stroke="black"
        onClick={trigger}
      />
      {shapes}
    </g>);
  });

  drawBoard = () => {
    const { world, players } = this.props;

    return (
      <div className="gameBoard" style={{ padding: '20px' }}>
        <svg height={(3 * B_W)} width={(3 * B_W)} x="0" y="0">
          {this.drawRooms(world, players)}
        </svg>
      </div>
    );
  }

  rotateClockwise = () => {
    this.setState({
      currentChosenRotation: (this.state.currentChosenRotation + 90) % 360
    });
  }

  rotateCounterClockwise = () => {
    this.setState({
      currentChosenRotation: (this.state.currentChosenRotation + 270) % 360
    });
  }

  assignRoom = () => {
    const { players, actions } = this.props;
    const room = {
      player: players.getIn([this.state.currentPlayerIndex, 'player']),
      board: players.getIn([this.state.currentPlayerIndex, 'role', 'boards', this.state.currentChosenRoom]),
      rotation: this.state.currentChosenRotation
    };
    actions.setRoom(this.state.currentChosenRoomIndex, room);

    if (players.size > (this.state.currentPlayerIndex + 1)) {
      actions.updateActivePlayer(this.state.currentPlayerIndex + 1);
      this.setState({
        currentPlayerIndex: this.state.currentPlayerIndex + 1,
        currentChosenRoomIndex: null,
        currentChosenRoom: 0,
        currentChosenRotation: 0
      });
    } else {
      actions.updateActivePlayer(0);
      this.props.roomsReady(`${this.props.players.getIn([0, 'playerName'])}, please choose an action`);
    }
  }

  render() {
    const { players } = this.props;
    const boardRendering = this.drawBoard();
    return (<div>
      <PlayerInfoComponent />
      <div className="roomSelect" style={{ marginLeft: '650px' }}>
        <h3 className="select-title">Choose Rooms</h3>
        <div className="room-select-form">
          <p>{players.getIn([this.state.currentPlayerIndex, 'playerName'])}, please select a location, a room, and set your rotation</p>
          <FormControl className="room-type-select">
            <InputLabel htmlFor="room-type">Room Type</InputLabel>
            <Select input={<Input id="room-type" />} value={this.state.currentChosenRoom} onChange={this.setRoom}>
              <MenuItem value={0}>Option 1</MenuItem>
              <MenuItem value={1}>Option 2</MenuItem>
            </Select>
          </FormControl>
          <Button raised className="main-button" onClick={this.rotateClockwise}>
            <i className="fa fa-rotate-right fa-lg" />
          </Button>
          <Button raised className="main-button" onClick={this.rotateCounterClockwise}>
            <i className="fa fa-rotate-left fa-lg" />
          </Button>
          <Button raised className="main-button" disabled={this.state.currentChosenRoomIndex === null} onClick={this.assignRoom}>
            Next <i className="fa fa-angle-double-right fa-lg" />
          </Button>
        </div>
        {boardRendering}
      </div>
    </div>);
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RoomSelect);
