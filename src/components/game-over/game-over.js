import React from 'react';
import PropTypes from 'prop-types';
import Immutable from 'immutable';

import './game-over.scss';

const GameOver = ({ players }) => {
  const sortedPlayers = players.sort((a, b) => ((a === b) ? 0 : (a < b)));
  const remainingScores = sortedPlayers.rest().map(player =>
    <div
      key={`other-players-div-${player.get('player')}`}
      className="other-players-div"
      style={{ backgroundColor: player.getIn(['role', 'primaryColor']), color: player.getIn(['role', 'secondaryColor']) }}
    >
      <p>{player.get('playerName')} - {player.get('score')} points</p>
    </div>
  );
  return (<div className="game-over">
    <h3 className="title">Game Over</h3>
    <div className="winner-div" style={{ backgroundColor: sortedPlayers.first().getIn(['role', 'primaryColor']), color: sortedPlayers.first().getIn(['role', 'secondaryColor']) }}>
      <h3>Congratulations, {sortedPlayers.first().get('playerName')}</h3>
      <p>You won with {sortedPlayers.first().get('score')} points.</p>
    </div>
    {remainingScores}
  </div>);
};

GameOver.propTypes = {
  players: PropTypes.instanceOf(Immutable.List).isRequired
};

export default GameOver;
