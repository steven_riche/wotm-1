import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Immutable from 'immutable';
import Button from 'material-ui/Button';
import { appendCard } from '../../actions/players';
import { drawCard, updateCurrentAction } from '../../actions/game';
import { confirmAction } from '../../game_logic/cancel_confirm_process';
import CardDialog from '../card-dialog/card-dialog';
import Card from '../card/card';
import Logo from '../../img/logo.png';

import './cardDeck.scss';

function mapStateToProps(state) {
  return {
    game: state.game
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({
      appendCard,
      updateCurrentAction,
      drawCard
    }, dispatch)
  };
}

export class CardDeck extends React.PureComponent {

  static propTypes = {
    game: PropTypes.instanceOf(Immutable.Map).isRequired,
    actions: PropTypes.shape({
      appendCard: PropTypes.func,
      updateCurrentAction: PropTypes.func,
      drawCard: PropTypes.func
    }).isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      open: false
    };
  }

  getCard = (cardIndex) => {
    const card = (this.props.game.hasIn(['cardDeck', cardIndex])) ? this.props.game.getIn(['cardDeck', cardIndex]) : null;
    const cardTitle = (card) ? card.get('title') : '';
    const cardDesc = (card) ? card.get('description') : '';
    const canDrawCard = this.props.game.get('canDrawCard');
    return (
      <Card
        cardTitle={cardTitle}
        cardDesc={cardDesc}
        buttonOnClick={() => this.drawCard(cardIndex)}
        buttonDisabled={!canDrawCard}
        buttonText="Draw"
      />
    );
  }

  drawCard = (cardIndex) => {
    const { game, actions } = this.props;
    Promise.resolve(actions.appendCard(game.get('playerTurn'), game.getIn(['cardDeck', cardIndex])))
      .then(actions.drawCard(cardIndex))
      .then(actions.updateCurrentAction({ type: 'DRAW_CARD', data: {} }))
      .then(confirmAction)
      .then(() => this.setState({ open: false }));
  }

  bottomButton = () => (<Button
    raised
    id="bottom-button"
    className="main-button"
    onClick={() => this.drawCard(3)}
    disabled={!this.props.game.get('canDrawCard')}
  >
    Draw Top Card of Deck
  </Button>);

  toggleDialog = () => this.setState({ open: !this.state.open });

  render() {
    const canDrawCard = this.props.game.get('canDrawCard');
    return (
      <div className="card-deck">
        <div className="card-deck-inner" onClick={this.toggleDialog} >
          <div
            className="card-div"
            style={{
              left: '30px',
              top: '20px',
              zIndex: 5
            }}
          />

          <div
            className="card-div"
            style={{
              left: '35px',
              top: '15px',
              zIndex: 6
            }}
          />

          <div
            className="card-div"
            style={{
              left: '40px',
              top: '10px',
              zIndex: 7
            }}
          />

          <div
            className="card-div"
            style={{
              left: '45px',
              top: '5px',
              zIndex: 8
            }}
          >
            <img src={Logo} className="card-logo" alt="card-logo" />
          </div>

          <div
            className={`card-div-transparent ${canDrawCard ? 'highlighted-primary' : ''}`}
            style={{
              left: '45px',
              top: '5px',
              zIndex: 9
            }}
          />

        </div>

        <CardDialog
          open={this.state.open}
          onRequestClose={this.toggleDialog}
          cards={[this.getCard(0), this.getCard(1), this.getCard(2)]}
          bottomButton={this.bottomButton()}
        />
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CardDeck);
