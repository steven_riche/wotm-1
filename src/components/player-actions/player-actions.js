import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Immutable from 'immutable';
import Button from 'material-ui/Button';
import { initAdd, initAddFirstPiece } from '../../game_logic/add_process';
import { initMove } from '../../game_logic/move_process';
import { initCapture } from '../../game_logic/capture_process';
import { cancelAction, confirmAction, forceNextTurn } from '../../game_logic/cancel_confirm_process';
import { updateBackup } from '../../actions/backup';
import { initializeBackup } from '../../actions/initStore';
import './player-actions.scss';

function mapStateToProps(state) {
  return {
    game: state.game,
    world: state.world,
    players: state.players
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({
      updateBackup,
      initializeBackup
    }, dispatch)
  };
}

export class PlayerActions extends React.PureComponent {
  static propTypes = {
    game: PropTypes.instanceOf(Immutable.Map).isRequired,
    world: PropTypes.instanceOf(Immutable.Map).isRequired,
    players: PropTypes.instanceOf(Immutable.List).isRequired,
    actions: PropTypes.shape({
      updateBackup: PropTypes.func,
      initializeBackup: PropTypes.func
    }).isRequired
  };

  initSpecial = () => {
    const { game, world, players, actions } = this.props;
    actions.updateBackup(game, world, players);
    players.getIn([game.get('playerTurn'), 'role', 'special', 'init'])();
  }

  componentWillMount() {
    const { game, world, players, actions } = this.props;
    actions.initializeBackup(game, world, players);
  }

  render() {
    const { game, players } = this.props;
    const currentAction = game.get('currentAction');
    const canConfirm = game.get('canConfirm');
    const cannotCapture = game.hasIn(['currentActionModifiers', 'orangeDebt']) && game.getIn(['currentActionModifiers', 'orangeDebt']);
    const disableButtons = currentAction.has('type') || game.get('canDrawCard') ||
      (game.get('action') === 0 && !game.getIn(['currentActionModifiers', 'freeAction']));
    const activePiecesCount = players.hasIn([game.get('playerTurn'), 'pieces']) ?
      players.getIn([game.get('playerTurn'), 'pieces']).filter(piece => piece.get('space') !== 'captured').size
      : 0;
    const finishTurnButton = (game.get('action') === 0 && !currentAction.has('type')) && !game.get('canDrawCard') ?
      (<Button raised className="finish-button" onClick={forceNextTurn}>
        Finish Turn <i className="fa fa-angle-double-right fa-lg" />
      </Button>)
      : null;

    return (
      <div className="player-actions">
        <Button
          raised
          className="main-button"
          onClick={activePiecesCount > 0 ? initAdd : initAddFirstPiece}
          disabled={disableButtons}
        >
          <i className="fa fa-user-plus fa-lg" /> Add
        </Button>

        <Button
          raised
          className="main-button"
          onClick={this.initSpecial}
          disabled={activePiecesCount === 0 || disableButtons}
        >
          <i className="fa fa-magic fa-lg" /> Special
        </Button>

        <Button
          raised
          className="main-button"
          onClick={initMove}
          disabled={activePiecesCount === 0 || disableButtons}
        >
          <i className="fa fa-arrows fa-lg" /> Move
        </Button>

        <Button
          raised
          className="main-button"
          onClick={initCapture}
          disabled={activePiecesCount === 0 || cannotCapture || disableButtons}
        >
          <i className="fa fa-dot-circle-o fa-lg" /> Capture
        </Button>

        {currentAction.has('type') ?
          <Button
            raised
            className="cancel-button"
            onClick={cancelAction}
          >
            <i className="fa fa-times-circle fa-lg" /> Cancel
          </Button>
        : null}

        {canConfirm ?
          <Button
            raised
            className="confirm-button"
            onClick={confirmAction}
          >
            <i className="fa fa-check-circle fa-lg" /> Confirm
          </Button>
        : null}

        {finishTurnButton}
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PlayerActions);
