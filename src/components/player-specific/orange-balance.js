import React from 'react';
import Avatar from 'material-ui/Avatar';
import Button from 'material-ui/Button';
import Chip from 'material-ui/Chip';
import store from '../../utils/store';
import { updateCurrentActionModifier } from '../../actions/game';
import { updateModifier } from '../../actions/players';

const OrangeBalance = (playerIndex) => {
  const { game, players } = store.getState();

  const isCurrentPlayer = game.get('playerTurn') === playerIndex;
  const currentBalance = players.getIn([playerIndex, 'modifiers', 'orangeBalance']) || 0;
  const currentBalanceAvatar = <Avatar style={{ backgroundColor: players.getIn([playerIndex, 'role', 'secondaryColor']), color: players.getIn([playerIndex, 'role', 'primaryColor']) }}>{`${currentBalance}`}</Avatar>;
  const isFreeAction = game.hasIn(['currentActionModifiers', 'freeAction']) && game.getIn(['currentActionModifiers', 'freeAction']);

  const spendTime = () => {
    store.dispatch(updateCurrentActionModifier('freeAction', true));
    store.dispatch(updateModifier(game.get('playerTurn'), 'orangeBalance', (currentBalance - 1)));
  };

  const borrowTime = () => {
    Promise.resolve(store.dispatch(updateCurrentActionModifier('freeAction', true)))
      .then(() => store.dispatch(updateCurrentActionModifier('orangeDebt', true)));
    store.dispatch(updateModifier(game.get('playerTurn'), 'orangeBalance', (currentBalance - 2)));
  };

  return (<div className="playerInfoOrangeBalance">
    <Chip
      avatar={currentBalanceAvatar}
      label={
        <div>
          <Button
            onClick={spendTime}
            disabled={!isCurrentPlayer || currentBalance <= 0 || isFreeAction}
            style={{
              fontSize: '12px',
              minHeight: '20px',
              minWidth: '70px',
              margin: '0',
              padding: '0'
            }}
          >
            Spend
          </Button>
          <Button
            onClick={borrowTime}
            disabled={!isCurrentPlayer || isFreeAction}
            style={{
              fontSize: '12px',
              minHeight: '20px',
              minWidth: '70px',
              margin: '0',
              padding: '0'
            }}
          >
            Borrow
          </Button>
        </div>
      }
    />
  </div>);
};

export default OrangeBalance;
