import React from 'react';
import Avatar from 'material-ui/Avatar';
import Button from 'material-ui/Button';
import Chip from 'material-ui/Chip';
import store from '../../utils/store';
import { updateCurrentActionModifier } from '../../actions/game';
import { updateModifier } from '../../actions/players';

const RedFuel = (playerIndex) => {
  const { game, players } = store.getState();

  const isCurrentPlayer = game.get('playerTurn') === playerIndex;
  const currentFuel = players.getIn([playerIndex, 'modifiers', 'redFuel']) || 0;
  const currentFuelAvatar = <Avatar style={{ backgroundColor: players.getIn([playerIndex, 'role', 'secondaryColor']), color: players.getIn([playerIndex, 'role', 'primaryColor']) }}>{`${currentFuel}`}</Avatar>;
  const hasFuel = isCurrentPlayer && currentFuel >= 2 && !(game.getIn(['currentActionModifiers', 'freeAction']));
  const useFuel = () => {
    store.dispatch(updateCurrentActionModifier('freeAction', true));
    store.dispatch(updateModifier(game.get('playerTurn'), 'redFuel', (players.getIn([game.get('playerTurn'), 'modifiers', 'redFuel']) - 2)));
  };

  return (<div className="playerInfoRedFuel">
    <Chip
      avatar={currentFuelAvatar}
      label={
        (<Button
          onClick={useFuel}
          disabled={!hasFuel}
          style={{
            fontSize: '12px',
            minHeight: '20px',
            margin: '0',
            padding: '0'
          }}
        >
          Use Fuel
        </Button>)
      }
    />
  </div>);
};

export default RedFuel;
