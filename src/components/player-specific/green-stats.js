import React from 'react';
import Avatar from 'material-ui/Avatar';
import Button from 'material-ui/Button';
import store from '../../utils/store';
import { updateCanConfirm, updateSpecialActionNeeded } from '../../actions/game';
import { updateModifier } from '../../actions/players';

const currentFuel = (playerIndex) => {
  const { players } = store.getState();
  if (players.hasIn([playerIndex, 'modifiers', 'greenFuel'])) {
    return players.getIn([playerIndex, 'modifiers', 'greenFuel']);
  }

  return 0;
};

const GreenStats = (playerIndex) => {
  const { game, players } = store.getState();

  const isCurrentPlayer = game.get('playerTurn') === playerIndex;

  const canIncrease = isCurrentPlayer && players.hasIn([playerIndex, 'modifiers', 'greenFuel']) && players.getIn([playerIndex, 'modifiers', 'greenFuel']) > 0;

  const increaseStrat = (type) => {
    let newAmount = 0;
    if (type === 'greenAdd') {
      newAmount = additionalAdd + 1;
    } else if (type === 'greenMove') {
      newAmount = additionalMove + 1;
    } else if (type === 'greenIncrease') {
      newAmount = additionalIncrease + 1;
    }

    Promise.resolve(store.dispatch(updateModifier(playerIndex, type, newAmount)))
      .then(() => store.dispatch(updateModifier(playerIndex, 'greenFuel', (currentFuel(playerIndex) - 1))))
      .then(() => {
        if (currentFuel(playerIndex) <= 0) {
          Promise.resolve(store.dispatch(updateCanConfirm(true)))
            .then(() => store.dispatch(updateSpecialActionNeeded(false)));
        }
      });
  };

  const additionalAdd = players.getIn([playerIndex, 'modifiers', 'greenAdd']) || 1;

  const additionalMove = players.getIn([playerIndex, 'modifiers', 'greenMove']) || 2;

  const additionalIncrease = players.getIn([playerIndex, 'modifiers', 'greenIncrease']) || 2;

  const addButtons = (<div>
    <Button
      fab
      style={{
        float: 'left',
        width: '32px',
        height: '32px',
        minHeight: '32px'
      }}
      onClick={() => increaseStrat('greenAdd')}
    >
      <i className="fa fa-user-plus fa-lg" />
    </Button>

    <Button
      fab
      style={{
        float: 'left',
        width: '32px',
        height: '32px',
        minHeight: '32px'
      }}
      onClick={() => increaseStrat('greenMove')}
    >
      <i className="fa fa-arrows fa-lg" />
    </Button>

    <Button
      fab
      style={{
        float: 'left',
        width: '32px',
        height: '32px',
        minHeight: '32px'
      }}
      onClick={() => increaseStrat('greenIncrease')}
    >
      <i className="fa fa-magic fa-lg" />
    </Button>

    <Avatar
      style={{
        backgroundColor: players.getIn([playerIndex, 'role', 'secondaryColor']),
        color: players.getIn([playerIndex, 'role', 'primaryColor']),
        width: '32px',
        minHeight: '32px',
        height: '32px',
        float: 'left'
      }}
    >
      {`${currentFuel(playerIndex)}`}
    </Avatar>
  </div>);

  return (<div className="playerInfoGreenStats" style={{ width: '170px' }}>
    <Avatar
      style={{
        backgroundColor: players.getIn([playerIndex, 'role', 'secondaryColor']),
        color: players.getIn([playerIndex, 'role', 'primaryColor']),
        width: '32px',
        height: '32px',
        float: 'left'
      }}
    >
      {`${additionalIncrease}`}
    </Avatar>

    {(canIncrease) ? addButtons : null}
  </div>);
};

export default GreenStats;
