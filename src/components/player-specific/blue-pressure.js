import React from 'react';
import Avatar from 'material-ui/Avatar';
import Button from 'material-ui/Button';
import Chip from 'material-ui/Chip';
import store from '../../utils/store';
import { updateCurrentActionModifier } from '../../actions/game';
import { updateModifier } from '../../actions/players';

const BluePressure = (playerIndex) => {
  const { game, players } = store.getState();

  const isCurrentPlayer = game.get('playerTurn') === playerIndex;
  const currentPressure = players.getIn([playerIndex, 'modifiers', 'bluePressure']) || 0;
  const currentPressureAvatar = <Avatar style={{ backgroundColor: players.getIn([playerIndex, 'role', 'secondaryColor']), color: players.getIn([playerIndex, 'role', 'primaryColor']) }}>{`${currentPressure}`}</Avatar>;
  const isUsingPressure = game.hasIn(['currentActionModifiers', 'bluePressure']) && game.getIn(['currentActionModifiers', 'bluePressure']);
  const triggerPressure = () => {
    store.dispatch(updateCurrentActionModifier('bluePressure', true));
    store.dispatch(updateModifier(game.get('playerTurn'), 'bluePressure', (players.getIn([game.get('playerTurn'), 'modifiers', 'bluePressure']) - 1)));
  };

  const buttonMessage = (isUsingPressure) ? 'Pressure Engaged' : 'Use Pressure';

  return (<div className="playerInfoBluePressure">
    <Chip
      avatar={currentPressureAvatar}
      label={
        (<Button
          onClick={triggerPressure}
          disabled={!isCurrentPlayer || currentPressure === 0 || isUsingPressure}
          style={{
            fontSize: '12px',
            minHeight: '20px',
            margin: '0',
            padding: '0'
          }}
        >
          {buttonMessage}
        </Button>)
      }
    />
  </div>);
};

export default BluePressure;
