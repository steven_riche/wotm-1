import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Immutable from 'immutable';

import './prompt.scss';

function mapStateToProps(state) {
  return {
    game: state.game
  };
}

const Prompt = ({ game }) => (
  <div className="prompt">
    <p>{game.get('prompt')}</p>
  </div>
);

Prompt.propTypes = { game: PropTypes.instanceOf(Immutable.Map).isRequired };

export default connect(mapStateToProps)(Prompt);
