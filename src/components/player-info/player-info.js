import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Immutable from 'immutable';
import { withStyles } from 'material-ui/styles';
import Avatar from 'material-ui/Avatar';
import Button from 'material-ui/Button';
import Chip from 'material-ui/Chip';
import store from '../../utils/store';
import CardDialog from '../card-dialog/card-dialog';
import Card from '../card/card';
import PlayerRoleInfoComponent from '../player-role-info/player-role-info';
import { freeActionScale, getAddAmount, getCardsIcon, getLastCaptured, getMoveAmount } from './player-info-utils';
import { bluePressure, greenFuel, orangeBalance, redFuel } from './card-extras';

import './player-info.scss';

function mapStateToProps(state) {
  return {
    game: state.game,
    players: state.players
  };
}

const styles = () => {
  const { players } = store.getState();
  const genStyles = {};
  players.forEach((player, i) => {
    genStyles[`playerStyle_${i}`] = {
      color: player.getIn(['role', 'primaryColor']),
      backgroundColor: player.getIn(['role', 'secondaryColor'])
    };

    genStyles[`playerSecStyle_${i}`] = {
      color: player.getIn(['role', 'secondaryColor']),
      backgroundColor: player.getIn(['role', 'primaryColor'])
    };
  });
  return genStyles;
};

export class PlayerInfo extends React.PureComponent {
  static propTypes = {
    game: PropTypes.instanceOf(Immutable.Map).isRequired,
    players: PropTypes.instanceOf(Immutable.List).isRequired,
    alwaysOpen: PropTypes.bool
  };

  static defaultProps = {
    alwaysOpen: false
  };

  constructor(props) {
    super(props);
    this.state = {
      slideOpen: this.props.alwaysOpen || false,
      dialogOpen: false,
      cards: []
    };
  }

  getCards = (player, playerIndex) => {
    const isPlayerTurn = this.props.game.get('playerTurn') === playerIndex;
    if (player.get('cards').size === 0) {
      return null;
    }

    const cards = player.get('cards').map((cardInfo) => {
      const closeDialog = () => this.setState({ dialogOpen: false });
      const addlContent = bluePressure(cardInfo) || orangeBalance(cardInfo) ||
        redFuel(cardInfo, isPlayerTurn) || greenFuel(cardInfo, isPlayerTurn, closeDialog);

      return (
        <Card
          cardTitle={cardInfo.get('title')}
          cardDesc={cardInfo.get('description')}
          buttonOnClick={cardInfo.get('activateButton') ? () => {
            cardInfo.get('activateButton')();
            closeDialog();
          } : null}
          buttonDisabled={cardInfo.get('activateButton') ? (!isPlayerTurn || cardInfo.get('activateButtonDisabled')()) : false}
          buttonText={cardInfo.get('activateButton') ? cardInfo.get('activateButtonText') : ''}
          addlContent={addlContent}
        />
      );
    });

    return cards.toArray();
  };

  drawInfo = (player, i) => {
    const freeAction = freeActionScale(player, i, this.props);

    const addAmount = getAddAmount(player);

    const moveAmount = getMoveAmount(player);

    const playerSpecificComponent = (player.hasIn(['role', 'scoreComponent'])) ? player.getIn(['role', 'scoreComponent'])(i) : null;

    const primaryColor = player.getIn(['role', 'primaryColor']);

    const secondaryColor = player.getIn(['role', 'secondaryColor']);

    const cardsIcon = getCardsIcon(player, i, this.props, this.openDialog, this.getCards);

    const lastCaptured = getLastCaptured(player, this.props);

    return (
      <div
        key={`player-info-${i}`}
        className={`player-info-tile ${!this.props.alwaysOpen && (i === this.props.game.get('playerTurn')) ? 'active' : ''}`}
        style={{ backgroundColor: primaryColor, border: `1px solid ${secondaryColor}` }}
      >
        <div className="player-name-headers">
          <h3 className="role-header" style={{ color: secondaryColor }}>{player.getIn(['role', 'name'])}</h3>
          <p className="player-name" style={{ color: secondaryColor }}>{player.get('playerName')}</p>
          {freeAction}
        </div>

        <div className="player-stats">
          <Chip
            avatar={<Avatar classes={{ root: this.props.classes[`playerStyle_${i}`] }}>
              <i className="fa fa-user-plus fa-lg fa-center-icon" />
            </Avatar>}
            label={addAmount}
            classes={{
              label: 'fontSize18'
            }}
          />

          <Chip
            avatar={<Avatar classes={{ root: this.props.classes[`playerStyle_${i}`] }}>
              <i className="fa fa-arrows fa-lg fa-center-icon" />
            </Avatar>}
            label={moveAmount}
            classes={{
              label: 'fontSize18'
            }}
          />
        </div>

        <div className="player-stats-long">
          <Chip
            avatar={<Avatar classes={{ root: this.props.classes[`playerStyle_${i}`] }}>
              <i className="fa fa-magic fa-lg fa-center-icon" />
            </Avatar>}
            label={player.getIn(['role', 'scoreSpecial'])}
            classes={{
              label: 'fontSize12'
            }}
          />

          {playerSpecificComponent}
        </div>

        <div className="player-extra-data">

          {cardsIcon}

          <PlayerRoleInfoComponent player={player.get('role')} />

          {lastCaptured}

        </div>

      </div>
    );
  };

  playerInfo() {
    return this.props.players.map((player, i) => this.drawInfo(player, i));
  }

  expandButton() {
    let expandButtonClass;
    if (this.state.slideOpen) {
      expandButtonClass = 'fa-minus-circle';
    } else {
      expandButtonClass = 'fa-plus-circle';
      if (this.props.game.get('specialActionNeeded')) {
        expandButtonClass += ' highlighted-primary-button';
      }
    }
    return (!this.props.alwaysOpen) ? (<div className="expand-button-div">
      <Button className="expand-button" style={{ margin: `${(this.props.players.size * 45) - 32}px 7px 0` }} onClick={this.toggleSlide}>
        <i className={`fa fa-3x ${expandButtonClass}`} />
      </Button>
    </div>
    )
    : null;
  }

  toggleSlide = () => {
    this.setState({
      slideOpen: !this.state.slideOpen
    });
  }

  openDialog = (cards) => {
    this.setState({
      cards,
      dialogOpen: true
    });
  }

  toggleDialog = () => {
    this.setState({
      dialogOpen: !this.state.dialogOpen
    });
  }

  render() {
    return (
      <div className={`player-info ${!this.state.slideOpen ? 'closed' : ''}`} style={{ height: `${(this.props.players.size * 90)} px` }}>
        <div style={{ float: 'left' }}>{this.playerInfo()}</div>
        {this.expandButton()}
        <CardDialog
          open={this.state.dialogOpen}
          onRequestClose={this.toggleDialog}
          cards={this.state.cards}
        />
      </div>
    );
  }
}

export default withStyles(styles)(connect(mapStateToProps)(PlayerInfo));
