import React from 'react';
import Button from 'material-ui/Button';

export const bluePressure = card => ((card.hasIn(['modifications', 'bluePressure'])) ?
  (<p><strong>{`Current Pressure: ${card.getIn(['modifications', 'bluePressure'])}`}</strong></p>)
  : null);

export const orangeBalance = card => ((card.hasIn(['modifications', 'orangeBalance'])) ?
  (<p><strong>{`Current Balance: ${card.getIn(['modifications', 'orangeBalance'])}`}</strong></p>)
  : null);

export const redFuel = (card, isPlayerTurn) => ((card.hasIn(['modifications', 'redFuel'])) ?
    (<div>
      <p>
        <Button
          raised
          className="main-button"
          onClick={card.get('useFuel')}
          disabled={!isPlayerTurn || card.getIn(['modifications', 'redFuel']) < 2}
        >
          Use Fuel
        </Button>
        <strong style={{ float: 'right', position: 'relative', top: '10px' }}>&nbsp;{`Current Fuel: ${card.getIn(['modifications', 'redFuel'])}`}</strong>
      </p>
    </div>)
    : null);

export const greenFuel = (card, isPlayerTurn, closeDialog) => ((card.has('activateButton2')) ?
    (<Button
      raised
      className="main-button"
      style={{ marginTop: '10px' }}
      onClick={() => {
        card.get('activateButton2')();
        closeDialog();
      }}
      disabled={card.get('activateButtonDisabled')()}
    >
      {card.get('activateButtonText2')}
    </Button>)
    : null);
