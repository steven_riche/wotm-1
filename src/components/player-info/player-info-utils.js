import React from 'react';
import Avatar from 'material-ui/Avatar';
import Badge from 'material-ui/Badge';
import Button from 'material-ui/Button';
import Chip from 'material-ui/Chip';
import { useDamageFreeAction } from '../../game_logic/cancel_confirm_process';
import { damageToFreeAction } from '../../game_config/constants/constants';

export const freeActionScale = (player, i, state) => {
  let damageReq = damageToFreeAction;

  if (player.get('cards').size > 0) {
    player.get('cards').forEach((card) => {
      if (card.hasIn(['modifications', 'damageReq'])) {
        damageReq = card.getIn(['modifications', 'damageReq']);
      }
    });
  }

  const extraAction = (player.get('damageCounter') >= damageReq) ?
    (<Button
      raised
      onClick={useDamageFreeAction}
      disabled={state.game.get('playerTurn') !== i}
      className="free-action-button"
      classes={{
        root: state.classes[`playerStyle_${i}`]
      }}
    >
      Free Action
    </Button>)
    : null;

  const secondaryColor = player.getIn(['role', 'secondaryColor']);

  return (<div className="player-damage">
    <i className={`fa fa-star${(player.get('damageCounter') < 1) ? '-o' : ''}`} style={{ color: secondaryColor }} />
    <i className={`fa fa-star${(player.get('damageCounter') < 2) ? '-o' : ''}`} style={{ color: secondaryColor }} />
    <i className={`fa fa-star${(player.get('damageCounter') < 3) ? '-o' : ''}`} style={{ color: secondaryColor }} />
    <i className={`fa fa-star${(player.get('damageCounter') < 4) ? '-o' : ''}`} style={{ color: secondaryColor }} />
    {
      (player.get('damageCounter') < 5) ?
        <i className="fa fa-star-o" style={{ color: secondaryColor }} />
        : <i className="fa fa-star fa-lg" style={{ color: secondaryColor }} />
    }
    {extraAction}
  </div>);
};

export const getAddAmount = (player) => {
  let addAmount = player.getIn(['role', 'scoreAdd']);

  if (typeof addAmount === 'function') {
    addAmount = addAmount();
  }

  if (player.get('cards').size > 0) {
    player.get('cards').forEach((card) => {
      if (card.hasIn(['modifications', 'add'])) {
        addAmount += card.getIn(['modifications', 'add']);
      }
    });
  }

  if (player.hasIn(['role', 'scoreAddUnique'])) {
    addAmount = <span>{addAmount} <i className="fa fa-asterisk" /></span>;
  }

  return addAmount;
};

export const getCardsIcon = (player, i, state, openDialog, getCards) => ((player.get('cards').size > 0) ?
  (<Badge
    badgeContent={player.get('cards').size}
    style={{ position: 'absolute', top: '10px' }}
    classes={{
      badge: state.classes[`playerStyle_${i}`]
    }}
    onClick={() => openDialog(getCards(player, i))}
  >
    <i className="fa fa-hand-paper-o fa-lg" style={{ color: player.getIn(['role', 'secondaryColor']) }} />
  </Badge>)
  : <i className="fa fa-hand-paper-o fa-lg" style={{ color: player.getIn(['role', 'secondaryColor']), position: 'absolute', top: '10px' }} />
);

export const getLastCaptured = (player, state) => {
  const hasCapturedPlayer = player.get('lastCaptured') !== null;
  const playerInfo = state.players.getIn([player.get('lastCaptured'), 'role']);
  const avatar = (hasCapturedPlayer) ?
    (<Avatar style={{ backgroundColor: playerInfo.get('primaryColor') }}>
      <svg height="30px" width="30px">
        {playerInfo.has('path') ? <path d={playerInfo.get('path')} fill={playerInfo.get('secondaryColor')} /> : null}
      </svg>
    </Avatar>)
    : null;
  return (hasCapturedPlayer) ?
    <Chip
      avatar={avatar}
      label="LC"
      classes={{
        label: 'fontSize12'
      }}
      style={{
        position: 'absolute',
        top: '40px'
      }}
    />
    : null;
};

export const getMoveAmount = (player) => {
  let moveAmount = player.getIn(['role', 'scoreMove']);

  if (typeof moveAmount === 'function') {
    moveAmount = moveAmount();
  }

  if (player.get('cards').size > 0) {
    player.get('cards').forEach((card) => {
      if (card.hasIn(['modifications', 'move'])) {
        moveAmount += card.getIn(['modifications', 'move']);
      }
    });
  }

  if (player.hasIn(['role', 'scoreMoveUnique'])) {
    moveAmount = <span>{moveAmount} <i className="fa fa-asterisk" /></span>;
  }

  return moveAmount;
};
