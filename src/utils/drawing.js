import React from 'react';
import Immutable from 'immutable';
import B_W from '../game_config/boards/board-size';
import { getCurrentInProgress } from '../game_logic/base_util';

export const drawBoard = (game, world, players, trigger) => (
  <div className="gameBoard" style={{ padding: '20px', marginLeft: '375px' }}>
    <svg height={(3 * B_W)} width={(3 * B_W)} x="0" y="0">
      {drawRooms(world)}
      {drawPieces(world, players, trigger)}
      {drawTemporaryPieces(game, world, players, trigger)}
      {drawShapeOverlays(game, world, trigger)}
    </svg>
  </div>
);

export const drawRooms = (world) => {
  if (!world.has('rooms') || world.get('rooms').size === 0) {
    return null;
  }

  return world.get('rooms').map((room, i) => (
    <g
      key={`room-${world.getIn(['layout', i, 'x'])}-${world.getIn(['layout', i, 'y'])}`}
      width={B_W}
      height={B_W}
      x={world.getIn(['layout', i, 'x'])}
      y={world.getIn(['layout', i, 'y'])}
      transform={`rotate(${room.get('rotation')} ${(B_W / 2) + world.getIn(['layout', i, 'x'])} ${(B_W / 2) + world.getIn(['layout', i, 'y'])})`}
    >
      <rect
        x={world.getIn(['layout', i, 'x'])}
        y={world.getIn(['layout', i, 'y'])}
        width={B_W}
        height={B_W}
        fill="#CCC"
        stroke="black"
      />
      {drawShapes(room.get('board'), { x: world.getIn(['layout', i, 'x']), y: world.getIn(['layout', i, 'y']) })}
    </g>
  ));
};

export const drawShapes = (board, offset) => {
  const doorSpaces = board.getIn(['type', 'doors']).keySeq()
    .reduce((a, b) => a.concat(board.getIn(['type', 'doors', b])), Immutable.List());

  return board.getIn(['type', 'spaces']).keySeq().map((space) => {
    let spaceColor = 'white';
    if (doorSpaces.includes(space)) {
      spaceColor = 'rgba(0, 255, 0, .3)';
    } else if (board.get('meditationSpaces').includes(space)) {
      spaceColor = 'rgba(0, 0, 255, .3)';
    }

    const points = board.getIn(['type', 'spaces', space, 'points'])
      .reduce((a, b) => `${a}${b.get('x') + offset.x}, ${b.get('y') + offset.y}\n`, '');

    const spaceString = `${board.get('name')}_${space}`;

    return (
      <polygon
        key={spaceString}
        id={spaceString}
        className="room-space"
        stroke="black"
        fill={spaceColor}
        points={points}
      />
    );
  });
};

export const drawShapeOverlays = (game, world, trigger) => {
  let highlightedSpaces = null;
  if (game.has('highlighted') && game.get('highlighted').size > 0) {
    highlightedSpaces = game.get('highlighted').map((spaceString) => {
      const spaceArray = spaceString.split('_');
      const roomIndex = world.get('rooms').findIndex(findRoom => findRoom.getIn(['board', 'name']) === spaceArray[0]);
      const layoutX = world.getIn(['layout', roomIndex, 'x']);
      const layoutY = world.getIn(['layout', roomIndex, 'y']);
      const room = world.getIn(['rooms', roomIndex]);

      const points = room.getIn(['board', 'type', 'spaces', spaceArray[1], 'points'])
        .reduce((a, b) => `${a}${b.get('x') + layoutX}, ${b.get('y') + layoutY}\n`, '');

      const className = `room-space highlighted-${spaceArray[2]}`;
      const rSpaceString = `${spaceArray[0]}_${spaceArray[1]}`;

      return (
        <g
          key={`${spaceString}_g`}
          width={B_W}
          height={B_W}
          x={layoutX}
          y={layoutY}
          transform={`rotate(${room.get('rotation')} ${(B_W / 2) + layoutX} ${(B_W / 2) + layoutY})`}
        >
          <polygon
            key={`${rSpaceString}_overlay`}
            id={`${rSpaceString}_overlay`}
            className={className}
            stroke="transparent"
            fill="transparent"
            points={points}
            onClick={() => trigger(rSpaceString)}
          />
        </g>
      );
    });
  }

  return (<g>
    {highlightedSpaces}
  </g>);
};

export const drawPieces = (world, players, trigger) => (
  players.map(player => player.get('pieces')
    .filter(piece => piece.get('space') !== 'captured')
    .map((piece) => {
      const roomIndex = world.get('rooms').findIndex(room => room.getIn(['board', 'name']) === piece.get('board'));
      const roomType = world.getIn(['rooms', roomIndex, 'board', 'type']);
      const roomLocation = world.getIn(['layout', roomIndex]);

      const centerX = roomType.getIn(['spaces', piece.get('space'), 'centerX']) + roomLocation.get('x');
      const centerY = roomType.getIn(['spaces', piece.get('space'), 'centerY']) + roomLocation.get('y');
      const rotation = world.getIn(['rooms', roomIndex, 'rotation']);
      const icon = (player.hasIn(['role', 'path'])) ? (<path
        d={player.getIn(['role', 'path'])}
        fill={player.getIn(['role', 'secondaryColor'])}
        transform={`translate(${centerX - 10} ${centerY - 10}) rotate(-${rotation} 10 10)`}
        onClick={() => trigger(`${piece.get('board')}_${piece.get('space')}`)}
      />)
      : null;

      return (<g
        transform={`rotate(${rotation} ${(B_W / 2) + roomLocation.get('x')} ${(B_W / 2) + roomLocation.get('y')})`}
      >
        <circle
          r="15"
          stroke={player.getIn(['role', 'secondaryColor'])}
          fill={player.getIn(['role', 'primaryColor'])}
          cx={centerX}
          cy={centerY}
          onClick={() => trigger(`${piece.get('board')}_${piece.get('space')}`)}
        />
        {icon}
      </g>);
    })
  )
);

export const drawTemporaryPieces = (game, world, players, trigger) => {
  const tempPieces = getCurrentInProgress(game);

  return tempPieces.map((piece) => {
    const player = players.get(piece.get('player'));
    const roomIndex = world.get('rooms').findIndex(room => room.getIn(['board', 'name']) === piece.get('board'));
    const roomType = world.getIn(['rooms', roomIndex, 'board', 'type']);
    const roomLocation = world.getIn(['layout', roomIndex]);

    return (<circle
      key={`${piece.get('board')}-${piece.get('space')}-${piece.get('player')}-temp`}
      r="15"
      id={`${piece.get('board')}-${piece.get('space')}-${piece.get('player')}-piece`}
      className="temporary"
      stroke={player.getIn(['role', 'secondaryColor'])}
      fill={player.getIn(['role', 'primaryColor'])}
      cx={roomType.getIn(['spaces', piece.get('space'), 'centerX']) + roomLocation.get('x')}
      cy={roomType.getIn(['spaces', piece.get('space'), 'centerY']) + roomLocation.get('y')}
      transform={`rotate(${world.getIn(['rooms', roomIndex, 'rotation'])} ${(B_W / 2) + roomLocation.get('x')} ${(B_W / 2) + roomLocation.get('y')})`}
      onClick={() => trigger(`${piece.get('board')}_${piece.get('space')}`)}
    />);
  });
};
