export default {
  INIT_WORLD: 'boards/INIT_WORLD',
  SET_LAYOUT: 'boards/SET_LAYOUT',
  SET_ROOM: 'boards/SET_ROOM'
};
