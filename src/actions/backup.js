import types from '../actionTypes/backup';

export const restoreFromBackup = backup => ({
  type: types.RESTORE_FROM_BACKUP,
  game: backup.get('game'),
  world: backup.get('world'),
  players: backup.get('players')
});

export const updateBackup = (game, world, players) => ({
  type: types.WRITE_TO_BACKUP,
  game,
  world,
  players
});
