import types from '../actionTypes/game';

export const addTriggers = triggers => ({
  type: types.ADD_TRIGGERABLE_SPACES,
  triggers
});

export const appendCurrentAction = newMove => ({
  type: types.APPEND_CURRENT_ACTION,
  newMove
});

export const appendPrompt = prompt => ({
  type: types.APPEND_PROMPT,
  prompt
});

export const clearTriggers = () => ({
  type: types.CLEAR_TRIGGERABLE_SPACES
});

export const drawCard = cardChoice => ({
  type: types.DRAW_CARD,
  cardChoice
});

export const discardCard = card => ({
  type: types.DISCARD_CARD,
  card
});

export const finishAction = (game) => {
  const action = (game.get('action') > 0 && !(game.hasIn(['currentActionModifiers', 'freeAction']) && game.getIn(['currentActionModifiers', 'freeAction'])))
    ? game.get('action') - 1 : game.get('action');

  const playerTurn = game.get('playerTurn');

  const totalTurn = game.get('totalTurn');

  return {
    type: types.SET_TURN,
    playerTurn,
    action,
    totalTurn
  };
};

export const forceNext = (game) => {
  const playerTurn = (game.get('playerTurn') + 1) % game.get('numPlayers');
  const action = 2;
  const totalTurn = game.get('totalTurn') + 1;
  return {
    type: types.SET_TURN,
    playerTurn,
    action,
    totalTurn
  };
};

export const loadCardDeck = cards => ({
  type: types.LOAD_CARD_DECK,
  cards
});

export const removeTrigger = space => ({
  type: types.REMOVE_TRIGGERABLE_SPACE,
  space
});

export const shuffleCardDeck = () => ({
  type: types.SHUFFLE_CARD_DECK
});

export const updateActionCount = newActionCount => ({
  type: types.UPDATE_ACTION_COUNT,
  newActionCount
});

export const updateActivePlayer = playerIndex => ({
  type: types.SET_ACTIVE_PLAYER,
  playerIndex
});

export const updateCanConfirm = canConfirm => ({
  type: types.UPDATE_CAN_CONFIRM,
  canConfirm
});

export const updateCanDraw = canDraw => ({
  type: types.UPDATE_CAN_DRAW,
  canDraw
});

export const updateCurrentAction = currentAction => ({
  type: types.UPDATE_CURRENT_ACTION,
  currentAction
});

export const updateCurrentActionModifier = (modifierName, modifierValue) => ({
  type: types.UPDATE_CURRENT_ACTION_MODIFIERS,
  modifierName,
  modifierValue
});

export const updateCurrentActionTarget = target => ({
  type: types.UPDATE_CURRENT_ACTION_TARGET,
  target
});

export const updateHighlighted = highlighted => ({
  type: types.UPDATE_HIGHLIGHTED,
  highlighted
});

export const updatePlayerCount = playerCount => ({
  type: types.UPDATE_PLAYER_COUNT,
  playerCount
});

export const updatePrompt = prompt => ({
  type: types.UPDATE_PROMPT,
  prompt
});

export const updateSpecialActionNeeded = specialActionNeeded => ({
  type: types.UPDATE_SPECIAL_ACTION_NEEDED,
  specialActionNeeded
});
