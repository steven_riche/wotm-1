import types from '../actionTypes/players';
import { generateUUID } from '../game_logic/base_util';

export const addPlayer = (playerRole, playerName) => {
  const playerIndex = generateUUID();

  return {
    type: types.ADD_PLAYER,
    playerRole,
    playerName,
    playerIndex
  };
};

export const appendCard = (playerIndex, card) => ({
  type: types.APPEND_CARD,
  playerIndex,
  card
});

export const getCaptured = (capturedPlayer, damageCounter, board, space) => ({
  type: types.GET_CAPTURED,
  playerIndex: capturedPlayer,
  damageCounter,
  board,
  space
});

export const modifyCard = (playerIndex, cardKey, modName, modValue) => ({
  type: types.MODIFY_CARD,
  playerIndex,
  cardKey,
  modName,
  modValue
});

export const updateCards = (playerIndex, cards) => ({
  type: types.UPDATE_CARDS,
  playerIndex,
  cards
});

export const updateCardRecords = (playerIndex, cardKey, recordType, turnIndex) => ({
  type: types.UPDATE_CARD_RECORDS,
  playerIndex,
  cardKey,
  recordType,
  turnIndex
});

export const updateDamage = (playerIndex, damage) => ({
  type: types.UPDATE_DAMAGE,
  playerIndex,
  damage
});

export const updateModifier = (playerIndex, modifierName, modifierValue) => ({
  type: types.UPDATE_MODIFIER,
  playerIndex,
  modifierName,
  modifierValue
});

export const updatePieces = (playerIndex, pieces) => ({
  type: types.UPDATE_PIECES,
  playerIndex,
  pieces
});

export const updatePiecesGroups = groups => ({
  type: types.UPDATE_PIECES_GROUPS,
  groups
});

export const updateScore = (scoringIndex, capturedIndex) => ({
  type: types.SCORE,
  playerIndex: scoringIndex,
  lastCaptured: capturedIndex
});
