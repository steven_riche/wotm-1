import Immutable from 'immutable';
import gameTypes from '../actionTypes/game';
import worldTypes from '../actionTypes/world';
import playerTypes from '../actionTypes/players';
import backupTypes from '../actionTypes/backup';
import { initialGame, initialWorld, initialPlayers } from '../game_logic/initial_states';

export const initializeGame = () => ({
  type: gameTypes.INIT_GAME,
  game: Immutable.fromJS(initialGame)
});

export const initializeWorld = () => ({
  type: worldTypes.INIT_WORLD,
  world: Immutable.fromJS(initialWorld)
});

export const initializePlayers = () => ({
  type: playerTypes.SET_ALL_PLAYERS,
  players: Immutable.fromJS(initialPlayers)
});

export const initializeBackup = (game, world, players) => ({
  type: backupTypes.WRITE_TO_BACKUP,
  game,
  world,
  players
});
