import types from '../actionTypes/world';

export const setLayout = layout => ({
  type: types.SET_LAYOUT,
  layout
});

export const setRoom = (roomIndex, room) => ({
  type: types.SET_ROOM,
  roomIndex,
  room
});
