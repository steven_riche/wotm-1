# War of the Mystics

This is the first iteration of an online version of **War of the Mystics**. At the moment, this is only a React + Redux project that runs locally.
However, the game is feature-complete and contains the entire "base game". Future versions will add artwork and improve the UI, as well as adding
online functionality, but the core of the gameplay should now be set.

## Commands

In order to run the game, run the commands:

```
npm install
npm run start
```

and navigate a browser to http://localhost:3000

In order to run unit tests, run the command:

```
npm run test
```

In order to run the linter, run the command:

```
npm run lint
```

## Game Setup

When first loaded, the game will walk you through adding players and setting up the board, then starting a game.

Do not refresh or reload, or it will start the game from the beginning!

Once the game starts, you can click the question mark icon in the lower right to view game rules, or click the info button on any player to see
details and strategy for that player.

## Configuration

The game state is completely controlled by the Redux state. So, if you would like to test different scenarios, you can directly change the game state
and start from there.

Go to `src/components/main.js` and go to line 24:

```
const LOAD_DEMO_GAME = false;
```

and change that value to true. Then, you can visit `src/game_logic/initial_states.js` to change the starting game state to whatever you would like to test.

## Next Steps

There is a lot remaining to do in future iterations.

Items to be done in the next iteration:

1. Set up Node + Express + Socket.io backend
2. Adapt a new front end that talks to the backend and just sends and receives the changes in game state
3. Set up a database solution that saves changes in game state (for use in eventual game analysis for balancing and for training AI)
4. Set up a matchmaking system and a way to invite other players to a specific game
5. Enable options to ping players when it is their turn
6. Some simple animations to show pieces moving (to make it easier for remote players to see game changes)

Items to be done in future iterations:

1. Actual art assets created and integrated into UI
2. More involved SVG animations for special abilities
3. Ability for ad revenue or donations on the live site
4. Tools to analyze games and see optimal strategy
5. React Native implementation to bring the game to mobile platforms
6. Integration of more experimental player roles to test game balance
7. Training of game AI from game data
8. Integration of live group chat into game
